﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaShekinah.DAL
{
   public  class EntradaSaidaDaoComandos
    {
        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();

        SqlDataReader dr;
        public String cadastroSaida(int paciente, DateTime dataHora, string texto_saida, bool ativoinativo, DateTime dataHoraExpectativa)
        {
            cmd.Parameters.AddWithValue("@paciente", paciente);
            cmd.Parameters.AddWithValue("@datahora", dataHora);
            cmd.Parameters.AddWithValue("@texto_saida", texto_saida);
            cmd.Parameters.AddWithValue("@ativoinativo", ativoinativo);
            cmd.Parameters.AddWithValue("@datahoraexpectativa", dataHoraExpectativa);
            cmd.CommandText = "insert into entrada_saida(paciente,datahora, descricao_saida, ativo_inativo,datahora_expectativa_retorno) values(@paciente,@datahora, @texto_saida, @ativoinativo,@datahoraexpectativa)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Cadastrado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir  no banco de dados" + ex;
            }
            return mensagem;

        }

        public String inativaSaidaIdoso(int idregistro, string txtMotivo)
        {
            cmd.Parameters.AddWithValue("@id", idregistro);
            cmd.Parameters.AddWithValue("@motivo", txtMotivo);


            cmd.CommandText = "update entrada_saida set ativo_inativo=0,motivo_inativacao=@motivo where id=@id";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Inativado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Paulo Faltou vc add no banco de dado as coluna correta ! Por favor olhar seu wpp dia 28/07/2020 e rodar os comandos inerentes a essa atualização" + ex;
            }
            return mensagem;

        }

        public String AtualizaRetorno(int paciente, int idregistro,DateTime dataHora, string texto_retorno, bool ativoinativo,bool retorno_paciente)
        {
            cmd.Parameters.AddWithValue("@paciente", paciente);
            cmd.Parameters.AddWithValue("@idregistro", idregistro);
            cmd.Parameters.AddWithValue("@datahora", dataHora);
            cmd.Parameters.AddWithValue("@texto_saida", texto_retorno);
            cmd.Parameters.AddWithValue("@ativoinativo", ativoinativo);
            cmd.Parameters.AddWithValue("@retornopaciente", retorno_paciente);

            cmd.CommandText = "update entrada_saida set descricao_retorno=@texto_saida,datahora_retorno=@datahora,ativo_inativo=@ativoinativo,retorno_concluido=@retornopaciente where paciente=@paciente and id=@idregistro";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "FInalizado o retorno com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir  no banco de dados" + ex;
            }
            return mensagem;

        }



    }

}
