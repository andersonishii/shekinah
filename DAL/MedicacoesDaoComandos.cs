﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SistemaShekinah.DAL
{
    class MedicacoesDaoComandos
    {
        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;
        public String cadastroMedicacao( String nomeQuimico, String nomeComercial, String formaFarmaceutica, String modoUso, String DataHoraCadastro, bool ativoinativo,String nomelab,String reacoesAdversas)
        {
            cmd.Parameters.AddWithValue("@nomequimico", nomeQuimico);
            cmd.Parameters.AddWithValue("@nomecomercial", nomeComercial);
            cmd.Parameters.AddWithValue("@formafarmaceutica", formaFarmaceutica);
            cmd.Parameters.AddWithValue("@modouso", modoUso);
            cmd.Parameters.AddWithValue("@datahoracadastro", DataHoraCadastro);
            cmd.Parameters.AddWithValue("@ativo", ativoinativo);
            cmd.Parameters.AddWithValue("@nomelab", nomelab);
            cmd.Parameters.AddWithValue("@reacoesadversas", reacoesAdversas);
            
            cmd.CommandText = "insert into medicacoes(nome_quimico,nome_comercial,forma_farmaceutica,modo_de_uso,datahoracadastro,ativo,nome_laboratorio,reacoes_adversas) " +
                "values(@nomequimico,@nomecomercial, @formafarmaceutica, @modouso, @datahoracadastro,@ativo ,@nomelab,@reacoesadversas)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Cadastrado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Evolução no banco de dados" + ex;
            }
            return mensagem;
        }

        public String atualizaMedicacao(String nomeQuimico, String nomeComercial, String formaFarmaceutica, String modoUso, String DataHoraCadastro, bool ativoinativo, String nomelab,int id,String reacoesAdversas)
        {
            cmd.Parameters.AddWithValue("@nomequimico", nomeQuimico);
            cmd.Parameters.AddWithValue("@nomecomercial", nomeComercial);
            cmd.Parameters.AddWithValue("@formafarmaceutica", formaFarmaceutica);
            cmd.Parameters.AddWithValue("@modouso", modoUso);
            cmd.Parameters.AddWithValue("@datahoracadastro", DataHoraCadastro);
            cmd.Parameters.AddWithValue("@ativo", ativoinativo);
            cmd.Parameters.AddWithValue("@nomelab", nomelab);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@reacoesAdversas", reacoesAdversas);
            

            cmd.CommandText = "update  medicacoes set nome_quimico= @nomequimico,nome_comercial=@nomecomercial,forma_farmaceutica= @formafarmaceutica, modo_de_uso=@modouso, datahoracadastro=@datahoracadastro,ativo=@ativo ,nome_laboratorio=@nomelab,reacoes_adversas=@reacoesAdversas where id=@id";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Atualização realizada com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao atualiar o dado" + ex;
            }
            return mensagem;
        }


        public String deleteMedicacao(int ID)
        {
            cmd.Parameters.AddWithValue("@id", ID);
            cmd.CommandText = " delete from medicacoes where id=@id";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Deletado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao deletar o Medicamento" + ex;
            }
            return mensagem;
        }

        public String inativaMedicacao(int ID)
        {
            cmd.Parameters.AddWithValue("@id", ID);
            cmd.CommandText = "update medicacoes set  ativo=0 where  id=@id";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Medicação Inativada com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao deletar o Medicamento" + ex;
            }
            return mensagem;
        }
        public String restricaoMedicacaoPaciente(bool ativo, int idpaciente, String medicacao,String observacao)
        {
            cmd.Parameters.AddWithValue("@ativo", ativo);
            cmd.Parameters.AddWithValue("@idpaciente", idpaciente);
            cmd.Parameters.AddWithValue("@medicacao", medicacao);
            cmd.Parameters.AddWithValue("@observacao", observacao);
            cmd.CommandText = " insert into paciente_restricao_medicacao (ativo,idpaciente,nome_medicamento_restricao,observacao) values(@ativo,@idpaciente,@medicacao,@observacao)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Restricao de Medicação inserida com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir restrição de medicação do paciente" + ex;
            }
            return mensagem;
        }

        public String inativaMedicacaoPaciente(int idpaciente)
        {
            cmd.Parameters.AddWithValue("@idpaciente", idpaciente);
            cmd.CommandText = " update paciente_restricao_medicacao set ativo=0 where idpaciente=@idpaciente";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Restricao de Medicação inativada com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inativar restrição de medicação do paciente" + ex;
            }
            return mensagem;
        }

        public String deleteRestricaoMedicacaoPaciente(int idpaciente)
        {
            cmd.Parameters.AddWithValue("@idpaciente", idpaciente);
            cmd.CommandText = " delete paciente_restricao_medicacao where idpaciente=@idpaciente";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Registro Anterior deletado com sucesso. Iniciando atualização do registro";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao deletar restrição de medicação do paciente" + ex;
            }
            return mensagem;
        }
    }
}
