﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaShekinah.DAL
{
    public class UsuarioDaoComandos
    {
        public bool tem = false;
        public string mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;

        public String cadastraUsuarios(String login, String senha, int codNivelDeUsuario, bool permiteLanRetroativo, bool ativo, String nomeDoUsuario)
        {
            cmd.Parameters.AddWithValue("@login", login);
            cmd.Parameters.AddWithValue("@senha", senha);
            cmd.Parameters.AddWithValue("@codniveluser", codNivelDeUsuario);
            cmd.Parameters.AddWithValue("@permiteretroativo", permiteLanRetroativo);
            cmd.Parameters.AddWithValue("@ativo", ativo);
            cmd.Parameters.AddWithValue("@nomedouser", nomeDoUsuario);

            cmd.CommandText = "insert into usuario (loginUser,psw,codNivelDeUsuario,permite_lanc_retroativo,ativo,nomeUsuario)" +
                "values (@login,@senha,@codniveluser,@permiteretroativo,@ativo,@nomedouser)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Cadastrado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Usuário  no banco de dados" + ex;
            }
            return mensagem;

        }

        public String cadatraAtividadeGrupoUsuario(string idPaciente, DateTime data_hora, string TipoAtividade, String relatorio, int codagrupador, bool ativoinativo)
        {
            cmd.Parameters.AddWithValue("@paciente", idPaciente);
            cmd.Parameters.AddWithValue("@datahora", data_hora);
            cmd.Parameters.AddWithValue("@tipoatividade", TipoAtividade);
            cmd.Parameters.AddWithValue("@relatorio", relatorio);
            cmd.Parameters.AddWithValue("@codagrupador", codagrupador);
            cmd.Parameters.AddWithValue("@ativo", ativoinativo);

            cmd.CommandText = "INSERT INTO dbo.atividadegrupo_funcionario (idfuncionario,datahora,tipo_atividade,relatorio,codigo_agrupador,ativo) values (@paciente,@datahora,@tipoatividade,@relatorio,@codagrupador,@ativo)";



            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Atividade do Usuário Registrada com sucesso. ";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Atividade em grupo do usuário no banco de dados" + ex;
            }
            return mensagem;

        }

        public String atualizaUsuario(String login, String senha, int codNivelDeUsuario, bool permiteLanRetroativo, bool ativo, String nomeDoUsuario, int id)
        {
            cmd.Parameters.AddWithValue("@login", login);
            cmd.Parameters.AddWithValue("@senha", senha);
            cmd.Parameters.AddWithValue("@codniveluser", codNivelDeUsuario);
            cmd.Parameters.AddWithValue("@permiteretroativo", permiteLanRetroativo);
            cmd.Parameters.AddWithValue("@ativo", ativo);
            cmd.Parameters.AddWithValue("@nomedouser", nomeDoUsuario);
            cmd.Parameters.AddWithValue("@id", id);

            cmd.CommandText = "update  usuario set loginUser=@login ,psw=@senha,codNivelDeUsuario=@codniveluser,permite_lanc_retroativo=@permiteretroativo,ativo=@ativo,nomeUsuario=@nomedouser where id=@id";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Atualizado com sucesso. Por favor reinicie a aplicação para surtir efeitos dessa atualização";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao atualizar  Usuário no banco de dados" + ex;
            }
            return mensagem;
        }


        public String inativarRegistro(int id)
        {
            cmd.Parameters.AddWithValue("@id", id);
            cmd.CommandText = "update  usuario set ativo=0 where id=@id";


            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Inativado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inativar  Usuário no banco de dados" + ex;
            }
            return mensagem;

        }
    }

  
}

