﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaShekinah.DAL
{
    public class AvaliacaoDaoComandos
    {
        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;



        public String inativaAvaliacao(int id, string observacao)
        {
            cmd.Parameters.AddWithValue("@idregistro", id);
            cmd.Parameters.AddWithValue("@obs", observacao);
     
            cmd.CommandText = "update avaliacao set ativo=0,motivo_inativacao=@obs where id=@idregistro";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Inativado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Paulo,  por favor rodar os comandos corretamente.Os mesmos foram enviados dia 27 / 07 / 2020.Verificar no seu wpp" + ex;
                //  mensagem = "Erro ao inativar Avaliação no banco de dados" + ex;
            }
            return mensagem;

        }
        public String cadastroAvaliacao(int paciente,string dataHora, string peso, String altura, int sistolica,int diastolica,int bpm,int rpm, int sp02, string grau,string obs, int dor,int hgt)
        {
            cmd.Parameters.AddWithValue("@paciente", paciente);
            cmd.Parameters.AddWithValue("@datahora", dataHora);
            cmd.Parameters.AddWithValue("@peso", peso);
            cmd.Parameters.AddWithValue("@altura", altura);
            cmd.Parameters.AddWithValue("@sistolica", sistolica);
            cmd.Parameters.AddWithValue("@diastolica", diastolica);
            cmd.Parameters.AddWithValue("@bpm", bpm);
            cmd.Parameters.AddWithValue("@rpm", rpm);
            cmd.Parameters.AddWithValue("@sp02", sp02);
            cmd.Parameters.AddWithValue("@grau", grau);
            cmd.Parameters.AddWithValue("@obs", obs);
            cmd.Parameters.AddWithValue("@dor", dor);
            cmd.Parameters.AddWithValue("@hgt", hgt);
            cmd.CommandText = "insert into avaliacao(paciente,datahora, peso, altura, sistolica, diastolica, bpm, rpm, sp02, grau,obs,dor,hgt,ativo ) values(@paciente,@datahora, @peso, @altura, @sistolica, @diastolica, @bpm, @rpm, @sp02, @grau,@obs, @dor,@hgt,1)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Cadastrado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Avaliação no banco de dados" + ex;
            }
            return mensagem;

        }
    }
}
