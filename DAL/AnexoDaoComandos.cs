﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SistemaShekinah.DAL
{
    public class AnexoDaoComandos
    {
        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;
        public String cadastroAnexo(int paciente, String tipoanexo, string dataHora, String origem, String destino, String obs,String tag,bool ativo  )
        {
            cmd.Parameters.AddWithValue("@paciente", paciente);
            cmd.Parameters.AddWithValue("@datahora", dataHora);
            cmd.Parameters.AddWithValue("@tipoanexo", tipoanexo);
            cmd.Parameters.AddWithValue("@origem", origem);
            cmd.Parameters.AddWithValue("@destino", destino);
            cmd.Parameters.AddWithValue("@obs", obs);
            cmd.Parameters.AddWithValue("@tag", tag);
            cmd.Parameters.AddWithValue("@ativo", ativo);
            cmd.CommandText = "insert into anexo_paciente values(@paciente,@tipoanexo,@datahora, @origem, @destino, @obs,@tag,@ativo )";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Cadastrado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Evolução no banco de dados" + ex;
            }
            return mensagem;
        }
    }
}
