﻿    using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaShekinah.DAL
{
    public class EvolucaoDaoComandos
    {
        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;
        public String cadastroEvolucao(int  Paciente,String DataHora,String tipoevolucao,String Evolucao)
        {
            cmd.Parameters.AddWithValue("@paciente", Paciente);
            cmd.Parameters.AddWithValue("@datahora", DataHora);
            cmd.Parameters.AddWithValue("@evolucao", Evolucao);
            cmd.Parameters.AddWithValue("@tipoevolucao", tipoevolucao);
            cmd.CommandText = "insert into evolucoes(datahora,paciente,tipoevolucao,descricaoevolucao,ativo)  values(@datahora,@paciente,@tipoevolucao,@evolucao,1)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Cadastrado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Evolução no banco de dados"+ex;
            }
            return mensagem;
        }
        public String inativaEvolucao(int id, string observacao)
        {
            cmd.Parameters.AddWithValue("@idregistro", id);
            cmd.Parameters.AddWithValue("@obs", observacao);
            cmd.CommandText = "update evolucoes set ativo=0,motivo_inativacao=@obs where id=@idregistro";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Inativado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Paulo,  por favor rodar os comandos corretamente.Os mesmos foram enviados dia 27 / 07 / 2020.Verificar no seu wpp" + ex;
                // mensagem = "Erro ao inativar Evolução no banco de dados" + ex;
            }
            return mensagem;
        }
    }
}
