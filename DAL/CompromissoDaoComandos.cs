﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.DAL
{
    public class CompromissoDaoComandos
    {
        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;
        public String cadastraEvento(int idpaciente,DateTime data_hora_inicial, DateTime data_hora_final, String titulo, String descricao, String cor, string tipo_tarefa, bool ativo)
        {
            cmd.Parameters.AddWithValue("@paciente", idpaciente);
            cmd.Parameters.AddWithValue("@data_inicial", data_hora_inicial);
            cmd.Parameters.AddWithValue("@data_final", data_hora_final);
            cmd.Parameters.AddWithValue("@titulo", titulo);
            cmd.Parameters.AddWithValue("@descricao", descricao);
            cmd.Parameters.AddWithValue("@cor", cor);
            cmd.Parameters.AddWithValue("@tipotarefa", tipo_tarefa);
            cmd.Parameters.AddWithValue("@ativo", ativo);
              cmd.CommandText = "insert into agenda_compromisso(paciente,datahora_inicial,datahora_final,titulo,informacao_compromisso_agenda,cor,tipo_tarefa,ativo) values(@paciente,@data_inicial,@data_final,@titulo,@descricao,@cor ,@tipotarefa,@ativo)";
          //  cmd.CommandText = "insert into agenda_compromisso(id) values(@paciente)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Cadastrado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Evento no banco de dados" + ex;
            }
            return mensagem;
        }

        public String atualiarCadastraEvento(int idregistro,int idpaciente, DateTime data_hora_inicial, DateTime data_hora_final, String titulo, String descricao, String cor, string tipo_tarefa, bool ativo)
        {
            cmd.Parameters.AddWithValue("@idregistro", idregistro);
            cmd.Parameters.AddWithValue("@paciente", idpaciente);
            cmd.Parameters.AddWithValue("@data_inicial", data_hora_inicial);
            cmd.Parameters.AddWithValue("@data_final", data_hora_final);
            cmd.Parameters.AddWithValue("@titulo", titulo);
            cmd.Parameters.AddWithValue("@descricao", descricao);
            cmd.Parameters.AddWithValue("@cor", cor);
            cmd.Parameters.AddWithValue("@tipotarefa", tipo_tarefa);
            cmd.Parameters.AddWithValue("@ativo", ativo);
            // cmd.CommandText = "=titulo=@titulo,informacao_compromisso_agenda=@descricao,cor=@cor,tipo_tarefa=@tipotarefa,ativo=@ativo where id=@idregistro";
            cmd.CommandText = "update agenda_compromisso set paciente=@paciente,datahora_inicial=@data_inicial,datahora_final=@data_final,    titulo=@titulo,informacao_compromisso_agenda=@descricao,cor=@cor,tipo_tarefa=@tipotarefa,ativo=@ativo        where id=@idregistro";

            //  cmd.CommandText = "insert into agenda_compromisso(id) values(@paciente)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Atualizado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao atualizar esse evento" + ex;
            }
            return mensagem;
        }

        public String cadastraEvento(int idpaciente, DateTime data_hora_inicial, String titulo, String descricao, String cor, string tipo_tarefa,bool diainteiro, bool ativo)
        {
            cmd.Parameters.AddWithValue("@paciente", idpaciente);
            cmd.Parameters.AddWithValue("@data_inicial", data_hora_inicial);
            cmd.Parameters.AddWithValue("@titulo", titulo);
            cmd.Parameters.AddWithValue("@descricao", descricao);
            cmd.Parameters.AddWithValue("@cor", cor);
            cmd.Parameters.AddWithValue("@tipotarefa", tipo_tarefa);
            cmd.Parameters.AddWithValue("@diatodo", diainteiro);
            cmd.Parameters.AddWithValue("@ativo", ativo);
            cmd.CommandText = "insert into agenda_compromisso(paciente,datahora_inicial,titulo,informacao_compromisso_agenda,cor,tipo_tarefa,dia_todo,ativo) values(@paciente,@data_inicial,@titulo,@descricao,@cor ,@tipotarefa,@diatodo,@ativo)";
            //  cmd.CommandText = "insert into agenda_compromisso(id) values(@paciente)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Cadastrado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Evento no banco de dados" + ex;
            }
            return mensagem;
        }

        public String atualizaCadastraTarefa(int idregistro,int idpaciente, DateTime data_hora_inicial, String titulo, String descricao, String cor, string tipo_tarefa, bool diainteiro, bool ativo)
        {
            cmd.Parameters.AddWithValue("@idregistro", idregistro);
            cmd.Parameters.AddWithValue("@paciente", idpaciente);
            cmd.Parameters.AddWithValue("@data_inicial", data_hora_inicial);
            cmd.Parameters.AddWithValue("@titulo", titulo);
            cmd.Parameters.AddWithValue("@descricao", descricao);
            cmd.Parameters.AddWithValue("@cor", cor);
            cmd.Parameters.AddWithValue("@tipotarefa", tipo_tarefa);
            cmd.Parameters.AddWithValue("@diatodo", diainteiro);
            cmd.Parameters.AddWithValue("@ativo", ativo);
            cmd.CommandText = "update agenda_compromisso set paciente=@paciente,datahora_inicial=@data_inicial, titulo=@titulo,informacao_compromisso_agenda=@descricao,cor=@cor,tipo_tarefa=@tipotarefa,dia_todo=@diatodo,ativo=@ativo        where id=@idregistro";

            //  cmd.CommandText = "insert into agenda_compromisso(id) values(@paciente)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Atualizado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao atualizar Evento no banco de dados" + ex;
            }
            return mensagem;
        }
    }
}
