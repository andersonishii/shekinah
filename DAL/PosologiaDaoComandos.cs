﻿using SistemaShekinah.Modelo;
using System;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SistemaShekinah.DAL
{
    public class PosologiaDaoComandos
    {
        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;
        public String cadastroPosologia(int paciente, int codigoPosologia, String nomeMedicacao, String dataPendencia, String dataHoraInicial, String dataFinal, int numPeriodicidade, String nomePeriodicidade, bool ativoinativo, bool usocontinuo,string posologia,string observacao,int idmedicacao,bool ehultimoregistro)
        {
            cmd.Parameters.AddWithValue("@paciente", paciente);
            cmd.Parameters.AddWithValue("@codigoposologia", codigoPosologia);
            cmd.Parameters.AddWithValue("@nomeMedicacao", nomeMedicacao);
            cmd.Parameters.AddWithValue("@dataPendencia", dataPendencia);
            cmd.Parameters.AddWithValue("@dataHoraInicial", dataHoraInicial);
            cmd.Parameters.AddWithValue("@dataFinal", dataFinal);
            cmd.Parameters.AddWithValue("@numeroPeriodicidade", numPeriodicidade);
            cmd.Parameters.AddWithValue("@nomePeriodicidade", nomePeriodicidade);
            cmd.Parameters.AddWithValue("@ativo", ativoinativo);
            cmd.Parameters.AddWithValue("@usocontinuo", usocontinuo);
            cmd.Parameters.AddWithValue("@posologia", posologia);
            cmd.Parameters.AddWithValue("@observacao", observacao);
            cmd.Parameters.AddWithValue("@idmedicacao", idmedicacao);
            cmd.Parameters.AddWithValue("@ultimoregistro", ehultimoregistro);
            
            cmd.CommandText = "insert into [posologia](paciente,codigo_posologia,nome_medicacao,data_pendencia,datahora_inicial,data_final,num_periodicidade,nome_periodicidade,ativo,uso_continuo,posologia,observacao,id_medicacao,ehultimoregistro) " +
                "values (@paciente, @codigoposologia,@nomeMedicacao,@dataPendencia,@dataHoraInicial, @dataFinal, @numeroPeriodicidade, @nomePeriodicidade,@ativo,@usocontinuo,@posologia,@observacao,@idmedicacao,@ultimoregistro)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Cadastrado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Evolução no banco de dados" + ex;
            }
            return mensagem;

        }
        public String atualizacaoPendenciaPosologia(int idposologia, int paciente, DateTime dataRegistroPosologia)
        {
            cmd.Parameters.AddWithValue("@codposologia", idposologia);
            cmd.Parameters.AddWithValue("@paciente", paciente);
            cmd.Parameters.AddWithValue("@data", dataRegistroPosologia);

            cmd.CommandText = "update posologia set datahora_registromedicacao='"+ dataRegistroPosologia+"',ativo=0 where id = "+idposologia+" and paciente = "+paciente+"";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Horário da Medicaçação registrado comn sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao atualizar a Pendencia de Posologia no banco de dados" + ex;
            }
            return mensagem;
        }


        public String inativiaPosologiaMedicacao(int idmedicacao)
        {
            cmd.Parameters.AddWithValue("@id", idmedicacao);
            string usuario = Usuario.NomeUsuario;
            string datehoje = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            cmd.CommandText = "update posologia set ativo=0,observacao='INATIVADO PELA ROTINA DE CADASTRO DE MEDICAÇÃO. USUARIO="+usuario+" DIA/HORA="+ datehoje + " ' where id_medicacao= @id";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Todas as pendencias de posologia dessa medicação foram inativas com Sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inativar as posologias dessa medicação. Por favor tente novamente" + ex;
            }
            return mensagem;
        }


        public String inativaApenasUmaPosologia(int id, int paciente, string Medicacao,string obs)
        {
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@paciente", paciente);
            cmd.Parameters.AddWithValue("@medicacao", Medicacao);
            cmd.Parameters.AddWithValue("@obs", obs);

            cmd.CommandText = "update posologia set ativo=0,observacao=@obs where id =@id and paciente=@paciente and nome_medicacao = @medicacao";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Uma Pendencia Foi inativada Com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inativar a Posologia no banco de dados" + ex;
            }
            return mensagem;
        }

        public String inativaMaisdeUmaPosologia(int idPosologia, int paciente, string Medicacao,string observacao)
        {
            cmd.Parameters.AddWithValue("@id", idPosologia);
            cmd.Parameters.AddWithValue("@paciente", paciente);
            cmd.Parameters.AddWithValue("@medicacao", Medicacao);
            cmd.Parameters.AddWithValue("@obs", observacao);
            cmd.CommandText = "update posologia set ativo=0,observacao=@obs where codigo_posologia =@id and paciente=@paciente and nome_medicacao = @medicacao";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Todas as pendencia do Medicamento  "+Medicacao+  " desse paciente Foram inativada Com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inativar a Posologia no banco de dados" + ex;
            }
            return mensagem;
        }
    }
}
