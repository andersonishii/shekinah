﻿ using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaShekinah.DAL
{
    class LoginDaoComandos
    {
        public bool tem=false;
        public String mensagem="";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;

        public bool verificarLogin(String login,String senha)
        {
            
            cmd.Parameters.AddWithValue("@login", login);
            cmd.Parameters.AddWithValue("@senha", senha);
            cmd.CommandText = "select * from dbo.usuario where loginUser=@login and psw=@senha";
            try
            {
                cmd.Connection = con.conectar();
                dr=cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    tem = true;
                    dr.Close();
                }
                con.desconectar();
            }catch(SqlException e)
            {
                this.mensagem = "Erro no banco de dados0"+e;
            }
  
            return tem;
        }


    }
}
