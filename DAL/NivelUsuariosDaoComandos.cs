﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SistemaShekinah.DAL
{
    class NivelUsuariosDaoComandos
    {
        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;
        public String cadastraNivel(String nomeNivel, bool ativoinativo, String cadPaciente, String anexoPaciente, String InfCompPaciente, String atividadeGrupo,String fotoDoPaciente, String agenda, String entradaSaidaPaciente, String atendimento, String avaliacao, String medicacao, String posologia, String cadastroUsuario, String nivelUsuario)
        {
            cmd.Parameters.AddWithValue("@ativo", ativoinativo);
            cmd.Parameters.AddWithValue("@nomeNivel", nomeNivel);
            cmd.Parameters.AddWithValue("@cadastroPaciente", cadPaciente);
            cmd.Parameters.AddWithValue("@anexoPaciente", anexoPaciente);
            cmd.Parameters.AddWithValue("@infcomp", InfCompPaciente);
            cmd.Parameters.AddWithValue("@atividadeGrupo", atividadeGrupo);
            cmd.Parameters.AddWithValue("@fotoPaciente", fotoDoPaciente);
            cmd.Parameters.AddWithValue("@agenda", agenda);
            cmd.Parameters.AddWithValue("@entradaSaidaPac", entradaSaidaPaciente);
            cmd.Parameters.AddWithValue("@atendimento", atendimento);
            cmd.Parameters.AddWithValue("@avalicao", avaliacao);
            cmd.Parameters.AddWithValue("@medicacao", medicacao);
            cmd.Parameters.AddWithValue("@posologia", posologia);
            cmd.Parameters.AddWithValue("@cadastrousuario", cadastroUsuario);
            cmd.Parameters.AddWithValue("@nivelusuario", nivelUsuario);

            cmd.CommandText = "insert into nivel_usuario(ativo,nome_Nivel,cadastro_paciente,anexo_paciente,inf_comp_paciente ,atividade_grupo ,foto_paciente ,agenda,entrada_saida_paciente,atendimento,avaliacao,medicacao,posologia,cadastro_usuario,nivel_usuario) " +
                "values(@ativo,@nomeNivel, @cadastroPaciente, @anexoPaciente, @infcomp,@atividadeGrupo ,@fotoPaciente,@agenda,@entradaSaidaPac,@atendimento,@avalicao,@medicacao,@posologia,@cadastrousuario,@nivelusuario)";
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Nivel de Usuário Cadastrado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Nivel de Usuários no banco de dados" + ex;
            }
            return mensagem;
        }


    public String atualizaNivelDeUsuario (int codIdPosologia,String nomeNivel, bool ativoinativo, String cadPaciente, String anexoPaciente, String InfCompPaciente, String atividadeGrupo, String fotoDoPaciente, String agenda, String entradaSaidaPaciente, String atendimento, String avaliacao, String medicacao, String posologia, String cadastroUsuario, String nivelUsuario)
    {
        cmd.Parameters.AddWithValue("@idcodposologia", codIdPosologia);
        cmd.Parameters.AddWithValue("@ativo", ativoinativo);
        cmd.Parameters.AddWithValue("@nomeNivel", nomeNivel);
        cmd.Parameters.AddWithValue("@cadastroPaciente", cadPaciente);
        cmd.Parameters.AddWithValue("@anexoPaciente", anexoPaciente);
        cmd.Parameters.AddWithValue("@infcomp", InfCompPaciente);
        cmd.Parameters.AddWithValue("@atividadeGrupo", atividadeGrupo);
        cmd.Parameters.AddWithValue("@fotoPaciente", fotoDoPaciente);
        cmd.Parameters.AddWithValue("@agenda", agenda);
        cmd.Parameters.AddWithValue("@entradaSaidaPac", entradaSaidaPaciente);
        cmd.Parameters.AddWithValue("@atendimento", atendimento);
        cmd.Parameters.AddWithValue("@avalicao", avaliacao);
        cmd.Parameters.AddWithValue("@medicacao", medicacao);
        cmd.Parameters.AddWithValue("@posologia", posologia);
        cmd.Parameters.AddWithValue("@cadastrousuario", cadastroUsuario);
        cmd.Parameters.AddWithValue("@nivelusuario", nivelUsuario);

        cmd.CommandText = "update  nivel_usuario set ativo=@ativo,nome_Nivel=@nomeNivel,cadastro_paciente=@cadastroPaciente,anexo_paciente= @anexoPaciente,inf_comp_paciente= @infcomp ," +
                "atividade_grupo=@atividadeGrupo  ,foto_paciente=@fotoPaciente ,agenda=@agenda,entrada_saida_paciente=@entradaSaidaPac,atendimento=@atendimento,avaliacao=@avalicao,medicacao=@medicacao,posologia=@posologia,cadastro_usuario=@cadastrousuario,nivel_usuario=@nivelusuario where id=@idcodposologia";
        try
        {
            cmd.Connection = con.conectar();
            cmd.ExecuteNonQuery();
            con.desconectar();
            this.mensagem = "Nivel de Usuário atualizado com sucesso";
            tem = true;
        }
        catch (SqlException ex)
        {
            mensagem = "Erro ao atualizar Nivel de Usuários no banco de dados" + ex;
        }
        return mensagem;
        }
    }
}
