﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.DAL
{
    public class PacienteDaoComandos
    {
     
        public int Id { get; set; }
        public String Nome { get; set; }
        public String cpf { get; set; }


        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;

        public String cadatraAtividadeGrupoPaciente(string idPaciente, DateTime data_hora, string TipoAtividade, String relatorio, int codagrupador, bool ativoinativo)
        {
            cmd.Parameters.AddWithValue("@paciente", idPaciente);
            cmd.Parameters.AddWithValue("@datahora", data_hora);
            cmd.Parameters.AddWithValue("@tipoatividade", TipoAtividade);
            cmd.Parameters.AddWithValue("@relatorio", relatorio);
            cmd.Parameters.AddWithValue("@codagrupador", codagrupador);
            cmd.Parameters.AddWithValue("@ativo", ativoinativo);

            cmd.CommandText = "INSERT INTO dbo.atividadegrupo_paciente (idpaciente,datahora,tipo_atividade,relatorio,codigo_agrupador,ativo) values (@paciente,@datahora,@tipoatividade,@relatorio,@codagrupador,@ativo)";

            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Atividade do Paciente Registrada com sucesso. ";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Atividade em grupo do paciente no banco de dados" + ex;
            }
            return mensagem;

        }

        public String registraInformacaoComplementarPaciente(bool ativoinativo,
            int id,
           int ComposicaoFamiliar,
           String obsComposicaoFamiliar,
           String nomePlanoSaude,
           int carenciaPlanoCirurgia,
           int carenciaPlanoConsulta,
           int carenciaPlanoOutros,
           String obsPlanoSaude,
           String necessidadesEspeciais,
           String obsnecessidadesEspeciais,
           String restricoesAlimentares,
           String obsrestricoesAlimentares,
           String alergia,
           String obsalergia,
           String unidadeTempoCirurgia,
           String unidadeTempoConsulta,
           String unidadeTempoOutros
           )
        {
            cmd.Parameters.AddWithValue("@ativoinativo", ativoinativo);
            cmd.Parameters.AddWithValue("@idPaciente", id);
            cmd.Parameters.AddWithValue("@composicaoFamiliar", ComposicaoFamiliar);
            cmd.Parameters.AddWithValue("@obsComposicaoFamiliar", obsComposicaoFamiliar);
            cmd.Parameters.AddWithValue("@nomePlanoSaude", nomePlanoSaude);
            cmd.Parameters.AddWithValue("@carenciaPlanoCirurgia", carenciaPlanoCirurgia);
            cmd.Parameters.AddWithValue("@carenciaPlanoConsulta", carenciaPlanoConsulta);
            cmd.Parameters.AddWithValue("@carenciaPlanoOutros", carenciaPlanoOutros);
            cmd.Parameters.AddWithValue("@obsPlanoSaude", obsPlanoSaude);
            cmd.Parameters.AddWithValue("@necessidadesEspeciais", necessidadesEspeciais);
            cmd.Parameters.AddWithValue("@obsnecessidadesEspeciais", obsnecessidadesEspeciais);
            cmd.Parameters.AddWithValue("@restricoesAlimentares", restricoesAlimentares);
            cmd.Parameters.AddWithValue("@obsrestricoesAlimentares", obsrestricoesAlimentares);
            cmd.Parameters.AddWithValue("@alergia", alergia);
            cmd.Parameters.AddWithValue("@obsalergia", obsalergia);
            cmd.Parameters.AddWithValue("@unidaTempoCirurgia", unidadeTempoCirurgia);
            cmd.Parameters.AddWithValue("@unidaTempoConsulta", unidadeTempoConsulta);
            cmd.Parameters.AddWithValue("@unidaTempoOutros", unidadeTempoOutros);


            cmd.CommandText = "update paciente set ativo=@ativoinativo, nome_plano_saude=@nomePlanoSaude,composicao_familiar=@composicaoFamiliar,obs_composicao_familiar=@obsComposicaoFamiliar,carencia_plano_cirurgia=@carenciaPlanoCirurgia,carencia_plano_consulta=@carenciaPlanoConsulta," +
                "carencia_plano_outros=@carenciaPlanoOutros  ,obs_plano_saude=@obsPlanoSaude,necessidades_especiais=@necessidadesEspeciais," +
                "obs_necessidades_especiais=@obsnecessidadesEspeciais,restricao_alimentares=@restricoesAlimentares," +
                "obs_restricao_alimentares=@obsrestricoesAlimentares,alergias=@alergia,obs_alergias=@obsalergia," +
                "unidadetempo_carencia_plano_cirurgia=@unidaTempoCirurgia,unidadetempo_carencia_plano_consulta=@unidaTempoConsulta,unidadetempo_carencia_plano_outros=@unidaTempoOutros where id = @idPaciente";
      
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Informações complementares do paciente cadastrados com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao cadastrar informações complementares do paciente. Por favor tente novamente" + ex;
                tem = false;
            }
            return mensagem;
        }
    

    public string getPacienteAtivoOuInativo(int ativoinativo)
        {
            StringBuilder sb = new StringBuilder();
            if (ativoinativo.Equals(0))
            {
                cmd.CommandText = "select id from paciente where ativo=0";
                
            }
            else
            {
                cmd.CommandText = "select id from paciente where ativo=1 ";
            }
           
            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    this.mensagem= sb.AppendLine (dr["id"].ToString()).ToString();
                }
                string[] tempReturn = mensagem.Split('\n');
                string idtratado = String.Join(",", tempReturn);
                //nessa varaivel ele retorna  dos ID de pacientes 
                return idtratado+"0";


                con.desconectar();
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao verificar " + ex;
            }
            return mensagem;
        }
        public List<PacienteDaoComandos> GetPacinteDB()
        {
            cmd.CommandText = "select nome,cpf from paciente";
            List<PacienteDaoComandos> _pacientes = new List<PacienteDaoComandos>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _pacientes.Add(new PacienteDaoComandos()
                    {
                    //    Id = Convert.ToInt32(dr["Id"]),
                        Nome = dr["Nome"].ToString(),
                        cpf = dr["cpf"].ToString(),
                    });
                }
                dr.Close();
                return _pacientes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }
        public String cadastroPaciente(String nomePaciente, String cpf,bool ativo,
            String tipIdentificacao,string numIdentificacao,
            String dataNasc,String raca, String cns, String sexo, 
            String dataAdmissao, String escolaridade, String tel1, 
            String tel2, String nomeContato, String telefoneContatoEmergencia,
            String nomeContatoEmergencia,String cep, String logradouro, 
            String numero_logradouro,String complemento, String uf,String municipio, 
            String nomeResponsavelCepPaciente, String obsEnderecoPaciente, 
            String cep_residAnterior, String logradouro_residAnterior, 
            String numero_logradouro_residAnterior, String complemento_residAnterior, 
            String uf_residAnterior, String municipio_residAnterior,
            String nomeResponsavelCepPaciente_residAnterior, 
            String obsEnderecoPaciente_residAnterior

        
            )

        {
            cmd.Parameters.AddWithValue("@paciente", nomePaciente);
            cmd.Parameters.AddWithValue("@cpf", cpf);
            cmd.Parameters.AddWithValue("@ativo", ativo);
            cmd.Parameters.AddWithValue("@tipodeIdentificacao", tipIdentificacao);
            cmd.Parameters.AddWithValue("@numeroIdentificacao", numIdentificacao);
            cmd.Parameters.AddWithValue("@dataNascimento", dataNasc);
            cmd.Parameters.AddWithValue("@raca", raca);
            cmd.Parameters.AddWithValue("@cns", cns);
            cmd.Parameters.AddWithValue("@sexo", sexo);
            cmd.Parameters.AddWithValue("@dataDeAdmissao", dataAdmissao);
            cmd.Parameters.AddWithValue("@escolaridade", escolaridade);
            cmd.Parameters.AddWithValue("@telefone1", tel1);
            cmd.Parameters.AddWithValue("@telefone2", tel2);
            cmd.Parameters.AddWithValue("@nomeContato", nomeContato);
            cmd.Parameters.AddWithValue("@telefoneContatoEmergencia", telefoneContatoEmergencia);
            cmd.Parameters.AddWithValue("@nomeContatoEmergencia", nomeContatoEmergencia);
            cmd.Parameters.AddWithValue("@cep", cep);
            cmd.Parameters.AddWithValue("@logradouro", logradouro);
            cmd.Parameters.AddWithValue("@numLogradouro", numero_logradouro);
            cmd.Parameters.AddWithValue("@complemento", complemento);
            cmd.Parameters.AddWithValue("@uf", uf);
            cmd.Parameters.AddWithValue("@municipio", municipio);
            cmd.Parameters.AddWithValue("@nomeResponsavelPacienteCep", nomeResponsavelCepPaciente);
            cmd.Parameters.AddWithValue("@obsEnderecoPaciente", obsEnderecoPaciente);
            cmd.Parameters.AddWithValue("@cepAnterior", cep_residAnterior);
            cmd.Parameters.AddWithValue("@logradouroAnterior", logradouro_residAnterior);
            cmd.Parameters.AddWithValue("@numLogradouroAnterior", numero_logradouro_residAnterior);
            cmd.Parameters.AddWithValue("@complementoAnterior", complemento_residAnterior);
            cmd.Parameters.AddWithValue("@ufAnterior", uf_residAnterior);
            cmd.Parameters.AddWithValue("@municipioAnterior", municipio_residAnterior);
            cmd.Parameters.AddWithValue("@nomeResponsavelPacienteCepAnterior", nomeResponsavelCepPaciente_residAnterior);
            cmd.Parameters.AddWithValue("@obsEnderecoPacienteAnterior", obsEnderecoPaciente_residAnterior);







            cmd.CommandText = "INSERT INTO dbo.paciente (nome,cpf,ativo ,tipo_identificacao," +
         "num_identificacao,data_nasc,raca,cns,sexo,data_admissao,escolaridade,telefone1 ,telefone2,nome_contato,telefone_contato_emergencia,nome_contato_emergencia,cep,logradouro,numero_logradouro,complemento,uf,municipio,nome_responsavel_cep_paciente," +
           "obs_endereco_paciente,cep_residencia_anterior,logradouro_residencia_anterior," +
                "numero_logradouro_residencia_anterior,complemento_residencia_anterior,uf_residencia_anterior ," +
                "municipio_residencia_anterior,nome_responsavel_cep_paciente_residencia_anterior," +
                "obs_endereco_paciente_residencia_anterior ) values(@paciente," +
         "@cpf,@ativo,@tipodeIdentificacao,@numeroIdentificacao,@dataNascimento, @raca, @cns,@sexo, @dataDeAdmissao,  @escolaridade,@telefone1,@telefone2, @nomeContato, @telefoneContatoEmergencia, " +
           "@nomeContatoEmergencia, @cep, @logradouro, @numLogradouro," +
           "@complemento,@uf,@municipio, @nomeResponsavelPacienteCep," +
           " @obsEnderecoPaciente,@cepAnterior, @logradouroAnterior," +
                "@numLogradouroAnterior, @complementoAnterior,  @ufAnterior, " +
                "@municipioAnterior,@nomeResponsavelPacienteCepAnterior,  " +
                "@obsEnderecoPacienteAnterior)";





            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Cadastrado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inserir Evolução no banco de dados" + ex;
            }
            return mensagem;
        }

  
        public String inativarPaciente(int idpaciente)
        {
            cmd.Parameters.AddWithValue("@id", idpaciente);
            cmd.CommandText = "update paciente set ativo=0 where id=@id";


            try
            {
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Paciente Inativado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inativar  Paciente  no banco de dados" + ex;
                tem = false;
            }
            return mensagem;

        }

        public String cadastraFotoPaciente(int idPaciente, bool ativoinativo, string tipoFoto, byte[] imagem)
        {
            cmd.Parameters.AddWithValue("@id", idPaciente);
            cmd.Parameters.AddWithValue("@ativoinativo", ativoinativo);
            cmd.Parameters.AddWithValue("@tipofoto", tipoFoto);
            cmd.Parameters.AddWithValue("@foto", imagem);
            cmd.CommandText = "insert into foto_paciente (ativo,id_paciente,foto,tipo_foto)values(@ativoinativo,@id,@foto,@tipofoto)";
            try
            {
               
                cmd.Connection = con.conectar();
                cmd.ExecuteNonQuery();
                con.desconectar();
                this.mensagem = "Paciente Inativado com sucesso";
                tem = true;
            }
            catch (SqlException ex)
            {
                mensagem = "Erro ao inativar  Paciente  no banco de dados" + ex;
                tem = false;
            }
            return mensagem;

        }





        public String atualizarCadastroPaciente(int ID, String nomePaciente, String cpf, bool ativo,
        String tipIdentificacao, string numIdentificacao,
        String dataNasc, String raca, String cns, String sexo,
        String dataAdmissao, String escolaridade, String tel1,
        String tel2, String nomeContato, String telefoneContatoEmergencia,
        String nomeContatoEmergencia, String cep, String logradouro,
        String numero_logradouro, String complemento, String uf, String municipio,
        String nomeResponsavelCepPaciente, String obsEnderecoPaciente,
        String cep_residAnterior, String logradouro_residAnterior,
        String numero_logradouro_residAnterior, String complemento_residAnterior,
        String uf_residAnterior, String municipio_residAnterior,
        String nomeResponsavelCepPaciente_residAnterior,
        String obsEnderecoPaciente_residAnterior


        )

    {
        cmd.Parameters.AddWithValue("@id", ID);
        cmd.Parameters.AddWithValue("@paciente", nomePaciente);
        cmd.Parameters.AddWithValue("@cpf", cpf);
        cmd.Parameters.AddWithValue("@ativo", ativo);
        cmd.Parameters.AddWithValue("@tipodeIdentificacao", tipIdentificacao);
        cmd.Parameters.AddWithValue("@numeroIdentificacao", numIdentificacao);
        cmd.Parameters.AddWithValue("@dataNascimento", dataNasc);
        cmd.Parameters.AddWithValue("@raca", raca);
        cmd.Parameters.AddWithValue("@cns", cns);
        cmd.Parameters.AddWithValue("@sexo", sexo);
        cmd.Parameters.AddWithValue("@dataDeAdmissao", dataAdmissao);
        cmd.Parameters.AddWithValue("@escolaridade", escolaridade);
        cmd.Parameters.AddWithValue("@telefone1", tel1);
        cmd.Parameters.AddWithValue("@telefone2", tel2);
        cmd.Parameters.AddWithValue("@nomeContato", nomeContato);
        cmd.Parameters.AddWithValue("@telefoneContatoEmergencia", telefoneContatoEmergencia);
        cmd.Parameters.AddWithValue("@nomeContatoEmergencia", nomeContatoEmergencia);
        cmd.Parameters.AddWithValue("@cep", cep);
        cmd.Parameters.AddWithValue("@logradouro", logradouro);
        cmd.Parameters.AddWithValue("@numLogradouro", numero_logradouro);
        cmd.Parameters.AddWithValue("@complemento", complemento);
        cmd.Parameters.AddWithValue("@uf", uf);
        cmd.Parameters.AddWithValue("@municipio", municipio);
        cmd.Parameters.AddWithValue("@nomeResponsavelPacienteCep", nomeResponsavelCepPaciente);
        cmd.Parameters.AddWithValue("@obsEnderecoPaciente", obsEnderecoPaciente);
        cmd.Parameters.AddWithValue("@cepAnterior", cep_residAnterior);
        cmd.Parameters.AddWithValue("@logradouroAnterior", logradouro_residAnterior);
        cmd.Parameters.AddWithValue("@numLogradouroAnterior", numero_logradouro_residAnterior);
        cmd.Parameters.AddWithValue("@complementoAnterior", complemento_residAnterior);
        cmd.Parameters.AddWithValue("@ufAnterior", uf_residAnterior);
        cmd.Parameters.AddWithValue("@municipioAnterior", municipio_residAnterior);
        cmd.Parameters.AddWithValue("@nomeResponsavelPacienteCepAnterior", nomeResponsavelCepPaciente_residAnterior);
        cmd.Parameters.AddWithValue("@obsEnderecoPacienteAnterior", obsEnderecoPaciente_residAnterior);




        cmd.CommandText = "update paciente set nome=@paciente,cpf=@cpf,ativo=@ativo ,tipo_identificacao=@tipodeIdentificacao" +
            ",num_identificacao=@numeroIdentificacao,data_nasc=@dataNascimento,raca=@raca,cns=@cns,sexo=@sexo,data_admissao=@dataDeAdmissao,escolaridade=@escolaridade," +
            "telefone1=@telefone1 ,telefone2=@telefone2,nome_contato=@nomeContato,telefone_contato_emergencia=@telefoneContatoEmergencia," +
            "nome_contato_emergencia=@nomeContatoEmergencia,cep=@cep,logradouro=@logradouro,numero_logradouro=@numLogradouro,complemento=@complemento,uf=@uf," +
            "municipio=@municipio,nome_responsavel_cep_paciente=@nomeResponsavelPacienteCep,obs_endereco_paciente=@obsEnderecoPaciente,cep_residencia_anterior=@cepAnterior," +
            "logradouro_residencia_anterior=@logradouroAnterior,numero_logradouro_residencia_anterior=@numLogradouroAnterior,complemento_residencia_anterior=@complementoAnterior," +
            "uf_residencia_anterior=@ufAnterior ,municipio_residencia_anterior=@municipioAnterior,nome_responsavel_cep_paciente_residencia_anterior=@nomeResponsavelPacienteCepAnterior,obs_endereco_paciente_residencia_anterior= @obsEnderecoPacienteAnterior where id=@id";
        try
        {
            cmd.Connection = con.conectar();
            cmd.ExecuteNonQuery();
            con.desconectar();
            this.mensagem = "Atualizado com sucesso";
            tem = true;
        }
        catch (SqlException ex)
        {
            mensagem = "Erro ao inserir Evolução no banco de dados" + ex;
            tem = false;
        }
        return mensagem;
    }





    public String inativaOuAtivaPaciente(int idpaciente,String motivo,int ativo_inativo,string tipoMotivo)
    {
        cmd.Parameters.AddWithValue("@ativoouinativo", ativo_inativo);
        cmd.Parameters.AddWithValue("@motivo", motivo);
        cmd.Parameters.AddWithValue("@idpaciente", idpaciente);
            cmd.Parameters.AddWithValue("@tipoMotivo", tipoMotivo);
            cmd.CommandText = "update paciente set ativo=@ativoouinativo,motivo=@motivo,tipo_motivo=@tipoMotivo where id=@idpaciente";


        try
        {
            cmd.Connection = con.conectar();
            cmd.ExecuteNonQuery();
            con.desconectar();
            this.mensagem = "Registrado executadado com sucesso";
            tem = true;
        }
        catch (SqlException ex)
        {
            mensagem = "Operação não executada.Erro ao executar operação com o  Paciente " + ex;
            tem = false;
            }
        return mensagem;

    }
    }





}
