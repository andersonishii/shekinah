﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaShekinah.Modelo
{
    public class Medicacoes
    {
        public int Id { get; set; }
        public String nomeFormaFarmaceutica { get; set; }
        public String abreviatura { get; set; }
        public String nomeMedicamento { get; set; }
        public String nomeComercial { get; set; }
        public String nomeQuimico { get; set; }
        public String observacao { get; set; }
        public int cpf { get; set; }

        public bool ativo { get; set; }

        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;

        public String restricaoMedicacao(int  codMedicacao)
        {
            string restricao = "";
            try
            {

                cmd.CommandText = "select (case when reacoes_adversas=''  then 'MENSAGEM AUTOMATICA, REAÇÃO NÃO CADASTRADA!' else reacoes_adversas  end ) as reacoes_adversas from medicacoes where id = '" + codMedicacao + "'";
                cmd.Connection = con.conectar();
                restricao = cmd.ExecuteScalar().ToString();
                cmd.ExecuteNonQuery();
                con.desconectar();
                return restricao;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }

        }

        public int idMedicacao(String nomeDaMedicacao)
        {
            int idmedicacao = 0;
 
            try
            {

                cmd.CommandText = "select id from dbo.medicacoes where nome_quimico='"+ nomeDaMedicacao + "'";
                cmd.Connection = con.conectar();
                idmedicacao = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                cmd.ExecuteNonQuery();
                con.desconectar();
                return idmedicacao;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }

        }





        public List<Medicacoes> GetMedicamentoCadastroDB()
        {
            cmd.CommandText = "select id,nome_quimico,nome_comercial,forma_farmaceutica,modo_de_uso,nome_laboratorio,ativo from medicacoes where ativo=1";
            List<Medicacoes> _medicamentoCadastrados = new List<Medicacoes>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _medicamentoCadastrados.Add(new Medicacoes()
                    {
                        Id = Convert.ToInt32(dr["id"]),
                        nomeQuimico = dr["nome_quimico"].ToString(),
                        nomeComercial =dr["nome_comercial"].ToString(),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                    });
                }
                dr.Close();
                return _medicamentoCadastrados;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }


        public List<Medicacoes> GetFormaFarmaceuticaDB()
        {
            cmd.CommandText = "select id, nome_forma_farmaceutica,abreviatura,ativo from forma_farmaceutica";
            List<Medicacoes> _formaFarmaceutica = new List<Medicacoes>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _formaFarmaceutica.Add(new Medicacoes()
                    {
                        Id = Convert.ToInt32(dr["id"]),
                        nomeFormaFarmaceutica = dr["nome_forma_farmaceutica"].ToString(),
                        abreviatura = dr["abreviatura"].ToString(),
                        ativo = Convert.ToBoolean( dr["ativo"].ToString()),
                    });
                }
                dr.Close();
                return _formaFarmaceutica;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }

        public List<Medicacoes> GetRestricaoMedicacao(int codpaciente)
        {
            cmd.CommandText = "select idpaciente,nome_medicamento_restricao,observacao from paciente_restricao_medicacao where idpaciente=" + codpaciente + "";
            List<Medicacoes> _restricaoMedicacao = new List<Medicacoes>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _restricaoMedicacao.Add(new Medicacoes()
                    {
                        Id = Convert.ToInt32(dr["idpaciente"]),
                        nomeMedicamento = dr["nome_medicamento_restricao"].ToString(),
                        observacao = dr["observacao"].ToString(),
                    });
                }
                dr.Close();
                return _restricaoMedicacao;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }


    }

}

