﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaShekinah.Modelo
{
    public class NivelUsuario
    {

        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;

        public  string nomeNivelUsuario { get; set; }


        public int Id { get; set; }
        public Boolean permiteLancRetroativo { get; set; }
        public Boolean ativo { get; set; }
   

        public String  retornaNivelPermissao(String tabela_permissao, String loginUser)
        {
            string nivelRetorno ="";
            try
            {

                cmd.CommandText = "select(select nu."+ tabela_permissao +" from nivel_usuario nu where nu.id = u.codNivelDeUsuario ) as Tipo  from usuario u where u.loginUser = '"+loginUser+"'";

              //  cmd.CommandText = "select "+tabela_permissao+ " from nivel_usuario where id=" + codNivelUsuario + "";

                cmd.Connection = con.conectar();
                nivelRetorno = cmd.ExecuteScalar().ToString();
                cmd.ExecuteNonQuery();
                con.desconectar();
                return nivelRetorno;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }

        }

        public List<NivelUsuario> GetNivelUserDB(int ativo)
        {
            if (ativo == 1)
            {
                cmd.CommandText = "select id,ativo,nome_Nivel from nivel_usuario where ativo=" + ativo + " ";
            }
            else
            {
                cmd.CommandText = "select id,ativo,nome_Nivel from nivel_usuario  ";
            }

            List<NivelUsuario> _nivelUser = new List<NivelUsuario>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _nivelUser.Add(new NivelUsuario()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                        nomeNivelUsuario = dr["nome_Nivel"].ToString(),
                    });
                }
                dr.Close();
                return _nivelUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }


    }
}
