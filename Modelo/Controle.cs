﻿using SistemaShekinah.Apresentacao;
using SistemaShekinah.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Modelo
{
    public class Controle
    {

        public bool tem;
        public String msg;
        public bool acessar(String login, String senha)
        {
            LoginDaoComandos loginDao = new LoginDaoComandos();
            tem = loginDao.verificarLogin(login, senha);
            if (!loginDao.mensagem.Equals(""))
            {
                this.msg = loginDao.mensagem;
            }

            return tem;
        }
        public String cadastroEvolucao(int Paciente, String DataHora, String TipoEvolucao, String Evolucao)
        {
            EvolucaoDaoComandos evolucao = new EvolucaoDaoComandos();

            this.msg = evolucao.cadastroEvolucao(Paciente, DataHora, TipoEvolucao, Evolucao);
            if (evolucao.tem)
            {
                this.tem = true;
            }
            return msg;

        }

        public String inativaAvaliacao(int IdRegistro, String obs)
        {
            AvaliacaoDaoComandos avaliacao = new AvaliacaoDaoComandos();

            this.msg = avaliacao.inativaAvaliacao(IdRegistro, obs);
            if (avaliacao.tem)
            {
                this.tem = true;
            }
            return msg;

        }

        public String inativaSaidaIdoso(int IdRegistro, String obs)
        {
            EntradaSaidaDaoComandos saida = new EntradaSaidaDaoComandos();

            this.msg = saida.inativaSaidaIdoso(IdRegistro, obs);
            if (saida.tem)
            {
                this.tem = true;
            }
            return msg;

        }


        public String inativaEvolucao(int IdRegistro, String obs)
        {
            EvolucaoDaoComandos evolucao = new EvolucaoDaoComandos();

            this.msg = evolucao.inativaEvolucao(IdRegistro, obs);
            if (evolucao.tem)
            {
                this.tem = true;
            }
            return msg;

        }



        public String cadastroAvaliacao(int Paciente, String DataHora, String Peso, String Altura, int Sistolica, int Diatolica, int Bpm, int Rpm, int sp02, String grau, String obs, int dor,int hgt)
        {
            AvaliacaoDaoComandos avaliacao = new AvaliacaoDaoComandos();

            this.msg = avaliacao.cadastroAvaliacao(Paciente, DataHora, Peso, Altura, Sistolica, Diatolica, Bpm, Rpm, sp02, grau, obs, dor, hgt);
            if (avaliacao.tem)
            {
                this.tem = true;
            }
            return msg;

        }

        static bool IsNullOrDefault<T>(T value)
        {
            return object.Equals(value, default(T));
        }


        public static int ParseInt(string value, int valorpadrao)
        {
            int valorDefault = valorpadrao;
            int parsedInt;
            if (int.TryParse(value, out parsedInt))
            {
                return parsedInt;
            }

            return valorDefault;
        }

        public string calculaIMC(String peso, String altura)
        {
            string resultimc;
            double imc;
            if (!String.IsNullOrEmpty(peso) && !String.IsNullOrEmpty(altura))
            {
                double _altura = Convert.ToDouble(altura);
                double _peso = Convert.ToDouble(peso);
                imc = _peso / (_altura * _altura);
                return imc.ToString();
            }
            else
            {
                return resultimc = "verifica peso ou altura";
            }


        }

        public String cadastroAnexo(int Paciente, String TipoAnexo, String DataHora, String origem, String Destino, String obs, String tag, bool ativoinativo)
        {
            AnexoDaoComandos anexo = new AnexoDaoComandos();

            this.msg = anexo.cadastroAnexo(Paciente, TipoAnexo, DataHora, origem, Destino, obs, tag, ativoinativo);
            if (anexo.tem)
            {
                this.tem = true;
            }
            return msg;

        }



        public String deleteMedicacao(int ID)
        {
            MedicacoesDaoComandos medicacao = new MedicacoesDaoComandos();

            this.msg = medicacao.deleteMedicacao(ID);
            if (medicacao.tem)
            {
                this.tem = true;
            }
            return msg;

        }

        public String inativaMedicacao(int idmedicacao)
        {
            MedicacoesDaoComandos medicacao = new MedicacoesDaoComandos();

            this.msg = medicacao.inativaMedicacao(idmedicacao);
            if (medicacao.tem)
            {
                this.tem = true;
            }
            return msg;

        }

        public String atualizaMedicacao(String nomeQuimico, String nomeComercial, String formaFarmaceutica, String modoUso, String DataHoraCadastro, bool ativoinativo, String nomelab, int id, String reacoesAdversas)
        {
            MedicacoesDaoComandos medicacao = new MedicacoesDaoComandos();

            this.msg = medicacao.atualizaMedicacao(nomeQuimico, nomeComercial, formaFarmaceutica, modoUso, DataHoraCadastro, ativoinativo, nomelab, id, reacoesAdversas);
            if (medicacao.tem)
            {
                this.tem = true;
            }
            return msg;

        }


        public String cadastroMedicacao(String nomeQuimico, String nomeComercial, String formaFarmaceutica, String modoUso, String DataHoraCadastro, bool ativoinativo, String nomelab, String reacoesAdversas)
        {
            MedicacoesDaoComandos medicacao = new MedicacoesDaoComandos();

            this.msg = medicacao.cadastroMedicacao(nomeQuimico, nomeComercial, formaFarmaceutica, modoUso, DataHoraCadastro, ativoinativo, nomelab, reacoesAdversas);
            if (medicacao.tem)
            {
                this.tem = true;
            }
            return msg;

        }

        public String cadastroPosologia(int paciente, int codigoPosologia, String nomeMedicacao, String dataPendencia, String dataHoraInicial, String dataFinal, int numPeriodicidade, String nomePeriodicidade, bool ativoinativo, bool usocontinuo, string posologia, string observacao, int idmedicacao, bool ehultimoregistro)
        {
            PosologiaDaoComandos pos = new PosologiaDaoComandos();
            this.msg = pos.cadastroPosologia(paciente, codigoPosologia, nomeMedicacao, dataPendencia, dataHoraInicial, dataFinal, numPeriodicidade, nomePeriodicidade, ativoinativo, usocontinuo, posologia, observacao, idmedicacao, ehultimoregistro);
            if (pos.tem)
            {
                this.tem = true;
            }
            return msg;

        }

        public String atualizacaoPendenciaPosologia(int idPosologia, int paciente, DateTime dataRegistro)
        {
            PosologiaDaoComandos pos = new PosologiaDaoComandos();
            this.msg = pos.atualizacaoPendenciaPosologia(idPosologia, paciente, dataRegistro);
            {
                this.tem = true;
            }
            return msg;

        }



        public String cadastroSaidaDePaciente(int Paciente, DateTime DataHora, String TextoSaida, Boolean ativoinativo,DateTime DataHoraExpectativa)
        {
            EntradaSaidaDaoComandos entrada = new EntradaSaidaDaoComandos();
            if ((Usuario.permiteLancarRetroativamente)&& (DataHora > DataHoraExpectativa))
            {
                msg = "Atenção Voce possui permissão para lançar retroativamente. Porém a data e hora de retorno escolhida é menor que a data e hora de Saida";
                this.msg = entrada.cadastroSaida(Paciente, DataHora, TextoSaida, ativoinativo, DataHoraExpectativa);
                if (entrada.tem)
                {
                    this.tem = true;
                }
            }
            else if(DataHora > DataHoraExpectativa)
            {
                msg = "Operação não permitida. A data e hora não pode ser inferior a data e hora da expectativa de retorno";

            }
            else
            {
                this.msg = entrada.cadastroSaida(Paciente, DataHora, TextoSaida, ativoinativo, DataHoraExpectativa);
                if (entrada.tem)
                {
                    this.tem = true;
                }
            }
            return msg;
        }

        public String atualizaRetornoDePaciente(int paciente, int idregistro, DateTime datahoraRetornoReal, string texto_retorno, bool ativoinativo, bool retorno_paciente,DateTime datahoraSaida)
        {
            EntradaSaidaDaoComandos entrada = new EntradaSaidaDaoComandos();


            //if(datahoraRetornoReal< datahoraSaida)
            if ((Usuario.permiteLancarRetroativamente) && (datahoraRetornoReal < datahoraSaida))
            {
                msg = "Atenção.Voce possui permissão para lançar retroativamente. Porém a data e hora de retorno é menor que a data e hora de Saida ";
                this.msg = entrada.AtualizaRetorno(paciente, idregistro, datahoraRetornoReal, texto_retorno, ativoinativo, retorno_paciente);
                if (entrada.tem)
                {
                    this.tem = true;
                }
            }
            else if(datahoraRetornoReal < datahoraSaida) 
            {
                msg = "Atenção. A data e hora de retorno deve ser maior que a data e hora de Saida Do Idoso";

            }
            else{
                this.msg = entrada.AtualizaRetorno(paciente, idregistro, datahoraRetornoReal, texto_retorno, ativoinativo, retorno_paciente);
                if (entrada.tem)
                {
                    this.tem = true;
                }

            }
          
            return msg;
        }

        public String cadastroDeUsuarios(String login, String senha, int codNiveluser, bool permiteLancRetroativo, bool ativo, String nomeDoUsuaario)
        {
            UsuarioDaoComandos usuarios = new UsuarioDaoComandos();
            this.msg = usuarios.cadastraUsuarios(login, senha, codNiveluser, permiteLancRetroativo, ativo, nomeDoUsuaario);
            if (usuarios.tem)
            {
                this.tem = true;
            }
            return msg;
        }

        public String atualizaUsuario(String login, String senha, int codNiveluser, bool permiteLancRetroativo, bool ativo, String nomeDoUsuaario, int id)
        {
            UsuarioDaoComandos usuarios = new UsuarioDaoComandos();
            this.msg = usuarios.atualizaUsuario(login, senha, codNiveluser, permiteLancRetroativo, ativo, nomeDoUsuaario, id);
            if (usuarios.tem)
            {
                this.tem = true;
            }
            return msg;
        }

        public String inativaUsuario(int ativo, int id)
        {
            if (ativo.Equals((int)1))
            {
                UsuarioDaoComandos usuarios = new UsuarioDaoComandos();
                this.msg = usuarios.inativarRegistro(id);
                if (usuarios.tem)
                {
                    this.tem = true;
                }
                return msg;
            }
            else
            {
                this.tem = false;
                msg = "O regitro já está inativo.Nenhuma alteração foi realizada";
                return msg;
            }


        }

        public String inativaPaciente(int ativo, int id)
        {
            if (ativo.Equals((int)1))
            {
                PacienteDaoComandos paciente = new PacienteDaoComandos();
                this.msg = paciente.inativarPaciente(id);
                if (paciente.tem)
                {
                    this.tem = true;
                }
                return msg;
            }
            else
            {
                this.tem = false;
                msg = "O regitro já está inativo.Nenhuma alteração foi realizada";
                return msg;
            }

        }

        public String inativaRestricaoMedicacaoPaciente(int ativo, int id)
        {
            if (ativo.Equals((int)1))
            {
                MedicacoesDaoComandos medicacao = new MedicacoesDaoComandos();
                this.msg = medicacao.inativaMedicacaoPaciente(id);
                if (medicacao.tem)
                {
                    this.tem = true;
                }
                return msg;
            }
            else
            {
                this.tem = false;
                msg = "O regitro já está inativo.Nenhuma alteração foi realizada";
                return msg;
            }

        }

        public String deletaRestricaoMedicacaoPaciente(int ativo, int idpaciente)
        {
            if (ativo.Equals((int)1))
            {
                MedicacoesDaoComandos medicacao = new MedicacoesDaoComandos();
                this.msg = medicacao.deleteRestricaoMedicacaoPaciente(idpaciente);
                if (medicacao.tem)
                {
                    this.tem = true;
                }
                return msg;
            }
            else
            {
                this.tem = false;
                msg = "O regitro já está inativo.Nenhuma alteração foi realizada";
                return msg;
            }

        }


        public String cadastroPaciente(String nomePaciente, String cpf, bool ativo,
           String tipIdentificacao, string numIdentificacao,
           String dataNasc, String raca, String cns, String sexo,
           String dataAdmissao, String escolaridade, String tel1,
           String tel2, String nomeContato, String telefoneContatoEmergencia,
           String nomeContatoEmergencia, String cep, String logradouro,
           String numero_logradouro, String complemento, String uf, String municipio,
           String nomeResponsavelCepPaciente, String obsEnderecoPaciente,
           String cep_residAnterior, String logradouro_residAnterior,
           String numero_logradouro_residAnterior, String complemento_residAnterior,
           String uf_residAnterior, String municipio_residAnterior,
           String nomeResponsavelCepPaciente_residAnterior,
           String obsEnderecoPaciente_residAnterior

           )
        {
            PacienteDaoComandos paciente = new PacienteDaoComandos();
            this.msg = paciente.cadastroPaciente(nomePaciente, cpf, ativo,
                tipIdentificacao, numIdentificacao, dataNasc, raca, cns, sexo,
                dataAdmissao, escolaridade, tel1, tel2, nomeContato, telefoneContatoEmergencia,
                nomeContatoEmergencia, cep, logradouro, numero_logradouro, complemento, uf,
                municipio, nomeResponsavelCepPaciente, obsEnderecoPaciente, cep_residAnterior,
                logradouro_residAnterior, numero_logradouro_residAnterior,
                complemento_residAnterior, uf_residAnterior, municipio_residAnterior, nomeResponsavelCepPaciente_residAnterior, obsEnderecoPaciente_residAnterior);
            if (paciente.tem)
            {
                this.tem = true;
            }
            return msg;
        }

        public String atualizaInformacaoComplementarUsuario(bool ativoinativo, int idusuario,
            int ComposicaoFamiliar,
           String obsComposicaoFamiliar,
           String nomePlanoSaude,
           int carenciaPlanoCirurgia,
           int carenciaPlanoConsulta,
           int carenciaPlanoOutros,
           String obsPlanoSaude,
           String necessidadesEspeciais,
           String obsnecessidadesEspeciais,
           String restricoesAlimentares,
           String obsrestricoesAlimentares,
           String alergia,
           String obsalergia,
           String unidadeTempoCirurgia,
           String unidadeTempoConsulta,
           String unidadeTempoOutros
           )
        {

            PacienteDaoComandos paciente = new PacienteDaoComandos();
            this.msg = paciente.registraInformacaoComplementarPaciente(ativoinativo, idusuario, ComposicaoFamiliar, obsComposicaoFamiliar, nomePlanoSaude, carenciaPlanoCirurgia, carenciaPlanoConsulta, carenciaPlanoOutros,
                obsPlanoSaude, necessidadesEspeciais, obsnecessidadesEspeciais, restricoesAlimentares, obsrestricoesAlimentares,
                alergia, obsalergia, unidadeTempoCirurgia, unidadeTempoConsulta, unidadeTempoOutros);
            if (paciente.tem)
            {
                this.tem = true;
            }
            return msg;
        }

        public String cadastraAlergiaMedicamentoPaciente(bool ativo, int idpaciente, String restricao_medicacao, String observacao)
        {
            MedicacoesDaoComandos medicacao = new MedicacoesDaoComandos();

            this.msg = medicacao.restricaoMedicacaoPaciente(ativo, idpaciente, restricao_medicacao, observacao);
            if (medicacao.tem)
            {
                this.tem = true;
            }
            return msg;

        }

        public String cadastroTarefa(int idpaciente, DateTime data_hora_inicial, DateTime data_hora_final, String titulo, String descricao, String cor, string tipo_tarefa, bool ativo)
        {
            CompromissoDaoComandos compromisso = new CompromissoDaoComandos();
            if (data_hora_inicial > data_hora_final)
            {
                msg = "Operação não permitida! A data e hora inicial devem ser igual ou inferior a data e hora inicial";
                return msg;

            }
            else
            {
                this.msg = compromisso.cadastraEvento(idpaciente, data_hora_inicial, data_hora_final, titulo, descricao, cor, tipo_tarefa, ativo);
                if (compromisso.tem)
                {
                    this.tem = true;
                }
                return msg;
            }
        }

        public String atualiarCadastroEvento(int idregistro,int idpaciente, DateTime data_hora_inicial, DateTime data_hora_final, String titulo, String descricao, String cor, string tipo_tarefa, bool ativo)
        {
            CompromissoDaoComandos compromisso = new CompromissoDaoComandos();
            if (data_hora_inicial > data_hora_final)
            {
                msg = "Operação não permitida! A data e hora inicial devem ser igual ou inferior a data e hora inicial";
                return msg;

            }
            else
            {
                this.msg = compromisso.atualiarCadastraEvento(idregistro,idpaciente, data_hora_inicial, data_hora_final, titulo, descricao, cor, tipo_tarefa, ativo);
                if (compromisso.tem)
                {
                    this.tem = true;
                }
                return msg;
            }
        }

        public String inativaPendenciaPosologia(int medicacao)
        {
            PosologiaDaoComandos pos = new PosologiaDaoComandos();
            this.msg = pos.inativiaPosologiaMedicacao(medicacao);
            if (pos.tem)
            {
                this.tem = true;
            }
            return msg;
        }


        public String inativaPendenciaPosologia(int id, int codPosologia, int paciente, String nomeRemedio)
        {
            PosologiaDaoComandos pos = new PosologiaDaoComandos();
            string usuario = Usuario.NomeUsuario;
            string datehoje = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            string textoInativacao = "INATIVADO PELO USUARIO=" + usuario + " DIA/HORA=" + datehoje + "  ATRAVÉS DO BOTÃO DE INATIVAR (DENTRO DO CADASTRO DE REGITRO DO HORÁRIO DA MEDICAÇÃO)";

            var userInput = MessageBox.Show("Deseja inativar todos os registros dessa posologia? Note clicando no botão SIM todos os registros dessa posologia desse paciente serão inativos. Clicando no botão NÃO apenas essa pendencia será inatiivada ", "Decisão", MessageBoxButtons.YesNoCancel);
            if (userInput == DialogResult.No)
            {
                this.msg = pos.inativaApenasUmaPosologia(id, paciente, nomeRemedio, textoInativacao);
                if (pos.tem)
                {
                    this.tem = true;
                }
                return msg;

            }
            else if (userInput == DialogResult.Yes)
            {
                this.msg = pos.inativaMaisdeUmaPosologia(codPosologia, paciente, nomeRemedio, textoInativacao);
                if (pos.tem)
                {
                    this.tem = true;
                }
                return msg;
            }
            else
            {
                MessageBox.Show("Nenhuma Inativação foi realizada.");
            }
            return msg;
        }

        public String cadastroEvento(int idpaciente, DateTime data_hora_inicial, String titulo, String descricao, String cor, string tipo_tarefa, bool diainteiro, bool ativo)
        {
            CompromissoDaoComandos compromisso = new CompromissoDaoComandos();

            this.msg = compromisso.cadastraEvento(idpaciente, data_hora_inicial, titulo, descricao, cor, tipo_tarefa, diainteiro, ativo);
            if (compromisso.tem)
            {
                this.tem = true;
            }
            return msg;
        }

        public String atualizaCadastroTarefa(int idregistro,int idpaciente, DateTime data_hora_inicial, String titulo, String descricao, String cor, string tipo_tarefa, bool diainteiro, bool ativo)
        {
            CompromissoDaoComandos compromisso = new CompromissoDaoComandos();

            this.msg = compromisso.atualizaCadastraTarefa(idregistro,idpaciente, data_hora_inicial, titulo, descricao, cor, tipo_tarefa, diainteiro, ativo);
            if (compromisso.tem)
            {
                this.tem = true;
            }
            return msg;
        }

        public String InativaOuAtivaPaciente(int ID, string motivo, int ativaOuinativa,string tipoMotivo)
        {
            PacienteDaoComandos paciente = new PacienteDaoComandos();
            this.msg = paciente.inativaOuAtivaPaciente(ID, motivo, ativaOuinativa, tipoMotivo);
            if (paciente.tem)
            {
                this.tem = true;
            }
            return msg;
        }

        public String CadastraNivelDeUsuario(String nomeNivel, bool ativoinativo, String cadPaciente, String anexoPaciente, String InfCompPaciente, String atividadeGrupo, String fotoDoPaciente,String agenda, String entradaSaidaPaciente, String atendimento, String avaliacao, String medicacao, String posologia, String cadastroUsuario, String nivelUsuario)
        {
            if (nomeNivel.Equals(""))
            {
                msg = "Campo nome do Nível não pode ficar em branco";
            }
            else
            {
                NivelUsuariosDaoComandos nivel = new NivelUsuariosDaoComandos();
                this.msg = nivel.cadastraNivel(nomeNivel, ativoinativo, cadPaciente, anexoPaciente, InfCompPaciente, atividadeGrupo, fotoDoPaciente,agenda, entradaSaidaPaciente, atendimento, avaliacao, medicacao, posologia, cadastroUsuario, nivelUsuario);
                if (nivel.tem)
                {
                    this.tem = true;
                }
            }
            return msg;
        }

        public String atualizarNivelDeUsuario(int codNivelUsuer,String nomeNivel, bool ativoinativo, String cadPaciente, String anexoPaciente, String InfCompPaciente, String atividadeGrupo, String fotoDoPaciente, String agenda, String entradaSaidaPaciente, String atendimento, String avaliacao, String medicacao, String posologia, String cadastroUsuario, String nivelUsuario)
        {
            if (nomeNivel.Equals(""))
            {
                msg = "Campo nome do Nível não pode ficar em branco";
            }
            else
            {
                NivelUsuariosDaoComandos nivel = new NivelUsuariosDaoComandos();
                this.msg = nivel.atualizaNivelDeUsuario(codNivelUsuer,nomeNivel, ativoinativo, cadPaciente, anexoPaciente, InfCompPaciente, atividadeGrupo, fotoDoPaciente, agenda, entradaSaidaPaciente, atendimento, avaliacao, medicacao, posologia, cadastroUsuario, nivelUsuario);
                if (nivel.tem)
                {
                    this.tem = true;
                }
            }
            return msg;
        }

        public String CadastraAtividadeEmGrupoPaciente(string idPaciente, DateTime data_hora,string TipoAtividade,String relatorio,int codagrupador, bool ativoinativo)
        {


            PacienteDaoComandos paciente = new PacienteDaoComandos();

            this.msg = paciente.cadatraAtividadeGrupoPaciente(idPaciente, data_hora, TipoAtividade, relatorio, codagrupador, ativoinativo);
                if (paciente.tem)
                {
                    this.tem = true;
                }
            
            return msg;
        }

        public String CadastraAtividadeEmGrupoUsuario(string idPaciente, DateTime data_hora, string TipoAtividade, String relatorio, int codagrupador, bool ativoinativo)
        {


            UsuarioDaoComandos user = new UsuarioDaoComandos();

            this.msg = user.cadatraAtividadeGrupoUsuario(idPaciente, data_hora, TipoAtividade, relatorio, codagrupador, ativoinativo);
            if (user.tem)
            {
                this.tem = true;
            }

            return msg;
        }

        public String CadastraFotoPaciente(int idPaciente, bool ativoinativo,string tipoFoto, byte[] imagem)
        {


            PacienteDaoComandos paciente = new PacienteDaoComandos();

            this.msg = paciente.cadastraFotoPaciente(idPaciente, ativoinativo, tipoFoto, imagem);
            if (paciente.tem)
            {
                this.tem = true;
            }

            return msg;
        }




        public string atualizaPaciente(int ID, String nomePaciente, String cpf, bool ativo,
            String tipIdentificacao, string numIdentificacao,
            String dataNasc, String raca, String cns, String sexo,
            String dataAdmissao, String escolaridade, String tel1,
            String tel2, String nomeContato, String telefoneContatoEmergencia,      
            String nomeContatoEmergencia, String cep, String logradouro,
            String numero_logradouro, String complemento, String uf, String municipio,
            String nomeResponsavelCepPaciente, String obsEnderecoPaciente,
            String cep_residAnterior, String logradouro_residAnterior,
            String numero_logradouro_residAnterior, String complemento_residAnterior,
            String uf_residAnterior, String municipio_residAnterior,
            String nomeResponsavelCepPaciente_residAnterior,
            String obsEnderecoPaciente_residAnterior)
        {
            PacienteDaoComandos paciente = new PacienteDaoComandos();
            this.msg = paciente.atualizarCadastroPaciente(ID, nomePaciente, cpf, ativo,
                tipIdentificacao, numIdentificacao, dataNasc, raca, cns, sexo,
                dataAdmissao, escolaridade, tel1, tel2, nomeContato, telefoneContatoEmergencia,
                nomeContatoEmergencia, cep, logradouro, numero_logradouro, complemento, uf,
                municipio, nomeResponsavelCepPaciente, obsEnderecoPaciente, cep_residAnterior,
                logradouro_residAnterior, numero_logradouro_residAnterior,
                complemento_residAnterior, uf_residAnterior, municipio_residAnterior, nomeResponsavelCepPaciente_residAnterior, obsEnderecoPaciente_residAnterior);
            if (paciente.tem)
            {
                this.tem = true;
            }
            return msg;
        }


    }




}
