﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaShekinah.Modelo
{
    public class Tipos
    {
        public int Id { get; set; }
        public string nomeTipoIdentidade { get; set; }
        public string nomeTipoRaca{ get; set; }
        public string nomeUnidadeTempo { get; set; }
        public string abreviaturaUnidadeTempo { get; set; }

        public string nomeMunicipio { get; set; }
        public string nomeUF { get; set; }
        public string siglaUF { get; set; }
        public string nomeTipoEscolaridade { get; set; }
        public bool ativo { get; set; }
        public string motivoInativacao { get; set; }


        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;

        public string tudoOuSoAtivo(int opcao)
        {
            string inf;
            if (opcao.Equals(0))
            {
                return inf = "0,1";
            }
            else
            {
                return inf = "1";
            }
        }

        public List<Tipos> getTipoRegistro()
        {
            cmd.CommandText = "select id,nome_tipo_identidade,ativo from tipo_identidade where ativo=1";
            List<Tipos> _TipoDocumentos = new List<Tipos>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _TipoDocumentos.Add(new Tipos()
                    {
                        Id = Convert.ToInt32(dr["id"]),
                        nomeTipoIdentidade = dr["nome_tipo_identidade"].ToString(),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                    });
                }
                dr.Close();
                return _TipoDocumentos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }


        public List<Tipos> getTipoRaca()
        {
            cmd.CommandText = "select id,nome_tipo_raca,ativo from tipo_raca where ativo=1";
            List<Tipos> _TipoRaca = new List<Tipos>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _TipoRaca.Add(new Tipos()
                    {
                        Id = Convert.ToInt32(dr["id"]),
                        nomeTipoRaca = dr["nome_tipo_raca"].ToString(),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                    });
                }
                dr.Close();
                return _TipoRaca;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }

        public List<Tipos> getTipoEscolaridade()
        {
            cmd.CommandText = "select id,nome_tipo_escolaridade,ativo from tipo_escolaridade where ativo=1";
            List<Tipos> _TipoEscolaridade = new List<Tipos>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _TipoEscolaridade.Add(new Tipos()
                    {
                        Id = Convert.ToInt32(dr["id"]),
                        nomeTipoEscolaridade = dr["nome_tipo_escolaridade"].ToString(),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                    });
                }
                dr.Close();
                return _TipoEscolaridade;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }


        public List<Tipos> getUF()
        {
            cmd.CommandText = "select id,sigla_uf,nome_uf,ativo from uf where ativo=1";
            List<Tipos> _UF = new List<Tipos>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _UF.Add(new Tipos()
                    {
                        Id = Convert.ToInt32(dr["id"]),
                        siglaUF = dr["sigla_uf"].ToString(),
                        nomeUF = dr["nome_uf"].ToString(),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                    });
                }
                dr.Close();
                return _UF;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }


        public List<Tipos> getUnidadeTempo()
        {
            cmd.CommandText = "select id,unidade_tempo,abreviatura,ativo from tipo_unidadetempo where ativo=1";
            List<Tipos> _UnidadeTempo = new List<Tipos>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _UnidadeTempo.Add(new Tipos()
                    {
                        Id = Convert.ToInt32(dr["id"]),
                        nomeUnidadeTempo = dr["unidade_tempo"].ToString(),
                        abreviaturaUnidadeTempo = dr["abreviatura"].ToString(),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                    });
                }
                dr.Close();
                return _UnidadeTempo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }

        public List<Tipos> getMunicipio()
        {
            cmd.CommandText = "select id,sigla_uf,municipio,ativo from municipio where ativo=1";
            List<Tipos> _Municipio = new List<Tipos>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _Municipio.Add(new Tipos()
                    {
                        Id = Convert.ToInt32(dr["id"]),
                        siglaUF = dr["sigla_uf"].ToString(),
                        nomeMunicipio = dr["municipio"].ToString(),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                    });
                }
                dr.Close();
                return _Municipio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }

        public List<Tipos> getMunicipio(String siga_uf)
        {
            cmd.CommandText = "select id,sigla_uf,municipio,ativo from municipio where ativo=1 and sigla_uf='"+ siga_uf + "'";
            List<Tipos> _Municipio = new List<Tipos>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _Municipio.Add(new Tipos()
                    {
                        Id = Convert.ToInt32(dr["id"]),
                        siglaUF = dr["sigla_uf"].ToString(),
                        nomeMunicipio = dr["municipio"].ToString(),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                    });
                }
                dr.Close();
                return _Municipio;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }


        public List<Tipos> getTipoMotivoInativacao(int  ativoOuInativo)
        {
            cmd.CommandText = "select id,motivo,ativo from tipo_motivo where  ativo in("+ tudoOuSoAtivo(ativoOuInativo) + ")";
            List<Tipos> _TipoInativacao = new List<Tipos>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _TipoInativacao.Add(new Tipos()
                    {
                        Id = Convert.ToInt32(dr["id"]),
                        motivoInativacao = dr["motivo"].ToString(),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                    });
                }
                dr.Close();
                return _TipoInativacao;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }

    }
}
