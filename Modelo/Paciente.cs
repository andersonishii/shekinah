﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaShekinah.Modelo
{
    public class Paciente
    {
        public int Id { get; set; }
        public String cpf { get; set; }
        public String Nome { get; set; }

        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;
        public List<Paciente> GetPacinteDB()
        {
            cmd.CommandText = "select id,nome,cpf from paciente  ";
            List<Paciente> _pacientes = new List<Paciente>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _pacientes.Add(new Paciente()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Nome = dr["Nome"].ToString(),
                        cpf = dr["cpf"].ToString(),
                    });
                }
                dr.Close();
                return _pacientes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }

        public int getMaiorIdUser()
        {
            int maiorIdObtido ;
            try
            {
                cmd.CommandText = "select max(id) from paciente";
         
                cmd.Connection = con.conectar();
                maiorIdObtido = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                cmd.ExecuteNonQuery();
                con.desconectar();
     
                return maiorIdObtido;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
          
        }

        public List<Paciente> GetPacinteDB(int ativo)
        {
            if (ativo == 1)
            {
                cmd.CommandText = "select id,nome,cpf from paciente where ativo="+ativo+" ";
            }
            else
            {
                cmd.CommandText = "select id,nome,cpf from paciente  ";
            }
        
            List<Paciente> _pacientes = new List<Paciente>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _pacientes.Add(new Paciente()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Nome = dr["Nome"].ToString(),
                        cpf = dr["cpf"].ToString(),
                    });
                }
                dr.Close();
                return _pacientes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }



    }
}



