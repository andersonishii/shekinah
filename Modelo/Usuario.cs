﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaShekinah.Modelo
{
   public class Usuario
    {
        public static string NomeUsuario { get; set; }

        public static bool permiteLancarRetroativamente { get; set; }
        public int Id { get; set; }
        public String cpf { get; set; }
        public String login { get; set; }
        public String codNiveldeUser { get; set; }
        public String senha { get; set; }
        public Boolean permiteLancRetroativo { get; set; }
        public Boolean ativo { get; set; }
        public String userRealName { get; set; }
    

        



        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;

        public int getMaiorIdUser()
        {
            int maiorIdObtido;
            try
            {
                cmd.CommandText = "select max(id) from usuario";

                cmd.Connection = con.conectar();
                maiorIdObtido = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                cmd.ExecuteNonQuery();
                con.desconectar();

                return maiorIdObtido;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }

        }

     

        public bool permiteRetroativo(String userLogin)
        {
            bool permiteLancRetroativo;
            try
            {
                cmd.CommandText = "select permite_lanc_retroativo from usuario where loginUser='"+ userLogin + "'";

                cmd.Connection = con.conectar();
                permiteLancRetroativo = Convert.ToBoolean(cmd.ExecuteScalar().ToString());
                cmd.ExecuteNonQuery();
                con.desconectar();
                return permiteLancRetroativo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }

        }

        public List<Usuario> getUsuariosDb(bool ativo)
        {
        
            cmd.CommandText = "select id,psw,loginUser,codNivelDeUsuario,permite_lanc_retroativo,ativo,nomeUsuario from usuario where ativo ='" + ativo + "' ";
  
            List<Usuario> _users = new List<Usuario>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _users.Add(new Usuario()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        senha = dr["psw"].ToString(),
                        login = dr["loginUser"].ToString(),
                        codNiveldeUser = dr["codNivelDeUsuario"].ToString(),
                        permiteLancRetroativo = Convert.ToBoolean(dr["permite_lanc_retroativo"].ToString()),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                        userRealName = dr["nomeUsuario"].ToString()
                    });
                }
                dr.Close();
                return _users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }

        public List<Usuario> getUsuariosDb()
        {

            cmd.CommandText = "select id,psw,loginUser,codNivelDeUsuario,permite_lanc_retroativo,ativo,nomeUsuario from usuario ";

            List<Usuario> _users = new List<Usuario>();
            try
            {
                cmd.Connection = con.conectar();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _users.Add(new Usuario()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        senha = dr["psw"].ToString(),
                        login = dr["loginUser"].ToString(),
                        codNiveldeUser = dr["codNivelDeUsuario"].ToString(),
                        permiteLancRetroativo = Convert.ToBoolean(dr["permite_lanc_retroativo"].ToString()),
                        ativo = Convert.ToBoolean(dr["ativo"].ToString()),
                        userRealName = dr["nomeUsuario"].ToString()
                    });
                }
                dr.Close();
                return _users;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }
        }

    }


}
