﻿namespace SistemaShekinah.Apresentacao
{
    partial class frmPrincipal
    {
       
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;



        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>

        
    
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblUserCoenctado = new System.Windows.Forms.Label();
            this.picAgendaAnotacoes = new System.Windows.Forms.PictureBox();
            this.picMedicacao = new System.Windows.Forms.PictureBox();
            this.picPaciente = new System.Windows.Forms.PictureBox();
            this.picAtendimento = new System.Windows.Forms.PictureBox();
            this.picPendencias = new System.Windows.Forms.PictureBox();
            this.picAdministracao = new System.Windows.Forms.PictureBox();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.pnlFilho = new System.Windows.Forms.Panel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip4 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip5 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip6 = new System.Windows.Forms.ToolTip(this.components);
            this.picRelatorio = new System.Windows.Forms.PictureBox();
            this.toolTip7 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAgendaAnotacoes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMedicacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAtendimento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPendencias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAdministracao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRelatorio)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(122, 631);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.picAgendaAnotacoes, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.picMedicacao, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.picPaciente, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.picAtendimento, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.picPendencias, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.picAdministracao, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblUserCoenctado, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.picRelatorio, 0, 6);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 34);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.26051F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.73949F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 201F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(122, 597);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // lblUserCoenctado
            // 
            this.lblUserCoenctado.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUserCoenctado.AutoSize = true;
            this.lblUserCoenctado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserCoenctado.Location = new System.Drawing.Point(3, 563);
            this.lblUserCoenctado.Name = "lblUserCoenctado";
            this.lblUserCoenctado.Size = new System.Drawing.Size(58, 13);
            this.lblUserCoenctado.TabIndex = 0;
            this.lblUserCoenctado.Text = "Usuário: ";
            this.lblUserCoenctado.Click += new System.EventHandler(this.lblUserCoenctado_Click);
            // 
            // picAgendaAnotacoes
            // 
            this.picAgendaAnotacoes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.picAgendaAnotacoes.Image = ((System.Drawing.Image)(resources.GetObject("picAgendaAnotacoes.Image")));
            this.picAgendaAnotacoes.Location = new System.Drawing.Point(32, 157);
            this.picAgendaAnotacoes.Name = "picAgendaAnotacoes";
            this.picAgendaAnotacoes.Size = new System.Drawing.Size(58, 40);
            this.picAgendaAnotacoes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picAgendaAnotacoes.TabIndex = 8;
            this.picAgendaAnotacoes.TabStop = false;
            this.picAgendaAnotacoes.Click += new System.EventHandler(this.PictureBox1_Click_1);
            this.picAgendaAnotacoes.MouseClick += new System.Windows.Forms.MouseEventHandler(this.picAgendaAnotacoes_MouseClick);
            this.picAgendaAnotacoes.MouseEnter += new System.EventHandler(this.picAgendaAnotacoes_MouseEnter);
            this.picAgendaAnotacoes.MouseHover += new System.EventHandler(this.picAgendaAnotacoes_MouseHover);
            this.picAgendaAnotacoes.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picAgendaAnotacoes_MouseMove);
            // 
            // picMedicacao
            // 
            this.picMedicacao.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.picMedicacao.Image = ((System.Drawing.Image)(resources.GetObject("picMedicacao.Image")));
            this.picMedicacao.Location = new System.Drawing.Point(32, 102);
            this.picMedicacao.Name = "picMedicacao";
            this.picMedicacao.Size = new System.Drawing.Size(58, 40);
            this.picMedicacao.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMedicacao.TabIndex = 9;
            this.picMedicacao.TabStop = false;
            this.picMedicacao.Click += new System.EventHandler(this.PictureBox3_Click);
            this.picMedicacao.MouseClick += new System.Windows.Forms.MouseEventHandler(this.picMedicacao_MouseClick);
            this.picMedicacao.MouseEnter += new System.EventHandler(this.picMedicacao_MouseEnter);
            this.picMedicacao.MouseHover += new System.EventHandler(this.picMedicacao_MouseHover);
            this.picMedicacao.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picMedicacao_MouseMove);
            // 
            // picPaciente
            // 
            this.picPaciente.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.picPaciente.Image = ((System.Drawing.Image)(resources.GetObject("picPaciente.Image")));
            this.picPaciente.Location = new System.Drawing.Point(32, 54);
            this.picPaciente.Name = "picPaciente";
            this.picPaciente.Size = new System.Drawing.Size(58, 42);
            this.picPaciente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picPaciente.TabIndex = 6;
            this.picPaciente.TabStop = false;
            this.picPaciente.Click += new System.EventHandler(this.pictureBox2_Click);
            this.picPaciente.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox5_MouseMove);
            this.picPaciente.MouseEnter += new System.EventHandler(this.picPaciente_MouseEnter);
            this.picPaciente.MouseHover += new System.EventHandler(this.picPaciente_MouseHover);
            this.picPaciente.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picPaciente_MouseMove);
            // 
            // picAtendimento
            // 
            this.picAtendimento.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.picAtendimento.Image = ((System.Drawing.Image)(resources.GetObject("picAtendimento.Image")));
            this.picAtendimento.Location = new System.Drawing.Point(32, 3);
            this.picAtendimento.Name = "picAtendimento";
            this.picAtendimento.Size = new System.Drawing.Size(58, 45);
            this.picAtendimento.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picAtendimento.TabIndex = 11;
            this.picAtendimento.TabStop = false;
            this.picAtendimento.Click += new System.EventHandler(this.PictureBox5_Click);
            this.picAtendimento.MouseClick += new System.Windows.Forms.MouseEventHandler(this.picAtendimento_MouseClick);
            this.picAtendimento.MouseEnter += new System.EventHandler(this.picAtendimento_MouseEnter);
            this.picAtendimento.MouseHover += new System.EventHandler(this.picAtendimento_MouseHover);
            this.picAtendimento.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox5_MouseMove);
            // 
            // picPendencias
            // 
            this.picPendencias.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.picPendencias.Image = ((System.Drawing.Image)(resources.GetObject("picPendencias.Image")));
            this.picPendencias.Location = new System.Drawing.Point(32, 216);
            this.picPendencias.Name = "picPendencias";
            this.picPendencias.Size = new System.Drawing.Size(58, 45);
            this.picPendencias.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picPendencias.TabIndex = 10;
            this.picPendencias.TabStop = false;
            this.picPendencias.Click += new System.EventHandler(this.PictureBox4_Click);
            this.picPendencias.MouseEnter += new System.EventHandler(this.picPendencias_MouseEnter);
            this.picPendencias.MouseHover += new System.EventHandler(this.picPendencias_MouseHover);
            this.picPendencias.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picPendencias_MouseMove);
            // 
            // picAdministracao
            // 
            this.picAdministracao.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.picAdministracao.Image = ((System.Drawing.Image)(resources.GetObject("picAdministracao.Image")));
            this.picAdministracao.Location = new System.Drawing.Point(32, 273);
            this.picAdministracao.Name = "picAdministracao";
            this.picAdministracao.Size = new System.Drawing.Size(58, 40);
            this.picAdministracao.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picAdministracao.TabIndex = 7;
            this.picAdministracao.TabStop = false;
            this.picAdministracao.Click += new System.EventHandler(this.PictureBox1_Click);
            this.picAdministracao.MouseClick += new System.Windows.Forms.MouseEventHandler(this.picAdministracao_MouseClick);
            this.picAdministracao.MouseEnter += new System.EventHandler(this.picAdministracao_MouseEnter);
            this.picAdministracao.MouseHover += new System.EventHandler(this.picAdministracao_MouseHover);
            this.picAdministracao.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picAdministracao_MouseMove);
            // 
            // pnlFilho
            // 
            this.pnlFilho.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFilho.Location = new System.Drawing.Point(122, 0);
            this.pnlFilho.Name = "pnlFilho";
            this.pnlFilho.Size = new System.Drawing.Size(1066, 631);
            this.pnlFilho.TabIndex = 1;
            this.pnlFilho.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlFilho_Paint);
            // 
            // picRelatorio
            // 
            this.picRelatorio.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.picRelatorio.Image = ((System.Drawing.Image)(resources.GetObject("picRelatorio.Image")));
            this.picRelatorio.Location = new System.Drawing.Point(32, 326);
            this.picRelatorio.Name = "picRelatorio";
            this.picRelatorio.Size = new System.Drawing.Size(58, 40);
            this.picRelatorio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picRelatorio.TabIndex = 12;
            this.picRelatorio.TabStop = false;
            this.picRelatorio.MouseClick += new System.Windows.Forms.MouseEventHandler(this.picRelatorio_MouseClick);
            this.picRelatorio.MouseEnter += new System.EventHandler(this.picRelatorio_MouseEnter);
            this.picRelatorio.MouseHover += new System.EventHandler(this.picRelatorio_MouseHover);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(1188, 631);
            this.Controls.Add(this.pnlFilho);
            this.Controls.Add(this.panel1);
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picAgendaAnotacoes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMedicacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAtendimento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPendencias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAdministracao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRelatorio)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.Panel pnlFilho;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblUserCoenctado;
        private System.Windows.Forms.PictureBox picPaciente;
        private System.Windows.Forms.PictureBox picAdministracao;
        private System.Windows.Forms.PictureBox picAgendaAnotacoes;
        private System.Windows.Forms.PictureBox picMedicacao;
        private System.Windows.Forms.PictureBox picPendencias;
        private System.Windows.Forms.PictureBox picAtendimento;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.ToolTip toolTip4;
        private System.Windows.Forms.ToolTip toolTip5;
        private System.Windows.Forms.ToolTip toolTip6;
        private System.Windows.Forms.PictureBox picRelatorio;
        private System.Windows.Forms.ToolTip toolTip7;
    }
}