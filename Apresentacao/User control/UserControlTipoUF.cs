﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao.User_control
{
    public partial class UserControlTipoUF : UserControl
    {
        Tipos cdb = new Tipos();

        public UserControlTipoUF()
        {
            InitializeComponent();
            cmbUF.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbMunicipio.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void comboBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Selecione a Unidade Federativa. Caso nao saibia, por favor selecione N/D. ", cmbUF);

        }

        public Tipos SelectUF
        {
            get
            {
                return (Tipos)cmbUF.SelectedItem;
            }


        }

        public Tipos SelectMunicipios
        {
            get
            {
                return (Tipos)cmbMunicipio.SelectedItem;
            }


        }

        private void UserControlTipoUF_Load(object sender, EventArgs e)
        {
            List<Tipos> _tipoUF = new List<Tipos>();
            _tipoUF = cdb.getUF();
            foreach (Tipos _tiposReg in _tipoUF)
            {
                cmbUF.DataSource = _tipoUF;
                cmbUF.ValueMember = "id";
                cmbUF.DisplayMember = "siglaUF";
            }

            _tipoUF = cdb.getMunicipio(SelectUF.siglaUF);
            foreach (Tipos _tiposReg in _tipoUF)
            {
                cmbMunicipio.DataSource = _tipoUF;
                cmbMunicipio.ValueMember = "id";
                cmbMunicipio.DisplayMember = "nomeMunicipio";
            }

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void UserControlTipoUF_Leave(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void cmbUF_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Tipos> _tipoUF = new List<Tipos>();
            

            _tipoUF = cdb.getMunicipio(SelectUF.siglaUF);
            foreach (Tipos _tiposReg in _tipoUF)
            {
                cmbMunicipio.DataSource = _tipoUF;
                cmbMunicipio.ValueMember = "id";
                cmbMunicipio.DisplayMember = "nomeMunicipio";
            }
        }

        private void cmbMunicipio_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
