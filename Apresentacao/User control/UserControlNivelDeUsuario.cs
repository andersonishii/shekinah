﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao.User_control
{
    public partial class UserControlNivelDeUsuario : UserControl
    {
        public UserControlNivelDeUsuario()
        {
            InitializeComponent();
            cmbNivelUser.DropDownStyle = ComboBoxStyle.DropDownList;
        }


        NivelUsuario cdb = new NivelUsuario();

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        public NivelUsuario SelectNivelUsuario
        {
            get
            {
                return (NivelUsuario)cmbNivelUser.SelectedItem;

            }
        }

        public int verificaInativo(CheckBox checkboxativo)
        {
            if (checkboxativo.Checked)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        private void chkAtivo_Click(object sender, EventArgs e)
        {

        }

        private void chkAtivo_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkAtivo.Checked)
            {
                MessageBox.Show("Atenção serão listado apenas os niveis de usuários com status ativo!");
                List<NivelUsuario> _nivelUser = new List<NivelUsuario>();
                _nivelUser = cdb.GetNivelUserDB(verificaInativo(chkAtivo));
                foreach (NivelUsuario _pac in _nivelUser)
                {
                    //MessageBox.Show(""+_pac.Nome);
                    cmbNivelUser.DataSource = _nivelUser;
                    cmbNivelUser.ValueMember = "id";
                    cmbNivelUser.DisplayMember = "nomeNivelUsuario";

                    //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
                }
            }
            else
            {
                MessageBox.Show("Atenção serão listado todos  os niveis de Usuarios");
                List<NivelUsuario> _nivelUser = new List<NivelUsuario>(verificaInativo(chkAtivo));
                _nivelUser = cdb.GetNivelUserDB(verificaInativo(chkAtivo));
                foreach (NivelUsuario _pac in _nivelUser)
                {
                    //MessageBox.Show(""+_pac.Nome);
                    cmbNivelUser.DataSource = _nivelUser;
                    cmbNivelUser.ValueMember = "id";
                    cmbNivelUser.DisplayMember = "nomeNivelUsuario";

                    //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
                }

            }
        }

        private void UserControlNivelDeUsuario_Load(object sender, EventArgs e)
        {
            List<NivelUsuario> _nivelUser = new List<NivelUsuario>(verificaInativo(chkAtivo));
            _nivelUser = cdb.GetNivelUserDB(verificaInativo(chkAtivo));
            foreach (NivelUsuario _nUser in _nivelUser)
            {
                //MessageBox.Show(""+_pac.Nome);
                cmbNivelUser.DataSource = _nivelUser;
                cmbNivelUser.ValueMember = "id";
                cmbNivelUser.DisplayMember = "nomeNivelUsuario";

                //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
            }
        }
    }
}
