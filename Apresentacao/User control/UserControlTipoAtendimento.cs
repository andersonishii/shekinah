﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao
{
    public partial class UserControlTipoAtendimento : UserControl
    {
        public UserControlTipoAtendimento()
        {
            InitializeComponent();
            cmbTipoEvolucao.DropDownStyle = ComboBoxStyle.DropDownList;


        }
        public Evolucao SelectTipoEvolucao
        {
            get
            {
                return (Evolucao)cmbTipoEvolucao.SelectedItem;
           
            }
        }

        private void UserControlTipoEvolucao_Load(object sender, EventArgs e)
        {
            //aqui ele carregar os tipos existente
            List<Evolucao> lista = new List<Evolucao>();
            lista.Add(new Evolucao() { ID = 1, descricaoEvolucao = "Evolução"});
            lista.Add(new Evolucao() { ID = 2, descricaoEvolucao = "Recusa de Alimentação" });
            lista.Add(new Evolucao() { ID = 3, descricaoEvolucao = "Recusa de Remedio" });
            lista.Add(new Evolucao() { ID = 4, descricaoEvolucao = "Medicação" });
            lista.Add(new Evolucao() { ID = 5, descricaoEvolucao = "Nutrição" });
            lista.Add(new Evolucao() { ID = 6, descricaoEvolucao = "Serviço Social" });
            //lista.Add(new Evolucao() { ID = 7, descricaoEvolucao = "Entrada do Paciente" });
            //lista.Add(new Evolucao() { ID = 8, descricaoEvolucao = "Saída do Paciente" });
            lista.Add(new Evolucao() { ID = 9, descricaoEvolucao = "Outros" });
            cmbTipoEvolucao.DataSource = lista;
            cmbTipoEvolucao.ValueMember = "ID";
            cmbTipoEvolucao.DisplayMember = "descricaoEvolucao";
        }
       


        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmbTipoEvolucao_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        
        private void cmbTipoEvolucao_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void lblTipoEv_Click(object sender, EventArgs e)
        {

        }
    }
}
