﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao.User_control
{
    public partial class UserControlTipoRaca : UserControl
    {

        Tipos cdb = new Tipos();
        public UserControlTipoRaca()
        {
            InitializeComponent();
            cmbRaca.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void lblRaca_Click(object sender, EventArgs e)
        {

        }

        public Tipos SelectTipoRaca
        {
            get
            {
                return (Tipos)cmbRaca.SelectedItem;
            }
        }

        private void UserControlTipoRaca_Load(object sender, EventArgs e)
        {
            List<Tipos> _tiposRegistros = new List<Tipos>();
            _tiposRegistros = cdb.getTipoRaca();
            foreach (Tipos _tiposReg in _tiposRegistros)
            {
                cmbRaca.DataSource = _tiposRegistros;
                cmbRaca.ValueMember = "id";
                cmbRaca.DisplayMember = "nomeTipoRaca";
            }
        }

        private void cmbRaca_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
