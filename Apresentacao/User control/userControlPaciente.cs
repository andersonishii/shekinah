﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static SistemaShekinah.DAL.PacienteDaoComandos;
using SistemaShekinah.DAL;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao
{
    public partial class userControlPaciente : UserControl
    {
        public userControlPaciente()
        {
            InitializeComponent();
            cmbPaciente.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint_1(object sender, PaintEventArgs e)
        {

        }

        public Paciente SelectPaciente
        {
            get
            {
                return (Paciente)cmbPaciente.SelectedItem;
            }
        }






        Paciente cdb = new Paciente();


        private void userControlPaciente_Load(object sender, EventArgs e)
        {
            
  
            List<Paciente> _paciente = new List<Paciente>(verificaInativo(chkAtivo));
            _paciente = cdb.GetPacinteDB(verificaInativo(chkAtivo));
            foreach (Paciente _pac in _paciente)
            {
                //MessageBox.Show(""+_pac.Nome);
                cmbPaciente.DataSource = _paciente;
                cmbPaciente.ValueMember = "id";
                cmbPaciente.DisplayMember = "Nome";

                //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
            }
        }

        private void cmbPaciente_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public int verificaInativo(CheckBox checkboxativo)
        {
            if (checkboxativo.Checked)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        private void chkAtivo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAtivo.Checked)
            {
                MessageBox.Show("Atenção serão listado apenas pacientes com status ativo!");
                List<Paciente> _paciente = new List<Paciente>();
                _paciente = cdb.GetPacinteDB(verificaInativo(chkAtivo));
                foreach (Paciente _pac in _paciente)
                {
                    //MessageBox.Show(""+_pac.Nome);
                    cmbPaciente.DataSource = _paciente;
                    cmbPaciente.ValueMember = "id";
                    cmbPaciente.DisplayMember = "Nome";

                    //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
                }
            }
            else
            {
                MessageBox.Show("Atenção serão listado todos  pacientes");
                List<Paciente> _paciente = new List<Paciente>(verificaInativo(chkAtivo));
                _paciente = cdb.GetPacinteDB(verificaInativo(chkAtivo));
                foreach (Paciente _pac in _paciente)
                {
                    //MessageBox.Show(""+_pac.Nome);
                    cmbPaciente.DataSource = _paciente;
                    cmbPaciente.ValueMember = "id";
                    cmbPaciente.DisplayMember = "Nome";

                    //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
                }

            }
        }
    }
}
