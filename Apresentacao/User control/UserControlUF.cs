﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao.User_control
{
    public partial class UserControlUF : UserControl
    {
        Tipos cdb = new Tipos();
        public UserControlUF()
        {
            InitializeComponent();
            cmbUF.DropDownStyle = ComboBoxStyle.DropDownList;
        }
        public Tipos SelectTipoRaca
        {
            get
            {
                return (Tipos)cmbUF.SelectedItem;
            }
        }

        private void cmbUF_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Selecione a U.F. caso não sabia , selecione a UF 'S/N' ", cmbUF);
        }

        private void cmbMunicipio_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void UserControlMunicipio_Load(object sender, EventArgs e)
        {
            List<Tipos> _UF = new List<Tipos>();
            _UF = cdb.getUF();
            foreach (Tipos _tiposReg in _UF)
            {
                cmbUF.DataSource = _UF;
                cmbUF.ValueMember = "id";
                cmbUF.DisplayMember = "siglaUF";
            }
        }
    }
}
