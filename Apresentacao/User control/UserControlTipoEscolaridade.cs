﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao.User_control
{
    public partial class UserControlTipoEscolaridade : UserControl
    {
        Tipos cdb = new Tipos();
        public UserControlTipoEscolaridade()
        {
            InitializeComponent();
            cmbTipoEscolaridade.DropDownStyle = ComboBoxStyle.DropDownList;

        }
        public Tipos SelectTipoEscolarodade
        {
            get
            {
                return (Tipos)cmbTipoEscolaridade.SelectedItem;
            }
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void UserControlTipoEscolaridade_Load(object sender, EventArgs e)
        {
            List<Tipos> _tiposRegistros = new List<Tipos>();
            _tiposRegistros = cdb.getTipoEscolaridade();
            foreach (Tipos _tiposReg in _tiposRegistros)
            {
                cmbTipoEscolaridade.DataSource = _tiposRegistros;
                cmbTipoEscolaridade.ValueMember = "id";
                cmbTipoEscolaridade.DisplayMember = "nomeTipoEscolaridade";
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmbTipoEscolaridade_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
