﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao.User_control
{
    public partial class UserControlTipoInativacao : UserControl
    {
        Tipos cdb = new Tipos();
        public UserControlTipoInativacao()
        {
            InitializeComponent();
            cmbMotivo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        public Tipos SelectTipoMotivacao
        {
            get
            {
                return (Tipos)cmbMotivo.SelectedItem;
            }
        }

        private void UserControlTipoInativacao_Load(object sender, EventArgs e)
        {
            List<Tipos> _tipoMotivo = new List<Tipos>();
         //   string verificaoCheckbox = tudoOuSoAtivo(Convert.ToInt32(chkAtivo.Checked));
            _tipoMotivo = cdb.getTipoMotivoInativacao(Convert.ToInt32(tudoOuSoAtivo(Convert.ToInt32(chkAtivo.Checked))));
            foreach (Tipos _tiposMot in _tipoMotivo)
            {
                cmbMotivo.DataSource = _tipoMotivo;
                cmbMotivo.ValueMember = "id";
                cmbMotivo.DisplayMember = "motivoInativacao";
            }
        }

        public string tudoOuSoAtivo(int opcao)
        {
            string inf;
            if (opcao.Equals(0))
            {
                return inf = "0,1";
            }
            else
            {
                return inf = "1";
            }
        }

        public Tipos SelectTipoInativacao
        {
            get
            {
                return (Tipos)cmbMotivo.SelectedItem;
            }


        }

        private void cmbMotivo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void chkAtivo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAtivo.Checked)
            {
                MessageBox.Show("Atenção serão listado apenas registros com status ativo!");
                List<Tipos> _tipoMotivo = new List<Tipos>();
                //   string verificaoCheckbox = tudoOuSoAtivo(Convert.ToInt32(chkAtivo.Checked));
                _tipoMotivo = cdb.getTipoMotivoInativacao(1);
                foreach (Tipos _tiposMot in _tipoMotivo)
                {
                    cmbMotivo.DataSource = _tipoMotivo;
                    cmbMotivo.ValueMember = "id";
                    cmbMotivo.DisplayMember = "nomeTipoIdentidade";
                }
            }
            
            else
            {
                MessageBox.Show("Atenção serão listado todos os  registros (ativos e inativos)");
                List<Tipos> _tipoMotivo = new List<Tipos>();
                //   string verificaoCheckbox = tudoOuSoAtivo(Convert.ToInt32(chkAtivo.Checked));
                _tipoMotivo = cdb.getTipoMotivoInativacao(0);
                foreach (Tipos _tiposMot in _tipoMotivo)
                {
                    cmbMotivo.DataSource = _tipoMotivo;
                    cmbMotivo.ValueMember = "id";
                    cmbMotivo.DisplayMember = "nomeTipoIdentidade";
                }

            }
        }
    }
}
