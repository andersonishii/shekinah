﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao
{
    public partial class UserControlTipoAnexo : UserControl
    {
        public UserControlTipoAnexo()
        {
            InitializeComponent();
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public Anexo SelectTipoEvolucao
        {
            get
            {
                return (Anexo)comboBox1.SelectedItem;

            }
        }

  

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void UserControlTipoAnexo_Load_1(object sender, EventArgs e)
        {
            List<Anexo> listas = new List<Anexo>();
            listas.Add(new Anexo() { ID = 1, descricaoTipoAnexo = "Documento" });
            listas.Add(new Anexo() { ID = 2, descricaoTipoAnexo = "Receita Médica" });
            listas.Add(new Anexo() { ID = 3, descricaoTipoAnexo = "Arquivos" });
            listas.Add(new Anexo() { ID = 4, descricaoTipoAnexo = "Outros" });
            comboBox1.DataSource = listas;
            comboBox1.ValueMember = "ID";
            comboBox1.DisplayMember = "descricaoTipoAnexo";
        }
    }
}
