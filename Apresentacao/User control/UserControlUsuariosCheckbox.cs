﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao.User_control
{
    public partial class UserControlUsuariosCheckbox : UserControl
    {
        Usuario user = new Usuario();
        public UserControlUsuariosCheckbox()
        {
            InitializeComponent();
            adjustListSize(chkUsuarios);
        }

        private static void adjustListSize(ListBox lst)
        {
            int h = lst.ItemHeight * lst.Items.Count;
            lst.Height = h + lst.Height - lst.ClientSize.Height;
        }


        public int verificaInativo(CheckBox checkboxativo)
        {
            if (chkAtivo.Checked)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public Usuario SelectPosicaoUsuario(int i)
        {

            {
                return (Usuario)chkUsuarios.CheckedItems[i];
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void chkUsuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            
           
            
        }

        public string tudoOuSoAtivo(int opcao)
        {
            string inf;
            if (opcao.Equals(0))
            {
                return inf = "0,1";
            }
            else
            {
                return inf = "1";
            }
        }



        private void UserControlUsuariosCheckbox_Load(object sender, EventArgs e)
        {
            
            List<Usuario> _usuario = new List<Usuario>();
            _usuario = user.getUsuariosDb(chkAtivo.Checked);
            foreach (Usuario _pac in _usuario)
            {
                //MessageBox.Show(""+_pac.Nome);
                chkUsuarios.DataSource = _usuario;
                chkUsuarios.ValueMember = "Id";
                chkUsuarios.DisplayMember = "userRealName";

                //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
            }
            
        }

        public void desmcarTodosOsMarcados()
        {
            foreach (int checados in chkUsuarios.CheckedIndices)
            {
                chkUsuarios.SetItemChecked(checados, false);
            }
        }

        public int VerifcaAtivoInativo(bool ativoinativo)
        {
            int opcao;
            ;
            if (ativoinativo == true)
            {
                return opcao = 1;
            }
            else
            {
                return opcao = 0;
            }


        }

        private void chkAtivo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAtivo.Checked)
            {
                MessageBox.Show("Atenção serão listado apenas usuarios com status ativo!");
                List<Usuario> _usuario = new List<Usuario>();
                _usuario = user.getUsuariosDb(chkAtivo.Checked);
                foreach (Usuario _pac in _usuario)
                {
                    //MessageBox.Show(""+_pac.Nome);
                    chkUsuarios.DataSource = _usuario;
                    chkUsuarios.ValueMember = "Id";
                    chkUsuarios.DisplayMember = "userRealName";

                    //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
                }
            }
            else
            {
                MessageBox.Show("Atenção serão listado todos  os usuarios");
                List<Usuario> _usuario = new List<Usuario>();
                _usuario = user.getUsuariosDb();
                foreach (Usuario _pac in _usuario)
                {
                    //MessageBox.Show(""+_pac.Nome);
                    chkUsuarios.DataSource = _usuario;
                    chkUsuarios.ValueMember = "Id";
                    chkUsuarios.DisplayMember = "userRealName";

                    //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
                }
            }
        }
    }
}
