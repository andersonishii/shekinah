﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao.User_control
{
    public partial class UserControlUnidadeDeTempo : UserControl
    {
        Tipos cdb = new Tipos();
        public UserControlUnidadeDeTempo()
        {
            InitializeComponent();
            cmbUnidadeTempo.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public Tipos SelectUnidadeTempo
        {
            get
            {
                return (Tipos)cmbUnidadeTempo.SelectedItem;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void UserControlUnidadeDeTempo_Load(object sender, EventArgs e)
        {
            List<Tipos> _tipoUnidadeTempo = new List<Tipos>();
            _tipoUnidadeTempo = cdb.getUnidadeTempo();
            foreach (Tipos _tiposReg in _tipoUnidadeTempo)
            {
                cmbUnidadeTempo.DataSource = _tipoUnidadeTempo;
                cmbUnidadeTempo.ValueMember = "Id";
                cmbUnidadeTempo.DisplayMember = "nomeUnidadeTempo";
            }

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
