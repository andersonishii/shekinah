﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao.User_control
{
    public partial class UserControlTipodeIdentificacao : UserControl
    {

        Tipos cdb = new Tipos();
        public UserControlTipodeIdentificacao()
        {
            InitializeComponent();
            cmbTipoIdentificacao.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public int verificaInativo(CheckBox checkboxativo)
        {
            if (checkboxativo.Checked)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }


        public Tipos SelectTipoIdentidade
        {
            get
            {
                return (Tipos)cmbTipoIdentificacao.SelectedItem;
            }
        }

        private void UserControlTipodeIdentificacao_Load(object sender, EventArgs e)
        {
            List<Tipos> _tiposRegistros = new List<Tipos>();
            _tiposRegistros = cdb.getTipoRegistro();
            foreach (Tipos _tiposReg in _tiposRegistros)
            {
                cmbTipoIdentificacao.DataSource = _tiposRegistros;
                cmbTipoIdentificacao.ValueMember = "id";
                cmbTipoIdentificacao.DisplayMember = "nomeTipoIdentidade";
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
