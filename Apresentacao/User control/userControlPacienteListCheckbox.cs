﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao
{
    public partial class userControlPacienteListCheckbox : UserControl
    {
        public userControlPacienteListCheckbox()
        {
            InitializeComponent();
            adjustListSize(chkPacientes);

        }

        private static void adjustListSize(ListBox lst)
        {
            int h = lst.ItemHeight * lst.Items.Count;
            lst.Height = h + lst.Height - lst.ClientSize.Height;
        }

        public Paciente SelectPaciente
        {
            get
            {
                return (Paciente)chkPacientes.SelectedItem;
            }
        }

        public Paciente SelectPosicaoPaciente(int i)
        {

            {
                //return (Paciente)chkPessoas.SelectedItem;
                return (Paciente)chkPacientes.CheckedItems[i];
            }
        }





        private void userControlPacienteListCheckbox_Load(object sender, EventArgs e)
        {
            Paciente cdb = new Paciente();
            List<Paciente> _paciente = new List<Paciente>();
            _paciente = cdb.GetPacinteDB(verificaInativo(chkAtivo));
            foreach (Paciente _pac in _paciente)
            {
                //MessageBox.Show(""+_pac.Nome);
                chkPacientes.DataSource = _paciente;
                chkPacientes.ValueMember = "id";
                chkPacientes.DisplayMember = "nome";
           
                 //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
            }
            chkPacientes.Height = chkPacientes.Items.Count * chkPacientes.ItemHeight;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        Paciente cdb = new Paciente();

        public int verificaInativo(CheckBox checkboxativo)
        {
            if (chkAtivo.Checked)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public void desmcarTodosOsMarcados()
        {
            foreach (int checados in chkPacientes.CheckedIndices)
            {
                chkPacientes.SetItemChecked(checados, false);
            }
        }

        private void chkAtivo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAtivo.Checked)
            {
                MessageBox.Show("Atenção serão listado apenas pacientes com status ativo!");
                List<Paciente> _paciente = new List<Paciente>();
                _paciente = cdb.GetPacinteDB(verificaInativo(chkAtivo));
                foreach (Paciente _pac in _paciente)
                {
                    //MessageBox.Show(""+_pac.Nome);
                    chkPacientes.DataSource = _paciente;
                    chkPacientes.ValueMember = "id";
                    chkPacientes.DisplayMember = "Nome";

                    //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
                }
            }
            else
            {
                MessageBox.Show("Atenção serão listado todos  pacientes");
                List<Paciente> _paciente = new List<Paciente>(verificaInativo(chkAtivo));
                _paciente = cdb.GetPacinteDB(verificaInativo(chkAtivo));
                foreach (Paciente _pac in _paciente)
                {
                    //MessageBox.Show(""+_pac.Nome);
                    chkPacientes.DataSource = _paciente;
                    chkPacientes.ValueMember = "id";
                    chkPacientes.DisplayMember = "Nome";

                    //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
                }
            }
        }
    }
}
