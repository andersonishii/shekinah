﻿namespace SistemaShekinah.Apresentacao
{
    partial class UserControlTipoAtendimento
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlTipoAtendimento));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTipoEv = new System.Windows.Forms.Label();
            this.cmbTipoEvolucao = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.lblTipoEv, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cmbTipoEvolucao, 1, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint_1);
            // 
            // lblTipoEv
            // 
            resources.ApplyResources(this.lblTipoEv, "lblTipoEv");
            this.lblTipoEv.Name = "lblTipoEv";
            this.lblTipoEv.Click += new System.EventHandler(this.lblTipoEv_Click);
            // 
            // cmbTipoEvolucao
            // 
            resources.ApplyResources(this.cmbTipoEvolucao, "cmbTipoEvolucao");
            this.cmbTipoEvolucao.FormattingEnabled = true;
            this.cmbTipoEvolucao.Name = "cmbTipoEvolucao";
            this.cmbTipoEvolucao.SelectedIndexChanged += new System.EventHandler(this.cmbTipoEvolucao_SelectedIndexChanged_1);
            // 
            // UserControlTipoAtendimento
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserControlTipoAtendimento";
            this.Load += new System.EventHandler(this.UserControlTipoEvolucao_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblTipoEv;
        public System.Windows.Forms.ComboBox cmbTipoEvolucao;
    }
}
