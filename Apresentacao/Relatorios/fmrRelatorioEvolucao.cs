﻿using Microsoft.Reporting.WinForms;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Relatorios
{
    public partial class fmrRelatorioEvolucao : Form
    {

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        SqlDataReader dr;
        DataTable dt;
        Paciente pc = new Paciente();
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();
        public fmrRelatorioEvolucao()
        {
            InitializeComponent();
        }

        private void fmrRelatorioEvolucao_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            String diaInicial = dtpDataInicial.Value.Date.ToString("dd-MM-yyyy");
            String diaFinal = dtpDataFinal.Value.Date.ToString("dd-MM-yyyy");
            int ativo = Convert.ToInt32(chkAtivo.Checked);

            // "e.tipoevolucao='" + tipoEvolucao + "'  and ( e.datahora between '" + DataEvolucao + " 00:00:00.000' and '" + DataEvolucao + " 23:59:59.999') order by datahora desc", con.conectar());
            if (chkTipoAtendimento.Checked == false)
            {
                SqlCommand cmd = new SqlCommand("select e.id,(select nome from paciente p where p.id=e.paciente ) as paciente,e.datahora,e.tipoevolucao,e.descricaoevolucao from evolucoes e where (case when e.ativo ='1' then '1' when e.ativo ='0' then '0' else '1' end )='" + ativo + "' and ( e.datahora between '" + diaInicial + " 00:00:00.000' and '" + diaFinal + " 23:59:59.999') and  e.paciente=" + userControlPaciente1.SelectPaciente.Id + "", con.conectar());
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapt.Fill(dt);
                ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                string exeFolder = Application.StartupPath;
                string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcRelatorioEvolucao.rdlc");
                //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                reportViewer1.LocalReport.ReportPath = reportPath;
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.DataSources.Add(rds);
                reportViewer1.RefreshReport();
                con.desconectar();
            }
            else
            {
                SqlCommand cmd = new SqlCommand("select e.id,(select nome from paciente p where p.id=e.paciente ) as paciente,e.datahora,e.tipoevolucao,e.descricaoevolucao from evolucoes e where  (case when e.ativo ='1' then '1' when e.ativo ='0' then '0' else '1' end )='" + ativo + "' and e.tipoevolucao='" + cmbTipoAtendimentos.SelectTipoEvolucao.descricaoEvolucao + "' and ( e.datahora between '" + diaInicial + " 00:00:00.000' and '" + diaFinal + " 23:59:59.999') and  e.paciente=" + userControlPaciente1.SelectPaciente.Id + "", con.conectar());
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapt.Fill(dt);
                ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                string exeFolder = Application.StartupPath;
                string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcRelatorioEvolucao.rdlc");
                //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                reportViewer1.LocalReport.ReportPath = reportPath;
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.DataSources.Add(rds);
                reportViewer1.RefreshReport();
                con.desconectar();
            }
          
        }

        private void chkTipoAtendimento_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkTipoAtendimento.Checked)
            {
                cmbTipoAtendimentos.Visible = true;
                MessageBox.Show("Atenção. Habilitando essa opção o relatório também filtrará por tipo de Evolução");
            }
            else
            {
                cmbTipoAtendimentos.Visible = false;
            }
        }

        private void chkAtivo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAtivo.Checked)
            {
                MessageBox.Show("Atenção serão listados apenas as Evoluções Ativos !");
            }
            else
            {
                MessageBox.Show("Atenção serão listados apenas as  Evoluções Inativos !");
            }
        }
    }
}
