﻿using Microsoft.Reporting.WinForms;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Relatorios
{
    public partial class fmrRelatorioAvaliacao : Form
    {
        public fmrRelatorioAvaliacao()
        {
            InitializeComponent();
        }


        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        SqlDataReader dr;
        DataTable dt;
        Paciente pc = new Paciente();
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();


        public string UsuarioSelecionado()
        {
            int maiorIdDb = pc.getMaiorIdUser() + 1;
            string idPacientes = string.Empty;
            if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 0)
            {

                for (int i = 0; i < maiorIdDb; i++)
                {

                    idPacientes += i + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;
                /*
      
                */
            }
            else
            {
                for (int i = 0; i < userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count; i++)
                {

                    idPacientes += userControlPacienteListCheckbox1.SelectPosicaoPaciente(i).Id + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;

            }

        }




        private void fmrRelatorioAvaliacao_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
            this.reportViewer1.RefreshReport();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            String dataInicial = dtpDataInicial.Value.Date.ToString("dd-MM-yyyy");
            String dataFinal = dtpDataFinal.Value.Date.ToString("dd-MM-yyyy");
            int ativo = Convert.ToInt32(chkAtivo.Checked);
            try
            {
                SqlCommand cmd = new SqlCommand("select id,(select nome from paciente p where p.id=a.paciente) " +
                    "as paciente,datahora,peso,altura,sistolica,diastolica,bpm,rpm,sp02,grau,obs,dor,hgt from avaliacao a where (case when a.ativo ='1' then '1' when a.ativo ='0' then '0' else '1' end )='" + ativo + "' and   a.paciente in(" + UsuarioSelecionado()+ ") and (a.datahora between '" + dataInicial + " 00:00:00.0000000' and '" + dataFinal + " 23:59:59.0000000')", con.conectar());
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapt.Fill(dt);
                ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                string exeFolder = Application.StartupPath;
                string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcRelatorioAvaliacao.rdlc");
                //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                reportViewer1.LocalReport.ReportPath = reportPath;
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.DataSources.Add(rds);
                reportViewer1.RefreshReport();
                con.desconectar();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Erro em " + ex);
            }

        }

        private void chkAtivo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAtivo.Checked)
            {
                MessageBox.Show("Atenção serão listados apenas as Avaliações Ativos !");
            }
            else
            {
                MessageBox.Show("Atenção serão listados apenas as  Avaliações Inativos !");
            }
        }
    }
}
