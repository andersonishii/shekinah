﻿using System;
using System.Collections.Generic;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Relatorios
{
    public partial class fmrRelatorioInfoComplementarPaciente : Form
    {
        public fmrRelatorioInfoComplementarPaciente()
        {
            InitializeComponent();
        }

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        SqlDataReader dr;
        DataTable dt;


        private void fmrRelatorioInfoComplementarPaciente_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        string sqlQuery = "SELECT id,ativo,nome,composicao_familiar, obs_composicao_familiar, nome_plano_saude, carencia_plano_cirurgia,unidadetempo_carencia_plano_cirurgia, carencia_plano_consulta,unidadetempo_carencia_plano_consulta, carencia_plano_outros,unidadetempo_carencia_plano_outros, obs_plano_saude, necessidades_especiais, obs_necessidades_especiais, restricao_alimentares, obs_restricao_alimentares, alergias, obs_alergias FROM dbo.paciente";


        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("" + sqlQuery + " where id =" + userControlPaciente1.SelectPaciente.Id + "", con.conectar());
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapt.Fill(dt);
                ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                string exeFolder = Application.StartupPath;
                string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcInfoCompPaciente.rdlc");
                //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                reportViewer1.LocalReport.ReportPath = reportPath;
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.DataSources.Add(rds);
                reportViewer1.RefreshReport();
                con.desconectar();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Erro em " + ex);
            }
        }
    }
}
