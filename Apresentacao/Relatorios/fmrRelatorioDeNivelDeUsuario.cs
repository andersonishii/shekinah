﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Relatorios
{
    public partial class fmrRelatorioDeNivelDeUsuario : Form
    {
        public fmrRelatorioDeNivelDeUsuario()
        {
            InitializeComponent();
        }

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        SqlDataReader dr;
        DataTable dt;
  

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void fmrRelatorioDeNivelDeUsuario_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT id,ativo,nome_Nivel ,cadastro_paciente,anexo_paciente,inf_comp_paciente ,atividade_grupo,agenda,entrada_saida_paciente ,atendimento,avaliacao,medicacao,posologia,cadastro_usuario,nivel_usuario,foto_paciente FROM nivel_usuario", con.conectar());
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapt.Fill(dt);
                ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                string exeFolder = Application.StartupPath;
                string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcRelatorioNivelDeUsuario.rdlc");
                //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                reportViewer1.LocalReport.ReportPath = reportPath;
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.DataSources.Add(rds);
                reportViewer1.RefreshReport();
                con.desconectar();

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Erro em " + ex);
            }
        }
    }
}
