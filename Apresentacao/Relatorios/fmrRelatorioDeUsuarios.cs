﻿using Microsoft.Reporting.WinForms;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Relatorios
{
    public partial class fmrRelatorioDeUsuarios : Form
    {
        public fmrRelatorioDeUsuarios()
        {
            InitializeComponent();
        }

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        SqlDataReader dr;
        DataTable dt;
        Paciente pc = new Paciente();
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();

        public string UsuarioSelecionado()
        {
            int maiorIdDb = user.getMaiorIdUser() + 1;
            string idUsers = string.Empty;
            if (userControlUsuariosCheckbox1.chkUsuarios.CheckedItems.Count == 0)
            {

                for (int i = 0; i < maiorIdDb; i++)
                {

                    idUsers += i + ",";
                }
                //gambis para concaternar
                idUsers += maiorIdDb;
                return idUsers;
                /*
      
                */
            }
            else
            {
                for (int i = 0; i < userControlUsuariosCheckbox1.chkUsuarios.CheckedItems.Count; i++)
                {

                    idUsers += userControlUsuariosCheckbox1.SelectPosicaoUsuario(i).Id + ",";
                }
                //gambis para concaternar
                idUsers += maiorIdDb;
                return idUsers;

            }

        }

        private void fmrRelatorioDeUsuarios_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT ID,loginUser,codNivelDeUsuario ,permite_lanc_retroativo ,ativo,nomeUsuario FROM usuario where id in(" + UsuarioSelecionado() + " )", con.conectar());
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapt.Fill(dt);
                ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                string exeFolder = Application.StartupPath;
                string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcCadastrodeUsuario.rdlc");
                //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                reportViewer1.LocalReport.ReportPath = reportPath;
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.DataSources.Add(rds);
                reportViewer1.RefreshReport();
                con.desconectar();

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Erro em " + ex);
            }
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void userControlUsuariosCheckbox1_Load(object sender, EventArgs e)
        {

        }
    }
}
