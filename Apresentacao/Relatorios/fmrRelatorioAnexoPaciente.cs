﻿using Microsoft.Reporting.WinForms;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Relatorios
{
    public partial class fmrRelatorioAnexoPaciente : Form
    {
        public fmrRelatorioAnexoPaciente()
        {
            InitializeComponent();
        }

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;

        SqlDataReader dr;
        DataTable dt;
        Paciente pc = new Paciente();
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();

        private void fmrRelatorioAnexoPaciente_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }


        public string UsuarioSelecionado()
        {
            int maiorIdDb = pc.getMaiorIdUser() + 1;
            string idPacientes = string.Empty;
            if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 0)
            {

                for (int i = 0; i < maiorIdDb; i++)
                {

                    idPacientes += i + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;
                /*
      
                */
            }
            else
            {
                for (int i = 0; i < userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count; i++)
                {

                    idPacientes += userControlPacienteListCheckbox1.SelectPosicaoPaciente(i).Id + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;

            }

        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            { 
                String diaInicial = dtpInicio.Value.Date.ToString("dd-MM-yyyy");
                String diaFinal = dtpFim.Value.Date.ToString("dd-MM-yyyy");
                SqlCommand cmd = new SqlCommand("SELECT id,(select p.nome from paciente p where p.id=paciente) as paciente,tipoanexo,datahora,origem,destino,obs,tag,ativo FROM dbo.anexo_paciente where paciente in(" + UsuarioSelecionado() + " ) and  (datahora between '" + diaInicial + " 00:00:00.0000000' and '" + diaFinal + " 23:59:59.0000000')", con.conectar());
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adapt.Fill(dt);
                ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                string exeFolder = Application.StartupPath;
                string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcRelatorioAnexoDoPaciente.rdlc");
                //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                reportViewer1.LocalReport.ReportPath = reportPath;
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.EnableHyperlinks =true;
                reportViewer1.LocalReport.DataSources.Add(rds);
                reportViewer1.RefreshReport();
                con.desconectar();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Erro em " + ex);
            }
        }

        private void dtpFim_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
