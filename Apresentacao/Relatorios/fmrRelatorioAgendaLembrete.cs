﻿using Microsoft.Reporting.WinForms;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Relatorios
{
    public partial class fmrRelatorioAgendaLembrete : Form
    {
        public fmrRelatorioAgendaLembrete()
        {
            InitializeComponent();
        }

        SqlCommand cmd;
        SqlDataAdapter adpt;
        SqlDataReader dr;
        DataTable dt;
        Paciente pc = new Paciente();
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();
        Conexao con = new Conexao();

        public string UsuarioSelecionado()
        {
            int maiorIdDb = pc.getMaiorIdUser() + 1;
            string idPacientes = string.Empty;
            if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 0)
            {
                for (int i = 0; i < maiorIdDb; i++)
                {
                    idPacientes += i + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;
            }
            else
            {
                for (int i = 0; i < userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count; i++)
                {
                    idPacientes += userControlPacienteListCheckbox1.SelectPosicaoPaciente(i).Id + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;

            }

        }

        private void fmrRelatorioAgendaLembrete_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkFiltrarCor.Checked)
                {
                    String diaInicial = dtpDataInicial.Value.Date.ToString("yyyy-MM-dd");
                    String diaFinal = dtpDataFinal.Value.Date.ToString("yyyy-MM-dd");
                    SqlCommand cmd = new SqlCommand("SELECT id,(select p.nome from paciente p where p.id=paciente) as paciente ,datahora_inicial,datahora_final,titulo,informacao_compromisso_agenda,cor,tipo_tarefa,dia_todo ,ativo " +
                         "FROM dbo.agenda_compromisso where tipo_tarefa='Lembrete' and ativo='" + chkAtivo.Checked + "' and paciente in(" + UsuarioSelecionado() + ")  and (datahora_inicial between '" + diaInicial + " 00:00:00.0000000' and '" + diaFinal + " 23:59:59.0000000') and cor='" + colorDialog1.Color.ToString() + "' ", con.conectar());

                    SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapt.Fill(dt);
                    ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                    string exeFolder = Application.StartupPath;
                    string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcAgendaLembrete.rdlc");
                    //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                    reportViewer1.LocalReport.ReportPath = reportPath;
                    reportViewer1.LocalReport.DataSources.Clear();
                    reportViewer1.LocalReport.DataSources.Add(rds);
                    reportViewer1.RefreshReport();
                    con.desconectar();
                }
                else
                {
                    String diaInicial = dtpDataInicial.Value.Date.ToString("yyyy-MM-dd");
                    String diaFinal = dtpDataFinal.Value.Date.ToString("yyyy-MM-dd");
                    SqlCommand cmd = new SqlCommand("SELECT id,(select p.nome from paciente p where p.id=paciente) as paciente ,datahora_inicial,datahora_final,titulo,informacao_compromisso_agenda,cor,tipo_tarefa,dia_todo ,ativo " +
                         "FROM dbo.agenda_compromisso where tipo_tarefa='Lembrete' and ativo='" + chkAtivo.Checked + "' and paciente in(" + UsuarioSelecionado() + ")  and (datahora_inicial between '" + diaInicial + " 00:00:00.0000000' and '" + diaFinal + " 23:59:59.0000000') ", con.conectar());

                    SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapt.Fill(dt);
                    ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                    string exeFolder = Application.StartupPath;
                    string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcAgendaLembrete.rdlc");
                    //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                    reportViewer1.LocalReport.ReportPath = reportPath;
                    reportViewer1.LocalReport.DataSources.Clear();
                    reportViewer1.LocalReport.DataSources.Add(rds);
                    reportViewer1.RefreshReport();
                    con.desconectar();

                }
             
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Erro em " + ex);
            }
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                btnCor.BackColor = colorDialog1.Color;
            }
        }

        private void chkFiltrarCor_CheckedChanged(object sender, EventArgs e)
        {

            if (chkFiltrarCor.Checked)
            {
                btnCor.Enabled = true;
                btnCor.Visible = true;
                lblCor.Visible = true;
            }
            else
            {
                btnCor.Enabled = false;
                btnCor.Visible = false;
                lblCor.Visible = false;
                btnCor.BackColor = Color.FromName("DarkGray");
                colorDialog1.Color = Color.FromArgb(255, 128, 128, 64);

            }
        }
    }
}
