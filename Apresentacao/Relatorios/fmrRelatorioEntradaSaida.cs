﻿using Microsoft.Reporting.WinForms;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Relatorios
{
    public partial class fmrRelatorioEntradaSaida : Form
    {
        public fmrRelatorioEntradaSaida()
        {
            InitializeComponent();
        }

        private void fmrRelatorioEntradaSaida_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }



        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        NivelUsuario nivelUser = new NivelUsuario();
        Paciente pc = new Paciente();


        private void btnListar_Click(object sender, EventArgs e)
        {

            try
            {
                String data = dtpDataInicial.Value.Date.ToString("yyyy-MM-dd");
                if (checkBox1.Checked)
                {
                    SqlCommand cmd = new SqlCommand(" select es.id, (select nome from paciente pc where pc.id = es.paciente) as paciente,es.datahora,es.descricao_saida,es.ativo_inativo,retorno_concluido as Retornou from entrada_saida as es where (datahora between '" + data + " 00:00:00.7733333' and '" + data + " 23:59:59.7733333' ) and paciente in(" + userControlPaciente1.SelectPaciente.Id + ") ", con.conectar());
                    SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapt.Fill(dt);
                    ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                    string exeFolder = Application.StartupPath;
                    string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcEntradaSaidaPacientes.rdlc");
                    //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                    reportViewer1.LocalReport.ReportPath = reportPath;
                    reportViewer1.LocalReport.DataSources.Clear();
                    reportViewer1.LocalReport.DataSources.Add(rds);
                    reportViewer1.RefreshReport();
                    con.desconectar();
                }
                else
                {
                    SqlCommand cmd = new SqlCommand(" select es.id, (select nome from paciente pc where pc.id = es.paciente) as paciente,es.datahora,es.descricao_saida,es.ativo_inativo,retorno_concluido as Retornou from entrada_saida as es where (datahora between '" + data + " 00:00:00.7733333' and '" + data + " 23:59:59.7733333' ) and paciente in(" + userControlPaciente1.SelectPaciente.Id + ") and retorno_concluido is null ", con.conectar());
                    SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapt.Fill(dt);
                    ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                    string exeFolder = Application.StartupPath;
                    string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcEntradaSaidaPacientes.rdlc");
                    //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                    reportViewer1.LocalReport.ReportPath = reportPath;
                    reportViewer1.LocalReport.DataSources.Clear();
                    reportViewer1.LocalReport.DataSources.Add(rds);
                    reportViewer1.RefreshReport();
                    con.desconectar();

                }

               
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Erro em " + ex);
            }
           
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
