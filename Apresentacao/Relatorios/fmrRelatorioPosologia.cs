﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Relatorios
{
    public partial class fmrRelatorioPosologia : Form
    {

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        SqlDataReader dr;
        DataTable dt;
        public fmrRelatorioPosologia()
        {
            InitializeComponent();
        }

        private void fmrRelatorioPosologia_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            String dataInicial = dtpDataInicial.Value.Date.ToString("dd-MM-yyyy");
            String dataFinal = dtpDataFinal.Value.Date.ToString("dd-MM-yyyy");
            int ativo = Convert.ToInt32(chkAtivo.Checked);
            string valorativo = "";

            try
            {
                if (ativo == 1)
                {
                    MessageBox.Show("Atenção . A listagem está exibindo apenas registros Ativos");
                    SqlCommand cmd = new SqlCommand("select (select nome from paciente p where p.id=pp.paciente) as paciente,pp.data_pendencia,(select nome_quimico from medicacoes m where m.id=pp.id_medicacao)as nome_medicacao,(select nome_comercial from medicacoes m where m.id=pp.id_medicacao)as id_medicacao,pp.posologia,pp.observacao,pp.ehultimoregistro as  ultimoRegistro,pp.ativo from posologia pp where pp.ativo=1 and  pp.paciente in(" + userControlPaciente1.SelectPaciente.Id + ") and (pp.data_pendencia between '" + dataInicial + " 00:00:00.0000000' and '" + dataFinal + " 23:59:59.0000000')", con.conectar());
                    SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapt.Fill(dt);
                    ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                    string exeFolder = Application.StartupPath;
                    string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcPosologia.rdlc");
                    //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                    reportViewer1.LocalReport.ReportPath = reportPath;
                    reportViewer1.LocalReport.DataSources.Clear();
                    reportViewer1.LocalReport.DataSources.Add(rds);
                    reportViewer1.RefreshReport();
                    con.desconectar();
                }
                else
                {
                    MessageBox.Show("Atenção . A listagem está exibindo apenas registros ativos e inativo");
                    SqlCommand cmd = new SqlCommand("select (select nome from paciente p where p.id=pp.paciente) as paciente,pp.data_pendencia,(select nome_quimico from medicacoes m where m.id=pp.id_medicacao)as nome_medicacao,(select nome_comercial from medicacoes m where m.id=pp.id_medicacao)as id_medicacao,pp.posologia,pp.observacao,pp.ehultimoregistro as  ultimoRegistro,pp.ativo from posologia pp where  pp.paciente in(" + userControlPaciente1.SelectPaciente.Id + ") and (pp.data_pendencia between '" + dataInicial + " 00:00:00.0000000' and '" + dataFinal + " 23:59:59.0000000')", con.conectar());
                    SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    adapt.Fill(dt);
                    ReportDataSource rds = new ReportDataSource("DataSet1", dt);
                    string exeFolder = Application.StartupPath;
                    string reportPath = Path.Combine(exeFolder, @"Apresentacao/Relatorios/rdlcPosologia.rdlc");
                    //string reportPath = Path.Combine(exeFolder, @"Reports\report.rdlc");
                    reportViewer1.LocalReport.ReportPath = reportPath;
                    reportViewer1.LocalReport.DataSources.Clear();
                    reportViewer1.LocalReport.DataSources.Add(rds);
                    reportViewer1.RefreshReport();
                    con.desconectar();
                }
         
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Erro em " + ex);
            }
        }
    }
}
