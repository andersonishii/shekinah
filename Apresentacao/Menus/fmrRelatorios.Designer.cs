﻿namespace SistemaShekinah.Apresentacao.Menus
{
    partial class fmrRelatorios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fmrRelatorios));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.cadastraisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pacienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuáriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atendimentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evoluçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avaliaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medicaçãoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeMedicaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuáriosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nívelDeUsuarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuáriosToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pacienteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.anexoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDePacienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informaçãoComplementarDoPacienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agendaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.agendaLembreteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.entadaSaidaDePacientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlRelatorio = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.posologiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.pnlRelatorio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.menuStrip1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1226, 52);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastraisToolStripMenuItem,
            this.atendimentoToolStripMenuItem,
            this.medicaçãoToolStripMenuItem1,
            this.usuáriosToolStripMenuItem1,
            this.pacienteToolStripMenuItem1,
            this.agendaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(1226, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // cadastraisToolStripMenuItem
            // 
            this.cadastraisToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pacienteToolStripMenuItem,
            this.usuáriosToolStripMenuItem});
            this.cadastraisToolStripMenuItem.Name = "cadastraisToolStripMenuItem";
            this.cadastraisToolStripMenuItem.Size = new System.Drawing.Size(73, 19);
            this.cadastraisToolStripMenuItem.Text = "Cadastrais";
            // 
            // pacienteToolStripMenuItem
            // 
            this.pacienteToolStripMenuItem.Name = "pacienteToolStripMenuItem";
            this.pacienteToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.pacienteToolStripMenuItem.Text = "Paciente";
            this.pacienteToolStripMenuItem.Click += new System.EventHandler(this.pacienteToolStripMenuItem_Click);
            // 
            // usuáriosToolStripMenuItem
            // 
            this.usuáriosToolStripMenuItem.Name = "usuáriosToolStripMenuItem";
            this.usuáriosToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.usuáriosToolStripMenuItem.Text = "Usuários";
            this.usuáriosToolStripMenuItem.Click += new System.EventHandler(this.usuáriosToolStripMenuItem_Click);
            // 
            // atendimentoToolStripMenuItem
            // 
            this.atendimentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evoluçãoToolStripMenuItem,
            this.avaliaçãoToolStripMenuItem});
            this.atendimentoToolStripMenuItem.Name = "atendimentoToolStripMenuItem";
            this.atendimentoToolStripMenuItem.Size = new System.Drawing.Size(89, 19);
            this.atendimentoToolStripMenuItem.Text = "Atendimento";
            // 
            // evoluçãoToolStripMenuItem
            // 
            this.evoluçãoToolStripMenuItem.Name = "evoluçãoToolStripMenuItem";
            this.evoluçãoToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.evoluçãoToolStripMenuItem.Text = "Evolução";
            this.evoluçãoToolStripMenuItem.Click += new System.EventHandler(this.evoluçãoToolStripMenuItem_Click);
            // 
            // avaliaçãoToolStripMenuItem
            // 
            this.avaliaçãoToolStripMenuItem.Name = "avaliaçãoToolStripMenuItem";
            this.avaliaçãoToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.avaliaçãoToolStripMenuItem.Text = "Avaliação";
            this.avaliaçãoToolStripMenuItem.Click += new System.EventHandler(this.avaliaçãoToolStripMenuItem_Click);
            // 
            // medicaçãoToolStripMenuItem1
            // 
            this.medicaçãoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroDeMedicaçãoToolStripMenuItem,
            this.posologiaToolStripMenuItem});
            this.medicaçãoToolStripMenuItem1.Name = "medicaçãoToolStripMenuItem1";
            this.medicaçãoToolStripMenuItem1.Size = new System.Drawing.Size(77, 19);
            this.medicaçãoToolStripMenuItem1.Text = "Medicação";
            // 
            // cadastroDeMedicaçãoToolStripMenuItem
            // 
            this.cadastroDeMedicaçãoToolStripMenuItem.Name = "cadastroDeMedicaçãoToolStripMenuItem";
            this.cadastroDeMedicaçãoToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.cadastroDeMedicaçãoToolStripMenuItem.Text = "Cadastro de Medicação";
            this.cadastroDeMedicaçãoToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeMedicaçãoToolStripMenuItem_Click);
            // 
            // usuáriosToolStripMenuItem1
            // 
            this.usuáriosToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nívelDeUsuarioToolStripMenuItem,
            this.usuáriosToolStripMenuItem2});
            this.usuáriosToolStripMenuItem1.Name = "usuáriosToolStripMenuItem1";
            this.usuáriosToolStripMenuItem1.Size = new System.Drawing.Size(64, 19);
            this.usuáriosToolStripMenuItem1.Text = "Usuários";
            // 
            // nívelDeUsuarioToolStripMenuItem
            // 
            this.nívelDeUsuarioToolStripMenuItem.Name = "nívelDeUsuarioToolStripMenuItem";
            this.nívelDeUsuarioToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.nívelDeUsuarioToolStripMenuItem.Text = "Nível de Usuario";
            this.nívelDeUsuarioToolStripMenuItem.Click += new System.EventHandler(this.nívelDeUsuarioToolStripMenuItem_Click);
            // 
            // usuáriosToolStripMenuItem2
            // 
            this.usuáriosToolStripMenuItem2.Name = "usuáriosToolStripMenuItem2";
            this.usuáriosToolStripMenuItem2.Size = new System.Drawing.Size(160, 22);
            this.usuáriosToolStripMenuItem2.Text = "Usuários";
            this.usuáriosToolStripMenuItem2.Click += new System.EventHandler(this.usuáriosToolStripMenuItem2_Click);
            // 
            // pacienteToolStripMenuItem1
            // 
            this.pacienteToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.anexoToolStripMenuItem,
            this.cadastroDePacienteToolStripMenuItem,
            this.informaçãoComplementarDoPacienteToolStripMenuItem});
            this.pacienteToolStripMenuItem1.Name = "pacienteToolStripMenuItem1";
            this.pacienteToolStripMenuItem1.Size = new System.Drawing.Size(64, 19);
            this.pacienteToolStripMenuItem1.Text = "Paciente";
            // 
            // anexoToolStripMenuItem
            // 
            this.anexoToolStripMenuItem.Name = "anexoToolStripMenuItem";
            this.anexoToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.anexoToolStripMenuItem.Text = "Anexo";
            this.anexoToolStripMenuItem.Click += new System.EventHandler(this.anexoToolStripMenuItem_Click);
            // 
            // cadastroDePacienteToolStripMenuItem
            // 
            this.cadastroDePacienteToolStripMenuItem.Name = "cadastroDePacienteToolStripMenuItem";
            this.cadastroDePacienteToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.cadastroDePacienteToolStripMenuItem.Text = "Cadastro de Paciente";
            this.cadastroDePacienteToolStripMenuItem.Click += new System.EventHandler(this.cadastroDePacienteToolStripMenuItem_Click);
            // 
            // informaçãoComplementarDoPacienteToolStripMenuItem
            // 
            this.informaçãoComplementarDoPacienteToolStripMenuItem.Name = "informaçãoComplementarDoPacienteToolStripMenuItem";
            this.informaçãoComplementarDoPacienteToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.informaçãoComplementarDoPacienteToolStripMenuItem.Text = "Informação Complementar do Paciente";
            this.informaçãoComplementarDoPacienteToolStripMenuItem.Click += new System.EventHandler(this.informaçãoComplementarDoPacienteToolStripMenuItem_Click);
            // 
            // agendaToolStripMenuItem
            // 
            this.agendaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.agendaLembreteToolStripMenuItem,
            this.entadaSaidaDePacientesToolStripMenuItem});
            this.agendaToolStripMenuItem.Name = "agendaToolStripMenuItem";
            this.agendaToolStripMenuItem.Size = new System.Drawing.Size(121, 19);
            this.agendaToolStripMenuItem.Text = "Agenda/Anotações";
            // 
            // agendaLembreteToolStripMenuItem
            // 
            this.agendaLembreteToolStripMenuItem.Name = "agendaLembreteToolStripMenuItem";
            this.agendaLembreteToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.agendaLembreteToolStripMenuItem.Text = "Agenda(Lembrete)";
            this.agendaLembreteToolStripMenuItem.Click += new System.EventHandler(this.agendaLembreteToolStripMenuItem_Click);
            // 
            // entadaSaidaDePacientesToolStripMenuItem
            // 
            this.entadaSaidaDePacientesToolStripMenuItem.Name = "entadaSaidaDePacientesToolStripMenuItem";
            this.entadaSaidaDePacientesToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.entadaSaidaDePacientesToolStripMenuItem.Text = "Entada/Saida de Pacientes";
            this.entadaSaidaDePacientesToolStripMenuItem.Click += new System.EventHandler(this.entadaSaidaDePacientesToolStripMenuItem_Click);
            // 
            // pnlRelatorio
            // 
            this.pnlRelatorio.ColumnCount = 1;
            this.pnlRelatorio.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlRelatorio.Controls.Add(this.pictureBox1, 0, 0);
            this.pnlRelatorio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRelatorio.Location = new System.Drawing.Point(0, 52);
            this.pnlRelatorio.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlRelatorio.Name = "pnlRelatorio";
            this.pnlRelatorio.RowCount = 1;
            this.pnlRelatorio.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlRelatorio.Size = new System.Drawing.Size(1226, 650);
            this.pnlRelatorio.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(458, 169);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(310, 312);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // posologiaToolStripMenuItem
            // 
            this.posologiaToolStripMenuItem.Name = "posologiaToolStripMenuItem";
            this.posologiaToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.posologiaToolStripMenuItem.Text = "Posologia";
            this.posologiaToolStripMenuItem.Click += new System.EventHandler(this.posologiaToolStripMenuItem_Click);
            // 
            // fmrRelatorios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(1226, 702);
            this.Controls.Add(this.pnlRelatorio);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "fmrRelatorios";
            this.Text = "Relatórios";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.fmrRelatorios_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlRelatorio.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel pnlRelatorio;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cadastraisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atendimentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evoluçãoToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem usuáriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medicaçãoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeMedicaçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuáriosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nívelDeUsuarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuáriosToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem avaliaçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pacienteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem anexoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agendaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDePacienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem agendaLembreteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informaçãoComplementarDoPacienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem entadaSaidaDePacientesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem posologiaToolStripMenuItem;
    }
}