﻿using SistemaShekinah.Apresentacao.Listagem;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Menus
{
    public partial class fmrAgendaAnotacoes : Form
    {
        public fmrAgendaAnotacoes()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
        }

        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();

        private void AbrirFormFilho(object formFilho)
        {
            if (this.pnlAgendaAnotacao.Controls.Count > 0)
                this.pnlAgendaAnotacao.Controls.RemoveAt(0);
            Form fh = formFilho as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pnlAgendaAnotacao.Controls.Add(fh);
            this.pnlAgendaAnotacao.Tag = fh;
            fh.Show();

        }

        private void btnAgenda_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("agenda", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                AbrirFormFilho(new fmrListagemAgenda());
                btnEntradaSaida.BackColor = Color.FromName("Control");
                btnAgenda.BackColor = Color.FromArgb(134, 132, 132);
            }
       
        }

        private void pnlAgendaAnotacao_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnAvalicao_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("entrada_saida_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                AbrirFormFilho(new fmrListagemEntradaSaida());
                btnEntradaSaida.BackColor = Color.FromName("Control");
                btnAgenda.BackColor = Color.FromArgb(134, 132, 132);
            }

        }
    }
}
