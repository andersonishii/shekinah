﻿using SistemaShekinah.Apresentacao.Listagem;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao
{
    public partial class fmrMedicacao : Form
    {
        public fmrMedicacao()
        {
            InitializeComponent();
        }
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();

        private void AbrirFormFilho(object formFilho)
        {
            if (this.pnlMedicacao.Controls.Count > 0)
                this.pnlMedicacao.Controls.RemoveAt(0);
            Form fh = formFilho as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pnlMedicacao.Controls.Add(fh);
            this.pnlMedicacao.Tag = fh;
            fh.Show();

        }

        private void btnMedicacao_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("medicacao", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                AbrirFormFilho(new fmrListagemCadastroMedicacoes());
            }
          
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("posologia", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                AbrirFormFilho(new fmrListagemPosologia());
            }
          
        }

        private void PnlMedicacao_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
