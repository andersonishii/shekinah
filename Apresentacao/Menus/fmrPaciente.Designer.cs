﻿namespace SistemaShekinah.Apresentacao
{
    partial class fmrPaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fmrPaciente));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlPaciente = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnInfComplementarPac = new System.Windows.Forms.Button();
            this.btnCadastroPaciente = new System.Windows.Forms.Button();
            this.btnAnexosPaciente = new System.Windows.Forms.Button();
            this.btnAtividadeEmGrupo = new System.Windows.Forms.Button();
            this.btnFotoPaciente = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPaciente = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlPaciente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.pnlPaciente);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1386, 425);
            this.panel1.TabIndex = 0;
            // 
            // pnlPaciente
            // 
            this.pnlPaciente.Controls.Add(this.pictureBox1);
            this.pnlPaciente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPaciente.Location = new System.Drawing.Point(0, 79);
            this.pnlPaciente.Name = "pnlPaciente";
            this.pnlPaciente.Size = new System.Drawing.Size(1386, 346);
            this.pnlPaciente.TabIndex = 2;
            this.pnlPaciente.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPaciente_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(433, 155);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(310, 312);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.71682F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.28318F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 290F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 207F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 523F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.Controls.Add(this.btnInfComplementarPac, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCadastroPaciente, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnAnexosPaciente, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnAtividadeEmGrupo, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnFotoPaciente, 4, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 44);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1386, 35);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // btnInfComplementarPac
            // 
            this.btnInfComplementarPac.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnInfComplementarPac.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnInfComplementarPac.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInfComplementarPac.Location = new System.Drawing.Point(342, 4);
            this.btnInfComplementarPac.Name = "btnInfComplementarPac";
            this.btnInfComplementarPac.Size = new System.Drawing.Size(271, 26);
            this.btnInfComplementarPac.TabIndex = 2;
            this.btnInfComplementarPac.Text = "Informações Complementares do Paciente";
            this.btnInfComplementarPac.UseVisualStyleBackColor = false;
            this.btnInfComplementarPac.Click += new System.EventHandler(this.btnInfComplementarPac_Click);
            // 
            // btnCadastroPaciente
            // 
            this.btnCadastroPaciente.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnCadastroPaciente.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.btnCadastroPaciente.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastroPaciente.Location = new System.Drawing.Point(219, 4);
            this.btnCadastroPaciente.Name = "btnCadastroPaciente";
            this.btnCadastroPaciente.Size = new System.Drawing.Size(115, 26);
            this.btnCadastroPaciente.TabIndex = 1;
            this.btnCadastroPaciente.Text = "Cadastro Paciente";
            this.btnCadastroPaciente.UseVisualStyleBackColor = false;
            this.btnCadastroPaciente.Click += new System.EventHandler(this.btnCadastroPaciente_Click);
            // 
            // btnAnexosPaciente
            // 
            this.btnAnexosPaciente.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnAnexosPaciente.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.btnAnexosPaciente.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnexosPaciente.Location = new System.Drawing.Point(3, 4);
            this.btnAnexosPaciente.Name = "btnAnexosPaciente";
            this.btnAnexosPaciente.Size = new System.Drawing.Size(134, 26);
            this.btnAnexosPaciente.TabIndex = 0;
            this.btnAnexosPaciente.Text = "Anexos do Paciente";
            this.btnAnexosPaciente.UseVisualStyleBackColor = false;
            this.btnAnexosPaciente.Click += new System.EventHandler(this.btnAnexosPaciente_Click);
            // 
            // btnAtividadeEmGrupo
            // 
            this.btnAtividadeEmGrupo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnAtividadeEmGrupo.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnAtividadeEmGrupo.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtividadeEmGrupo.Location = new System.Drawing.Point(632, 4);
            this.btnAtividadeEmGrupo.Name = "btnAtividadeEmGrupo";
            this.btnAtividadeEmGrupo.Size = new System.Drawing.Size(181, 26);
            this.btnAtividadeEmGrupo.TabIndex = 2;
            this.btnAtividadeEmGrupo.Text = "Atividade em Grupo";
            this.btnAtividadeEmGrupo.UseVisualStyleBackColor = false;
            this.btnAtividadeEmGrupo.Click += new System.EventHandler(this.btnAtividadeEmGrupo_Click);
            // 
            // btnFotoPaciente
            // 
            this.btnFotoPaciente.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnFotoPaciente.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnFotoPaciente.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFotoPaciente.Location = new System.Drawing.Point(839, 4);
            this.btnFotoPaciente.Name = "btnFotoPaciente";
            this.btnFotoPaciente.Size = new System.Drawing.Size(181, 26);
            this.btnFotoPaciente.TabIndex = 3;
            this.btnFotoPaciente.Text = "Foto do Paciente";
            this.btnFotoPaciente.UseVisualStyleBackColor = false;
            this.btnFotoPaciente.Click += new System.EventHandler(this.btnFotoPaciente_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.DarkCyan;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblPaciente, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1386, 44);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblPaciente
            // 
            this.lblPaciente.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPaciente.AutoSize = true;
            this.lblPaciente.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaciente.Location = new System.Drawing.Point(3, 9);
            this.lblPaciente.Name = "lblPaciente";
            this.lblPaciente.Size = new System.Drawing.Size(105, 26);
            this.lblPaciente.TabIndex = 1;
            this.lblPaciente.Text = "Paciente";
            // 
            // fmrPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(1386, 425);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "fmrPaciente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.pnlPaciente.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblPaciente;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel pnlPaciente;
        private System.Windows.Forms.Button btnAnexosPaciente;
        private System.Windows.Forms.Button btnCadastroPaciente;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAtividadeEmGrupo;
        private System.Windows.Forms.Button btnInfComplementarPac;
        private System.Windows.Forms.Button btnFotoPaciente;
    }
}