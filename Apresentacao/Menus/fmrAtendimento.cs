﻿using SistemaShekinah.Apresentacao.Listagem;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao
{
    public partial class fmrAtendimento : Form
    {
        public fmrAtendimento()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
        }
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();

        private void AbrirFormFilho(object formFilho)
        {
            if (this.pnlAvalicaoEvolucao.Controls.Count > 0)
                this.pnlAvalicaoEvolucao.Controls.RemoveAt(0);
            Form fh = formFilho as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pnlAvalicaoEvolucao.Controls.Add(fh);
            this.pnlAvalicaoEvolucao.Tag = fh;
            fh.Show();

        }



        private void btnAvalicao_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("avaliacao", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                AbrirFormFilho(new fmrListagemAvaliacao());
                btnAvalicao.BackColor = Color.FromArgb(134, 132, 132);
                btnAvalicao.Font = new Font(btnAvalicao.Font, FontStyle.Bold);
                btnEvolucao.BackColor = Color.FromName("Control");
            }
           
        

            //this.BackColor = Color.FromArgb(134,132, 132);
        }

        private void pnlAvalicaoEvolucao_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnEvolucao_Click_1(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("atendimento", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                AbrirFormFilho(new fmrListagemEvolucoes());
                btnAvalicao.BackColor = Color.FromName("Control");
                btnEvolucao.BackColor = Color.FromArgb(134, 132, 132);
            }


            
          
        }
    }
}
