﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.Apresentacao.Listagem;
using SistemaShekinah.Modelo;
using System;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Menus
{
    public partial class fmrAdminConfig : Form
    {
        public fmrAdminConfig()
        {
            InitializeComponent();
        }

        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();
    /*
        nivelUser.re
        Usuario.NomeUsuario = txtLogin.Text;
        Usuario.permiteLancarRetroativamente = user.permiteRetroativo(txtLogin.Text);*/

        private void AbrirFormFilho(object formFilho)
        {
 

            if (this.pnlAdminConfig.Controls.Count > 0)
                this.pnlAdminConfig.Controls.RemoveAt(0);
            Form fh = formFilho as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pnlAdminConfig.Controls.Add(fh);
            this.pnlAdminConfig.Tag = fh;
            fh.Show();

        }


        private void pnlAdminConfig_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("nivel_usuario", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                AbrirFormFilho(new fmrListagemDeNivelDeUsuarios());
            }
          
        }

        private void btnAgenda_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("cadastro_usuario", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                AbrirFormFilho(new fmrListagemUsuarios());
            }
        }

        private void LblAgendaAnotacoes_Click(object sender, EventArgs e)
        {

        }

        private void LblAgendaAnotacoes_Click_1(object sender, EventArgs e)
        {

        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
