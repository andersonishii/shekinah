﻿namespace SistemaShekinah.Apresentacao.Menus
{
    partial class fmrAgendaAnotacoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fmrAgendaAnotacoes));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblAgendaAnotacoes = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnEntradaSaida = new System.Windows.Forms.Button();
            this.btnAgenda = new System.Windows.Forms.Button();
            this.pnlAgendaAnotacao = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.pnlAgendaAnotacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.DarkCyan;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblAgendaAnotacoes, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1164, 44);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // lblAgendaAnotacoes
            // 
            this.lblAgendaAnotacoes.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblAgendaAnotacoes.AutoSize = true;
            this.lblAgendaAnotacoes.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgendaAnotacoes.Location = new System.Drawing.Point(3, 9);
            this.lblAgendaAnotacoes.Name = "lblAgendaAnotacoes";
            this.lblAgendaAnotacoes.Size = new System.Drawing.Size(226, 26);
            this.lblAgendaAnotacoes.TabIndex = 0;
            this.lblAgendaAnotacoes.Text = "Agenda / Anotações";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.67194F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 89.32806F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 240F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.tableLayoutPanel2.Controls.Add(this.btnEntradaSaida, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnAgenda, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 44);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1164, 35);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // btnEntradaSaida
            // 
            this.btnEntradaSaida.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnEntradaSaida.AutoSize = true;
            this.btnEntradaSaida.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.btnEntradaSaida.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEntradaSaida.Location = new System.Drawing.Point(84, 4);
            this.btnEntradaSaida.Name = "btnEntradaSaida";
            this.btnEntradaSaida.Size = new System.Drawing.Size(193, 27);
            this.btnEntradaSaida.TabIndex = 1;
            this.btnEntradaSaida.Text = "Entrada / Saida de Pacientes";
            this.btnEntradaSaida.UseVisualStyleBackColor = false;
            this.btnEntradaSaida.Click += new System.EventHandler(this.btnAvalicao_Click);
            // 
            // btnAgenda
            // 
            this.btnAgenda.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnAgenda.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.btnAgenda.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgenda.Location = new System.Drawing.Point(3, 4);
            this.btnAgenda.Name = "btnAgenda";
            this.btnAgenda.Size = new System.Drawing.Size(75, 27);
            this.btnAgenda.TabIndex = 2;
            this.btnAgenda.Text = "Agenda";
            this.btnAgenda.UseVisualStyleBackColor = false;
            this.btnAgenda.Click += new System.EventHandler(this.btnAgenda_Click);
            // 
            // pnlAgendaAnotacao
            // 
            this.pnlAgendaAnotacao.AutoScroll = true;
            this.pnlAgendaAnotacao.AutoSize = true;
            this.pnlAgendaAnotacao.Controls.Add(this.pictureBox1);
            this.pnlAgendaAnotacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAgendaAnotacao.Location = new System.Drawing.Point(0, 79);
            this.pnlAgendaAnotacao.Name = "pnlAgendaAnotacao";
            this.pnlAgendaAnotacao.Size = new System.Drawing.Size(1164, 371);
            this.pnlAgendaAnotacao.TabIndex = 3;
            this.pnlAgendaAnotacao.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlAgendaAnotacao_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(433, 155);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(310, 312);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // fmrAgendaAnotacoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(1164, 450);
            this.Controls.Add(this.pnlAgendaAnotacao);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "fmrAgendaAnotacoes";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.pnlAgendaAnotacao.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblAgendaAnotacoes;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnEntradaSaida;
        private System.Windows.Forms.Button btnAgenda;
        private System.Windows.Forms.Panel pnlAgendaAnotacao;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}