﻿using SistemaShekinah.Apresentacao.Relatorios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Menus
{
    public partial class fmrRelatorios : Form
    {
        public fmrRelatorios()
        {
            InitializeComponent();
        }

        private void AbrirFormFilho(object formFilho)
        {
            if (this.pnlRelatorio.Controls.Count > 0)
                this.pnlRelatorio.Controls.RemoveAt(0);
            Form fh = formFilho as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pnlRelatorio.Controls.Add(fh);
            this.pnlRelatorio.Tag = fh;
            fh.Show();

        }

        private void fmrRelatorios_Load(object sender, EventArgs e)
        {
            menuStrip1.BackColor = Color.MediumAquamarine;
        }

        private void pacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioPacientes());
        }

        private void evoluçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioEvolucao());
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void usuáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioDeUsuarios());
        }

        private void medicaçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioCadastroMedicacoes());
        }

        private void cadastroDeMedicaçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioCadastroMedicacoes());
        }

        private void usuáriosToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioDeUsuarios());
        }

        private void nívelDeUsuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioDeNivelDeUsuario());
        }

        private void avaliaçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioAvaliacao());
        }

        private void anexoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioAnexoPaciente());
        }

        private void agendaEventoToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void cadastroDePacienteToolStripMenuItem_Click(object sender, EventArgs e)
        { 
             AbrirFormFilho(new fmRelatorioCadastroPacienteFull());
        }

        private void agendaLembreteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioAgendaLembrete());
        }

        private void informaçãoComplementarDoPacienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioInfoComplementarPaciente());
        }

        private void entadaSaidaDePacientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioEntradaSaida());
            
        }

        private void posologiaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrRelatorioPosologia
                ());
        }
    }
}
