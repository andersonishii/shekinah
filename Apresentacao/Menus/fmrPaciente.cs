﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.Apresentacao.Listagem;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao
{
    public partial class fmrPaciente : Form
    {
        public fmrPaciente()
        {
            InitializeComponent();
        }

        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();

        public void AbrirFormFilho(object formFilho)
        {
            if (this.pnlPaciente.Controls.Count > 0)
                this.pnlPaciente.Controls.RemoveAt(0);
            Form fh = formFilho as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pnlPaciente.Controls.Add(fh);
            this.pnlPaciente.Tag = fh;
            fh.Show();

        }

        private void pnlPaciente_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCadastroPaciente_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("cadastro_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                AbrirFormFilho(new fmrListagemPaciente());
                btnAnexosPaciente.BackColor = Color.FromArgb(134, 132, 132);
                btnAnexosPaciente.Font = new Font(btnAnexosPaciente.Font, FontStyle.Bold);
                btnCadastroPaciente.BackColor = Color.FromName("Control");
            }

        

        }

        private void btnAnexosPaciente_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("anexo_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                // AbrirFormFilho(new fmrAnexosPaciente());
                AbrirFormFilho(new fmrAnexosPaciente());
                btnAnexosPaciente.BackColor = Color.FromArgb(134, 132, 132);
                btnAnexosPaciente.Font = new Font(btnAnexosPaciente.Font, FontStyle.Bold);
                btnCadastroPaciente.BackColor = Color.FromName("Control");
            }
              
        }

        private void btnAtividadeEmGrupo_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("atividade_grupo", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                // AbrirFormFilho(new fmrAnexosPaciente());

                AbrirFormFilho(new fmrListagemAtividadeEmGrupo());
                btnAnexosPaciente.BackColor = Color.FromArgb(134, 132, 132);
                btnAnexosPaciente.Font = new Font(btnAnexosPaciente.Font, FontStyle.Bold);
                btnCadastroPaciente.BackColor = Color.FromName("Control");
            }

        }

        private void btnInfComplementarPac_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("inf_comp_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {
                AbrirFormFilho(new fmrListagemDetalhesPaciente());
                btnAnexosPaciente.BackColor = Color.FromArgb(134, 132, 132);
                btnAnexosPaciente.Font = new Font(btnAnexosPaciente.Font, FontStyle.Bold);
                btnCadastroPaciente.BackColor = Color.FromName("Control");
            }
               
            

        }

        private void btnFotoPaciente_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("foto_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Proibir")
            {
                MessageBox.Show("Usuário não possui permissão para o Módulo");
            }
            else
            {   
                AbrirFormFilho(new fmrListagemFoto());
                /*
                btnAnexosPaciente.BackColor = Color.FromArgb(134, 132, 132);
                btnAnexosPaciente.Font = new Font(btnAnexosPaciente.Font, FontStyle.Bold);
                btnCadastroPaciente.BackColor = Color.FromName("Control");
                */
                
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
