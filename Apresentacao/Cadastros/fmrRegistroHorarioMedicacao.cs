﻿using SistemaShekinah.Apresentacao.Listagem;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrRegistroHorarioMedicacao : Form
    {
        public fmrRegistroHorarioMedicacao()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            var userInput = MessageBox.Show("Deseja realmente sair dessa tela?", "Decisão", MessageBoxButtons.YesNo);

            if (userInput == DialogResult.Yes)
            {
                this.Dispose();
            }
            else
            {
                MessageBox.Show("Nennhum informação foi atualizada");
            }
        }

        Controle control = new Controle();

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            DateTime myDate =(dtpDataRegistro.Value.Date)  + (dtpHoraRegistro.Value.TimeOfDay);
            int idRegistro = Convert.ToInt32(txtIdRegistro.Text);
            int idpaciente = Convert.ToInt32(txtIdPaciente.Text);

            String dataSelecionada = dtpDataRegistro.Value.Date.ToString("dd-MM-yyyy");
            String dataPendencia = (Convert.ToDateTime(txtHoraPendencia.Text)).Date.ToString("dd-MM-yyyy");

            DateTime _dataSelecionada= (Convert.ToDateTime(dataSelecionada));
            DateTime _dataPendencia = (Convert.ToDateTime(dataPendencia));

            if ((  _dataPendencia== _dataSelecionada)|| Usuario.permiteLancarRetroativamente)
            {
                String mensagem = control.atualizacaoPendenciaPosologia(idRegistro, idpaciente, myDate);
                if (control.tem)
                {
                    MessageBox.Show("Posologia Registrada com Sucesso");
                  
                    fmrListagemPosologia listagePosologoa = new fmrListagemPosologia(true);
                    /*
                    InitializeComponent();
                    listagePosologoa.Refresh();
                    listagePosologoa.carregaEvolucoes("1",3);
                    listagePosologoa.dataGridView1.Update();
                    listagePosologoa.dataGridView1.Refresh();
                    System.Threading.Thread.Sleep(500);
                    listagePosologoa.ShowDialog();
                    */
                    MessageBox.Show("Por favor atualize manualmente o formulário da listagem de Posologia");
                    this.Close();
                  
                }
                else
                {
                    MessageBox.Show(mensagem, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Operação não permitida . Você nao tem permissão para registros retroativos");
            }
        }

        void child_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Refresh();
        }

        private void FmrRegistroHorarioMedicacao_Load(object sender, EventArgs e)
        {
            Medicacoes med = new Medicacoes();
            txtRestricaoMedicacao.Text = med.restricaoMedicacao(Convert.ToInt32( txtIdmedicacao.Text));
        }

        private void btnInativar_Click(object sender, EventArgs e)
        {
            Medicacoes med = new Medicacoes();
            int id = Convert.ToInt32(txtIdRegistro.Text);
            int idpaciente = Convert.ToInt32 (txtIdPaciente.Text);
            int codPosologia = Convert.ToInt32(txtCodPosologia.Text);
            int iddamedicacao= med.idMedicacao(txtNomeMedicacao.Text);
            string nomeMedicacao = txtNomeMedicacao.Text;
            String mensagem = control.inativaPendenciaPosologia(id, codPosologia, idpaciente, Convert.ToString(iddamedicacao));
            if (control.tem)
            {
                MessageBox.Show(mensagem, "Inativado com sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Dispose();
            }
            else
            {
                MessageBox.Show(mensagem, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
                
        }

    }
}
