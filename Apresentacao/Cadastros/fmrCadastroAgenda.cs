﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrCadastroAgenda : Form
    {
        public fmrCadastroAgenda()
        {
            InitializeComponent();
        }

        Controle control = new Controle();
        DateTime dataHoraAgora = DateTime.Now;

        private void label3_Click(object sender, EventArgs e)
        {

        }

        public void LimpaDadosEvento()
        {
          
            chkAtivo.Checked = true;


        }

        public void LimpaDadosLembrete()
        {
            btnCorLembrete.BackColor = Color.Transparent;
            chkAtivo.Checked = true;
            chkDiaTodoLembrete.Checked = false;
            txtTituloLembrete.Clear();
            txtDescricaoLembrete.Clear();
            dtpDiaLembrete.Value = dataHoraAgora;
            dtpHoraLembrete.Value = dataHoraAgora;
        }

        public bool VerifcaAtivoInativo(bool ativoinativo)
        {
            if (ativoinativo == true)
            {
                return ativoinativo = true;
            }
            else
            {
                return ativoinativo = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  DateTime dataHoraInicialSelecionada = (dtpDiaInicioEvento.Value.Date) + (dtpHoraInicioEvento.Value.TimeOfDay);
        //    DateTime dataHorafinalSelecionada = (dtpDiaFimEvento.Value.Date) + (dtpHoraFimEvento.Value.TimeOfDay);
            DateTime dataHoraInicialLembrete = (dtpDiaLembrete.Value.Date) + (dtpHoraLembrete.Value.TimeOfDay);
            bool UsoContinuo = VerifcaAtivoInativo(chkDiaTodoLembrete.Checked);
            bool regativo = VerifcaAtivoInativo(chkAtivo.Checked);
            //Color myColor = ColorTranslator.FromHtml(colorDialog1.Color.Name);
            //  String corSelecionada = (colorDialog1.Color.ToArgb() & 0x00FFFFFF).ToString("X6");
            //  String corSelecionada = (colorDialog1.Color.ToArgb() ).ToString("X6");
            String corSelecionada = colorDialog1.Color.ToString();
            String mensagem;

                    // mensagem = control.cadastroEvento(cmbPaciente.SelectPaciente.Id, dataHoraInicialSelecionada, txtTituloLembrete.Text, txtDescricaoLembrete.Text, colorDialog1.Color.Name, "Lembrete", chkAtivo.Checked,chkAtivo.Checked);
                    mensagem = control.cadastroEvento(cmbPaciente.SelectPaciente.Id, dataHoraInicialLembrete, txtTituloLembrete.Text, txtDescricaoLembrete.Text, corSelecionada, "Lembrete", UsoContinuo, regativo);

                    if (control.tem)
                    {
                        MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                       // LimpaDadosEvento();
                        LimpaDadosLembrete();
                    }
                    else
                    {
                        MessageBox.Show(control.msg);
                    }
             
            
        }

        private void tabEvento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnCorEvento_Click(object sender, EventArgs e)
        {
     
        }

        private void btnCorLembrete_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                btnCorLembrete.BackColor = colorDialog1.Color;
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            var userInput = MessageBox.Show("Você tem certeza que deseja limpar todos os campos desse formulário?", "Decisão", MessageBoxButtons.YesNo);
            if (userInput == DialogResult.Yes)
            {

                LimpaDadosEvento();
                LimpaDadosLembrete();
            }
            else{
                MessageBox.Show("Nenhuma campo foi Limpo!");
            }

        }

        private void fmrCadastroAgenda_Load(object sender, EventArgs e)
        {

            if ((!string.IsNullOrWhiteSpace(txtNomePaciente.Text)))
            {
                cmbPaciente.cmbPaciente.SelectedIndex = cmbPaciente.cmbPaciente.FindStringExact(txtNomePaciente.Text);
            }

        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {

            DateTime dataHoraInicialLembrete = (dtpDiaLembrete.Value.Date) + (dtpHoraLembrete.Value.TimeOfDay);
            bool UsoContinuo = VerifcaAtivoInativo(chkDiaTodoLembrete.Checked);
            bool regativo = VerifcaAtivoInativo(chkAtivo.Checked);
            int idregistro = Convert.ToInt32(txtIdRegistro.Text);
            String corSelecionada = colorDialog1.Color.ToString();
            String mensagem;

    
                    // mensagem = control.cadastroEvento(cmbPaciente.SelectPaciente.Id, dataHoraInicialSelecionada, txtTituloLembrete.Text, txtDescricaoLembrete.Text, colorDialog1.Color.Name, "Lembrete", chkAtivo.Checked,chkAtivo.Checked);
                    mensagem = control.atualizaCadastroTarefa(idregistro, cmbPaciente.SelectPaciente.Id, dataHoraInicialLembrete, txtTituloLembrete.Text, txtDescricaoLembrete.Text, corSelecionada, "Lembrete", UsoContinuo, regativo);

                    if (control.tem)
                    {
                        MessageBox.Show(mensagem, "Atualização", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LimpaDadosEvento();
                        LimpaDadosLembrete();
                        btnGravar.Visible = true;
                        btnLimpar.Visible = true;
                        btnAtualizar.Visible = false;
                    }
                    else
                    {
                        MessageBox.Show(control.msg);
                    }
                    
            
        }
    }
}
