﻿namespace SistemaShekinah.Apresentacao.Cadastros
{
    partial class fmrCadastroAgenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chkAtivo = new System.Windows.Forms.CheckBox();
            this.cmbPaciente = new SistemaShekinah.Apresentacao.userControlPaciente();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tabEvento = new System.Windows.Forms.TabControl();
            this.tabLembrete = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCorLembrete = new System.Windows.Forms.Button();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDescricaoLembrete = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTituloLembrete = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.dtpHoraLembrete = new System.Windows.Forms.DateTimePicker();
            this.chkDiaTodoLembrete = new System.Windows.Forms.CheckBox();
            this.dtpDiaLembrete = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNomePaciente = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtIdRegistro = new System.Windows.Forms.TextBox();
            this.userControlPaciente2 = new SistemaShekinah.Apresentacao.userControlPaciente();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabEvento.SuspendLayout();
            this.tabLembrete.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.75F));
            this.tableLayoutPanel1.Controls.Add(this.chkAtivo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cmbPaciente, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 40);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // chkAtivo
            // 
            this.chkAtivo.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkAtivo.AutoSize = true;
            this.chkAtivo.Checked = true;
            this.chkAtivo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAtivo.Location = new System.Drawing.Point(699, 11);
            this.chkAtivo.Name = "chkAtivo";
            this.chkAtivo.Size = new System.Drawing.Size(98, 17);
            this.chkAtivo.TabIndex = 0;
            this.chkAtivo.Text = "Registro Ativo?";
            this.chkAtivo.UseVisualStyleBackColor = true;
            // 
            // cmbPaciente
            // 
            this.cmbPaciente.Location = new System.Drawing.Point(3, 3);
            this.cmbPaciente.Name = "cmbPaciente";
            this.cmbPaciente.Size = new System.Drawing.Size(487, 34);
            this.cmbPaciente.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Location = new System.Drawing.Point(103, 304);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(8, 8);
            this.tableLayoutPanel5.TabIndex = 5;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 800F));
            this.tableLayoutPanel2.Controls.Add(this.tabEvento, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 40);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(800, 361);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // tabEvento
            // 
            this.tabEvento.Controls.Add(this.tabLembrete);
            this.tabEvento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabEvento.Location = new System.Drawing.Point(3, 3);
            this.tabEvento.Name = "tabEvento";
            this.tabEvento.SelectedIndex = 0;
            this.tabEvento.Size = new System.Drawing.Size(794, 355);
            this.tabEvento.TabIndex = 0;
            this.tabEvento.SelectedIndexChanged += new System.EventHandler(this.tabEvento_SelectedIndexChanged);
            // 
            // tabLembrete
            // 
            this.tabLembrete.Controls.Add(this.tableLayoutPanel12);
            this.tabLembrete.Controls.Add(this.tableLayoutPanel10);
            this.tabLembrete.Controls.Add(this.tableLayoutPanel9);
            this.tabLembrete.Controls.Add(this.tableLayoutPanel8);
            this.tabLembrete.Location = new System.Drawing.Point(4, 22);
            this.tabLembrete.Name = "tabLembrete";
            this.tabLembrete.Padding = new System.Windows.Forms.Padding(3);
            this.tabLembrete.Size = new System.Drawing.Size(786, 329);
            this.tabLembrete.TabIndex = 1;
            this.tabLembrete.Text = "Lembrete";
            this.tabLembrete.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.820513F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 92.17949F));
            this.tableLayoutPanel12.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.btnCorLembrete, 1, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 194);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(780, 39);
            this.tableLayoutPanel12.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Cor";
            // 
            // btnCorLembrete
            // 
            this.btnCorLembrete.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnCorLembrete.BackColor = System.Drawing.Color.DarkGray;
            this.btnCorLembrete.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCorLembrete.Location = new System.Drawing.Point(63, 4);
            this.btnCorLembrete.Name = "btnCorLembrete";
            this.btnCorLembrete.Size = new System.Drawing.Size(149, 31);
            this.btnCorLembrete.TabIndex = 1;
            this.btnCorLembrete.Text = "Selecionar Cor";
            this.btnCorLembrete.UseVisualStyleBackColor = false;
            this.btnCorLembrete.Click += new System.EventHandler(this.btnCorLembrete_Click);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.820513F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 92.17949F));
            this.tableLayoutPanel10.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtDescricaoLembrete, 1, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 57);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(780, 137);
            this.tableLayoutPanel10.TabIndex = 2;
            this.tableLayoutPanel10.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel10_Paint);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Lembrete";
            // 
            // txtDescricaoLembrete
            // 
            this.txtDescricaoLembrete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDescricaoLembrete.Location = new System.Drawing.Point(63, 3);
            this.txtDescricaoLembrete.Name = "txtDescricaoLembrete";
            this.txtDescricaoLembrete.Size = new System.Drawing.Size(714, 131);
            this.txtDescricaoLembrete.TabIndex = 1;
            this.txtDescricaoLembrete.Text = "";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.820513F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 92.17949F));
            this.tableLayoutPanel9.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtTituloLembrete, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 29);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(780, 28);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Titulo";
            // 
            // txtTituloLembrete
            // 
            this.txtTituloLembrete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTituloLembrete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTituloLembrete.Location = new System.Drawing.Point(63, 3);
            this.txtTituloLembrete.Name = "txtTituloLembrete";
            this.txtTituloLembrete.Size = new System.Drawing.Size(714, 20);
            this.txtTituloLembrete.TabIndex = 1;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 4;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 560F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Controls.Add(this.dtpHoraLembrete, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.chkDiaTodoLembrete, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.dtpDiaLembrete, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(780, 26);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // dtpHoraLembrete
            // 
            this.dtpHoraLembrete.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpHoraLembrete.Location = new System.Drawing.Point(121, 3);
            this.dtpHoraLembrete.Name = "dtpHoraLembrete";
            this.dtpHoraLembrete.ShowUpDown = true;
            this.dtpHoraLembrete.Size = new System.Drawing.Size(63, 20);
            this.dtpHoraLembrete.TabIndex = 1;
            // 
            // chkDiaTodoLembrete
            // 
            this.chkDiaTodoLembrete.AutoSize = true;
            this.chkDiaTodoLembrete.Location = new System.Drawing.Point(203, 3);
            this.chkDiaTodoLembrete.Name = "chkDiaTodoLembrete";
            this.chkDiaTodoLembrete.Size = new System.Drawing.Size(74, 17);
            this.chkDiaTodoLembrete.TabIndex = 2;
            this.chkDiaTodoLembrete.Text = "Dia Inteiro";
            this.chkDiaTodoLembrete.UseVisualStyleBackColor = true;
            // 
            // dtpDiaLembrete
            // 
            this.dtpDiaLembrete.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDiaLembrete.Location = new System.Drawing.Point(3, 3);
            this.dtpDiaLembrete.Name = "dtpDiaLembrete";
            this.dtpDiaLembrete.Size = new System.Drawing.Size(98, 20);
            this.dtpDiaLembrete.TabIndex = 0;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.31414F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.68586F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 613F));
            this.tableLayoutPanel7.Controls.Add(this.btnGravar, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.btnLimpar, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.btnAtualizar, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 401);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(800, 71);
            this.tableLayoutPanel7.TabIndex = 7;
            // 
            // btnGravar
            // 
            this.btnGravar.BackColor = System.Drawing.Color.DarkGray;
            this.btnGravar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(59, 28);
            this.btnGravar.TabIndex = 0;
            this.btnGravar.Text = "Gravar";
            this.btnGravar.UseVisualStyleBackColor = false;
            this.btnGravar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.BackColor = System.Drawing.Color.DarkGray;
            this.btnLimpar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.Location = new System.Drawing.Point(78, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(70, 28);
            this.btnLimpar.TabIndex = 1;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = false;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.BackColor = System.Drawing.Color.DarkGray;
            this.btnAtualizar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizar.Location = new System.Drawing.Point(3, 39);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(69, 28);
            this.btnAtualizar.TabIndex = 2;
            this.btnAtualizar.Text = "Atualizar";
            this.btnAtualizar.UseVisualStyleBackColor = false;
            this.btnAtualizar.Visible = false;
            this.btnAtualizar.Click += new System.EventHandler(this.btnAtualizar_Click);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 4;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 74F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 480F));
            this.tableLayoutPanel13.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.txtNomePaciente, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.label9, 2, 0);
            this.tableLayoutPanel13.Controls.Add(this.txtIdRegistro, 3, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 472);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(800, 34);
            this.tableLayoutPanel13.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "nome Paciente";
            this.label8.Visible = false;
            // 
            // txtNomePaciente
            // 
            this.txtNomePaciente.Location = new System.Drawing.Point(126, 3);
            this.txtNomePaciente.Name = "txtNomePaciente";
            this.txtNomePaciente.Size = new System.Drawing.Size(100, 20);
            this.txtNomePaciente.TabIndex = 1;
            this.txtNomePaciente.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(249, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "idregostro";
            this.label9.Visible = false;
            // 
            // txtIdRegistro
            // 
            this.txtIdRegistro.Location = new System.Drawing.Point(323, 3);
            this.txtIdRegistro.Name = "txtIdRegistro";
            this.txtIdRegistro.Size = new System.Drawing.Size(100, 20);
            this.txtIdRegistro.TabIndex = 3;
            this.txtIdRegistro.Visible = false;
            // 
            // userControlPaciente2
            // 
            this.userControlPaciente2.Location = new System.Drawing.Point(214, 210);
            this.userControlPaciente2.Name = "userControlPaciente2";
            this.userControlPaciente2.Size = new System.Drawing.Size(8, 8);
            this.userControlPaciente2.TabIndex = 1;
            // 
            // fmrCadastroAgenda
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(800, 533);
            this.Controls.Add(this.tableLayoutPanel13);
            this.Controls.Add(this.tableLayoutPanel7);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.userControlPaciente2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.Name = "fmrCadastroAgenda";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agenda ( Lembrete)";
            this.Load += new System.EventHandler(this.fmrCadastroAgenda_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tabEvento.ResumeLayout(false);
            this.tabLembrete.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private userControlPaciente userControlPaciente2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtNomePaciente;
        public userControlPaciente cmbPaciente;
        public System.Windows.Forms.CheckBox chkAtivo;
        public System.Windows.Forms.Button btnLimpar;
        public System.Windows.Forms.Button btnGravar;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtIdRegistro;
        public System.Windows.Forms.Button btnAtualizar;
        public System.Windows.Forms.TabControl tabEvento;
        private System.Windows.Forms.TabPage tabLembrete;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Button btnCorLembrete;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.RichTextBox txtDescricaoLembrete;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtTituloLembrete;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        public System.Windows.Forms.DateTimePicker dtpHoraLembrete;
        public System.Windows.Forms.CheckBox chkDiaTodoLembrete;
        public System.Windows.Forms.DateTimePicker dtpDiaLembrete;
    }
}