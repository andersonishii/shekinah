﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrCadastroNivelDeUsuarioPermissoes : Form
    {
        public fmrCadastroNivelDeUsuarioPermissoes()
        {
            InitializeComponent();
        }

        Controle control = new Controle();

        private void rdbPacienteTotal_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            String dataHoraAtual = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            bool ativoinativo;
            ativoinativo = VerifcaAtivoInativo(chkAtivo.Checked);

            string nomeNivel = txtNomeNivel.Text;
            RadioButton cadastroPaciente =pnlCadastroPaciente.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton anexosDoPaciente = pnlAnexosPaciente.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton infComplementares = pnlInfComp.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton atividadeGrupo = pnlAtividadeGrupo.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton fotoDoPaciente = pnlFotoPaciente.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton agenda = pnlAgenda.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton entradaSaida = pnlEntradaSaida.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton atendimento = pnlAtendimento.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton avaliacao = pnlAvaliacao.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton medicacao = pnlMedicacao.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton posologia = pnlPosologia.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton usuario = pnlCadastroUsuario.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton nivelusuario = pnlNivelUsuario.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();

            String mensagem = control.CadastraNivelDeUsuario(nomeNivel, ativoinativo, cadastroPaciente.Text.ToString(),anexosDoPaciente.Text.ToString(),infComplementares.Text.ToString(),
                atividadeGrupo.Text.ToString(), fotoDoPaciente.Text.ToString(),agenda.Text.ToString(),entradaSaida.Text.ToString(),atendimento.Text.ToString(),avaliacao.Text.ToString(), medicacao.Text.ToString(),posologia.Text.ToString(),usuario.Text.ToString(), nivelusuario.Text.ToString());

            if (control.tem)
            { 
                MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);

               limpaCampos();
            }
            else
            {
                MessageBox.Show(control.msg);
            }
            

        }

        public void limpaCampos()
        {
            txtNomeNivel.Clear();
            rdbPacienteProibir.Checked = true;
            rdbAnexoPacienteProibir.Checked = true;
            rdbInfComplementarPacienteProibir.Checked = true;
            rdbAtividadeEmGrupoProibir.Checked = true;
            rdbAgendaProibir.Checked = true;
            rdbEntradaSaidaProibir.Checked = true;
            rdbAtendimentoProibir.Checked = true;
            rdbAvaliacaoProibir.Checked = true;
            rdbMedicacaoProibir.Checked = true;
            rdbPosologiaProibir.Checked = true;
            rdbCadastroUsuarioProibir.Checked = true;
            rdbNivelUsuarioProibir.Checked = true;
        }

        public bool VerifcaAtivoInativo(bool ativoinativo)
        {

            if (ativoinativo == true)
            {
                return ativoinativo = true;
            }
            else
            {
                return ativoinativo = false;
            }

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void fmrCadastroNivelDeUsuarioPermissoes_Load(object sender, EventArgs e)
        {

            if ((!string.IsNullOrWhiteSpace(txtCadPaciente.Text)))
            {
                switch (txtCadPaciente.Text)
                {
                    case "Total":
                        rdbPacienteTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbPacienteProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbPacienteApenasConsulta.Checked = true;
                        break;
                }
     
            }

            if ((!string.IsNullOrWhiteSpace(txtAnexoPaciente.Text)))
            {
                switch (txtAnexoPaciente.Text)
                {
                    case "Total":
                        rdbAnexoPacienteTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbAnexoPacienteProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbAnexoPacienteConsulta.Checked = true;
                        break;
                }

            }

            if ((!string.IsNullOrWhiteSpace(txtInfCompPac.Text)))
            {
                switch (txtInfCompPac.Text)
                {
                    case "Total":
                        rdbInfComplementarPacienteTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbInfComplementarPacienteProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbInfComplementarPacienteConsulta.Checked = true;
                        break;
                }

            }

            if ((!string.IsNullOrWhiteSpace(txtAtividadeEmgrupo.Text)))
            {
                switch (txtAtividadeEmgrupo.Text)
                {
                    case "Total":
                        rdbAtividadeEmGrupoTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbAtividadeEmGrupoProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbAtividadeEmGrupoConsulta.Checked = true;
                        break;
                }

            }

            if ((!string.IsNullOrWhiteSpace(txtAgenda.Text)))
            {
                switch (txtAgenda.Text)
                {
                    case "Total":
                        rdbAgendaTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbAgendaProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbAgendaConsulta.Checked = true;
                        break;
                }

            }

            if ((!string.IsNullOrWhiteSpace(txtEntSaida.Text)))
            {
                switch (txtEntSaida.Text)
                {
                    case "Total":
                        rdbEntradaSaidaTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbEntradaSaidaProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbEntradaSaidaConsulta.Checked = true;
                        break;
                }
                 
            }

            if ((!string.IsNullOrWhiteSpace(txtEvolucao.Text)))
            {
                switch (txtEvolucao.Text)
                {
                    case "Total":
                        rdbAtendimentoTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbAtendimentoProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbAtendimentoConsulta.Checked = true;
                        break;
                }

            }

            if ((!string.IsNullOrWhiteSpace(txtAvaliacao.Text)))
            {
                switch (txtAvaliacao.Text)
                {
                    case "Total":
                        rdbAvaliacaoTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbAvaliacaoProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbAvaliacaoConsulta.Checked = true;
                        break;
                }

            }

            if ((!string.IsNullOrWhiteSpace(txtMedicacao.Text)))
            {
                switch (txtMedicacao.Text)
                {
                    case "Total":
                        rdbMedicacaoTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbMedicacaoProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbMedicacaoConsulta.Checked = true;
                        break;
                }

            }

            if ((!string.IsNullOrWhiteSpace(txttPosologia.Text)))
            {
                switch (txttPosologia.Text)
                {
                    case "Total":
                        rdbPosologiaTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbPosologiaProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbPosologiaConsulta.Checked = true;
                        break;
                }

            }

            if ((!string.IsNullOrWhiteSpace(txtCadUser.Text)))
            {
                switch (txtCadUser.Text)
                {
                    case "Total":
                        rdbCadastroUsuarioTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbCadastroUsuarioProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbCadastroUsuarioConsulta.Checked = true;
                        break;
                }

            }

            if ((!string.IsNullOrWhiteSpace(txtNivelUser.Text)))
            {
                switch (txtNivelUser.Text)
                {
                    case "Total":
                        rdbNivelUsuarioTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbNivelUsuarioProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbNivelUsuarioConsulta.Checked = true;
                        break;
                }

            }


            if ((!string.IsNullOrWhiteSpace(txtFotoPaciente.Text)))
            {
                switch (txtFotoPaciente.Text)
                {
                    case "Total":
                        rdbFotoTotal.Checked = true;
                        break;
                    case "Proibir":
                        rdbFotoProibir.Checked = true;
                        break;
                    case "Apenas Consulta":
                        rdbFotoConsulta.Checked = true;
                        break;
                }

            }
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            //String dataHoraAtual = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            bool ativoinativo;
            ativoinativo = VerifcaAtivoInativo(chkAtivo.Checked);

            string nomeNivel = txtNomeNivel.Text;
            int codregistro = Convert.ToInt32(txtIdNivelUser.Text);
            RadioButton cadastroPaciente = pnlCadastroPaciente.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton anexosDoPaciente = pnlAnexosPaciente.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton infComplementares = pnlInfComp.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton atividadeGrupo = pnlAtividadeGrupo.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton fotoDoPaciente = pnlFotoPaciente.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton agenda = pnlAgenda.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton entradaSaida = pnlEntradaSaida.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton atendimento = pnlAtendimento.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton avaliacao = pnlAvaliacao.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton medicacao = pnlMedicacao.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton posologia = pnlPosologia.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton usuario = pnlCadastroUsuario.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            RadioButton nivelusuario = pnlNivelUsuario.Controls.OfType<RadioButton>().Where(x => x.Checked).FirstOrDefault();
            String mensagem = control.atualizarNivelDeUsuario(codregistro, nomeNivel, ativoinativo, cadastroPaciente.Text.ToString(), anexosDoPaciente.Text.ToString(), infComplementares.Text.ToString(),
           atividadeGrupo.Text.ToString(), fotoDoPaciente.Text.ToString(), agenda.Text.ToString(), entradaSaida.Text.ToString(), atendimento.Text.ToString(), avaliacao.Text.ToString(), medicacao.Text.ToString(), posologia.Text.ToString(), usuario.Text.ToString(), nivelusuario.Text.ToString());

            if (control.tem)
            {
                MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);

                limpaCampos();
                btnAtualizar.Visible = false;
                this.Close();
            }
            else
            {
                MessageBox.Show(control.msg);
            }
            

        }
    }
}
