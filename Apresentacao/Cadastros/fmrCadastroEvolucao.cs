﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrCadastroEvolucao : Form
    {
        public fmrCadastroEvolucao()
        {
            InitializeComponent();

        }



        Controle control = new Controle();

        public void LimpaTela()
        {
            txtEvolucao.Text = "";
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            String DiaAtual = DateTime.Now.ToString("dd-MM-yyyy");
            String DiaDatePicker = dtpData.Value.Date.ToString("dd-MM-yyyy");
            DateTime dataHoraSelecionada = (dtpData.Value.Date) + (dtpHora.Value.TimeOfDay);
            DateTime dataHoraAgora = DateTime.Now;
            if ((DiaDatePicker.Equals(DiaAtual))|| Usuario.permiteLancarRetroativamente)
            {

                String mensagem = control.cadastroEvolucao(userControlPaciente1.SelectPaciente.Id, dataHoraSelecionada.ToString(), userControlTipoAtendimento1.SelectTipoEvolucao.descricaoEvolucao, txtEvolucao.Text);

                if (control.tem)
                {
                    MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEvolucao.Text = "";
                  
                }
                else
                {
                    MessageBox.Show(control.msg);
                }


            }
            else
            {
                MessageBox.Show("Negado, apenas o Administrador do sistema conseguirá digitar a evolução de dias retroativos.");
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimpaTela();
        }

        private void txtEvolucao_TextChanged(object sender, EventArgs e)
        {

        }

        private void fmrCadastroEvolucao_Load(object sender, EventArgs e)
        {

            if ((!string.IsNullOrWhiteSpace(txtNomePaciente.Text)))
            {
                userControlPaciente1.cmbPaciente.SelectedIndex = userControlPaciente1.cmbPaciente.FindStringExact(txtNomePaciente.Text);
            }

            
            if ((!string.IsNullOrWhiteSpace(txtNomeEvolucao.Text)))
            {
                userControlTipoAtendimento1.cmbTipoEvolucao.SelectedIndex = userControlTipoAtendimento1.cmbTipoEvolucao.FindStringExact(txtNomeEvolucao.Text);
            }
        }

        private void UserControlTipoAtendimento1_Load(object sender, EventArgs e)
        {

        }

        private void btnInativar_Click(object sender, EventArgs e)
        {
            this.Hide();
            fmrInativacaoEvolucao evolucao = new fmrInativacaoEvolucao();
            evolucao.txtIdRegistro.Text = txtCodigoRegistro.Text;
            evolucao.Show();
            this.Close();
        }
    }
}
