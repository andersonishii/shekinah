﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao
{
    public partial class fmrAvaliacao : Form
    {
        public fmrAvaliacao()
        {
            InitializeComponent();
            /*
            this.StartPosition = FormStartPosition.CenterScreen;
            this.WindowState = FormWindowState.Maximized;
            this.FormBorderStyle = FormBorderStyle.None;
            */
         
        }

        Controle control = new Controle();
        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente número e vírgula");
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente uma vírgula");
            }

            lblIMC.Text = control.calculaIMC(txtPeso.Text, txtAltura.Text);



        }

        private void fmrAvaliacao_Load(object sender, EventArgs e)
        {

            if ((!string.IsNullOrWhiteSpace(txtNomePaciente.Text)) && (!string.IsNullOrWhiteSpace(txtNomePaciente.Text)))
            {
                userControlPaciente2.cmbPaciente.SelectedIndex = userControlPaciente2.cmbPaciente.FindStringExact(txtNomePaciente.Text);
            }
        }

        private void txtPeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente número e vírgula");
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente uma vírgula");
            }
            lblIMC.Text = control.calculaIMC(txtPeso.Text, txtAltura.Text);
        }

        public double calculaIMC(double peso, double altura)
        {
            double imc = peso / (altura * altura);
            return imc;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("O IMC é " + control.calculaIMC(txtPeso
                    .Text, txtAltura.Text));
        }

        private void tblIMC_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(lblIMC.Text.Equals("Verifica peso ou altura")){
                tblIMC.BackColor = Color.Aqua;
                this.BackColor = Color.Brown;
            }

        }

        private void LblGrau_Click(object sender, EventArgs e)
        {

        }

        private void txtBpm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;

            }
              
        }

        private void txtRpm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;

            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;

            }
        }

        private void textBox2_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;

            }
        }

        private void txtOximetria_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;

            }
        }

        private void txtGrau_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente número e vírgula");
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente uma vírgula");
            }


        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimparCampos();
        }

        private void TblIMC_Paint(object sender, PaintEventArgs e)
        {

        }


        public static int ParseInt(string value, int valorpadrao)
        {
            int valorDefault = valorpadrao;
            int parsedInt;
            if (int.TryParse(value, out parsedInt))
            {
                return parsedInt;
            }

            return valorDefault;
        }

        private void BtnGravar_Click(object sender, EventArgs e)
        {
            // String DiaAtual = DateTime.Now.ToString("yyyy-MM-dd");
            String DiaAtual = DateTime.Now.ToString("dd-MM-yyyy");
            String DiaDatePicker = dateTimePicker1.Value.Date.ToString("dd-MM-yyyy");
            DateTime dataHoraAgora = DateTime.Now;
            if ((DiaDatePicker.Equals(DiaAtual)) || Usuario.permiteLancarRetroativamente)
            {

                int sistolica = Controle.ParseInt(txtSistolica.Text, 0);
                int diastolica = Controle.ParseInt(txtDiastolica.Text, 0);
                int bpm = Controle.ParseInt(txtBpm.Text, 0);
                int rpm = Controle.ParseInt(txtRpm.Text, 0);
                int sp02 = Controle.ParseInt(txtOximetria.Text, 0);
                int dor = Controle.ParseInt(txtDor.Text, 0);
                int hgt = Controle.ParseInt(txtHgt.Text, 0);

                String mensagem = control.cadastroAvaliacao(userControlPaciente2.SelectPaciente.Id, dataHoraAgora.ToString("g"), txtPeso.Text,txtAltura.Text, sistolica, diastolica, bpm, rpm, sp02,txtGrau.Text,txtObs.Text, dor, hgt);

                if (control.tem)
                {
                    MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LimparCampos();
                }
                else
                {
                    MessageBox.Show(control.msg);
                }


            }
            else
            {
                MessageBox.Show("Negado, apenas o Administrador do sistema conseguirá digitar a evolução de dias retroativos.");
            }
        }

        private void TxtPeso_TextChanged(object sender, EventArgs e)
        {

        }

        private void LblIMC_Click(object sender, EventArgs e)
        {

        }

        private void LblDescIMC_Click(object sender, EventArgs e)
        {

        }

        private void LblIndicadorIMC_Click(object sender, EventArgs e)
        {

        }

        private void TxtGrau_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtAltura_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtBpm_TextChanged(object sender, EventArgs e)
        {

        }
        private void LimparCampos()
        {
            txtPeso.Clear();
            txtAltura.Clear();
            txtBpm.Clear();
            txtGrau.Clear();
            txtOximetria.Clear();
            txtSistolica.Clear();
            txtDiastolica.Clear();
            txtRpm.Clear();
            txtObs.Clear();
            txtDor.Clear();
            txtHgt.Clear();
        }

        private void TableLayoutPanel13_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void TxtDor_TextChanged(object sender, EventArgs e)
        {

        }

        private void TableLayoutPanel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtDor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;

            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel13_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void txtHgt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;

            }
        }

        private void btnInativar_Click(object sender, EventArgs e)
        {
            this.Hide();
            fmrInativacaoAvaliacao avaliacao = new fmrInativacaoAvaliacao();
            avaliacao.txtId.Text = txtId.Text;
            avaliacao.Show();
            this.Close();
        }
    }
 
}
