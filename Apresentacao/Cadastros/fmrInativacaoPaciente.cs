﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrInativacaoPaciente : Form
    {
        Controle control = new Controle();
        public fmrInativacaoPaciente()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fmrInativacaoPaciente_Load(object sender, EventArgs e)
        {
            if (!chkAtivo.Checked)
            {
                this.Text = "Ativar do Paciente";
                btnInativar.Text = "Ativar";
            }
      
        }

        public int AtivaOuInativa(int op)
        {
            int valor;
            if (op == 1)
            {
                return valor = 0;
            }
            else
            {
                return valor = 1;
            }
        }

        private void btnInativar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtCodPaciente.Text);
            int valor = AtivaOuInativa(Convert.ToInt32(chkAtivo.Checked));
            string tipoMotivo = cmbMotivo.SelectTipoMotivacao.motivoInativacao;

            String mensagem = control.InativaOuAtivaPaciente(id,txtMotivo.Text,valor, tipoMotivo);
            
            if (control.tem)
            {
                MessageBox.Show(mensagem, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Text = "Operação Executada com sucesso";
                this.btnInativar.Enabled = false;
                this.Close();
            }
            else
            {
                MessageBox.Show(control.msg);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
  
        }
    }
}
