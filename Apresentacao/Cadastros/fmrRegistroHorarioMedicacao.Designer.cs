﻿namespace SistemaShekinah.Apresentacao.Cadastros
{
    partial class fmrRegistroHorarioMedicacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblCodRegistro = new System.Windows.Forms.Label();
            this.txtIdRegistro = new System.Windows.Forms.TextBox();
            this.lblHoraPendencia = new System.Windows.Forms.Label();
            this.txtHoraPendencia = new System.Windows.Forms.TextBox();
            this.txtIdPaciente = new System.Windows.Forms.TextBox();
            this.lblIdPaciente = new System.Windows.Forms.Label();
            this.txtCodPosologia = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDataRegistro = new System.Windows.Forms.Label();
            this.dtpDataRegistro = new System.Windows.Forms.DateTimePicker();
            this.dtpHoraRegistro = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtIdmedicacao = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNomeMedicacao = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnInativar = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRestricaoMedicacao = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.33614F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.66386F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 93F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 91F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel1.Controls.Add(this.lblCodRegistro, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtIdRegistro, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblHoraPendencia, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtHoraPendencia, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtIdPaciente, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblIdPaciente, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtCodPosologia, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 6, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 26);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblCodRegistro
            // 
            this.lblCodRegistro.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCodRegistro.AutoSize = true;
            this.lblCodRegistro.Location = new System.Drawing.Point(3, 6);
            this.lblCodRegistro.Name = "lblCodRegistro";
            this.lblCodRegistro.Size = new System.Drawing.Size(75, 13);
            this.lblCodRegistro.TabIndex = 0;
            this.lblCodRegistro.Text = "ID do Registro";
            this.lblCodRegistro.Visible = false;
            // 
            // txtIdRegistro
            // 
            this.txtIdRegistro.Location = new System.Drawing.Point(87, 3);
            this.txtIdRegistro.Name = "txtIdRegistro";
            this.txtIdRegistro.Size = new System.Drawing.Size(82, 20);
            this.txtIdRegistro.TabIndex = 1;
            this.txtIdRegistro.Visible = false;
            // 
            // lblHoraPendencia
            // 
            this.lblHoraPendencia.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblHoraPendencia.AutoSize = true;
            this.lblHoraPendencia.Location = new System.Drawing.Point(211, 6);
            this.lblHoraPendencia.Name = "lblHoraPendencia";
            this.lblHoraPendencia.Size = new System.Drawing.Size(84, 13);
            this.lblHoraPendencia.TabIndex = 2;
            this.lblHoraPendencia.Text = "Data Pendência";
            this.lblHoraPendencia.Visible = false;
            // 
            // txtHoraPendencia
            // 
            this.txtHoraPendencia.Location = new System.Drawing.Point(304, 3);
            this.txtHoraPendencia.Name = "txtHoraPendencia";
            this.txtHoraPendencia.Size = new System.Drawing.Size(100, 20);
            this.txtHoraPendencia.TabIndex = 3;
            this.txtHoraPendencia.Visible = false;
            // 
            // txtIdPaciente
            // 
            this.txtIdPaciente.Location = new System.Drawing.Point(517, 3);
            this.txtIdPaciente.Name = "txtIdPaciente";
            this.txtIdPaciente.Size = new System.Drawing.Size(94, 20);
            this.txtIdPaciente.TabIndex = 4;
            this.txtIdPaciente.Visible = false;
            // 
            // lblIdPaciente
            // 
            this.lblIdPaciente.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblIdPaciente.AutoSize = true;
            this.lblIdPaciente.Location = new System.Drawing.Point(437, 6);
            this.lblIdPaciente.Name = "lblIdPaciente";
            this.lblIdPaciente.Size = new System.Drawing.Size(63, 13);
            this.lblIdPaciente.TabIndex = 5;
            this.lblIdPaciente.Text = "ID Paciente";
            this.lblIdPaciente.Visible = false;
            // 
            // txtCodPosologia
            // 
            this.txtCodPosologia.Enabled = false;
            this.txtCodPosologia.Location = new System.Drawing.Point(708, 3);
            this.txtCodPosologia.Name = "txtCodPosologia";
            this.txtCodPosologia.Size = new System.Drawing.Size(84, 20);
            this.txtCodPosologia.TabIndex = 6;
            this.txtCodPosologia.Visible = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(617, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Cód. Posologia";
            this.label2.Visible = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.97872F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.02128F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 460F));
            this.tableLayoutPanel2.Controls.Add(this.lblDataRegistro, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dtpDataRegistro, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.dtpHoraRegistro, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 26);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(800, 37);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // lblDataRegistro
            // 
            this.lblDataRegistro.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDataRegistro.AutoSize = true;
            this.lblDataRegistro.Location = new System.Drawing.Point(3, 12);
            this.lblDataRegistro.Name = "lblDataRegistro";
            this.lblDataRegistro.Size = new System.Drawing.Size(87, 13);
            this.lblDataRegistro.TabIndex = 0;
            this.lblDataRegistro.Text = "Data do Registro";
            // 
            // dtpDataRegistro
            // 
            this.dtpDataRegistro.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtpDataRegistro.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataRegistro.Location = new System.Drawing.Point(104, 8);
            this.dtpDataRegistro.Name = "dtpDataRegistro";
            this.dtpDataRegistro.Size = new System.Drawing.Size(114, 20);
            this.dtpDataRegistro.TabIndex = 1;
            // 
            // dtpHoraRegistro
            // 
            this.dtpHoraRegistro.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtpHoraRegistro.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpHoraRegistro.Location = new System.Drawing.Point(342, 8);
            this.dtpHoraRegistro.Name = "dtpHoraRegistro";
            this.dtpHoraRegistro.ShowUpDown = true;
            this.dtpHoraRegistro.Size = new System.Drawing.Size(92, 20);
            this.dtpHoraRegistro.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(238, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Hora do Registro";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.60188F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.39812F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 185F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel3.Controls.Add(this.txtIdmedicacao, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtNomeMedicacao, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 63);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(800, 34);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // txtIdmedicacao
            // 
            this.txtIdmedicacao.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtIdmedicacao.Enabled = false;
            this.txtIdmedicacao.Location = new System.Drawing.Point(399, 7);
            this.txtIdmedicacao.Name = "txtIdmedicacao";
            this.txtIdmedicacao.Size = new System.Drawing.Size(179, 20);
            this.txtIdmedicacao.TabIndex = 3;
            this.txtIdmedicacao.Visible = false;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nome Medicação";
            // 
            // txtNomeMedicacao
            // 
            this.txtNomeMedicacao.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtNomeMedicacao.Enabled = false;
            this.txtNomeMedicacao.Location = new System.Drawing.Point(103, 7);
            this.txtNomeMedicacao.Name = "txtNomeMedicacao";
            this.txtNomeMedicacao.Size = new System.Drawing.Size(201, 20);
            this.txtNomeMedicacao.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(310, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "ID Medicação";
            this.label4.Visible = false;
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnRegistrar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrar.Location = new System.Drawing.Point(3, 3);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(196, 28);
            this.btnRegistrar.TabIndex = 0;
            this.btnRegistrar.Text = "Registrar Horário da Medicação";
            this.btnRegistrar.UseVisualStyleBackColor = false;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // btnSair
            // 
            this.btnSair.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnSair.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.Location = new System.Drawing.Point(306, 3);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(75, 28);
            this.btnSair.TabIndex = 1;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = false;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnInativar
            // 
            this.btnInativar.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnInativar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInativar.Location = new System.Drawing.Point(223, 3);
            this.btnInativar.Name = "btnInativar";
            this.btnInativar.Size = new System.Drawing.Size(75, 28);
            this.btnInativar.TabIndex = 2;
            this.btnInativar.Text = "Inativar";
            this.btnInativar.UseVisualStyleBackColor = false;
            this.btnInativar.Click += new System.EventHandler(this.btnInativar_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 697F));
            this.tableLayoutPanel5.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtRestricaoMedicacao, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 97);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(800, 123);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 26);
            this.label5.TabIndex = 0;
            this.label5.Text = "Restrição da Medicação";
            // 
            // txtRestricaoMedicacao
            // 
            this.txtRestricaoMedicacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRestricaoMedicacao.Enabled = false;
            this.txtRestricaoMedicacao.Location = new System.Drawing.Point(106, 3);
            this.txtRestricaoMedicacao.Name = "txtRestricaoMedicacao";
            this.txtRestricaoMedicacao.Size = new System.Drawing.Size(691, 117);
            this.txtRestricaoMedicacao.TabIndex = 1;
            this.txtRestricaoMedicacao.Text = "";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.51908F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.48092F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 215F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 281F));
            this.tableLayoutPanel4.Controls.Add(this.btnSair, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnRegistrar, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnInativar, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 220);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(800, 34);
            this.tableLayoutPanel4.TabIndex = 5;
            // 
            // fmrRegistroHorarioMedicacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(800, 256);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "fmrRegistroHorarioMedicacao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Horario de Registro Da Medicação";
            this.Load += new System.EventHandler(this.FmrRegistroHorarioMedicacao_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblCodRegistro;
        public System.Windows.Forms.TextBox txtIdRegistro;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblDataRegistro;
        private System.Windows.Forms.DateTimePicker dtpDataRegistro;
        private System.Windows.Forms.DateTimePicker dtpHoraRegistro;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblHoraPendencia;
        public System.Windows.Forms.TextBox txtHoraPendencia;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Label lblIdPaciente;
        public System.Windows.Forms.TextBox txtIdPaciente;
        private System.Windows.Forms.Button btnInativar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtNomeMedicacao;
        public System.Windows.Forms.TextBox txtCodPosologia;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtIdmedicacao;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox txtRestricaoMedicacao;
    }
}