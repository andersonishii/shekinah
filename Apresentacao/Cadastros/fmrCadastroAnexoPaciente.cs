﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao
{
    public partial class fmrCadastroAnexoPaciente : Form
    {
        public int idPacienteSelecionado;


        public fmrCadastroAnexoPaciente()
        {
            InitializeComponent();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.ProgressChanged += worker_ProgressChanged;
            worker.DoWork += Worcker_doWork;
            btnNovo.Enabled = false;
        }


        public void criaPastaTodosUsuario()
        {


            for (int i = 0; i < userControlPaciente1.cmbPaciente.Items.Count; i++)
            {
                string value = userControlPaciente1.cmbPaciente.GetItemText(userControlPaciente1.cmbPaciente.SelectedValue);
                // userControlPaciente1.SelectPaciente.Id;
            }
        }





        public void CopiaArquivo(string origem, string destino)
        {


            FileStream fsSaida = new FileStream(destino, FileMode.Create);
            FileStream fsOrigem = new FileStream(origem, FileMode.Open);
            byte[] bt = new byte[1048756];
            int lerByte;
            while ((lerByte = fsOrigem.Read(bt, 0, bt.Length)) > 0)
            {
                fsSaida.Write(bt, 0, lerByte);
                int barraprogressopercent = (int)(fsOrigem.Position * 100 / fsOrigem.Length);
                worker.ReportProgress(barraprogressopercent);
            }
            fsOrigem.Close();
            fsSaida.Close();



        }
        Controle control = new Controle();

        private void Worcker_doWork(object sender, DoWorkEventArgs e)
        {
            //CopiaArquivo(txtOrigem.Text, txtDestino.Text + '\\' + Path.GetFileName(txtOrigem.Text));
            bool checkExistenciaDiretorio;
            string caminhoConcatenado = txtDestino.Text + "\\" + idPacienteSelecionado;
            checkExistenciaDiretorio = Exists(caminhoConcatenado);
            //se já existe a pasta do paicnete ele apenas copia o arquivo
            if (checkExistenciaDiretorio)
            {
                //MessageBox.Show("Diretorio Existe");
                CopiaArquivo(txtOrigem.Text, caminhoConcatenado + '\\' + Path.GetFileName(txtOrigem.Text));
            }
            else
            {

                // criaPasta(caminhoConcatenado);
                CopiaArquivo(txtOrigem.Text, caminhoConcatenado + '\\' + Path.GetFileName(txtOrigem.Text));


            }
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
            lblPercentual.Text = progressBar1.Value.ToString() + "%";
        }

        BackgroundWorker worker = new BackgroundWorker();
        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        public bool VerifcaAtivoInativo(bool ativoinativo)
        {
            if (ativoinativo == true)
            {
                return ativoinativo = true;
            }
            else
            {
                return ativoinativo = false;
            }
        }




        private void btnDestino_Click(object sender, EventArgs e)
        {
            string selectedPath = "";
            MessageBox.Show("Por questões de integridade dos dados , somente o Administrador do sistema pode alterar o local de destino dos arquivos ");
        }
        public static bool Exists(string path)
        {
            if (Directory.Exists(path))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool criaPasta(string pasta, int idpaciente)
        {
            bool checkExistenciaDiretorio;
            string caminhoConcatenado = txtDestino.Text + "\\" + idpaciente;
            checkExistenciaDiretorio = Exists(caminhoConcatenado);
            //se já existe a pasta do paicnete ele apenas copia o arquivo
            if (!checkExistenciaDiretorio)
            {
                Directory.CreateDirectory(caminhoConcatenado);
                MessageBox.Show("Pasta do paciente criada com sucesso");
                return true;
            }
            else if (checkExistenciaDiretorio)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private void btnOrigem_Click(object sender, EventArgs e)
        {
            string arquivoOrigem = "";
            Thread t = new Thread((ThreadStart)(() =>
            {
                OpenFileDialog saveFileDialog1 = new OpenFileDialog();

                //saveFileDialog1.Filter = "JSON Files (*.json)|*.json";
                saveFileDialog1.FilterIndex = 2;
                saveFileDialog1.RestoreDirectory = true;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    arquivoOrigem = saveFileDialog1.FileName;
                }
            }));
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
            t.Join();
            txtOrigem.Text = arquivoOrigem;
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            string caminhoConcatenado = txtDestino.Text + "\\" + idPacienteSelecionado;
            idPacienteSelecionado = userControlPaciente1.SelectPaciente.Id;
            bool verificaPasta = criaPasta(caminhoConcatenado, idPacienteSelecionado);
            if (File.Exists(txtOrigem.Text) && verificaPasta)
            {
                //se já existe a pasta do paicnete ele apenas copia o arquivo
                worker.RunWorkerAsync();
                btnGravar.Enabled = false;
                Controle control = new Controle();
                String DiaAtual = DateTime.Now.ToString("dd-MM-yyyy");
                String DiaDatePicker = dateTimePicker1.Value.Date.ToString("dd-MM-yyyy");
                DateTime dataHoraAgora = DateTime.Now;
                bool ativoinativo;
                ativoinativo = VerifcaAtivoInativo(chkAtivoInativo.Checked);
                String mensagem = control.cadastroAnexo(userControlPaciente1.SelectPaciente.Id, userControlTipoAnexo1.SelectTipoEvolucao.descricaoTipoAnexo, /*dataHoraAgora.ToString("g")*/DiaDatePicker, txtOrigem.Text, txtDestino.Text + "\\" + idPacienteSelecionado+"\\"+ Path.GetFileName(txtOrigem.Text), txtObs.Text, txtTag.Text, ativoinativo);

                if (control.tem)
                {
                    MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnNovo.Enabled = true;
                }
                else
                {
                    MessageBox.Show(control.msg);
                }
            }
            else
            {
                MessageBox.Show("Selecione um arquivo arquivo");
            }
        }
 


   



   

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            fmrCadastroAnexoPaciente c = new fmrCadastroAnexoPaciente();
            c.txtNomePaciente.Text= userControlPaciente1.SelectPaciente.Nome;
            c.Show();
            c.Activate();
            c.BringToFront();
            c.Focus();
            c.TopMost = true;
            c.TopMost = false;
            this.Dispose();
          
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TxtOrigem_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtDestino_TextChanged(object sender, EventArgs e)
        {

        }

        private void ProgressBar1_Click_1(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
        
        }

        private void txtObs_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_2(object sender, EventArgs e)
        {
          
        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            criaPastaTodosUsuario();
        }

        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void fmrCadastroAnexoPaciente_Load(object sender, EventArgs e)
        {

            if ((!string.IsNullOrWhiteSpace(txtNomePaciente.Text)))
            {
                userControlPaciente1.cmbPaciente.SelectedIndex = userControlPaciente1.cmbPaciente.FindStringExact(txtNomePaciente.Text);
            }
        }
    }
}
