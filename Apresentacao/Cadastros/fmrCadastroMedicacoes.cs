﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao
{
    public partial class fmrCaCadastroPosologia : Form
    {
        public fmrCaCadastroPosologia()
        {
            InitializeComponent();
            cbxFormaFarmaceutica.DropDownStyle = ComboBoxStyle.DropDownList;
        }





        Controle control = new Controle();
        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            String dataHoraAtual = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
            bool ativoinativo;

            //se fo atualização de registro ele cai no primeiro IF
            if (!String.IsNullOrEmpty(txtFormaFarmaceutica_Gambis.Text))
            {
                ativoinativo = VerifcaAtivoInativo(chkAtivo.Checked);
                int codRegistro = int.Parse(txtCodigo.Text);
                String mensagem = control.atualizaMedicacao(txtNomeQuimico.Text, txtNomeComercial.Text, cbxFormaFarmaceutica.Text, txtModoDeUso.Text, dataHoraAtual, ativoinativo, txtNomeLaboratorio.Text, codRegistro, txtreacoesAdversas.Text);

                if (control.tem)
                {
                    MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Text = "Cadastro de Medicações";
                    this.btnCadastrar.Text = "Cadastrar";
                    this.btnLimpar.Visible = true;
                    this.btnInativar.Visible = false;

                    limpaCampos();
                }
                else
                {
                    MessageBox.Show(control.msg);
                }

            }
            //se for via cadastro normal ele cai cai
            else
            {
                ativoinativo = VerifcaAtivoInativo(chkAtivo.Checked);
                String mensagem = control.cadastroMedicacao(txtNomeQuimico.Text, txtNomeComercial.Text, cbxFormaFarmaceutica.Text, txtModoDeUso.Text, dataHoraAtual, ativoinativo, txtNomeLaboratorio.Text, txtreacoesAdversas.Text);

                if (control.tem)
                {
                    MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpaCampos();
                }
                else
                {
                    MessageBox.Show(control.msg);
                }
            }

        }

        public bool VerifcaAtivoInativo(bool ativoinativo)
        {

            if (ativoinativo == true)
            {
                return ativoinativo = true;
            }
            else
            {
                return ativoinativo = false;
            }

        }

        public Medicacoes SelectFormaFarmaceutica
        {
            get
            {
                return (Medicacoes)cbxFormaFarmaceutica.SelectedItem;
            }
        }

        private void fmrCadastroMedicacoes_Load(object sender, EventArgs e)
        {
            Medicacoes med = new Medicacoes();
            List<Medicacoes> _medicacao = new List<Medicacoes>();
            _medicacao = med.GetFormaFarmaceuticaDB();
            foreach (Medicacoes _pac in _medicacao)
            {
                cbxFormaFarmaceutica.DataSource = _medicacao;
                cbxFormaFarmaceutica.ValueMember = "nomeFormaFarmaceutica";
                cbxFormaFarmaceutica.DisplayMember = "nomeFormaFarmaceutica";
                if (!string.IsNullOrWhiteSpace(txtFormaFarmaceutica_Gambis.Text))
                {
                    cbxFormaFarmaceutica.SelectedIndex = cbxFormaFarmaceutica.FindStringExact(txtFormaFarmaceutica_Gambis.Text);
                }

            }

        }

        void limpaCampos()
        {
            txtNomeQuimico.Clear();
            txtNomeLaboratorio.Clear();
            txtNomeComercial.Clear();
            txtModoDeUso.Clear();
            txtFormaFarmaceutica_Gambis.Clear();
            txtreacoesAdversas.Clear();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtFormaFarmaceutica_Gambis.Text))
            {
                int id = int.Parse(txtCodigo.Text);
                String mensagem = control.deleteMedicacao(id);
                if (control.tem)
                {
                    MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpaCampos();
                    fmrListagemCadastroMedicacoes list = new fmrListagemCadastroMedicacoes();
                    list.dataListagemMedicacoes.Refresh();
                    btnCadastrar.Text = "Cadastrar";
                    btnLimpar.Text = "Limpar";
                    txtCodigo.Text = "";

                }
                else
                {
                    MessageBox.Show(control.msg);
                }

            }
            else
            {
                limpaCampos();

            }

        }

        public void InativaPosologia(int id)
        {
            var userInput = MessageBox.Show("Deja também inativar a posologia de Todos os paciente dessa medicação.?", "Decisão", MessageBoxButtons.YesNo);
            if (userInput == DialogResult.Yes)
            {
                String mensagem = control.inativaPendenciaPosologia(id);
                if (control.tem)
                {
                    MessageBox.Show(mensagem, "Inativação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show(control.msg);
                }
            }
            else
            {
                MessageBox.Show("Apenas a medicação foi inativada");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txtCodigo.Text);
            String mensagem = control.inativaMedicacao(id);
            var userInput = MessageBox.Show("Deseja inativar essa medicação?", "Decisão", MessageBoxButtons.YesNo);
            if (userInput == DialogResult.Yes)
            {
                if (control.tem)
                {
                    MessageBox.Show(mensagem, "Inativação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpaCampos();
                    btnInativar.Visible = false;
                    btnLimpar.Visible = true;
                    btnCadastrar.Text = "Cadastrar";
                    fmrListagemCadastroMedicacoes list = new fmrListagemCadastroMedicacoes();
                    list.dataListagemMedicacoes.Refresh();
                    InativaPosologia(id);
                }
                else
                {
                    MessageBox.Show(control.msg);
                }
            }
            else
            {
                MessageBox.Show("Nenhuma ação foi executada");
            }
              
        }



        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {
        }
    }
}
    


