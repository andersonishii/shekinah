﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrCadastroPosologia : Form
    {
        DataTable table = new DataTable();
        public fmrCadastroPosologia()
        {
            InitializeComponent();
            cmbMedicamento.DropDownStyle = ComboBoxStyle.DropDownList;

            txtCodigoPosologia.Text = getLastaCodPosologia().ToString();
            table.Clear();
            criaTabela();
            dataGridView1.DataSource = table;
            //oculta as colunas que não são necessárias para o usuário8
            this.dataGridView1.Columns["Medicação"].ReadOnly =true;
            this.dataGridView1.Columns["Periodicidade"].ReadOnly = true;
            this.dataGridView1.Columns["Posologia"].ReadOnly = true;
            this.dataGridView1.Columns["ID paciente"].Visible = false;
            this.dataGridView1.Columns["Data hora Inicial"].Visible = false;
            this.dataGridView1.Columns["Data hora Final"].Visible = false;
            this.dataGridView1.Columns["Periodo Periodidicade"].Visible = false;
            this.dataGridView1.Columns["Codigo da Posologia"].Visible = false;
            this.dataGridView1.Columns["Uso continuo?"].Visible = false;
            this.dataGridView1.Columns["Codigo Medicacao"].Visible = false;
            this.dataGridView1.Columns["Ultimo Registro"].ReadOnly = true;
            this.dataGridView1.Columns["ID Medicacao"].Visible = false;
        }

        public void criaTabela()
        {
            table.Columns.Add("ID paciente", typeof(int));
            table.Columns.Add("Data Hora da Pendencia", typeof(DateTime));
            table.Columns.Add("Medicação", typeof(String));
            table.Columns.Add("Periodicidade", typeof(String));
            table.Columns.Add("Periodo Periodidicade", typeof(int));
            table.Columns.Add("Data hora Inicial", typeof(DateTime));
            table.Columns.Add("Data hora Final", typeof(DateTime));
            table.Columns.Add("Codigo da Posologia", typeof(int));
            table.Columns.Add("Uso continuo?", typeof(bool));
            table.Columns.Add("Posologia", typeof(int));
            table.Columns.Add("Codigo Medicacao", typeof(String));
            table.Columns.Add("Nome do Paciente", typeof(String));
            table.Columns.Add("Ultimo Registro", typeof(bool));
            table.Columns.Add("ID Medicacao", typeof(int));
        }



        public Medicacoes Selectemedicacao
        {
            get
            {
                return (Medicacoes)cmbMedicamento.SelectedItem;
            }
        }

        private void fmrCadastroPosologia_Load(object sender, EventArgs e)
        {


            Medicacoes med = new Medicacoes();
          

            
            //carrega o usuario customizado
            if ((!string.IsNullOrWhiteSpace(txtNomePaciente.Text)))
            {
                userControlPaciente1.cmbPaciente.SelectedIndex = userControlPaciente1.cmbPaciente.FindStringExact(txtNomePaciente.Text);
            }

      
            {
                List<Medicacoes> _medicacoes = new List<Medicacoes>();
                _medicacoes = med.GetMedicamentoCadastroDB();
                foreach (Medicacoes _med in _medicacoes)
                {
                    //MessageBox.Show(""+_pac.Nome);
                    cmbMedicamento.DataSource = _medicacoes;
                    cmbMedicamento.ValueMember = "id";
                    cmbMedicamento.DisplayMember = "nomeQuimico";

                    //_paciente.Add(new Paciente() { Id = _pac.Id, cpf = _pac.cpf, Nome = _pac.Nome });
                }

                //table.Columns.Add("Ativo?", typeof(bool));
            }
            List<Medicacoes> _restricaoMedicacao = new List<Medicacoes>();
            //preenche a restrição de medicação do paciente
            _restricaoMedicacao = med.GetRestricaoMedicacao(userControlPaciente1.SelectPaciente.Id);

            foreach (Medicacoes _pac in _restricaoMedicacao)
            {
                txtRestricaoMedicacaoPaciente.AppendText(_pac.nomeMedicamento.ToString() + Environment.NewLine);
            }
        }

        public Medicacoes SelectMedicacao
        {
            get
            {
                return (Medicacoes)cmbMedicamento.SelectedItem;
            }
        }

            public bool VerifcaAtivoInativo(bool ativoinativo)
            {
                if (ativoinativo == true)
                {
                    return ativoinativo = true;
                }
                else
                {
                    return ativoinativo = false;
                }
            }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUsoContinuo.Checked == true)
            {
                MessageBox.Show("Habilite esse campo apenas se o medicamento for de uso contínuo. OBS: O sistema gerará pendência para os próximos 6 meses. Ao término desse período você será avisado para cadastrar novamente a posologia");
                desabilitaCamposContinuo(false);
            }
            else
            {
                desabilitaCamposContinuo(true);
            }
        }

        public void desabilitaCamposContinuo(Boolean value)
        {


            //dtpInicio.Enabled = value;
            dtpFim.Enabled = value;
            //dtpHoraMinutoSegundo.Enabled = value;
            //txtPeriodicidade.Enabled = value;
            //rdbDias.Enabled = value;
            // rdbHora.Enabled = value;
            //rdbMeses.Enabled = value;
            // rdbSemanas.Enabled = value;

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {

        }

        SqlCommand cmd = new SqlCommand();
        Conexao con = new Conexao();
        SqlDataReader dr;
        SqlDataAdapter da = new SqlDataAdapter();

        public int getLastaCodPosologia()
        {
            da.SelectCommand = new SqlCommand("select (  CASE WHEN max (codigo_posologia) IS NULL then 1 else max (codigo_posologia)+1  end ) from posologia");
            da.SelectCommand.Connection = con.conectar();
            da.SelectCommand.ExecuteNonQuery();
            int intID = Convert.ToInt32(da.SelectCommand.ExecuteScalar());
            con.desconectar();
            return intID;
        }


        public bool checaDataInicialMenorQueDataFinal(DateTime data1, DateTime data2)
        {
            if(data1> data2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        TimeSpan span1;
        TimeSpan span3;
        DateTime myDate;
        DateTime myDate2;


        private void button1_Click(object sender, EventArgs e)
        {
            bool checkDatas = checaDataInicialMenorQueDataFinal(dtpInicio.Value,dtpFim.Value);
            if ((String.IsNullOrEmpty(txtPeriodicidade.Text))|| (String.IsNullOrEmpty(txtPosologia.Text))|| checkDatas)
            {
                MessageBox.Show("Obrigatório o preenchimento da posologia e periodicidade. Verifique também se a data inicial não é maior que a data final");
            }
            else
            {
                bool ativoinativo = VerifcaAtivoInativo(chkAtivo.Checked);
                bool usoContinuo = VerifcaAtivoInativo(chkUsoContinuo.Checked);
                int diferenca = (obterDiferencaDias(dtpInicio.Value.Date, dtpFim.Value.Date) * -1);
                //  MessageBox.Show("a dierneça de dias é "+diferenca);
                DateTime dateinicio = dtpInicio.Value.Date;
                DateTime datafinal = dtpFim.Value.Date;
                TimeSpan hora = dtpHoraMinutoSegundo.Value.TimeOfDay;
                TimeSpan hora2 = dtpHoraMinutoSegundo.Value.TimeOfDay;

                //cria uma hora zerada para comprar acom a data do dtpHoraMinutoSegundo
                TimeSpan horazero = new TimeSpan(00,00,00);
               

                //caso seja 00:00:00 ele add um segundo(é gambis mesmo)
                // if(hora.Minutes.ToString() == horazero)
                if (hora.Minutes == horazero.Minutes)
                {
                
                    int dia = Convert.ToInt32(dateinicio.ToString("dd"));
                    int mes = Convert.ToInt32(dateinicio.ToString("MM"));
                    int ano = Convert.ToInt32(dateinicio.ToString("yyyy"));
                    int horas = Convert.ToInt32(dtpHoraMinutoSegundo.Value.Hour);
                    int minutos = Convert.ToInt32(dtpHoraMinutoSegundo.Value.Minute);
                    int segundos = Convert.ToInt32(dtpHoraMinutoSegundo.Value.Second);
                    DateTime dt = new DateTime(ano, mes, dia, horas, minutos, segundos+1);
                    myDate = dt;


                }
                 else
                 {
                    myDate = dateinicio + hora;
                 }

                //a data será add a data da pendencia + hora zerada caso seja 
                //DateTime myDate = dateinicio + span3;
                //t DateTime myDate = dateinicio + hora;
                DateTime datafinalcomhra = datafinal + hora;
                int campoPeriodicidade = Convert.ToInt32(txtPeriodicidade.Text);
                int codPosologia = Convert.ToInt32(txtCodigoPosologia.Text);
                //essa expressao lambida varre todos os combobo combox e verifica qual deles está marcado.
                RadioButton radioBtn = this.tableLayoutPanel4.Controls.OfType<RadioButton>()
                                                        .Where(x => x.Checked).FirstOrDefault();
             
          
                if (chkUsoContinuo.Checked.Equals(true))
                {
                    dtpFim.Value = dtpInicio.Value.AddDays(175);

                    switch (radioBtn.Text)
                    {
                        case "Hora":
                            addTableHoras(userControlPaciente1.SelectPaciente.Id, campoPeriodicidade, myDate, Selectemedicacao.nomeQuimico, radioBtn.Text, myDate, dtpFim.Value.Date, campoPeriodicidade, codPosologia, usoContinuo, txtPosologia.Text, SelectMedicacao.Id, userControlPaciente1.SelectPaciente.Nome, hora, Selectemedicacao.Id);
                            txtPosologia.Clear();
                            txtPeriodicidade.Clear();
                            //addTableDiario(campoPeriodicidade, myDate, radioBtn.Text,cmbMedicamento.SelectedValue.ToString() , myDate, dtpFim.Value, ativoinativo, codPosologia);
                            break;
                        case "Dia(s)":
                            addTableDiario(userControlPaciente1.SelectPaciente.Id, campoPeriodicidade, myDate, Selectemedicacao.nomeQuimico, radioBtn.Text, myDate, dtpFim.Value.Date, campoPeriodicidade, codPosologia, usoContinuo, txtPosologia.Text, SelectMedicacao.Id, userControlPaciente1.SelectPaciente.Nome, hora, Selectemedicacao.Id);
                            txtPosologia.Clear();
                            txtPeriodicidade.Clear();
                            //addTableDiario(campoPeriodicidade, myDate, radioBtn.Text,cmbMedicamento.SelectedValue.ToString() , myDate, dtpFim.Value, ativoinativo, codPosologia);
                            break;
                        case "Semana(s)":
                            addTableSemanal(userControlPaciente1.SelectPaciente.Id, campoPeriodicidade, myDate, Selectemedicacao.nomeQuimico, radioBtn.Text, myDate, dtpFim.Value.Date, campoPeriodicidade, codPosologia, usoContinuo, txtPosologia.Text, SelectMedicacao.Id, userControlPaciente1.SelectPaciente.Nome, hora, Selectemedicacao.Id);
                            txtPosologia.Clear();
                            txtPeriodicidade.Clear();
                            //addTableDiario(campoPeriodicidade, myDate, radioBtn.Text,cmbMedicamento.SelectedValue.ToString() , myDate, dtpFim.Value, ativoinativo, codPosologia);
                            break;
                        case "Mes(es)":
                            addTableMensal(userControlPaciente1.SelectPaciente.Id, campoPeriodicidade, myDate, Selectemedicacao.nomeQuimico, radioBtn.Text, myDate, dtpFim.Value.Date, campoPeriodicidade, codPosologia, usoContinuo, txtPosologia.Text, SelectMedicacao.Id, userControlPaciente1.SelectPaciente.Nome, hora, Selectemedicacao.Id);
                            txtPosologia.Clear();
                            txtPeriodicidade.Clear();
                            //addTableDiario(campoPeriodicidade, myDate, radioBtn.Text,cmbMedicamento.SelectedValue.ToString() , myDate, dtpFim.Value, ativoinativo, codPosologia);
                            break;
                        default:
                            MessageBox.Show("Caiu no default. Entre em contato com o desenvolvedor do software");
                            txtPosologia.Clear();
                            txtPeriodicidade.Clear();
                            Console.WriteLine("Other");
                            break;
                    }
                    dtpFim.Value = DateTime.Now.AddDays(0);
                }
                else
                {
                    switch (radioBtn.Text)
                    {
                        case "Hora":
                            addTableHoras(userControlPaciente1.SelectPaciente.Id, campoPeriodicidade, myDate, Selectemedicacao.nomeQuimico, radioBtn.Text, myDate, dtpFim.Value.Date, campoPeriodicidade, codPosologia, usoContinuo, txtPosologia.Text, SelectMedicacao.Id, userControlPaciente1.SelectPaciente.Nome, hora, Selectemedicacao.Id);
                            txtPosologia.Clear();
                            txtPeriodicidade.Clear();
                            //addTableDiario(campoPeriodicidade, myDate, radioBtn.Text,cmbMedicamento.SelectedValue.ToString() , myDate, dtpFim.Value, ativoinativo, codPosologia);
                            break;
                        case "Dia(s)":
                            addTableDiario(userControlPaciente1.SelectPaciente.Id, campoPeriodicidade, myDate, Selectemedicacao.nomeQuimico, radioBtn.Text, myDate, dtpFim.Value.Date, campoPeriodicidade, codPosologia, usoContinuo, txtPosologia.Text, SelectMedicacao.Id, userControlPaciente1.SelectPaciente.Nome, hora, Selectemedicacao.Id);
                            txtPosologia.Clear();
                            txtPeriodicidade.Clear();
                            //addTableDiario(campoPeriodicidade, myDate, radioBtn.Text,cmbMedicamento.SelectedValue.ToString() , myDate, dtpFim.Value, ativoinativo, codPosologia);
                            break;
                        case "Semana(s)":
                            addTableSemanal(userControlPaciente1.SelectPaciente.Id, campoPeriodicidade, myDate, Selectemedicacao.nomeQuimico, radioBtn.Text, myDate, dtpFim.Value.Date, campoPeriodicidade, codPosologia, usoContinuo, txtPosologia.Text, SelectMedicacao.Id, userControlPaciente1.SelectPaciente.Nome, hora, Selectemedicacao.Id);
                            txtPosologia.Clear();
                            txtPeriodicidade.Clear();
                            //addTableDiario(campoPeriodicidade, myDate, radioBtn.Text,cmbMedicamento.SelectedValue.ToString() , myDate, dtpFim.Value, ativoinativo, codPosologia);
                            break;
                        case "Mes(es)":
                            addTableMensal(userControlPaciente1.SelectPaciente.Id, campoPeriodicidade, myDate, Selectemedicacao.nomeQuimico, radioBtn.Text, myDate, dtpFim.Value.Date, campoPeriodicidade, codPosologia, usoContinuo, txtPosologia.Text, SelectMedicacao.Id, userControlPaciente1.SelectPaciente.Nome, hora, Selectemedicacao.Id);
                            txtPosologia.Clear();
                            txtPeriodicidade.Clear();
                            //addTableDiario(campoPeriodicidade, myDate, radioBtn.Text,cmbMedicamento.SelectedValue.ToString() , myDate, dtpFim.Value, ativoinativo, codPosologia);
                            break;
                        default:
                            MessageBox.Show("caiu no default. Entre em contato com o desenvolvedor do software");
                            txtPosologia.Clear();
                            txtPeriodicidade.Clear();
                            Console.WriteLine("Other");
                            break;
                    }
                    dtpFim.Value = DateTime.Now.AddDays(0);
                }
          
            }
        }




        public void addTableHoras(int idpaciente, int campoPeriodicidade, DateTime data, String nomeMedicacao, String nomePeriodidicade,DateTime datainicial,DateTime datafinal,int periodoperiodicidade,int codposologia,bool usocontinuo,string posologia,int idMedicacaoSelecionada,string nome_paciente, TimeSpan hora,int idmedicacao)
        {
            int i = 1;
            int qtdeRepeteDiario = (24 / campoPeriodicidade) * (obterDiferencaDias(datainicial, datafinal) + 1);
            TimeSpan horazero = new TimeSpan(00, 00, 00);

            //caso seja 00:00:00 ele add um segundo(é gambis mesmo)
  
            DateTime datafinalcomhra = datafinal + hora;
            DateTime auxilar = data;

            while (i <= qtdeRepeteDiario + 1)
            {
                auxilar = data.AddHours(campoPeriodicidade);
                if (i == qtdeRepeteDiario+1)
                {
                    table.Rows.Add(idpaciente,/*data.ToString("dd/MM/yyyy HH:mm")*/data, nomeMedicacao, nomePeriodidicade, periodoperiodicidade, datainicial, datafinal, codposologia, usocontinuo, posologia, idMedicacaoSelecionada, nome_paciente, true, idmedicacao);
                    data = data.AddHours(campoPeriodicidade);
                    i++;
                }
                else
                {
                    table.Rows.Add(idpaciente, data.ToString(), nomeMedicacao, nomePeriodidicade, periodoperiodicidade, datainicial, datafinal, codposologia, usocontinuo, posologia, idMedicacaoSelecionada, nome_paciente,false, idmedicacao);
                    data = data.AddHours(campoPeriodicidade);
                    i++;
                }
            }
            MessageBox.Show("Verifique se data e horário estão corretos. Caso não estejam , por favor corrigir manualmente na tabela ");

      
        }

        public void addTableDiario(int idpaciente, int campoPeriodicidade, DateTime data, String nomeMedicacao, String nomePeriodidicade, DateTime datainicial, DateTime datafinal, int periodoperiodicidade, int codposologia, bool usocontinuo, string posologia, int idMedicacaoSelecionada, string nome_paciente, TimeSpan hora, int idmedicacao)
        {
          

            int i = 1;
            DateTime datafinalcomhra = datafinal + hora;
            DateTime auxilar = data;

            while (datafinalcomhra >=  data)
            {
                auxilar =data.AddDays(campoPeriodicidade);
                if(auxilar> datafinalcomhra)
                {
                    table.Rows.Add(idpaciente, data.ToString(), nomeMedicacao, nomePeriodidicade, periodoperiodicidade, datainicial, datafinal, codposologia, usocontinuo, posologia, idMedicacaoSelecionada, nome_paciente,true, idmedicacao);
                    data = data.AddDays(campoPeriodicidade);
                }
                else
                {
                    table.Rows.Add(idpaciente, data.ToString(), nomeMedicacao, nomePeriodidicade, periodoperiodicidade, datainicial, datafinal, codposologia, usocontinuo, posologia, idMedicacaoSelecionada, nome_paciente,false, idmedicacao);
                    data = data.AddDays(campoPeriodicidade);
                }
            }
            MessageBox.Show("Verifique se data e horário estão corretos. Caso não estejam , por favor corrigir manualmente na tabela ");
        }

        public void addTableSemanal(int idpaciente, int campoPeriodicidade, DateTime data, String nomeMedicacao, String nomePeriodidicade, DateTime datainicial, DateTime datafinal, int periodoperiodicidade, int codposologia, bool usocontinuo, string posologia , int idMedicacaoSelecionada, string nome_paciente, TimeSpan hora, int idmedicacao)
        {
            int i = 1;
            DateTime datafinalcomhra = datafinal + hora;
            DateTime auxilar = data;
            int periodicidadeSemanal = campoPeriodicidade * 7;
            while (datafinalcomhra >= data)
            {
                auxilar = data.AddDays(periodicidadeSemanal);
                if (auxilar > datafinalcomhra)
                {
                    table.Rows.Add(idpaciente, data.ToString(), nomeMedicacao, nomePeriodidicade, periodoperiodicidade, datainicial, datafinal, codposologia, usocontinuo, posologia, idMedicacaoSelecionada, nome_paciente, true, idmedicacao);
                    data = data.AddDays(periodicidadeSemanal);
                }
                else
                {
                    table.Rows.Add(idpaciente, data.ToString(), nomeMedicacao, nomePeriodidicade, periodoperiodicidade, datainicial, datafinal, codposologia, usocontinuo, posologia, idMedicacaoSelecionada, nome_paciente,false, idmedicacao);
                    data = data.AddDays(periodicidadeSemanal);
                }
            }
            MessageBox.Show("Verifique se data e horário estão corretos. Caso não estejam , por favor corrigir manualmente na tabela ");
        }

        public void addTableMensal(int idpaciente, int campoPeriodicidade, DateTime data, String nomeMedicacao, String nomePeriodidicade, DateTime datainicial, DateTime datafinal, int periodoperiodicidade, int codposologia, bool usocontinuo, string posologia, int idMedicacaoSelecionada, string nome_paciente, TimeSpan hora, int idmedicacao)
        {
            int i = 1;
            DateTime datafinalcomhra = datafinal + hora;
            DateTime auxilar = data;
            int periodicidadeMensal= campoPeriodicidade;
            while (datafinalcomhra >= data)
            {
                auxilar = data.AddMonths(periodicidadeMensal);
                if (auxilar > datafinalcomhra)
                {
                    table.Rows.Add(idpaciente, data.ToString(), nomeMedicacao, nomePeriodidicade, periodoperiodicidade, datainicial, datafinal, codposologia, usocontinuo, posologia, idMedicacaoSelecionada, nome_paciente, true, idmedicacao);
                    data = data.AddMonths(periodicidadeMensal);
                }
                else
                {
                    table.Rows.Add(idpaciente, data.ToString(), nomeMedicacao, nomePeriodidicade, periodoperiodicidade, datainicial, datafinal, codposologia, usocontinuo, posologia, idMedicacaoSelecionada, nome_paciente,false, idmedicacao);
                    data = data.AddMonths(periodicidadeMensal);
                }
            }
            MessageBox.Show("Verifique se data e horário estão corretos. Caso não estejam , por favor corrigir manualmente na tabela ");

        
        }





        public int getVezesRepetedias(int periodicidade,DateTime data1,DateTime data2)
        {
            /*
            DateTime dateinicio = data1;
            DateTime datafim = data2;
            TimeSpan hora = hours;
            DateTime myDate = dateinicio + hora;
            */
            int dias = obterDiferencaDias(data1, data2);
            int vezesrepte =24/((periodicidade)*-1);
            int retorno = vezesrepte * dias;
          //  int retorno = vezesrepte);
          //  MessageBox.Show(">>>>" + vezesrepte.ToString());
            return retorno;
        }

    


        public int obterDiferencaDias(DateTime data1, DateTime data2)
        {
            return (data2-data1).Days;
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        public void limpaParaNovoRegistro()
        {
            LimpaDataGrid();
            txtPeriodicidade.Clear();
            txtPosologia.Clear();
            rdbHora.Checked = true;
            chkUsoContinuo.Checked=true;

        }


        Controle control = new Controle();

        public int verificaBoolean(String valor)
        {
            if (valor.Equals("True"))
            {
                return 1;
            }
            else{
                return 0;
            }
         
        }

        private void btnGravarPosologia_Click(object sender, EventArgs e)
        { 
            if (dataGridView1.RowCount==1)
            {
                MessageBox.Show("Por favor inserir ao menos um dado na tabela de pendências ");

            }
            else
            {
             
              //  var userInput = MessageBox.Show("Deseja ocultar mensagem de sucesso de inserção no banco de dados ?", "Decisão", MessageBoxButtons.YesNo);
                for (int i = 0; i < (dataGridView1.Rows.Count-1); i++)
                {
                    int id = Convert.ToInt32(dataGridView1.Rows[i].Cells["ID paciente"].Value);
                    int codPosologia = Convert.ToInt32(dataGridView1.Rows[i].Cells["Codigo da Posologia"].Value);
                    // string nomeMedicacao = dataGridView1.Rows[i].Cells["Medicação"].Value.ToString();
                    string nomeMedicacao = dataGridView1.Rows[i].Cells["ID Medicacao"].Value.ToString();
                    DateTime dataHoraPendencia = Convert.ToDateTime(dataGridView1.Rows[i].Cells["Data Hora da Pendencia"].Value.ToString());
                    DateTime datahoraInicial = Convert.ToDateTime(dataGridView1.Rows[i].Cells["Data hora Inicial"].Value.ToString());
                    DateTime dataFinal = Convert.ToDateTime(dataGridView1.Rows[i].Cells["Data hora Final"].Value.ToString());
                    int periodoPeriodicidade = Convert.ToInt32(dataGridView1.Rows[i].Cells["Periodo Periodidicade"].Value);
                    string nomePeriodidicade = dataGridView1.Rows[i].Cells["Periodicidade"].Value.ToString();
                    Boolean ativoativo = VerifcaAtivoInativo(chkAtivo.Checked);
                    Boolean usocontinuo = VerifcaAtivoInativo(chkUsoContinuo.Checked);
                    string posologia = dataGridView1.Rows[i].Cells["Posologia"].Value.ToString();
                    string observacao = txtObs.Text;
                    int idMedicacao = Convert.ToInt32(dataGridView1.Rows[i].Cells["Codigo Medicacao"].Value);
                    Boolean ehUltimoRegistro =Convert.ToBoolean(verificaBoolean(dataGridView1.Rows[i].Cells["Ultimo Registro"].Value.ToString()));
           
                    // Boolean ehUltimoRegistro = dataGridView1.Rows[i].Cells["Ultimo Registro"].Value;

                    String mensagem = control.cadastroPosologia(id, codPosologia, nomeMedicacao, dataHoraPendencia.ToString(), datahoraInicial.ToString(), dataFinal.ToString(), periodoPeriodicidade, nomePeriodidicade, ativoativo, usocontinuo, posologia, observacao, idMedicacao, ehUltimoRegistro);
 
                    if (control.tem)
                    {
                  
                     /*   if (userInput == DialogResult.No)
                        {
                            MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            btnGravarPosologia.Enabled = false;
                            btnNovoRegistro.Enabled = true;
                        }
                        else
                        {*/
                            int ultimo = dataGridView1.Rows.Count - 1;
                            if (i+1== ultimo)
                            {
                                MessageBox.Show(mensagem, "Todos os registros foram cadastrados com sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                          
                                btnGravarPosologia.Visible = false;
                                btnNovoRegistro.Visible = true;
                                btnNovoRegistro.Enabled = true;
                            LimpaDataGrid();
                        }
                       /* }*/
                    }
                    else
                    {
                        MessageBox.Show(control.msg);
                    }
                    
                }
            }


        }


        public void limpaCampos()
        {
            txtPosologia.Clear();
            txtPeriodicidade.Clear();
            txtObs.Clear();
            txtCodigoPosologia.Clear();
        }

        public void LimpaDataGrid()
        {
            int rowCount = dataGridView1.Rows.Count - 1;
            for (int i = rowCount - 1; i >= 0; i--)
            {
                DataGridViewRow dr = dataGridView1.Rows[i];
                dataGridView1.Rows.Remove(dr);
            }

        }

        private void btnNovoRegistro_Click(object sender, EventArgs e)
        {
            limpaCampos();
            txtCodigoPosologia.Text = getLastaCodPosologia().ToString();
            btnGravarPosologia.Visible = true;
            LimpaDataGrid();
            btnNovoRegistro.Visible = false;
        }

        private void txtPosologia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente número e vírgula");
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("Este campo aceita somente uma vírgula");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnDeletarTudo_Click(object sender, EventArgs e)
        {
            var userInput = MessageBox.Show("Todos os dados inseridos na tabela serão excluídos. Deseja excluí-los?", "Decisão", MessageBoxButtons.YesNo);
            if (userInput == DialogResult.Yes)
            {
                LimpaDataGrid();
            }
            else
            {
                MessageBox.Show("Nenhum dado foi deletado");
            }
               
        }

        private void TableLayoutPanel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
  
        }

        private void userControlPaciente1_MouseClick(object sender, MouseEventArgs e)
        {
            txtRestricaoMedicacaoPaciente.Clear();
            Medicacoes med = new Medicacoes();
            List<Medicacoes> _medicacao = new List<Medicacoes>();
            _medicacao = med.GetRestricaoMedicacao(userControlPaciente1.SelectPaciente.Id);
            foreach (Medicacoes _pac in _medicacao)
            {
                txtRestricaoMedicacaoPaciente.AppendText(_pac.nomeMedicamento.ToString() + Environment.NewLine);
            }
        }

        private void userControlPaciente1_MouseDown(object sender, MouseEventArgs e)
        {
            txtRestricaoMedicacaoPaciente.Clear();
            Medicacoes med = new Medicacoes();
            List<Medicacoes> _medicacao = new List<Medicacoes>();
            _medicacao = med.GetRestricaoMedicacao(userControlPaciente1.SelectPaciente.Id);
            foreach (Medicacoes _pac in _medicacao)
            {
                txtRestricaoMedicacaoPaciente.AppendText(_pac.nomeMedicamento.ToString() + Environment.NewLine);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnInativar_Click(object sender, EventArgs e)
        {

        }

        private void cmbMedicamento_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
    }
}
