﻿namespace SistemaShekinah.Apresentacao
{
    partial class fmrAvaliacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblData = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.userControlPaciente2 = new SistemaShekinah.Apresentacao.userControlPaciente();
            this.lblDadosAntrometricos = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.txtAltura = new System.Windows.Forms.TextBox();
            this.tblIMC = new System.Windows.Forms.TableLayoutPanel();
            this.lblIMC = new System.Windows.Forms.Label();
            this.lblDescIMC = new System.Windows.Forms.Label();
            this.txtDor = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPressaoArterial = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lblSistolica = new System.Windows.Forms.Label();
            this.lblDiastolica = new System.Windows.Forms.Label();
            this.txtSistolica = new System.Windows.Forms.TextBox();
            this.txtDiastolica = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFrequenciaCardiaca = new System.Windows.Forms.Label();
            this.lblOximetria = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.lblBpm = new System.Windows.Forms.Label();
            this.txtBpm = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtOximetria = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFrequenciaResp = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.lblRpm = new System.Windows.Forms.Label();
            this.txtRpm = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtGrau = new System.Windows.Forms.TextBox();
            this.LblGrau = new System.Windows.Forms.Label();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtObs = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.btnInativar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.txtHgt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.txtNomePaciente = new System.Windows.Forms.TextBox();
            this.lblNomePaciente = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.lblDadosAntrometricos.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tblIMC.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.txtId, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(997, 31);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(501, 3);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(100, 20);
            this.txtId.TabIndex = 0;
            this.txtId.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(199, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "ID(deixar essa label sempre visible=false)";
            this.label11.Visible = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.81818F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.18182F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 669F));
            this.tableLayoutPanel2.Controls.Add(this.lblData, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dateTimePicker1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.userControlPaciente2, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 31);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(997, 41);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // lblData
            // 
            this.lblData.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblData.AutoSize = true;
            this.lblData.Location = new System.Drawing.Point(3, 14);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(94, 13);
            this.lblData.TabIndex = 1;
            this.lblData.Text = "Data da avaliação";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dateTimePicker1.Location = new System.Drawing.Point(107, 10);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(217, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // userControlPaciente2
            // 
            this.userControlPaciente2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.userControlPaciente2.Location = new System.Drawing.Point(330, 7);
            this.userControlPaciente2.Name = "userControlPaciente2";
            this.userControlPaciente2.Size = new System.Drawing.Size(471, 27);
            this.userControlPaciente2.TabIndex = 2;
            // 
            // lblDadosAntrometricos
            // 
            this.lblDadosAntrometricos.ColumnCount = 2;
            this.lblDadosAntrometricos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.4F));
            this.lblDadosAntrometricos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 680F));
            this.lblDadosAntrometricos.Controls.Add(this.label2, 0, 0);
            this.lblDadosAntrometricos.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDadosAntrometricos.Location = new System.Drawing.Point(0, 72);
            this.lblDadosAntrometricos.Name = "lblDadosAntrometricos";
            this.lblDadosAntrometricos.RowCount = 1;
            this.lblDadosAntrometricos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49F));
            this.lblDadosAntrometricos.Size = new System.Drawing.Size(997, 25);
            this.lblDadosAntrometricos.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Dados Antropométricos";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.649289F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 91.35071F));
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtPeso, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtAltura, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 97);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(997, 48);
            this.tableLayoutPanel3.TabIndex = 3;
            this.tableLayoutPanel3.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel3_Paint);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Peso";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Altura";
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(89, 3);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(100, 20);
            this.txtPeso.TabIndex = 2;
            this.txtPeso.TextChanged += new System.EventHandler(this.TxtPeso_TextChanged);
            this.txtPeso.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPeso_KeyPress);
            // 
            // txtAltura
            // 
            this.txtAltura.Location = new System.Drawing.Point(89, 27);
            this.txtAltura.Name = "txtAltura";
            this.txtAltura.Size = new System.Drawing.Size(100, 20);
            this.txtAltura.TabIndex = 3;
            this.txtAltura.TextChanged += new System.EventHandler(this.TxtAltura_TextChanged);
            this.txtAltura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            // 
            // tblIMC
            // 
            this.tblIMC.ColumnCount = 3;
            this.tblIMC.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblIMC.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.tblIMC.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 781F));
            this.tblIMC.Controls.Add(this.lblIMC, 1, 0);
            this.tblIMC.Controls.Add(this.lblDescIMC, 0, 0);
            this.tblIMC.Controls.Add(this.txtDor, 1, 1);
            this.tblIMC.Controls.Add(this.label8, 0, 1);
            this.tblIMC.Dock = System.Windows.Forms.DockStyle.Top;
            this.tblIMC.Location = new System.Drawing.Point(0, 145);
            this.tblIMC.Name = "tblIMC";
            this.tblIMC.RowCount = 2;
            this.tblIMC.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tblIMC.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tblIMC.Size = new System.Drawing.Size(997, 49);
            this.tblIMC.TabIndex = 4;
            this.tblIMC.Paint += new System.Windows.Forms.PaintEventHandler(this.TblIMC_Paint);
            // 
            // lblIMC
            // 
            this.lblIMC.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblIMC.AutoSize = true;
            this.lblIMC.Location = new System.Drawing.Point(131, 3);
            this.lblIMC.Name = "lblIMC";
            this.lblIMC.Size = new System.Drawing.Size(26, 13);
            this.lblIMC.TabIndex = 0;
            this.lblIMC.Text = "IMC";
            this.lblIMC.Click += new System.EventHandler(this.LblIMC_Click);
            // 
            // lblDescIMC
            // 
            this.lblDescIMC.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDescIMC.AutoSize = true;
            this.lblDescIMC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescIMC.Location = new System.Drawing.Point(3, 3);
            this.lblDescIMC.Name = "lblDescIMC";
            this.lblDescIMC.Size = new System.Drawing.Size(32, 13);
            this.lblDescIMC.TabIndex = 1;
            this.lblDescIMC.Text = "IMC: ";
            this.lblDescIMC.Click += new System.EventHandler(this.LblDescIMC_Click);
            // 
            // txtDor
            // 
            this.txtDor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtDor.Location = new System.Drawing.Point(94, 24);
            this.txtDor.Name = "txtDor";
            this.txtDor.Size = new System.Drawing.Size(100, 20);
            this.txtDor.TabIndex = 2;
            this.txtDor.TextChanged += new System.EventHandler(this.TxtDor_TextChanged);
            this.txtDor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDor_KeyPress);
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Dor";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.lblPressaoArterial, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 194);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(997, 28);
            this.tableLayoutPanel4.TabIndex = 5;
            // 
            // lblPressaoArterial
            // 
            this.lblPressaoArterial.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblPressaoArterial.AutoSize = true;
            this.lblPressaoArterial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPressaoArterial.Location = new System.Drawing.Point(3, 7);
            this.lblPressaoArterial.Name = "lblPressaoArterial";
            this.lblPressaoArterial.Size = new System.Drawing.Size(96, 13);
            this.lblPressaoArterial.TabIndex = 0;
            this.lblPressaoArterial.Text = "Pressão Arterial";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.649289F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 91.35071F));
            this.tableLayoutPanel5.Controls.Add(this.lblSistolica, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblDiastolica, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtSistolica, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtDiastolica, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 222);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.14815F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.85185F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(997, 54);
            this.tableLayoutPanel5.TabIndex = 6;
            this.tableLayoutPanel5.Paint += new System.Windows.Forms.PaintEventHandler(this.TableLayoutPanel5_Paint);
            // 
            // lblSistolica
            // 
            this.lblSistolica.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblSistolica.AutoSize = true;
            this.lblSistolica.Location = new System.Drawing.Point(3, 6);
            this.lblSistolica.Name = "lblSistolica";
            this.lblSistolica.Size = new System.Drawing.Size(46, 13);
            this.lblSistolica.TabIndex = 0;
            this.lblSistolica.Text = "Sistólica";
            // 
            // lblDiastolica
            // 
            this.lblDiastolica.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDiastolica.AutoSize = true;
            this.lblDiastolica.Location = new System.Drawing.Point(3, 33);
            this.lblDiastolica.Name = "lblDiastolica";
            this.lblDiastolica.Size = new System.Drawing.Size(53, 13);
            this.lblDiastolica.TabIndex = 1;
            this.lblDiastolica.Text = "Diastólica";
            // 
            // txtSistolica
            // 
            this.txtSistolica.Location = new System.Drawing.Point(89, 3);
            this.txtSistolica.Name = "txtSistolica";
            this.txtSistolica.Size = new System.Drawing.Size(100, 20);
            this.txtSistolica.TabIndex = 2;
            this.txtSistolica.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            this.txtSistolica.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // txtDiastolica
            // 
            this.txtDiastolica.Location = new System.Drawing.Point(89, 29);
            this.txtDiastolica.Name = "txtDiastolica";
            this.txtDiastolica.Size = new System.Drawing.Size(100, 20);
            this.txtDiastolica.TabIndex = 3;
            this.txtDiastolica.TextChanged += new System.EventHandler(this.TextBox2_TextChanged);
            this.txtDiastolica.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress_1);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.lblFrequenciaCardiaca, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 276);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(997, 26);
            this.tableLayoutPanel6.TabIndex = 7;
            // 
            // lblFrequenciaCardiaca
            // 
            this.lblFrequenciaCardiaca.AccessibleRole = System.Windows.Forms.AccessibleRole.SplitButton;
            this.lblFrequenciaCardiaca.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFrequenciaCardiaca.AutoSize = true;
            this.lblFrequenciaCardiaca.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrequenciaCardiaca.Location = new System.Drawing.Point(3, 6);
            this.lblFrequenciaCardiaca.Name = "lblFrequenciaCardiaca";
            this.lblFrequenciaCardiaca.Size = new System.Drawing.Size(126, 13);
            this.lblFrequenciaCardiaca.TabIndex = 0;
            this.lblFrequenciaCardiaca.Text = "Frequência Cardíaca";
            // 
            // lblOximetria
            // 
            this.lblOximetria.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblOximetria.AutoSize = true;
            this.lblOximetria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOximetria.Location = new System.Drawing.Point(3, 7);
            this.lblOximetria.Name = "lblOximetria";
            this.lblOximetria.Size = new System.Drawing.Size(59, 13);
            this.lblOximetria.TabIndex = 1;
            this.lblOximetria.Text = "Oximetria";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 4;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.32909F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.67091F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 295F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 455F));
            this.tableLayoutPanel7.Controls.Add(this.lblBpm, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtBpm, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 302);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(997, 24);
            this.tableLayoutPanel7.TabIndex = 8;
            // 
            // lblBpm
            // 
            this.lblBpm.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblBpm.AutoSize = true;
            this.lblBpm.Location = new System.Drawing.Point(3, 0);
            this.lblBpm.Name = "lblBpm";
            this.lblBpm.Size = new System.Drawing.Size(36, 24);
            this.lblBpm.TabIndex = 0;
            this.lblBpm.Text = "B.P.M.";
            // 
            // txtBpm
            // 
            this.txtBpm.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtBpm.Location = new System.Drawing.Point(45, 3);
            this.txtBpm.Name = "txtBpm";
            this.txtBpm.Size = new System.Drawing.Size(103, 20);
            this.txtBpm.TabIndex = 1;
            this.txtBpm.TextChanged += new System.EventHandler(this.TxtBpm_TextChanged);
            this.txtBpm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBpm_KeyPress);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "SpO2 %";
            // 
            // txtOximetria
            // 
            this.txtOximetria.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtOximetria.Location = new System.Drawing.Point(88, 30);
            this.txtOximetria.Name = "txtOximetria";
            this.txtOximetria.Size = new System.Drawing.Size(102, 20);
            this.txtOximetria.TabIndex = 3;
            this.txtOximetria.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOximetria_KeyPress);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.lblFrequenciaResp, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 326);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(997, 27);
            this.tableLayoutPanel8.TabIndex = 9;
            // 
            // lblFrequenciaResp
            // 
            this.lblFrequenciaResp.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblFrequenciaResp.AutoSize = true;
            this.lblFrequenciaResp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrequenciaResp.Location = new System.Drawing.Point(3, 7);
            this.lblFrequenciaResp.Name = "lblFrequenciaResp";
            this.lblFrequenciaResp.Size = new System.Drawing.Size(142, 13);
            this.lblFrequenciaResp.TabIndex = 0;
            this.lblFrequenciaResp.Text = "Frequência Respiratória";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 4;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.83547F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 87.16453F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 454F));
            this.tableLayoutPanel9.Controls.Add(this.lblRpm, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtRpm, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 353);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(997, 26);
            this.tableLayoutPanel9.TabIndex = 10;
            // 
            // lblRpm
            // 
            this.lblRpm.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblRpm.AutoSize = true;
            this.lblRpm.Location = new System.Drawing.Point(3, 6);
            this.lblRpm.Name = "lblRpm";
            this.lblRpm.Size = new System.Drawing.Size(40, 13);
            this.lblRpm.TabIndex = 11;
            this.lblRpm.Text = "R.P.M.";
            // 
            // txtRpm
            // 
            this.txtRpm.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtRpm.Location = new System.Drawing.Point(63, 3);
            this.txtRpm.Name = "txtRpm";
            this.txtRpm.Size = new System.Drawing.Size(102, 20);
            this.txtRpm.TabIndex = 12;
            this.txtRpm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRpm_KeyPress);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.578053F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 91.42194F));
            this.tableLayoutPanel10.Controls.Add(this.lblOximetria, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.txtOximetria, 1, 1);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 379);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 2;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(997, 51);
            this.tableLayoutPanel10.TabIndex = 11;
            this.tableLayoutPanel10.Paint += new System.Windows.Forms.PaintEventHandler(this.TableLayoutPanel10_Paint);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 430);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.Size = new System.Drawing.Size(997, 26);
            this.tableLayoutPanel11.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Temperatura";
            // 
            // txtGrau
            // 
            this.txtGrau.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtGrau.Location = new System.Drawing.Point(85, 3);
            this.txtGrau.Name = "txtGrau";
            this.txtGrau.Size = new System.Drawing.Size(103, 20);
            this.txtGrau.TabIndex = 14;
            this.txtGrau.TextChanged += new System.EventHandler(this.TxtGrau_TextChanged);
            this.txtGrau.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGrau_KeyPress);
            // 
            // LblGrau
            // 
            this.LblGrau.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.LblGrau.AutoSize = true;
            this.LblGrau.Location = new System.Drawing.Point(3, 6);
            this.LblGrau.Name = "LblGrau";
            this.LblGrau.Size = new System.Drawing.Size(47, 13);
            this.LblGrau.TabIndex = 13;
            this.LblGrau.Text = "Grau º C";
            this.LblGrau.Click += new System.EventHandler(this.LblGrau_Click);
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.268933F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 91.73106F));
            this.tableLayoutPanel12.Controls.Add(this.LblGrau, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtGrau, 1, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(0, 456);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 2;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(997, 27);
            this.tableLayoutPanel12.TabIndex = 13;
            // 
            // btnGravar
            // 
            this.btnGravar.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.btnGravar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnGravar.Size = new System.Drawing.Size(56, 27);
            this.btnGravar.TabIndex = 15;
            this.btnGravar.Text = "Gravar";
            this.btnGravar.UseVisualStyleBackColor = false;
            this.btnGravar.Click += new System.EventHandler(this.BtnGravar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.btnLimpar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.Location = new System.Drawing.Point(89, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(134, 27);
            this.btnLimpar.TabIndex = 16;
            this.btnLimpar.Text = "Limpar Campos";
            this.btnLimpar.UseVisualStyleBackColor = false;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.857143F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 92.14286F));
            this.tableLayoutPanel15.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.txtObs, 1, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(0, 573);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(997, 74);
            this.tableLayoutPanel15.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Observações";
            // 
            // txtObs
            // 
            this.txtObs.Location = new System.Drawing.Point(81, 3);
            this.txtObs.Name = "txtObs";
            this.txtObs.Size = new System.Drawing.Size(913, 66);
            this.txtObs.TabIndex = 16;
            this.txtObs.Text = "";
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 4;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.96296F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.03704F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 601F));
            this.tableLayoutPanel16.Controls.Add(this.btnLimpar, 1, 0);
            this.tableLayoutPanel16.Controls.Add(this.btnGravar, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.btnInativar, 2, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(0, 647);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(997, 33);
            this.tableLayoutPanel16.TabIndex = 17;
            // 
            // btnInativar
            // 
            this.btnInativar.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.btnInativar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInativar.Location = new System.Drawing.Point(229, 3);
            this.btnInativar.Name = "btnInativar";
            this.btnInativar.Size = new System.Drawing.Size(75, 27);
            this.btnInativar.TabIndex = 17;
            this.btnInativar.Text = "Inativar Avaliação";
            this.btnInativar.UseVisualStyleBackColor = false;
            this.btnInativar.Visible = false;
            this.btnInativar.Click += new System.EventHandler(this.btnInativar_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Observações";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 3;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.577755F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90.42225F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel14.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.txtHgt, 1, 1);
            this.tableLayoutPanel14.Controls.Add(this.label10, 0, 1);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(0, 483);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 3;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(997, 90);
            this.tableLayoutPanel14.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "H.G.T.";
            // 
            // txtHgt
            // 
            this.txtHgt.Location = new System.Drawing.Point(96, 26);
            this.txtHgt.Name = "txtHgt";
            this.txtHgt.Size = new System.Drawing.Size(100, 20);
            this.txtHgt.TabIndex = 2;
            this.txtHgt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHgt_KeyPress);
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(39, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "H.G.T.";
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.121364F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 92.87864F));
            this.tableLayoutPanel13.Controls.Add(this.txtNomePaciente, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.lblNomePaciente, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 680);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(997, 95);
            this.tableLayoutPanel13.TabIndex = 18;
            this.tableLayoutPanel13.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel13_Paint_1);
            // 
            // txtNomePaciente
            // 
            this.txtNomePaciente.Location = new System.Drawing.Point(74, 3);
            this.txtNomePaciente.Name = "txtNomePaciente";
            this.txtNomePaciente.Size = new System.Drawing.Size(100, 20);
            this.txtNomePaciente.TabIndex = 0;
            this.txtNomePaciente.Visible = false;
            // 
            // lblNomePaciente
            // 
            this.lblNomePaciente.AutoSize = true;
            this.lblNomePaciente.Location = new System.Drawing.Point(3, 0);
            this.lblNomePaciente.Name = "lblNomePaciente";
            this.lblNomePaciente.Size = new System.Drawing.Size(49, 26);
            this.lblNomePaciente.TabIndex = 1;
            this.lblNomePaciente.Text = "Nome Paciente";
            this.lblNomePaciente.Visible = false;
            // 
            // fmrAvaliacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(997, 749);
            this.Controls.Add(this.tableLayoutPanel13);
            this.Controls.Add(this.tableLayoutPanel16);
            this.Controls.Add(this.tableLayoutPanel15);
            this.Controls.Add(this.tableLayoutPanel14);
            this.Controls.Add(this.tableLayoutPanel12);
            this.Controls.Add(this.tableLayoutPanel11);
            this.Controls.Add(this.tableLayoutPanel10);
            this.Controls.Add(this.tableLayoutPanel9);
            this.Controls.Add(this.tableLayoutPanel8);
            this.Controls.Add(this.tableLayoutPanel7);
            this.Controls.Add(this.tableLayoutPanel6);
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tblIMC);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.lblDadosAntrometricos);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.Name = "fmrAvaliacao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Avaliação";
            this.Load += new System.EventHandler(this.fmrAvaliacao_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.lblDadosAntrometricos.ResumeLayout(false);
            this.lblDadosAntrometricos.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tblIMC.ResumeLayout(false);
            this.tblIMC.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblData;
        private userControlPaciente userControlPaciente1;
        private System.Windows.Forms.TableLayoutPanel lblDadosAntrometricos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tblIMC;
        private System.Windows.Forms.Label lblIMC;
        private System.Windows.Forms.Label lblDescIMC;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label lblPressaoArterial;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label lblSistolica;
        private System.Windows.Forms.Label lblDiastolica;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lblFrequenciaCardiaca;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label lblBpm;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label lblFrequenciaResp;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label lblRpm;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label lblOximetria;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LblGrau;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtPeso;
        public System.Windows.Forms.TextBox txtAltura;
        public System.Windows.Forms.TextBox txtDor;
        public System.Windows.Forms.TextBox txtSistolica;
        public System.Windows.Forms.TextBox txtDiastolica;
        public System.Windows.Forms.TextBox txtBpm;
        public System.Windows.Forms.TextBox txtRpm;
        public System.Windows.Forms.TextBox txtOximetria;
        public System.Windows.Forms.TextBox txtGrau;
        public System.Windows.Forms.RichTextBox txtObs;
        public System.Windows.Forms.Button btnGravar;
        public System.Windows.Forms.Button btnLimpar;
        public userControlPaciente userControlPaciente2;
        public System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label lblNomePaciente;
        public System.Windows.Forms.TextBox txtNomePaciente;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtHgt;
        public System.Windows.Forms.Button btnInativar;
        public System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label11;
    }
}