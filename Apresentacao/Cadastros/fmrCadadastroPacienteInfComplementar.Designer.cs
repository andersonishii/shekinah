﻿namespace SistemaShekinah.Apresentacao.Cadastros
{
    partial class fmrCadadastroPacienteInfComplementar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNomePaciente = new System.Windows.Forms.TextBox();
            this.chkAtivo = new System.Windows.Forms.CheckBox();
            this.chkEhDuploClique = new System.Windows.Forms.CheckBox();
            this.txtCodigoPaciente = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cbxPaciente = new SistemaShekinah.Apresentacao.userControlPaciente();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.txtObsPlanoSaude = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCarenciaCirurgia = new System.Windows.Forms.TextBox();
            this.txtCarenciaConsulta = new System.Windows.Forms.TextBox();
            this.txtCarenciaOutros = new System.Windows.Forms.TextBox();
            this.unidadeTempoCirurgia = new SistemaShekinah.Apresentacao.User_control.UserControlUnidadeDeTempo();
            this.unidadeTempoConsulta = new SistemaShekinah.Apresentacao.User_control.UserControlUnidadeDeTempo();
            this.unidadeTempoOutros = new SistemaShekinah.Apresentacao.User_control.UserControlUnidadeDeTempo();
            this.txtUnidTempoCirurgia = new System.Windows.Forms.TextBox();
            this.txtUnidTempoConsulta = new System.Windows.Forms.TextBox();
            this.txtUnidTempoOutros = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label28 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNomePlano = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.txtObsAlergiaMedicacao = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.lbxListaRestricaoMedicacao = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.btnRemover = new System.Windows.Forms.Button();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAdicionarAlergia = new System.Windows.Forms.Button();
            this.txtRestricaoMedicacao = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.txtNecessidadeEspecial = new System.Windows.Forms.RichTextBox();
            this.txtObsNecessidadeEspecial = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.txtRestricaoAlimentar = new System.Windows.Forms.RichTextBox();
            this.txtObsRestricaoAlimentar = new System.Windows.Forms.RichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.txtAlergias = new System.Windows.Forms.RichTextBox();
            this.txtObsAlergias = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.label19 = new System.Windows.Forms.Label();
            this.txtObsComposicaoFamiliar = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.label18 = new System.Windows.Forms.Label();
            this.txtComposiçãoFamiliar = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAtualizarUsuario = new System.Windows.Forms.Button();
            this.btnInativar = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 91F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 121F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 121F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 148F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtNomePaciente, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkAtivo, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkEhDuploClique, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtCodigoPaciente, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label17, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 39);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome Paciente";
            this.label1.Visible = false;
            // 
            // txtNomePaciente
            // 
            this.txtNomePaciente.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtNomePaciente.Location = new System.Drawing.Point(98, 9);
            this.txtNomePaciente.Name = "txtNomePaciente";
            this.txtNomePaciente.Size = new System.Drawing.Size(85, 20);
            this.txtNomePaciente.TabIndex = 1;
            this.txtNomePaciente.Visible = false;
            // 
            // chkAtivo
            // 
            this.chkAtivo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.chkAtivo.AutoSize = true;
            this.chkAtivo.Checked = true;
            this.chkAtivo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAtivo.Location = new System.Drawing.Point(698, 11);
            this.chkAtivo.Name = "chkAtivo";
            this.chkAtivo.Size = new System.Drawing.Size(56, 17);
            this.chkAtivo.TabIndex = 2;
            this.chkAtivo.Text = "Ativo?";
            this.chkAtivo.UseVisualStyleBackColor = true;
            // 
            // chkEhDuploClique
            // 
            this.chkEhDuploClique.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.chkEhDuploClique.AutoSize = true;
            this.chkEhDuploClique.Location = new System.Drawing.Point(554, 11);
            this.chkEhDuploClique.Name = "chkEhDuploClique";
            this.chkEhDuploClique.Size = new System.Drawing.Size(92, 17);
            this.chkEhDuploClique.TabIndex = 3;
            this.chkEhDuploClique.Text = "è duplo clique";
            this.chkEhDuploClique.UseVisualStyleBackColor = true;
            this.chkEhDuploClique.Visible = false;
            // 
            // txtCodigoPaciente
            // 
            this.txtCodigoPaciente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtCodigoPaciente.Location = new System.Drawing.Point(438, 9);
            this.txtCodigoPaciente.Name = "txtCodigoPaciente";
            this.txtCodigoPaciente.Size = new System.Drawing.Size(100, 20);
            this.txtCodigoPaciente.TabIndex = 5;
            this.txtCodigoPaciente.Visible = false;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(310, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Cod Paciente";
            this.label17.Visible = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.cbxPaciente, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 39);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(800, 38);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // cbxPaciente
            // 
            this.cbxPaciente.Location = new System.Drawing.Point(3, 3);
            this.cbxPaciente.Name = "cbxPaciente";
            this.cbxPaciente.Size = new System.Drawing.Size(668, 32);
            this.cbxPaciente.TabIndex = 0;
            this.cbxPaciente.Load += new System.EventHandler(this.cbxPaciente_Load);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.tabControl1, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 77);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(800, 310);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(794, 304);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel10);
            this.tabPage2.Controls.Add(this.tableLayoutPanel9);
            this.tabPage2.Controls.Add(this.tableLayoutPanel8);
            this.tabPage2.Controls.Add(this.tableLayoutPanel7);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(786, 278);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Plano de Saude";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.46154F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 86.53846F));
            this.tableLayoutPanel10.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtObsPlanoSaude, 1, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 167);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(780, 100);
            this.tableLayoutPanel10.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Observações:";
            // 
            // txtObsPlanoSaude
            // 
            this.txtObsPlanoSaude.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObsPlanoSaude.Location = new System.Drawing.Point(108, 3);
            this.txtObsPlanoSaude.Name = "txtObsPlanoSaude";
            this.txtObsPlanoSaude.Size = new System.Drawing.Size(669, 94);
            this.txtObsPlanoSaude.TabIndex = 1;
            this.txtObsPlanoSaude.Text = "";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 4;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.54008F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.45992F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 337F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel9.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.txtCarenciaCirurgia, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtCarenciaConsulta, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.txtCarenciaOutros, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.unidadeTempoCirurgia, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.unidadeTempoConsulta, 2, 1);
            this.tableLayoutPanel9.Controls.Add(this.unidadeTempoOutros, 2, 2);
            this.tableLayoutPanel9.Controls.Add(this.txtUnidTempoCirurgia, 3, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtUnidTempoConsulta, 3, 1);
            this.tableLayoutPanel9.Controls.Add(this.txtUnidTempoOutros, 3, 2);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 65);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 3;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(780, 102);
            this.tableLayoutPanel9.TabIndex = 2;
            this.tableLayoutPanel9.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel9_Paint);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Cirurgia";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Consulta";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 26);
            this.label7.TabIndex = 2;
            this.label7.Text = "Procedimentos Especiais";
            // 
            // txtCarenciaCirurgia
            // 
            this.txtCarenciaCirurgia.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtCarenciaCirurgia.Location = new System.Drawing.Point(112, 6);
            this.txtCarenciaCirurgia.Name = "txtCarenciaCirurgia";
            this.txtCarenciaCirurgia.Size = new System.Drawing.Size(77, 20);
            this.txtCarenciaCirurgia.TabIndex = 3;
            this.txtCarenciaCirurgia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // txtCarenciaConsulta
            // 
            this.txtCarenciaConsulta.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtCarenciaConsulta.Location = new System.Drawing.Point(112, 38);
            this.txtCarenciaConsulta.Name = "txtCarenciaConsulta";
            this.txtCarenciaConsulta.Size = new System.Drawing.Size(77, 20);
            this.txtCarenciaConsulta.TabIndex = 4;
            this.txtCarenciaConsulta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox2_KeyPress);
            // 
            // txtCarenciaOutros
            // 
            this.txtCarenciaOutros.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtCarenciaOutros.Location = new System.Drawing.Point(112, 73);
            this.txtCarenciaOutros.Name = "txtCarenciaOutros";
            this.txtCarenciaOutros.Size = new System.Drawing.Size(77, 20);
            this.txtCarenciaOutros.TabIndex = 5;
            this.txtCarenciaOutros.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // unidadeTempoCirurgia
            // 
            this.unidadeTempoCirurgia.Location = new System.Drawing.Point(195, 3);
            this.unidadeTempoCirurgia.Name = "unidadeTempoCirurgia";
            this.unidadeTempoCirurgia.Size = new System.Drawing.Size(182, 26);
            this.unidadeTempoCirurgia.TabIndex = 6;
            // 
            // unidadeTempoConsulta
            // 
            this.unidadeTempoConsulta.Location = new System.Drawing.Point(195, 35);
            this.unidadeTempoConsulta.Name = "unidadeTempoConsulta";
            this.unidadeTempoConsulta.Size = new System.Drawing.Size(182, 26);
            this.unidadeTempoConsulta.TabIndex = 7;
            // 
            // unidadeTempoOutros
            // 
            this.unidadeTempoOutros.Location = new System.Drawing.Point(195, 67);
            this.unidadeTempoOutros.Name = "unidadeTempoOutros";
            this.unidadeTempoOutros.Size = new System.Drawing.Size(182, 30);
            this.unidadeTempoOutros.TabIndex = 8;
            // 
            // txtUnidTempoCirurgia
            // 
            this.txtUnidTempoCirurgia.Location = new System.Drawing.Point(532, 3);
            this.txtUnidTempoCirurgia.Name = "txtUnidTempoCirurgia";
            this.txtUnidTempoCirurgia.Size = new System.Drawing.Size(100, 20);
            this.txtUnidTempoCirurgia.TabIndex = 9;
            this.txtUnidTempoCirurgia.Visible = false;
            // 
            // txtUnidTempoConsulta
            // 
            this.txtUnidTempoConsulta.Location = new System.Drawing.Point(532, 35);
            this.txtUnidTempoConsulta.Name = "txtUnidTempoConsulta";
            this.txtUnidTempoConsulta.Size = new System.Drawing.Size(100, 20);
            this.txtUnidTempoConsulta.TabIndex = 10;
            this.txtUnidTempoConsulta.Visible = false;
            // 
            // txtUnidTempoOutros
            // 
            this.txtUnidTempoOutros.Location = new System.Drawing.Point(532, 67);
            this.txtUnidTempoOutros.Name = "txtUnidTempoOutros";
            this.txtUnidTempoOutros.Size = new System.Drawing.Size(100, 20);
            this.txtUnidTempoOutros.TabIndex = 11;
            this.txtUnidTempoOutros.Visible = false;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.label28, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 37);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(780, 28);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(361, 7);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(57, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "Carência";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.94872F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 87.05128F));
            this.tableLayoutPanel7.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtNomePlano, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(780, 34);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nome do Plano";
            // 
            // txtNomePlano
            // 
            this.txtNomePlano.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtNomePlano.Location = new System.Drawing.Point(104, 7);
            this.txtNomePlano.Name = "txtNomePlano";
            this.txtNomePlano.Size = new System.Drawing.Size(431, 20);
            this.txtNomePlano.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tableLayoutPanel15);
            this.tabPage3.Controls.Add(this.tableLayoutPanel12);
            this.tabPage3.Controls.Add(this.tableLayoutPanel13);
            this.tabPage3.Controls.Add(this.tableLayoutPanel11);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(786, 278);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Alergias a Medicação";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.20513F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 81.79487F));
            this.tableLayoutPanel15.Controls.Add(this.txtObsAlergiaMedicacao, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 174);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(780, 100);
            this.tableLayoutPanel15.TabIndex = 4;
            // 
            // txtObsAlergiaMedicacao
            // 
            this.txtObsAlergiaMedicacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObsAlergiaMedicacao.Location = new System.Drawing.Point(145, 3);
            this.txtObsAlergiaMedicacao.Name = "txtObsAlergiaMedicacao";
            this.txtObsAlergiaMedicacao.Size = new System.Drawing.Size(632, 94);
            this.txtObsAlergiaMedicacao.TabIndex = 0;
            this.txtObsAlergiaMedicacao.Text = "";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 43);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Observações:";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.lbxListaRestricaoMedicacao, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel14, 1, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 74);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(780, 100);
            this.tableLayoutPanel12.TabIndex = 3;
            // 
            // lbxListaRestricaoMedicacao
            // 
            this.lbxListaRestricaoMedicacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbxListaRestricaoMedicacao.FormattingEnabled = true;
            this.lbxListaRestricaoMedicacao.Location = new System.Drawing.Point(3, 3);
            this.lbxListaRestricaoMedicacao.Name = "lbxListaRestricaoMedicacao";
            this.lbxListaRestricaoMedicacao.Size = new System.Drawing.Size(384, 94);
            this.lbxListaRestricaoMedicacao.TabIndex = 0;
            this.lbxListaRestricaoMedicacao.SelectedIndexChanged += new System.EventHandler(this.lbxListaRestricaoMedicacao_SelectedIndexChanged);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Controls.Add(this.btnRemover, 0, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(393, 3);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 3;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(384, 94);
            this.tableLayoutPanel14.TabIndex = 1;
            // 
            // btnRemover
            // 
            this.btnRemover.BackColor = System.Drawing.Color.DarkGray;
            this.btnRemover.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemover.Location = new System.Drawing.Point(3, 3);
            this.btnRemover.Name = "btnRemover";
            this.btnRemover.Size = new System.Drawing.Size(75, 25);
            this.btnRemover.TabIndex = 0;
            this.btnRemover.Text = "Remover";
            this.btnRemover.UseVisualStyleBackColor = false;
            this.btnRemover.Click += new System.EventHandler(this.btnRemover_Click);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 217F));
            this.tableLayoutPanel13.Controls.Add(this.btnAdicionarAlergia, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.txtRestricaoMedicacao, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 39);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(780, 35);
            this.tableLayoutPanel13.TabIndex = 2;
            this.tableLayoutPanel13.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel13_Paint);
            // 
            // btnAdicionarAlergia
            // 
            this.btnAdicionarAlergia.BackColor = System.Drawing.Color.DarkGray;
            this.btnAdicionarAlergia.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdicionarAlergia.Location = new System.Drawing.Point(566, 3);
            this.btnAdicionarAlergia.Name = "btnAdicionarAlergia";
            this.btnAdicionarAlergia.Size = new System.Drawing.Size(137, 29);
            this.btnAdicionarAlergia.TabIndex = 1;
            this.btnAdicionarAlergia.Text = "Adicionar Alergia";
            this.btnAdicionarAlergia.UseVisualStyleBackColor = false;
            this.btnAdicionarAlergia.Click += new System.EventHandler(this.btnAdicionarAlergia_Click);
            // 
            // txtRestricaoMedicacao
            // 
            this.txtRestricaoMedicacao.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtRestricaoMedicacao.Location = new System.Drawing.Point(3, 7);
            this.txtRestricaoMedicacao.Name = "txtRestricaoMedicacao";
            this.txtRestricaoMedicacao.Size = new System.Drawing.Size(557, 20);
            this.txtRestricaoMedicacao.TabIndex = 2;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(780, 36);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(232, 11);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(315, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Digite abaixo as restrições de medicação do paciente.";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tableLayoutPanel16);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(786, 278);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Necessidades Especiais";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 2;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.92308F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.07692F));
            this.tableLayoutPanel16.Controls.Add(this.txtNecessidadeEspecial, 1, 0);
            this.tableLayoutPanel16.Controls.Add(this.txtObsNecessidadeEspecial, 1, 1);
            this.tableLayoutPanel16.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.label12, 0, 1);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(780, 272);
            this.tableLayoutPanel16.TabIndex = 0;
            // 
            // txtNecessidadeEspecial
            // 
            this.txtNecessidadeEspecial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNecessidadeEspecial.Location = new System.Drawing.Point(135, 3);
            this.txtNecessidadeEspecial.Name = "txtNecessidadeEspecial";
            this.txtNecessidadeEspecial.Size = new System.Drawing.Size(642, 130);
            this.txtNecessidadeEspecial.TabIndex = 0;
            this.txtNecessidadeEspecial.Text = "";
            // 
            // txtObsNecessidadeEspecial
            // 
            this.txtObsNecessidadeEspecial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObsNecessidadeEspecial.Location = new System.Drawing.Point(135, 139);
            this.txtObsNecessidadeEspecial.Name = "txtObsNecessidadeEspecial";
            this.txtObsNecessidadeEspecial.Size = new System.Drawing.Size(642, 130);
            this.txtObsNecessidadeEspecial.TabIndex = 1;
            this.txtObsNecessidadeEspecial.Text = "";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 61);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Digite as Necessidades";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 197);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Observações:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tableLayoutPanel17);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(786, 278);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Restrição de Alimentos";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.30769F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.69231F));
            this.tableLayoutPanel17.Controls.Add(this.txtRestricaoAlimentar, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.txtObsRestricaoAlimentar, 1, 1);
            this.tableLayoutPanel17.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.label14, 0, 1);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(780, 272);
            this.tableLayoutPanel17.TabIndex = 0;
            // 
            // txtRestricaoAlimentar
            // 
            this.txtRestricaoAlimentar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRestricaoAlimentar.Location = new System.Drawing.Point(137, 3);
            this.txtRestricaoAlimentar.Name = "txtRestricaoAlimentar";
            this.txtRestricaoAlimentar.Size = new System.Drawing.Size(640, 130);
            this.txtRestricaoAlimentar.TabIndex = 0;
            this.txtRestricaoAlimentar.Text = "";
            // 
            // txtObsRestricaoAlimentar
            // 
            this.txtObsRestricaoAlimentar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObsRestricaoAlimentar.Location = new System.Drawing.Point(137, 139);
            this.txtObsRestricaoAlimentar.Name = "txtObsRestricaoAlimentar";
            this.txtObsRestricaoAlimentar.Size = new System.Drawing.Size(640, 130);
            this.txtObsRestricaoAlimentar.TabIndex = 1;
            this.txtObsRestricaoAlimentar.Text = "";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 26);
            this.label13.TabIndex = 2;
            this.label13.Text = "Digite as Restrições Alimentares";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 197);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Observações:";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tableLayoutPanel18);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(786, 278);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Alergias";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 2;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.69231F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.30769F));
            this.tableLayoutPanel18.Controls.Add(this.txtAlergias, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.txtObsAlergias, 1, 1);
            this.tableLayoutPanel18.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.label16, 0, 1);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 2;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(780, 272);
            this.tableLayoutPanel18.TabIndex = 0;
            // 
            // txtAlergias
            // 
            this.txtAlergias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAlergias.Location = new System.Drawing.Point(180, 3);
            this.txtAlergias.Name = "txtAlergias";
            this.txtAlergias.Size = new System.Drawing.Size(597, 130);
            this.txtAlergias.TabIndex = 0;
            this.txtAlergias.Text = "";
            // 
            // txtObsAlergias
            // 
            this.txtObsAlergias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObsAlergias.Location = new System.Drawing.Point(180, 139);
            this.txtObsAlergias.Name = "txtObsAlergias";
            this.txtObsAlergias.Size = new System.Drawing.Size(597, 130);
            this.txtObsAlergias.TabIndex = 1;
            this.txtObsAlergias.Text = "";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 61);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(146, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Digite as alergias do paciente";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 197);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "Observações:";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.tableLayoutPanel21);
            this.tabPage7.Controls.Add(this.tableLayoutPanel20);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(786, 278);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Composição Familiar";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 2;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.92308F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 88.07692F));
            this.tableLayoutPanel21.Controls.Add(this.label19, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.txtObsComposicaoFamiliar, 1, 0);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(3, 58);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(780, 200);
            this.tableLayoutPanel21.TabIndex = 2;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 93);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Observações:";
            // 
            // txtObsComposicaoFamiliar
            // 
            this.txtObsComposicaoFamiliar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObsComposicaoFamiliar.Location = new System.Drawing.Point(96, 3);
            this.txtObsComposicaoFamiliar.Name = "txtObsComposicaoFamiliar";
            this.txtObsComposicaoFamiliar.Size = new System.Drawing.Size(681, 194);
            this.txtObsComposicaoFamiliar.TabIndex = 1;
            this.txtObsComposicaoFamiliar.Text = "";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.05128F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 87.94872F));
            this.tableLayoutPanel20.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.txtComposiçãoFamiliar, 1, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(780, 55);
            this.tableLayoutPanel20.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 14);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 26);
            this.label18.TabIndex = 0;
            this.label18.Text = "Composição Familiar";
            // 
            // txtComposiçãoFamiliar
            // 
            this.txtComposiçãoFamiliar.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtComposiçãoFamiliar.Location = new System.Drawing.Point(96, 17);
            this.txtComposiçãoFamiliar.Name = "txtComposiçãoFamiliar";
            this.txtComposiçãoFamiliar.Size = new System.Drawing.Size(100, 20);
            this.txtComposiçãoFamiliar.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.88889F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.11111F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 621F));
            this.tableLayoutPanel4.Controls.Add(this.btnCadastrar, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnLimpar, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 387);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(800, 35);
            this.tableLayoutPanel4.TabIndex = 3;
            this.tableLayoutPanel4.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel4_Paint);
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCadastrar.AutoSize = true;
            this.btnCadastrar.BackColor = System.Drawing.Color.DarkGray;
            this.btnCadastrar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar.Location = new System.Drawing.Point(12, 4);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(71, 27);
            this.btnCadastrar.TabIndex = 0;
            this.btnCadastrar.Text = "Cadastrar";
            this.btnCadastrar.UseVisualStyleBackColor = false;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnLimpar.AutoSize = true;
            this.btnLimpar.BackColor = System.Drawing.Color.DarkGray;
            this.btnLimpar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.Location = new System.Drawing.Point(108, 4);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(58, 27);
            this.btnLimpar.TabIndex = 1;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = false;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 3;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 590F));
            this.tableLayoutPanel19.Controls.Add(this.btnAtualizarUsuario, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.btnInativar, 1, 0);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(0, 422);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 1;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(800, 39);
            this.tableLayoutPanel19.TabIndex = 4;
            // 
            // btnAtualizarUsuario
            // 
            this.btnAtualizarUsuario.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAtualizarUsuario.BackColor = System.Drawing.Color.DarkGray;
            this.btnAtualizarUsuario.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizarUsuario.Location = new System.Drawing.Point(15, 4);
            this.btnAtualizarUsuario.Name = "btnAtualizarUsuario";
            this.btnAtualizarUsuario.Size = new System.Drawing.Size(75, 30);
            this.btnAtualizarUsuario.TabIndex = 0;
            this.btnAtualizarUsuario.Text = "Atualizar";
            this.btnAtualizarUsuario.UseVisualStyleBackColor = false;
            this.btnAtualizarUsuario.Visible = false;
            this.btnAtualizarUsuario.Click += new System.EventHandler(this.btnAtualizarUsuario_Click);
            // 
            // btnInativar
            // 
            this.btnInativar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnInativar.BackColor = System.Drawing.Color.DarkGray;
            this.btnInativar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInativar.Location = new System.Drawing.Point(120, 4);
            this.btnInativar.Name = "btnInativar";
            this.btnInativar.Size = new System.Drawing.Size(75, 30);
            this.btnInativar.TabIndex = 1;
            this.btnInativar.Text = "Inativar";
            this.btnInativar.UseVisualStyleBackColor = false;
            this.btnInativar.Visible = false;
            this.btnInativar.Click += new System.EventHandler(this.btnInativar_Click);
            // 
            // fmrCadadastroPacienteInfComplementar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(800, 467);
            this.Controls.Add(this.tableLayoutPanel19);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "fmrCadadastroPacienteInfComplementar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Informações Complementares do Paciente";
            this.Load += new System.EventHandler(this.fmrCadadastroPacienteInfComplementar_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel19.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.TextBox txtNomePaciente;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.CheckBox chkAtivo;
        public userControlPaciente cbxPaciente;
        public System.Windows.Forms.Button btnCadastrar;
        public System.Windows.Forms.Button btnLimpar;
        public System.Windows.Forms.CheckBox chkEhDuploClique;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtCodigoPaciente;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        public System.Windows.Forms.Button btnAtualizarUsuario;
        public System.Windows.Forms.Button btnInativar;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.RichTextBox txtObsPlanoSaude;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtCarenciaCirurgia;
        public System.Windows.Forms.TextBox txtCarenciaConsulta;
        public System.Windows.Forms.TextBox txtCarenciaOutros;
        public User_control.UserControlUnidadeDeTempo unidadeTempoCirurgia;
        public User_control.UserControlUnidadeDeTempo unidadeTempoConsulta;
        public User_control.UserControlUnidadeDeTempo unidadeTempoOutros;
        public System.Windows.Forms.TextBox txtUnidTempoCirurgia;
        public System.Windows.Forms.TextBox txtUnidTempoConsulta;
        public System.Windows.Forms.TextBox txtUnidTempoOutros;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtNomePlano;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        public System.Windows.Forms.RichTextBox txtObsAlergiaMedicacao;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        public System.Windows.Forms.ListBox lbxListaRestricaoMedicacao;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Button btnRemover;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Button btnAdicionarAlergia;
        public System.Windows.Forms.TextBox txtRestricaoMedicacao;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        public System.Windows.Forms.RichTextBox txtNecessidadeEspecial;
        public System.Windows.Forms.RichTextBox txtObsNecessidadeEspecial;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        public System.Windows.Forms.RichTextBox txtRestricaoAlimentar;
        public System.Windows.Forms.RichTextBox txtObsRestricaoAlimentar;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        public System.Windows.Forms.RichTextBox txtAlergias;
        public System.Windows.Forms.RichTextBox txtObsAlergias;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.RichTextBox txtObsComposicaoFamiliar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txtComposiçãoFamiliar;
    }
}