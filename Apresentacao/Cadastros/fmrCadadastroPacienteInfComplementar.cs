﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrCadadastroPacienteInfComplementar : Form
    {

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        SqlDataReader dr;
        DataTable dt;


        Controle control = new Controle();
        Util util = new Util();
        public fmrCadadastroPacienteInfComplementar()
        {
            InitializeComponent();
        }

        private void txtComposiçãoFamiliar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                MessageBox.Show("O campo aceita apenas números");
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                MessageBox.Show("O campo aceita apenas números");
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                MessageBox.Show("O campo aceita apenas números");
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                MessageBox.Show("O campo aceita apenas números");
            }
        }

        private void tableLayoutPanel12_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel13_Paint(object sender, PaintEventArgs e)
        {

        }

        private void fmrCadadastroPacienteInfComplementar_Load(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage2;
            if ((!string.IsNullOrWhiteSpace(txtNomePaciente.Text)))
            {
                cbxPaciente.cmbPaciente.SelectedIndex = cbxPaciente.cmbPaciente.FindStringExact(txtNomePaciente.Text);
            }
            if (!chkAtivo.Checked)
            {
                btnInativar.Text = "Ativar";
            }





            if ((!string.IsNullOrWhiteSpace(txtUnidTempoCirurgia.Text)) && (!string.IsNullOrWhiteSpace(txtUnidTempoConsulta.Text)) && (!string.IsNullOrWhiteSpace(txtUnidTempoOutros.Text)))
            {

                unidadeTempoCirurgia.cmbUnidadeTempo.SelectedIndex = unidadeTempoCirurgia.cmbUnidadeTempo.FindStringExact(txtUnidTempoCirurgia.Text);
                unidadeTempoConsulta.cmbUnidadeTempo.SelectedIndex = unidadeTempoCirurgia.cmbUnidadeTempo.FindStringExact(txtUnidTempoConsulta.Text);
                unidadeTempoOutros.cmbUnidadeTempo.SelectedIndex = unidadeTempoCirurgia.cmbUnidadeTempo.FindStringExact(txtUnidTempoOutros.Text);
            }

            if (chkEhDuploClique.Checked)
            {
                cmd = new SqlCommand();
                cmd.CommandText = "select nome_medicamento_restricao,observacao from  paciente_restricao_medicacao where idpaciente=" + cbxPaciente.SelectPaciente.Id + "   ";

                try
                {
                    cmd.Connection = con.conectar();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        string nomeMedicamentoRestricao = dr["nome_medicamento_restricao"].ToString();
                        string obsRestricaoMedicacao = dr["observacao"].ToString();
                        lbxListaRestricaoMedicacao.Items.Add(nomeMedicamentoRestricao);
                        txtObsAlergiaMedicacao.Text = obsRestricaoMedicacao;

                    }
                    dr.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    con.desconectar();
                }

            }



        }

        public bool checaSeExiste(string textoprocurado)
        {
            if (lbxListaRestricaoMedicacao.Items.Cast<Object>().Any(x => x.ToString() == textoprocurado))
            {
                return false;

            }
            else
            {
                return true;
            }
        }

        private void btnAdicionarAlergia_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtRestricaoMedicacao.Text) && (checaSeExiste(txtRestricaoMedicacao.Text)))
            {
                lbxListaRestricaoMedicacao.Items.Add(txtRestricaoMedicacao.Text);
                txtRestricaoMedicacao.Clear();
                txtRestricaoMedicacao.Focus();
            }
            else
            {
                MessageBox.Show("Por favor digite no campo , antes de clicar nesse botão ou verifique se o item já foi digitado");
            }
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            try
            {
                if (lbxListaRestricaoMedicacao.Items.Count > 0)
                {
                    lbxListaRestricaoMedicacao.Items.RemoveAt(lbxListaRestricaoMedicacao.SelectedIndex);
                }
                else if (lbxListaRestricaoMedicacao.SelectedIndex == -1)
                {
                    MessageBox.Show("Selecione o item para removê-lo");
                }
                else
                {
                    MessageBox.Show("Não pode remover, pois o item não foi selecionado ou a lista de restrição está vazia");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ops. Ocorreu um erro. Tente remover novamente" + ex);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Valor" + checaSeExiste(txtRestricaoMedicacao.Text));
        }

        private void tableLayoutPanel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            foreach (string s in lbxListaRestricaoMedicacao.Items)
            {
                MessageBox.Show("O item da vez é" + s);
            }
        }

        public void limpaCampos()
        {
            txtNomePaciente.Clear();
            txtComposiçãoFamiliar.Clear();
            txtObsComposicaoFamiliar.Clear();
            txtNomePlano.Clear();
            txtCarenciaCirurgia.Clear();
            txtCarenciaConsulta.Clear();
            txtCarenciaOutros.Clear();
            txtUnidTempoCirurgia.Clear();
            txtUnidTempoConsulta.Clear();
            txtUnidTempoOutros.Clear();
            txtObsPlanoSaude.Clear();
            txtRestricaoMedicacao.Clear();
            lbxListaRestricaoMedicacao.Items.Clear();
            txtObsAlergiaMedicacao.Clear();
            txtNecessidadeEspecial.Clear();
            txtObsNecessidadeEspecial.Clear();
            txtRestricaoAlimentar.Clear();
            txtObsRestricaoAlimentar.Clear();
            txtAlergias.Clear();
            txtObsAlergias.Clear();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            var userInput = MessageBox.Show("Você tem certeza que deseja limpar todos os campos desse formulário?", "Decisão", MessageBoxButtons.YesNo);
            if (userInput == DialogResult.Yes)
            {
                limpaCampos();
            }
            else
            {
                MessageBox.Show("Nenhuma alteração foi realizada");
            }

        }



        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            AtualizaUsuario();
        }

        private void cbxPaciente_Load(object sender, EventArgs e)
        {

        }

        public bool VerificaAtivoInativo(bool ativoinativo)
        {

            if (ativoinativo == true)
            {
                return ativoinativo = true;
            }
            else
            {
                return ativoinativo = false;
            }
        }


        public bool CadastraRestricaoMedicamentos()
        {
            bool foicadastradocomSucesso;
            bool varcheck = false;
            //se a lista for igual a zero ele retorna true também pq senao ali na hora de cadsatrar sempre daria false
            if (lbxListaRestricaoMedicacao.Items.Count == 0)
            {
                return varcheck = true;
            }
            else
            {
                var userInput = MessageBox.Show("Deseja ocultar mensagem de sucesso de inserção no banco de dados ?", "Decisão", MessageBoxButtons.YesNo);

                for (int i = 0; i < lbxListaRestricaoMedicacao.Items.Count; i++)
                {
                    bool ativoinativo = VerificaAtivoInativo(chkAtivo.Checked);
                    int idpaciente = cbxPaciente.SelectPaciente.Id;
                    String nome_medicacao = lbxListaRestricaoMedicacao.Items[i].ToString();
                    String obsalergiamedicacao = txtObsAlergiaMedicacao.Text;
                    String mensagem = control.cadastraAlergiaMedicamentoPaciente(ativoinativo, idpaciente, nome_medicacao, obsalergiamedicacao);
                    if (control.tem)
                    {
                        if (userInput == DialogResult.No)
                        {
                            MessageBox.Show(mensagem, "Inserido", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            varcheck = true;
                        }
                        else
                        {
                            if (lbxListaRestricaoMedicacao.Items.Count - 1 == i)
                            {
                                MessageBox.Show(mensagem, "Todas as alergia de medicações ,  foram inseridas com sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                varcheck = true;
                            }
                            // varcheck = true;

                        }
                    }
                    else
                    {
                        MessageBox.Show(control.msg);
                        varcheck = false;
                    }
                }
                return foicadastradocomSucesso = varcheck;
            }
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnInativar_Click(object sender, EventArgs e)
        {
            this.Hide();
            fmrInativacaoPaciente inativapac = new fmrInativacaoPaciente();
            inativapac.txtNomePaciente.Text = txtNomePaciente.Text;
            inativapac.txtCodPaciente.Text = txtCodigoPaciente.Text;
            inativapac.chkAtivo.Checked = chkAtivo.Checked;
            inativapac.Show();
            this.Close();
        }

        public void AtualizaUsuario()
        {
            bool ativoinativo = VerificaAtivoInativo(chkAtivo.Checked);
            int idpaciente = cbxPaciente.SelectPaciente.Id;
            int ComposicaoFamiliar = Controle.ParseInt(txtComposiçãoFamiliar.Text, 0);
            String obsComposicaoFamiliar = txtObsComposicaoFamiliar.Text;
            String nomePlanoSaude = txtNomePlano.Text;
            int carenciaPlanoCirurgia = Controle.ParseInt(txtCarenciaCirurgia.Text, 0);
            int carenciaPlanoConsulta = Controle.ParseInt(txtCarenciaConsulta.Text, 0);
            int carenciaPlanoOutros = Controle.ParseInt(txtCarenciaOutros.Text, 0);
            String obsPlanoSaude = txtObsPlanoSaude.Text;
            String necessidadesEspeciais = txtNecessidadeEspecial.Text;
            String obsnecessidadesEspeciais = txtObsNecessidadeEspecial.Text;
            String restricoesAlimentares = txtRestricaoAlimentar.Text;
            String obsrestricoesAlimentares = txtObsRestricaoAlimentar.Text;
            String alergia = txtAlergias.Text;
            String obsalergia = txtObsAlergias.Text;
            String unidadeTempoPlanoCirurgia = unidadeTempoCirurgia.SelectUnidadeTempo.nomeUnidadeTempo.ToString();
            String unidadeTempoPlanoConsulta = unidadeTempoConsulta.SelectUnidadeTempo.nomeUnidadeTempo.ToString();
            String unidadeTempoPlanoOutros = unidadeTempoOutros.SelectUnidadeTempo.nomeUnidadeTempo.ToString();



            //priramente ele cadastrada a medicacao
            bool cadrestricaoMed = CadastraRestricaoMedicamentos();


            String mensagem = control.atualizaInformacaoComplementarUsuario(ativoinativo, idpaciente, ComposicaoFamiliar, obsComposicaoFamiliar, nomePlanoSaude, carenciaPlanoCirurgia, carenciaPlanoConsulta,
                carenciaPlanoOutros, obsPlanoSaude, necessidadesEspeciais, obsnecessidadesEspeciais, restricoesAlimentares, obsrestricoesAlimentares,
                alergia, obsalergia, unidadeTempoPlanoCirurgia, unidadeTempoPlanoConsulta, unidadeTempoPlanoOutros);
            if (control.tem && cadrestricaoMed)
            {
                MessageBox.Show(mensagem, "Atualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpaCampos();

            }
            else
            {
                MessageBox.Show(control.msg);
            }
        }

        private void btnAtualizarUsuario_Click(object sender, EventArgs e)
        {

            int verifica = util.VerificaAtivoInativo(chkAtivo.Checked);
            int idpaciente = Convert.ToInt32(txtCodigoPaciente.Text);
            String mensagem = control.deletaRestricaoMedicacaoPaciente(verifica, idpaciente);

            if (control.tem)
            {
                AtualizaUsuario();
                this.Close();

            }
            else
            {
                MessageBox.Show(control.msg);
            }


        }

        private void lbxListaRestricaoMedicacao_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}

