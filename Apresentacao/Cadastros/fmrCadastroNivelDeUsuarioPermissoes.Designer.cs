﻿namespace SistemaShekinah.Apresentacao.Cadastros
{
    partial class fmrCadastroNivelDeUsuarioPermissoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNomeNivel = new System.Windows.Forms.TextBox();
            this.chkAtivo = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlCadastroPaciente = new System.Windows.Forms.Panel();
            this.rdbPacienteProibir = new System.Windows.Forms.RadioButton();
            this.rdbPacienteApenasConsulta = new System.Windows.Forms.RadioButton();
            this.rdbPacienteTotal = new System.Windows.Forms.RadioButton();
            this.pnlAnexosPaciente = new System.Windows.Forms.Panel();
            this.rdbAnexoPacienteProibir = new System.Windows.Forms.RadioButton();
            this.rdbAnexoPacienteConsulta = new System.Windows.Forms.RadioButton();
            this.rdbAnexoPacienteTotal = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlAtividadeGrupo = new System.Windows.Forms.Panel();
            this.rdbAtividadeEmGrupoProibir = new System.Windows.Forms.RadioButton();
            this.rdbAtividadeEmGrupoConsulta = new System.Windows.Forms.RadioButton();
            this.rdbAtividadeEmGrupoTotal = new System.Windows.Forms.RadioButton();
            this.pnlInfComp = new System.Windows.Forms.Panel();
            this.rdbInfComplementarPacienteProibir = new System.Windows.Forms.RadioButton();
            this.rdbInfComplementarPacienteConsulta = new System.Windows.Forms.RadioButton();
            this.rdbInfComplementarPacienteTotal = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCadPaciente = new System.Windows.Forms.TextBox();
            this.txtAnexoPaciente = new System.Windows.Forms.TextBox();
            this.txtInfCompPac = new System.Windows.Forms.TextBox();
            this.txtAtividadeEmgrupo = new System.Windows.Forms.TextBox();
            this.pnlFotoPaciente = new System.Windows.Forms.Panel();
            this.rdbFotoProibir = new System.Windows.Forms.RadioButton();
            this.rdbFotoConsulta = new System.Windows.Forms.RadioButton();
            this.rdbFotoTotal = new System.Windows.Forms.RadioButton();
            this.txtFotoPaciente = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.pnlPosologia = new System.Windows.Forms.Panel();
            this.rdbPosologiaProibir = new System.Windows.Forms.RadioButton();
            this.rdbPosologiaConsulta = new System.Windows.Forms.RadioButton();
            this.rdbPosologiaTotal = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlEntradaSaida = new System.Windows.Forms.Panel();
            this.rdbEntradaSaidaProibir = new System.Windows.Forms.RadioButton();
            this.rdbEntradaSaidaConsulta = new System.Windows.Forms.RadioButton();
            this.rdbEntradaSaidaTotal = new System.Windows.Forms.RadioButton();
            this.pnlAgenda = new System.Windows.Forms.Panel();
            this.rdbAgendaProibir = new System.Windows.Forms.RadioButton();
            this.rdbAgendaConsulta = new System.Windows.Forms.RadioButton();
            this.rdbAgendaTotal = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAgenda = new System.Windows.Forms.TextBox();
            this.txtEntSaida = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlAvaliacao = new System.Windows.Forms.Panel();
            this.rdbAvaliacaoProibir = new System.Windows.Forms.RadioButton();
            this.rdbAvaliacaoConsulta = new System.Windows.Forms.RadioButton();
            this.rdbAvaliacaoTotal = new System.Windows.Forms.RadioButton();
            this.pnlAtendimento = new System.Windows.Forms.Panel();
            this.rdbAtendimentoProibir = new System.Windows.Forms.RadioButton();
            this.rdbAtendimentoConsulta = new System.Windows.Forms.RadioButton();
            this.rdbAtendimentoTotal = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtEvolucao = new System.Windows.Forms.TextBox();
            this.txtAvaliacao = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlMedicacao = new System.Windows.Forms.Panel();
            this.rdbMedicacaoProibir = new System.Windows.Forms.RadioButton();
            this.rdbMedicacaoConsulta = new System.Windows.Forms.RadioButton();
            this.rdbMedicacaoTotal = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtMedicacao = new System.Windows.Forms.TextBox();
            this.txttPosologia = new System.Windows.Forms.TextBox();
            this.pnlNivelUsuario = new System.Windows.Forms.Panel();
            this.rdbNivelUsuarioProibir = new System.Windows.Forms.RadioButton();
            this.rdbNivelUsuarioConsulta = new System.Windows.Forms.RadioButton();
            this.rdbNivelUsuarioTotal = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlCadastroUsuario = new System.Windows.Forms.Panel();
            this.rdbCadastroUsuarioProibir = new System.Windows.Forms.RadioButton();
            this.rdbCadastroUsuarioConsulta = new System.Windows.Forms.RadioButton();
            this.rdbCadastroUsuarioTotal = new System.Windows.Forms.RadioButton();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtCadUser = new System.Windows.Forms.TextBox();
            this.txtNivelUser = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.btnGravar = new System.Windows.Forms.Button();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label20 = new System.Windows.Forms.Label();
            this.txtIdNivelUser = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.pnlCadastroPaciente.SuspendLayout();
            this.pnlAnexosPaciente.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.pnlAtividadeGrupo.SuspendLayout();
            this.pnlInfComp.SuspendLayout();
            this.pnlFotoPaciente.SuspendLayout();
            this.pnlPosologia.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.pnlEntradaSaida.SuspendLayout();
            this.pnlAgenda.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.pnlAvaliacao.SuspendLayout();
            this.pnlAtendimento.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.pnlMedicacao.SuspendLayout();
            this.pnlNivelUsuario.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.pnlCadastroUsuario.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.83333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.16666F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 212F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtNomeNivel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkAtivo, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 39);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome Nivel";
            // 
            // txtNomeNivel
            // 
            this.txtNomeNivel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtNomeNivel.Location = new System.Drawing.Point(90, 9);
            this.txtNomeNivel.Name = "txtNomeNivel";
            this.txtNomeNivel.Size = new System.Drawing.Size(345, 20);
            this.txtNomeNivel.TabIndex = 1;
            // 
            // chkAtivo
            // 
            this.chkAtivo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkAtivo.AutoSize = true;
            this.chkAtivo.Checked = true;
            this.chkAtivo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAtivo.Location = new System.Drawing.Point(590, 11);
            this.chkAtivo.Name = "chkAtivo";
            this.chkAtivo.Size = new System.Drawing.Size(56, 17);
            this.chkAtivo.TabIndex = 2;
            this.chkAtivo.Text = "Ativo?";
            this.chkAtivo.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 39);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(800, 24);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(349, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Módulo Paciente";
            // 
            // pnlCadastroPaciente
            // 
            this.pnlCadastroPaciente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlCadastroPaciente.Controls.Add(this.rdbPacienteProibir);
            this.pnlCadastroPaciente.Controls.Add(this.rdbPacienteApenasConsulta);
            this.pnlCadastroPaciente.Controls.Add(this.rdbPacienteTotal);
            this.pnlCadastroPaciente.Location = new System.Drawing.Point(154, 8);
            this.pnlCadastroPaciente.Name = "pnlCadastroPaciente";
            this.pnlCadastroPaciente.Size = new System.Drawing.Size(441, 28);
            this.pnlCadastroPaciente.TabIndex = 0;
            // 
            // rdbPacienteProibir
            // 
            this.rdbPacienteProibir.AutoSize = true;
            this.rdbPacienteProibir.Checked = true;
            this.rdbPacienteProibir.Location = new System.Drawing.Point(169, 1);
            this.rdbPacienteProibir.Name = "rdbPacienteProibir";
            this.rdbPacienteProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbPacienteProibir.TabIndex = 2;
            this.rdbPacienteProibir.TabStop = true;
            this.rdbPacienteProibir.Text = "Proibir";
            this.rdbPacienteProibir.UseVisualStyleBackColor = true;
            // 
            // rdbPacienteApenasConsulta
            // 
            this.rdbPacienteApenasConsulta.AutoSize = true;
            this.rdbPacienteApenasConsulta.Location = new System.Drawing.Point(55, 1);
            this.rdbPacienteApenasConsulta.Name = "rdbPacienteApenasConsulta";
            this.rdbPacienteApenasConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbPacienteApenasConsulta.TabIndex = 1;
            this.rdbPacienteApenasConsulta.Text = "Apenas Consulta";
            this.rdbPacienteApenasConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbPacienteTotal
            // 
            this.rdbPacienteTotal.AutoSize = true;
            this.rdbPacienteTotal.Location = new System.Drawing.Point(0, -1);
            this.rdbPacienteTotal.Name = "rdbPacienteTotal";
            this.rdbPacienteTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbPacienteTotal.TabIndex = 0;
            this.rdbPacienteTotal.Text = "Total";
            this.rdbPacienteTotal.UseVisualStyleBackColor = true;
            this.rdbPacienteTotal.CheckedChanged += new System.EventHandler(this.rdbPacienteTotal_CheckedChanged);
            // 
            // pnlAnexosPaciente
            // 
            this.pnlAnexosPaciente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlAnexosPaciente.Controls.Add(this.rdbAnexoPacienteProibir);
            this.pnlAnexosPaciente.Controls.Add(this.rdbAnexoPacienteConsulta);
            this.pnlAnexosPaciente.Controls.Add(this.rdbAnexoPacienteTotal);
            this.pnlAnexosPaciente.Location = new System.Drawing.Point(154, 47);
            this.pnlAnexosPaciente.Name = "pnlAnexosPaciente";
            this.pnlAnexosPaciente.Size = new System.Drawing.Size(441, 22);
            this.pnlAnexosPaciente.TabIndex = 3;
            // 
            // rdbAnexoPacienteProibir
            // 
            this.rdbAnexoPacienteProibir.AutoSize = true;
            this.rdbAnexoPacienteProibir.Checked = true;
            this.rdbAnexoPacienteProibir.Location = new System.Drawing.Point(170, 0);
            this.rdbAnexoPacienteProibir.Name = "rdbAnexoPacienteProibir";
            this.rdbAnexoPacienteProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbAnexoPacienteProibir.TabIndex = 2;
            this.rdbAnexoPacienteProibir.TabStop = true;
            this.rdbAnexoPacienteProibir.Text = "Proibir";
            this.rdbAnexoPacienteProibir.UseVisualStyleBackColor = true;
            // 
            // rdbAnexoPacienteConsulta
            // 
            this.rdbAnexoPacienteConsulta.AutoSize = true;
            this.rdbAnexoPacienteConsulta.Location = new System.Drawing.Point(55, -2);
            this.rdbAnexoPacienteConsulta.Name = "rdbAnexoPacienteConsulta";
            this.rdbAnexoPacienteConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbAnexoPacienteConsulta.TabIndex = 1;
            this.rdbAnexoPacienteConsulta.Text = "Apenas Consulta";
            this.rdbAnexoPacienteConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbAnexoPacienteTotal
            // 
            this.rdbAnexoPacienteTotal.AutoSize = true;
            this.rdbAnexoPacienteTotal.Location = new System.Drawing.Point(0, 0);
            this.rdbAnexoPacienteTotal.Name = "rdbAnexoPacienteTotal";
            this.rdbAnexoPacienteTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbAnexoPacienteTotal.TabIndex = 0;
            this.rdbAnexoPacienteTotal.Text = "Total";
            this.rdbAnexoPacienteTotal.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.75F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 201F));
            this.tableLayoutPanel3.Controls.Add(this.pnlAtividadeGrupo, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.pnlInfComp, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.pnlCadastroPaciente, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.pnlAnexosPaciente, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtCadPaciente, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtAnexoPaciente, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtInfCompPac, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtAtividadeEmgrupo, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.pnlFotoPaciente, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtFotoPaciente, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.label19, 0, 4);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 63);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 61.53846F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.46154F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(800, 173);
            this.tableLayoutPanel3.TabIndex = 4;
            // 
            // pnlAtividadeGrupo
            // 
            this.pnlAtividadeGrupo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlAtividadeGrupo.Controls.Add(this.rdbAtividadeEmGrupoProibir);
            this.pnlAtividadeGrupo.Controls.Add(this.rdbAtividadeEmGrupoConsulta);
            this.pnlAtividadeGrupo.Controls.Add(this.rdbAtividadeEmGrupoTotal);
            this.pnlAtividadeGrupo.Location = new System.Drawing.Point(154, 113);
            this.pnlAtividadeGrupo.Name = "pnlAtividadeGrupo";
            this.pnlAtividadeGrupo.Size = new System.Drawing.Size(441, 17);
            this.pnlAtividadeGrupo.TabIndex = 3;
            // 
            // rdbAtividadeEmGrupoProibir
            // 
            this.rdbAtividadeEmGrupoProibir.AutoSize = true;
            this.rdbAtividadeEmGrupoProibir.Checked = true;
            this.rdbAtividadeEmGrupoProibir.Location = new System.Drawing.Point(170, 0);
            this.rdbAtividadeEmGrupoProibir.Name = "rdbAtividadeEmGrupoProibir";
            this.rdbAtividadeEmGrupoProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbAtividadeEmGrupoProibir.TabIndex = 2;
            this.rdbAtividadeEmGrupoProibir.TabStop = true;
            this.rdbAtividadeEmGrupoProibir.Text = "Proibir";
            this.rdbAtividadeEmGrupoProibir.UseVisualStyleBackColor = true;
            // 
            // rdbAtividadeEmGrupoConsulta
            // 
            this.rdbAtividadeEmGrupoConsulta.AutoSize = true;
            this.rdbAtividadeEmGrupoConsulta.Location = new System.Drawing.Point(55, 0);
            this.rdbAtividadeEmGrupoConsulta.Name = "rdbAtividadeEmGrupoConsulta";
            this.rdbAtividadeEmGrupoConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbAtividadeEmGrupoConsulta.TabIndex = 1;
            this.rdbAtividadeEmGrupoConsulta.Text = "Apenas Consulta";
            this.rdbAtividadeEmGrupoConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbAtividadeEmGrupoTotal
            // 
            this.rdbAtividadeEmGrupoTotal.AutoSize = true;
            this.rdbAtividadeEmGrupoTotal.Location = new System.Drawing.Point(1, 0);
            this.rdbAtividadeEmGrupoTotal.Name = "rdbAtividadeEmGrupoTotal";
            this.rdbAtividadeEmGrupoTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbAtividadeEmGrupoTotal.TabIndex = 0;
            this.rdbAtividadeEmGrupoTotal.Text = "Total";
            this.rdbAtividadeEmGrupoTotal.UseVisualStyleBackColor = true;
            // 
            // pnlInfComp
            // 
            this.pnlInfComp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlInfComp.Controls.Add(this.rdbInfComplementarPacienteProibir);
            this.pnlInfComp.Controls.Add(this.rdbInfComplementarPacienteConsulta);
            this.pnlInfComp.Controls.Add(this.rdbInfComplementarPacienteTotal);
            this.pnlInfComp.Location = new System.Drawing.Point(154, 75);
            this.pnlInfComp.Name = "pnlInfComp";
            this.pnlInfComp.Size = new System.Drawing.Size(441, 26);
            this.pnlInfComp.TabIndex = 4;
            // 
            // rdbInfComplementarPacienteProibir
            // 
            this.rdbInfComplementarPacienteProibir.AutoSize = true;
            this.rdbInfComplementarPacienteProibir.Checked = true;
            this.rdbInfComplementarPacienteProibir.Location = new System.Drawing.Point(170, 9);
            this.rdbInfComplementarPacienteProibir.Name = "rdbInfComplementarPacienteProibir";
            this.rdbInfComplementarPacienteProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbInfComplementarPacienteProibir.TabIndex = 2;
            this.rdbInfComplementarPacienteProibir.TabStop = true;
            this.rdbInfComplementarPacienteProibir.Text = "Proibir";
            this.rdbInfComplementarPacienteProibir.UseVisualStyleBackColor = true;
            // 
            // rdbInfComplementarPacienteConsulta
            // 
            this.rdbInfComplementarPacienteConsulta.AutoSize = true;
            this.rdbInfComplementarPacienteConsulta.Location = new System.Drawing.Point(55, 7);
            this.rdbInfComplementarPacienteConsulta.Name = "rdbInfComplementarPacienteConsulta";
            this.rdbInfComplementarPacienteConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbInfComplementarPacienteConsulta.TabIndex = 1;
            this.rdbInfComplementarPacienteConsulta.Text = "Apenas Consulta";
            this.rdbInfComplementarPacienteConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbInfComplementarPacienteTotal
            // 
            this.rdbInfComplementarPacienteTotal.AutoSize = true;
            this.rdbInfComplementarPacienteTotal.Location = new System.Drawing.Point(0, 7);
            this.rdbInfComplementarPacienteTotal.Name = "rdbInfComplementarPacienteTotal";
            this.rdbInfComplementarPacienteTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbInfComplementarPacienteTotal.TabIndex = 0;
            this.rdbInfComplementarPacienteTotal.Text = "Total";
            this.rdbInfComplementarPacienteTotal.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Cadastro de Paciente";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Anexos do Paciente";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Atividade Em Grupo";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(145, 26);
            this.label6.TabIndex = 4;
            this.label6.Text = "Informações Complementares do Paciente";
            // 
            // txtCadPaciente
            // 
            this.txtCadPaciente.Location = new System.Drawing.Point(601, 3);
            this.txtCadPaciente.Name = "txtCadPaciente";
            this.txtCadPaciente.Size = new System.Drawing.Size(100, 20);
            this.txtCadPaciente.TabIndex = 5;
            this.txtCadPaciente.Visible = false;
            // 
            // txtAnexoPaciente
            // 
            this.txtAnexoPaciente.Location = new System.Drawing.Point(601, 47);
            this.txtAnexoPaciente.Name = "txtAnexoPaciente";
            this.txtAnexoPaciente.Size = new System.Drawing.Size(100, 20);
            this.txtAnexoPaciente.TabIndex = 6;
            this.txtAnexoPaciente.Visible = false;
            // 
            // txtInfCompPac
            // 
            this.txtInfCompPac.Location = new System.Drawing.Point(601, 75);
            this.txtInfCompPac.Name = "txtInfCompPac";
            this.txtInfCompPac.Size = new System.Drawing.Size(100, 20);
            this.txtInfCompPac.TabIndex = 7;
            this.txtInfCompPac.Visible = false;
            // 
            // txtAtividadeEmgrupo
            // 
            this.txtAtividadeEmgrupo.Location = new System.Drawing.Point(601, 107);
            this.txtAtividadeEmgrupo.Name = "txtAtividadeEmgrupo";
            this.txtAtividadeEmgrupo.Size = new System.Drawing.Size(100, 20);
            this.txtAtividadeEmgrupo.TabIndex = 8;
            this.txtAtividadeEmgrupo.Visible = false;
            // 
            // pnlFotoPaciente
            // 
            this.pnlFotoPaciente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlFotoPaciente.Controls.Add(this.rdbFotoProibir);
            this.pnlFotoPaciente.Controls.Add(this.rdbFotoConsulta);
            this.pnlFotoPaciente.Controls.Add(this.rdbFotoTotal);
            this.pnlFotoPaciente.Location = new System.Drawing.Point(154, 147);
            this.pnlFotoPaciente.Name = "pnlFotoPaciente";
            this.pnlFotoPaciente.Size = new System.Drawing.Size(441, 17);
            this.pnlFotoPaciente.TabIndex = 9;
            // 
            // rdbFotoProibir
            // 
            this.rdbFotoProibir.AutoSize = true;
            this.rdbFotoProibir.Checked = true;
            this.rdbFotoProibir.Location = new System.Drawing.Point(170, 0);
            this.rdbFotoProibir.Name = "rdbFotoProibir";
            this.rdbFotoProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbFotoProibir.TabIndex = 2;
            this.rdbFotoProibir.TabStop = true;
            this.rdbFotoProibir.Text = "Proibir";
            this.rdbFotoProibir.UseVisualStyleBackColor = true;
            // 
            // rdbFotoConsulta
            // 
            this.rdbFotoConsulta.AutoSize = true;
            this.rdbFotoConsulta.Location = new System.Drawing.Point(55, 0);
            this.rdbFotoConsulta.Name = "rdbFotoConsulta";
            this.rdbFotoConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbFotoConsulta.TabIndex = 1;
            this.rdbFotoConsulta.Text = "Apenas Consulta";
            this.rdbFotoConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbFotoTotal
            // 
            this.rdbFotoTotal.AutoSize = true;
            this.rdbFotoTotal.Location = new System.Drawing.Point(1, 0);
            this.rdbFotoTotal.Name = "rdbFotoTotal";
            this.rdbFotoTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbFotoTotal.TabIndex = 0;
            this.rdbFotoTotal.Text = "Total";
            this.rdbFotoTotal.UseVisualStyleBackColor = true;
            // 
            // txtFotoPaciente
            // 
            this.txtFotoPaciente.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtFotoPaciente.Location = new System.Drawing.Point(601, 146);
            this.txtFotoPaciente.Name = "txtFotoPaciente";
            this.txtFotoPaciente.Size = new System.Drawing.Size(100, 20);
            this.txtFotoPaciente.TabIndex = 10;
            this.txtFotoPaciente.Visible = false;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 149);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 13);
            this.label19.TabIndex = 11;
            this.label19.Text = "Foto do Paciente";
            // 
            // pnlPosologia
            // 
            this.pnlPosologia.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlPosologia.Controls.Add(this.rdbPosologiaProibir);
            this.pnlPosologia.Controls.Add(this.rdbPosologiaConsulta);
            this.pnlPosologia.Controls.Add(this.rdbPosologiaTotal);
            this.pnlPosologia.Location = new System.Drawing.Point(154, 31);
            this.pnlPosologia.Name = "pnlPosologia";
            this.pnlPosologia.Size = new System.Drawing.Size(439, 19);
            this.pnlPosologia.TabIndex = 4;
            // 
            // rdbPosologiaProibir
            // 
            this.rdbPosologiaProibir.AutoSize = true;
            this.rdbPosologiaProibir.Checked = true;
            this.rdbPosologiaProibir.Location = new System.Drawing.Point(169, 3);
            this.rdbPosologiaProibir.Name = "rdbPosologiaProibir";
            this.rdbPosologiaProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbPosologiaProibir.TabIndex = 2;
            this.rdbPosologiaProibir.TabStop = true;
            this.rdbPosologiaProibir.Text = "Proibir";
            this.rdbPosologiaProibir.UseVisualStyleBackColor = true;
            // 
            // rdbPosologiaConsulta
            // 
            this.rdbPosologiaConsulta.AutoSize = true;
            this.rdbPosologiaConsulta.Location = new System.Drawing.Point(58, 3);
            this.rdbPosologiaConsulta.Name = "rdbPosologiaConsulta";
            this.rdbPosologiaConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbPosologiaConsulta.TabIndex = 1;
            this.rdbPosologiaConsulta.Text = "Apenas Consulta";
            this.rdbPosologiaConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbPosologiaTotal
            // 
            this.rdbPosologiaTotal.AutoSize = true;
            this.rdbPosologiaTotal.Location = new System.Drawing.Point(3, 3);
            this.rdbPosologiaTotal.Name = "rdbPosologiaTotal";
            this.rdbPosologiaTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbPosologiaTotal.TabIndex = 0;
            this.rdbPosologiaTotal.Text = "Total";
            this.rdbPosologiaTotal.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 236);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(800, 28);
            this.tableLayoutPanel4.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(317, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Módulo Agenda/ Anotações";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.375F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.625F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 202F));
            this.tableLayoutPanel5.Controls.Add(this.pnlEntradaSaida, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.pnlAgenda, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label9, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtAgenda, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtEntSaida, 2, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 264);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.98039F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.01961F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(800, 51);
            this.tableLayoutPanel5.TabIndex = 6;
            // 
            // pnlEntradaSaida
            // 
            this.pnlEntradaSaida.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlEntradaSaida.Controls.Add(this.rdbEntradaSaidaProibir);
            this.pnlEntradaSaida.Controls.Add(this.rdbEntradaSaidaConsulta);
            this.pnlEntradaSaida.Controls.Add(this.rdbEntradaSaidaTotal);
            this.pnlEntradaSaida.Location = new System.Drawing.Point(154, 28);
            this.pnlEntradaSaida.Name = "pnlEntradaSaida";
            this.pnlEntradaSaida.Size = new System.Drawing.Size(440, 19);
            this.pnlEntradaSaida.TabIndex = 5;
            // 
            // rdbEntradaSaidaProibir
            // 
            this.rdbEntradaSaidaProibir.AutoSize = true;
            this.rdbEntradaSaidaProibir.Checked = true;
            this.rdbEntradaSaidaProibir.Location = new System.Drawing.Point(169, 6);
            this.rdbEntradaSaidaProibir.Name = "rdbEntradaSaidaProibir";
            this.rdbEntradaSaidaProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbEntradaSaidaProibir.TabIndex = 2;
            this.rdbEntradaSaidaProibir.TabStop = true;
            this.rdbEntradaSaidaProibir.Text = "Proibir";
            this.rdbEntradaSaidaProibir.UseVisualStyleBackColor = true;
            // 
            // rdbEntradaSaidaConsulta
            // 
            this.rdbEntradaSaidaConsulta.AutoSize = true;
            this.rdbEntradaSaidaConsulta.Location = new System.Drawing.Point(58, 3);
            this.rdbEntradaSaidaConsulta.Name = "rdbEntradaSaidaConsulta";
            this.rdbEntradaSaidaConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbEntradaSaidaConsulta.TabIndex = 1;
            this.rdbEntradaSaidaConsulta.Text = "Apenas Consulta";
            this.rdbEntradaSaidaConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbEntradaSaidaTotal
            // 
            this.rdbEntradaSaidaTotal.AutoSize = true;
            this.rdbEntradaSaidaTotal.Location = new System.Drawing.Point(2, 3);
            this.rdbEntradaSaidaTotal.Name = "rdbEntradaSaidaTotal";
            this.rdbEntradaSaidaTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbEntradaSaidaTotal.TabIndex = 0;
            this.rdbEntradaSaidaTotal.Text = "Total";
            this.rdbEntradaSaidaTotal.UseVisualStyleBackColor = true;
            // 
            // pnlAgenda
            // 
            this.pnlAgenda.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlAgenda.Controls.Add(this.rdbAgendaProibir);
            this.pnlAgenda.Controls.Add(this.rdbAgendaConsulta);
            this.pnlAgenda.Controls.Add(this.rdbAgendaTotal);
            this.pnlAgenda.Location = new System.Drawing.Point(154, 3);
            this.pnlAgenda.Name = "pnlAgenda";
            this.pnlAgenda.Size = new System.Drawing.Size(440, 19);
            this.pnlAgenda.TabIndex = 5;
            // 
            // rdbAgendaProibir
            // 
            this.rdbAgendaProibir.AutoSize = true;
            this.rdbAgendaProibir.Checked = true;
            this.rdbAgendaProibir.Location = new System.Drawing.Point(169, 3);
            this.rdbAgendaProibir.Name = "rdbAgendaProibir";
            this.rdbAgendaProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbAgendaProibir.TabIndex = 2;
            this.rdbAgendaProibir.TabStop = true;
            this.rdbAgendaProibir.Text = "Proibir";
            this.rdbAgendaProibir.UseVisualStyleBackColor = true;
            // 
            // rdbAgendaConsulta
            // 
            this.rdbAgendaConsulta.AutoSize = true;
            this.rdbAgendaConsulta.Location = new System.Drawing.Point(58, 3);
            this.rdbAgendaConsulta.Name = "rdbAgendaConsulta";
            this.rdbAgendaConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbAgendaConsulta.TabIndex = 1;
            this.rdbAgendaConsulta.Text = "Apenas Consulta";
            this.rdbAgendaConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbAgendaTotal
            // 
            this.rdbAgendaTotal.AutoSize = true;
            this.rdbAgendaTotal.Location = new System.Drawing.Point(3, 3);
            this.rdbAgendaTotal.Name = "rdbAgendaTotal";
            this.rdbAgendaTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbAgendaTotal.TabIndex = 0;
            this.rdbAgendaTotal.Text = "Total";
            this.rdbAgendaTotal.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Agenda";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(141, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Entrada/Saida de Pacientes";
            // 
            // txtAgenda
            // 
            this.txtAgenda.Location = new System.Drawing.Point(600, 3);
            this.txtAgenda.Name = "txtAgenda";
            this.txtAgenda.Size = new System.Drawing.Size(100, 20);
            this.txtAgenda.TabIndex = 7;
            this.txtAgenda.Visible = false;
            // 
            // txtEntSaida
            // 
            this.txtEntSaida.Location = new System.Drawing.Point(600, 28);
            this.txtEntSaida.Name = "txtEntSaida";
            this.txtEntSaida.Size = new System.Drawing.Size(100, 20);
            this.txtEntSaida.TabIndex = 8;
            this.txtEntSaida.Visible = false;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(339, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Módulo Atendimento";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 315);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(800, 23);
            this.tableLayoutPanel6.TabIndex = 7;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.375F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.625F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 203F));
            this.tableLayoutPanel7.Controls.Add(this.pnlAvaliacao, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.pnlAtendimento, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label12, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.txtEvolucao, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtAvaliacao, 2, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 338);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(800, 49);
            this.tableLayoutPanel7.TabIndex = 8;
            // 
            // pnlAvaliacao
            // 
            this.pnlAvaliacao.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlAvaliacao.Controls.Add(this.rdbAvaliacaoProibir);
            this.pnlAvaliacao.Controls.Add(this.rdbAvaliacaoConsulta);
            this.pnlAvaliacao.Controls.Add(this.rdbAvaliacaoTotal);
            this.pnlAvaliacao.Location = new System.Drawing.Point(154, 27);
            this.pnlAvaliacao.Name = "pnlAvaliacao";
            this.pnlAvaliacao.Size = new System.Drawing.Size(439, 19);
            this.pnlAvaliacao.TabIndex = 5;
            // 
            // rdbAvaliacaoProibir
            // 
            this.rdbAvaliacaoProibir.AutoSize = true;
            this.rdbAvaliacaoProibir.Checked = true;
            this.rdbAvaliacaoProibir.Location = new System.Drawing.Point(169, 3);
            this.rdbAvaliacaoProibir.Name = "rdbAvaliacaoProibir";
            this.rdbAvaliacaoProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbAvaliacaoProibir.TabIndex = 2;
            this.rdbAvaliacaoProibir.TabStop = true;
            this.rdbAvaliacaoProibir.Text = "Proibir";
            this.rdbAvaliacaoProibir.UseVisualStyleBackColor = true;
            // 
            // rdbAvaliacaoConsulta
            // 
            this.rdbAvaliacaoConsulta.AutoSize = true;
            this.rdbAvaliacaoConsulta.Location = new System.Drawing.Point(58, 3);
            this.rdbAvaliacaoConsulta.Name = "rdbAvaliacaoConsulta";
            this.rdbAvaliacaoConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbAvaliacaoConsulta.TabIndex = 1;
            this.rdbAvaliacaoConsulta.Text = "Apenas Consulta";
            this.rdbAvaliacaoConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbAvaliacaoTotal
            // 
            this.rdbAvaliacaoTotal.AutoSize = true;
            this.rdbAvaliacaoTotal.Location = new System.Drawing.Point(3, 3);
            this.rdbAvaliacaoTotal.Name = "rdbAvaliacaoTotal";
            this.rdbAvaliacaoTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbAvaliacaoTotal.TabIndex = 0;
            this.rdbAvaliacaoTotal.Text = "Total";
            this.rdbAvaliacaoTotal.UseVisualStyleBackColor = true;
            // 
            // pnlAtendimento
            // 
            this.pnlAtendimento.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlAtendimento.Controls.Add(this.rdbAtendimentoProibir);
            this.pnlAtendimento.Controls.Add(this.rdbAtendimentoConsulta);
            this.pnlAtendimento.Controls.Add(this.rdbAtendimentoTotal);
            this.pnlAtendimento.Location = new System.Drawing.Point(154, 3);
            this.pnlAtendimento.Name = "pnlAtendimento";
            this.pnlAtendimento.Size = new System.Drawing.Size(439, 18);
            this.pnlAtendimento.TabIndex = 5;
            // 
            // rdbAtendimentoProibir
            // 
            this.rdbAtendimentoProibir.AutoSize = true;
            this.rdbAtendimentoProibir.Checked = true;
            this.rdbAtendimentoProibir.Location = new System.Drawing.Point(169, 3);
            this.rdbAtendimentoProibir.Name = "rdbAtendimentoProibir";
            this.rdbAtendimentoProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbAtendimentoProibir.TabIndex = 2;
            this.rdbAtendimentoProibir.TabStop = true;
            this.rdbAtendimentoProibir.Text = "Proibir";
            this.rdbAtendimentoProibir.UseVisualStyleBackColor = true;
            // 
            // rdbAtendimentoConsulta
            // 
            this.rdbAtendimentoConsulta.AutoSize = true;
            this.rdbAtendimentoConsulta.Location = new System.Drawing.Point(58, 3);
            this.rdbAtendimentoConsulta.Name = "rdbAtendimentoConsulta";
            this.rdbAtendimentoConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbAtendimentoConsulta.TabIndex = 1;
            this.rdbAtendimentoConsulta.Text = "Apenas Consulta";
            this.rdbAtendimentoConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbAtendimentoTotal
            // 
            this.rdbAtendimentoTotal.AutoSize = true;
            this.rdbAtendimentoTotal.Location = new System.Drawing.Point(3, 3);
            this.rdbAtendimentoTotal.Name = "rdbAtendimentoTotal";
            this.rdbAtendimentoTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbAtendimentoTotal.TabIndex = 0;
            this.rdbAtendimentoTotal.Text = "Total";
            this.rdbAtendimentoTotal.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Evolução";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 30);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "Avaliacao";
            // 
            // txtEvolucao
            // 
            this.txtEvolucao.Location = new System.Drawing.Point(599, 3);
            this.txtEvolucao.Name = "txtEvolucao";
            this.txtEvolucao.Size = new System.Drawing.Size(100, 20);
            this.txtEvolucao.TabIndex = 8;
            this.txtEvolucao.Visible = false;
            // 
            // txtAvaliacao
            // 
            this.txtAvaliacao.Location = new System.Drawing.Point(599, 27);
            this.txtAvaliacao.Name = "txtAvaliacao";
            this.txtAvaliacao.Size = new System.Drawing.Size(100, 20);
            this.txtAvaliacao.TabIndex = 9;
            this.txtAvaliacao.Visible = false;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 387);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(800, 23);
            this.tableLayoutPanel8.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(340, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "Módulo Medicações";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(315, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(170, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Módulo Administrador/Config";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 3;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.375F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.625F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 203F));
            this.tableLayoutPanel9.Controls.Add(this.pnlMedicacao, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label16, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.pnlPosologia, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.txtMedicacao, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.txttPosologia, 2, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 410);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.7037F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.2963F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(800, 54);
            this.tableLayoutPanel9.TabIndex = 10;
            // 
            // pnlMedicacao
            // 
            this.pnlMedicacao.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlMedicacao.Controls.Add(this.rdbMedicacaoProibir);
            this.pnlMedicacao.Controls.Add(this.rdbMedicacaoConsulta);
            this.pnlMedicacao.Controls.Add(this.rdbMedicacaoTotal);
            this.pnlMedicacao.Location = new System.Drawing.Point(154, 3);
            this.pnlMedicacao.Name = "pnlMedicacao";
            this.pnlMedicacao.Size = new System.Drawing.Size(439, 22);
            this.pnlMedicacao.TabIndex = 5;
            // 
            // rdbMedicacaoProibir
            // 
            this.rdbMedicacaoProibir.AutoSize = true;
            this.rdbMedicacaoProibir.Checked = true;
            this.rdbMedicacaoProibir.Location = new System.Drawing.Point(169, 3);
            this.rdbMedicacaoProibir.Name = "rdbMedicacaoProibir";
            this.rdbMedicacaoProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbMedicacaoProibir.TabIndex = 2;
            this.rdbMedicacaoProibir.TabStop = true;
            this.rdbMedicacaoProibir.Text = "Proibir";
            this.rdbMedicacaoProibir.UseVisualStyleBackColor = true;
            // 
            // rdbMedicacaoConsulta
            // 
            this.rdbMedicacaoConsulta.AutoSize = true;
            this.rdbMedicacaoConsulta.Location = new System.Drawing.Point(58, 3);
            this.rdbMedicacaoConsulta.Name = "rdbMedicacaoConsulta";
            this.rdbMedicacaoConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbMedicacaoConsulta.TabIndex = 1;
            this.rdbMedicacaoConsulta.Text = "Apenas Consulta";
            this.rdbMedicacaoConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbMedicacaoTotal
            // 
            this.rdbMedicacaoTotal.AutoSize = true;
            this.rdbMedicacaoTotal.Location = new System.Drawing.Point(3, 3);
            this.rdbMedicacaoTotal.Name = "rdbMedicacaoTotal";
            this.rdbMedicacaoTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbMedicacaoTotal.TabIndex = 0;
            this.rdbMedicacaoTotal.Text = "Total";
            this.rdbMedicacaoTotal.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 7);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Medicação";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "Posologia";
            // 
            // txtMedicacao
            // 
            this.txtMedicacao.Location = new System.Drawing.Point(599, 3);
            this.txtMedicacao.Name = "txtMedicacao";
            this.txtMedicacao.Size = new System.Drawing.Size(100, 20);
            this.txtMedicacao.TabIndex = 6;
            this.txtMedicacao.Visible = false;
            this.txtMedicacao.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // txttPosologia
            // 
            this.txttPosologia.Location = new System.Drawing.Point(599, 31);
            this.txttPosologia.Name = "txttPosologia";
            this.txttPosologia.Size = new System.Drawing.Size(100, 20);
            this.txttPosologia.TabIndex = 7;
            this.txttPosologia.Visible = false;
            // 
            // pnlNivelUsuario
            // 
            this.pnlNivelUsuario.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlNivelUsuario.Controls.Add(this.rdbNivelUsuarioProibir);
            this.pnlNivelUsuario.Controls.Add(this.rdbNivelUsuarioConsulta);
            this.pnlNivelUsuario.Controls.Add(this.rdbNivelUsuarioTotal);
            this.pnlNivelUsuario.Location = new System.Drawing.Point(153, 36);
            this.pnlNivelUsuario.Name = "pnlNivelUsuario";
            this.pnlNivelUsuario.Size = new System.Drawing.Size(434, 19);
            this.pnlNivelUsuario.TabIndex = 5;
            // 
            // rdbNivelUsuarioProibir
            // 
            this.rdbNivelUsuarioProibir.AutoSize = true;
            this.rdbNivelUsuarioProibir.Checked = true;
            this.rdbNivelUsuarioProibir.Location = new System.Drawing.Point(169, 3);
            this.rdbNivelUsuarioProibir.Name = "rdbNivelUsuarioProibir";
            this.rdbNivelUsuarioProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbNivelUsuarioProibir.TabIndex = 2;
            this.rdbNivelUsuarioProibir.TabStop = true;
            this.rdbNivelUsuarioProibir.Text = "Proibir";
            this.rdbNivelUsuarioProibir.UseVisualStyleBackColor = true;
            // 
            // rdbNivelUsuarioConsulta
            // 
            this.rdbNivelUsuarioConsulta.AutoSize = true;
            this.rdbNivelUsuarioConsulta.Location = new System.Drawing.Point(58, 3);
            this.rdbNivelUsuarioConsulta.Name = "rdbNivelUsuarioConsulta";
            this.rdbNivelUsuarioConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbNivelUsuarioConsulta.TabIndex = 1;
            this.rdbNivelUsuarioConsulta.Text = "Apenas Consulta";
            this.rdbNivelUsuarioConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbNivelUsuarioTotal
            // 
            this.rdbNivelUsuarioTotal.AutoSize = true;
            this.rdbNivelUsuarioTotal.Location = new System.Drawing.Point(3, 3);
            this.rdbNivelUsuarioTotal.Name = "rdbNivelUsuarioTotal";
            this.rdbNivelUsuarioTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbNivelUsuarioTotal.TabIndex = 0;
            this.rdbNivelUsuarioTotal.Text = "Total";
            this.rdbNivelUsuarioTotal.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.label14, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 464);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(800, 25);
            this.tableLayoutPanel10.TabIndex = 11;
            this.tableLayoutPanel10.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel10_Paint);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 3;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.5F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.5F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 208F));
            this.tableLayoutPanel11.Controls.Add(this.pnlCadastroUsuario, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.label17, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.pnlNivelUsuario, 1, 1);
            this.tableLayoutPanel11.Controls.Add(this.label18, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.txtCadUser, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.txtNivelUser, 2, 1);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 489);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(800, 61);
            this.tableLayoutPanel11.TabIndex = 12;
            // 
            // pnlCadastroUsuario
            // 
            this.pnlCadastroUsuario.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlCadastroUsuario.Controls.Add(this.rdbCadastroUsuarioProibir);
            this.pnlCadastroUsuario.Controls.Add(this.rdbCadastroUsuarioConsulta);
            this.pnlCadastroUsuario.Controls.Add(this.rdbCadastroUsuarioTotal);
            this.pnlCadastroUsuario.Location = new System.Drawing.Point(153, 5);
            this.pnlCadastroUsuario.Name = "pnlCadastroUsuario";
            this.pnlCadastroUsuario.Size = new System.Drawing.Size(434, 19);
            this.pnlCadastroUsuario.TabIndex = 6;
            // 
            // rdbCadastroUsuarioProibir
            // 
            this.rdbCadastroUsuarioProibir.AutoSize = true;
            this.rdbCadastroUsuarioProibir.Checked = true;
            this.rdbCadastroUsuarioProibir.Location = new System.Drawing.Point(169, 3);
            this.rdbCadastroUsuarioProibir.Name = "rdbCadastroUsuarioProibir";
            this.rdbCadastroUsuarioProibir.Size = new System.Drawing.Size(54, 17);
            this.rdbCadastroUsuarioProibir.TabIndex = 2;
            this.rdbCadastroUsuarioProibir.TabStop = true;
            this.rdbCadastroUsuarioProibir.Text = "Proibir";
            this.rdbCadastroUsuarioProibir.UseVisualStyleBackColor = true;
            // 
            // rdbCadastroUsuarioConsulta
            // 
            this.rdbCadastroUsuarioConsulta.AutoSize = true;
            this.rdbCadastroUsuarioConsulta.Location = new System.Drawing.Point(58, 3);
            this.rdbCadastroUsuarioConsulta.Name = "rdbCadastroUsuarioConsulta";
            this.rdbCadastroUsuarioConsulta.Size = new System.Drawing.Size(105, 17);
            this.rdbCadastroUsuarioConsulta.TabIndex = 1;
            this.rdbCadastroUsuarioConsulta.Text = "Apenas Consulta";
            this.rdbCadastroUsuarioConsulta.UseVisualStyleBackColor = true;
            // 
            // rdbCadastroUsuarioTotal
            // 
            this.rdbCadastroUsuarioTotal.AutoSize = true;
            this.rdbCadastroUsuarioTotal.Location = new System.Drawing.Point(3, 3);
            this.rdbCadastroUsuarioTotal.Name = "rdbCadastroUsuarioTotal";
            this.rdbCadastroUsuarioTotal.Size = new System.Drawing.Size(49, 17);
            this.rdbCadastroUsuarioTotal.TabIndex = 0;
            this.rdbCadastroUsuarioTotal.Text = "Total";
            this.rdbCadastroUsuarioTotal.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(98, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Cadatro de Usuário";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 39);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Nível de Usuário";
            // 
            // txtCadUser
            // 
            this.txtCadUser.Location = new System.Drawing.Point(594, 3);
            this.txtCadUser.Name = "txtCadUser";
            this.txtCadUser.Size = new System.Drawing.Size(100, 20);
            this.txtCadUser.TabIndex = 7;
            this.txtCadUser.Visible = false;
            // 
            // txtNivelUser
            // 
            this.txtNivelUser.Location = new System.Drawing.Point(594, 33);
            this.txtNivelUser.Name = "txtNivelUser";
            this.txtNivelUser.Size = new System.Drawing.Size(100, 20);
            this.txtNivelUser.TabIndex = 8;
            this.txtNivelUser.Visible = false;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 5;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 114F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 215F));
            this.tableLayoutPanel12.Controls.Add(this.btnAtualizar, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.btnGravar, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(0, 550);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(800, 33);
            this.tableLayoutPanel12.TabIndex = 13;
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.BackColor = System.Drawing.Color.DarkGray;
            this.btnAtualizar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizar.Location = new System.Drawing.Point(198, 3);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(75, 27);
            this.btnAtualizar.TabIndex = 1;
            this.btnAtualizar.Text = "Atualizar";
            this.btnAtualizar.UseVisualStyleBackColor = false;
            this.btnAtualizar.Visible = false;
            this.btnAtualizar.Click += new System.EventHandler(this.btnAtualizar_Click);
            // 
            // btnGravar
            // 
            this.btnGravar.BackColor = System.Drawing.Color.DarkGray;
            this.btnGravar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGravar.Location = new System.Drawing.Point(3, 3);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(75, 27);
            this.btnGravar.TabIndex = 0;
            this.btnGravar.Text = "Gravar";
            this.btnGravar.UseVisualStyleBackColor = false;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.75F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 86.25F));
            this.tableLayoutPanel13.Controls.Add(this.label20, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.txtIdNivelUser, 1, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 583);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(800, 27);
            this.tableLayoutPanel13.TabIndex = 14;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "id registro";
            this.label20.Visible = false;
            // 
            // txtIdNivelUser
            // 
            this.txtIdNivelUser.Location = new System.Drawing.Point(113, 3);
            this.txtIdNivelUser.Name = "txtIdNivelUser";
            this.txtIdNivelUser.Size = new System.Drawing.Size(100, 20);
            this.txtIdNivelUser.TabIndex = 1;
            this.txtIdNivelUser.Visible = false;
            // 
            // fmrCadastroNivelDeUsuarioPermissoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(800, 667);
            this.Controls.Add(this.tableLayoutPanel13);
            this.Controls.Add(this.tableLayoutPanel12);
            this.Controls.Add(this.tableLayoutPanel11);
            this.Controls.Add(this.tableLayoutPanel10);
            this.Controls.Add(this.tableLayoutPanel9);
            this.Controls.Add(this.tableLayoutPanel8);
            this.Controls.Add(this.tableLayoutPanel7);
            this.Controls.Add(this.tableLayoutPanel6);
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.Name = "fmrCadastroNivelDeUsuarioPermissoes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Nível de Usuário";
            this.Load += new System.EventHandler(this.fmrCadastroNivelDeUsuarioPermissoes_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.pnlCadastroPaciente.ResumeLayout(false);
            this.pnlCadastroPaciente.PerformLayout();
            this.pnlAnexosPaciente.ResumeLayout(false);
            this.pnlAnexosPaciente.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.pnlAtividadeGrupo.ResumeLayout(false);
            this.pnlAtividadeGrupo.PerformLayout();
            this.pnlInfComp.ResumeLayout(false);
            this.pnlInfComp.PerformLayout();
            this.pnlFotoPaciente.ResumeLayout(false);
            this.pnlFotoPaciente.PerformLayout();
            this.pnlPosologia.ResumeLayout(false);
            this.pnlPosologia.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.pnlEntradaSaida.ResumeLayout(false);
            this.pnlEntradaSaida.PerformLayout();
            this.pnlAgenda.ResumeLayout(false);
            this.pnlAgenda.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.pnlAvaliacao.ResumeLayout(false);
            this.pnlAvaliacao.PerformLayout();
            this.pnlAtendimento.ResumeLayout(false);
            this.pnlAtendimento.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.pnlMedicacao.ResumeLayout(false);
            this.pnlMedicacao.PerformLayout();
            this.pnlNivelUsuario.ResumeLayout(false);
            this.pnlNivelUsuario.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.pnlCadastroUsuario.ResumeLayout(false);
            this.pnlCadastroUsuario.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlCadastroPaciente;
        private System.Windows.Forms.RadioButton rdbPacienteApenasConsulta;
        private System.Windows.Forms.RadioButton rdbPacienteProibir;
        private System.Windows.Forms.Panel pnlAnexosPaciente;
        private System.Windows.Forms.RadioButton rdbAnexoPacienteProibir;
        private System.Windows.Forms.RadioButton rdbAnexoPacienteConsulta;
        private System.Windows.Forms.RadioButton rdbAnexoPacienteTotal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel pnlAtividadeGrupo;
        private System.Windows.Forms.RadioButton rdbAtividadeEmGrupoProibir;
        private System.Windows.Forms.RadioButton rdbAtividadeEmGrupoConsulta;
        private System.Windows.Forms.RadioButton rdbAtividadeEmGrupoTotal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnlInfComp;
        private System.Windows.Forms.RadioButton rdbInfComplementarPacienteProibir;
        private System.Windows.Forms.RadioButton rdbInfComplementarPacienteConsulta;
        private System.Windows.Forms.RadioButton rdbInfComplementarPacienteTotal;
        private System.Windows.Forms.Panel pnlPosologia;
        private System.Windows.Forms.RadioButton rdbPosologiaProibir;
        private System.Windows.Forms.RadioButton rdbPosologiaConsulta;
        private System.Windows.Forms.RadioButton rdbPosologiaTotal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel pnlEntradaSaida;
        private System.Windows.Forms.RadioButton rdbEntradaSaidaProibir;
        private System.Windows.Forms.RadioButton rdbEntradaSaidaConsulta;
        private System.Windows.Forms.RadioButton rdbEntradaSaidaTotal;
        private System.Windows.Forms.Panel pnlAgenda;
        private System.Windows.Forms.RadioButton rdbAgendaProibir;
        private System.Windows.Forms.RadioButton rdbAgendaConsulta;
        private System.Windows.Forms.RadioButton rdbAgendaTotal;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Panel pnlAvaliacao;
        private System.Windows.Forms.RadioButton rdbAvaliacaoProibir;
        private System.Windows.Forms.RadioButton rdbAvaliacaoConsulta;
        private System.Windows.Forms.RadioButton rdbAvaliacaoTotal;
        private System.Windows.Forms.Panel pnlAtendimento;
        private System.Windows.Forms.RadioButton rdbAtendimentoProibir;
        private System.Windows.Forms.RadioButton rdbAtendimentoConsulta;
        private System.Windows.Forms.RadioButton rdbAtendimentoTotal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel pnlMedicacao;
        private System.Windows.Forms.RadioButton rdbMedicacaoProibir;
        private System.Windows.Forms.RadioButton rdbMedicacaoConsulta;
        private System.Windows.Forms.RadioButton rdbMedicacaoTotal;
        private System.Windows.Forms.Panel pnlNivelUsuario;
        private System.Windows.Forms.RadioButton rdbNivelUsuarioProibir;
        private System.Windows.Forms.RadioButton rdbNivelUsuarioConsulta;
        private System.Windows.Forms.RadioButton rdbNivelUsuarioTotal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel pnlCadastroUsuario;
        private System.Windows.Forms.RadioButton rdbCadastroUsuarioProibir;
        private System.Windows.Forms.RadioButton rdbCadastroUsuarioConsulta;
        private System.Windows.Forms.RadioButton rdbCadastroUsuarioTotal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        public System.Windows.Forms.TextBox txtCadPaciente;
        public System.Windows.Forms.TextBox txtAnexoPaciente;
        public System.Windows.Forms.TextBox txtInfCompPac;
        public System.Windows.Forms.TextBox txtAtividadeEmgrupo;
        public System.Windows.Forms.TextBox txtAgenda;
        public System.Windows.Forms.TextBox txtEntSaida;
        public System.Windows.Forms.TextBox txtEvolucao;
        public System.Windows.Forms.TextBox txtAvaliacao;
        public System.Windows.Forms.TextBox txtMedicacao;
        public System.Windows.Forms.TextBox txttPosologia;
        public System.Windows.Forms.TextBox txtCadUser;
        public System.Windows.Forms.TextBox txtNivelUser;
        public System.Windows.Forms.TextBox txtNomeNivel;
        public System.Windows.Forms.CheckBox chkAtivo;
        public System.Windows.Forms.Button btnGravar;
        public System.Windows.Forms.RadioButton rdbPacienteTotal;
        private System.Windows.Forms.Panel pnlFotoPaciente;
        private System.Windows.Forms.RadioButton rdbFotoProibir;
        private System.Windows.Forms.RadioButton rdbFotoConsulta;
        private System.Windows.Forms.RadioButton rdbFotoTotal;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.TextBox txtFotoPaciente;
        public System.Windows.Forms.Button btnAtualizar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.TextBox txtIdNivelUser;
    }
}