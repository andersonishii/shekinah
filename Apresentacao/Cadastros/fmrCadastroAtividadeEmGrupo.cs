﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrCadastroAtividadeEmGrupo : Form
    {
        public fmrCadastroAtividadeEmGrupo()
        {
            InitializeComponent();
        }

        Conexao con = new Conexao();
        SqlDataAdapter adpt;
        SqlDataReader dr;
        DataTable dt;
        SqlCommand cmd = new SqlCommand();
        /*
        Paciente pc = new Paciente();
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();
        */
        public int getMaiorID()
        {
            int maiorIdObtido;
            try
            {
                cmd.CommandText = "select   (CASE WHEN max(codigo_agrupador) IS NULL THEN '0' else max(codigo_agrupador) end ) as maxid from atividadegrupo_funcionario";

                cmd.Connection = con.conectar();
                maiorIdObtido = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                maiorIdObtido = maiorIdObtido + 1;
                cmd.ExecuteNonQuery();
                con.desconectar();

                return maiorIdObtido;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.desconectar();
            }

        }

       

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {
          
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            int qtdeUserChecados = cbxFuncionario.chkUsuarios.CheckedItems.Count;
            int qtdePacienteChecados = cbxPaciente.chkPacientes.CheckedItems.Count;
            DateTime dataHoraSelecionada = (dtpData.Value.Date) + (dtpHora.Value.TimeOfDay);
            String DiaAtual = DateTime.Now.ToString("dd-MM-yyyy");
            String DiaDatePicker = dtpData.Value.Date.ToString("dd-MM-yyyy");
 
             if (qtdeUserChecados==0 || qtdePacienteChecados==0)
            {
                MessageBox.Show("Ao menos um usuário e um paciente deve ser selecionado//Verifique também se o usuário possui permissão para lançar retroativamente");
            }
            else
            {
                if ((DiaDatePicker.Equals(DiaAtual)) || Usuario.permiteLancarRetroativamente)
                {
                    Controle control = new Controle();
                    //varrer quais são os checkbox que estão marcados(pessoas)
                    for (int i = 0; i < cbxFuncionario.chkUsuarios.CheckedItems.Count; i++)
                    {
                        String mensagem = control.CadastraAtividadeEmGrupoUsuario((cbxFuncionario.SelectPosicaoUsuario(i).Id).ToString(), dataHoraSelecionada, txtTipoAtividade.Text, txtRelatorio.Text, Convert.ToInt32(txtID.Text), chkAtivo.Checked);
                        if (control.tem)
                        {
                            int ultimo = cbxFuncionario.chkUsuarios.CheckedItems.Count;
                            //so vai exibir a mensagem quando o registro for o ultimo (nesse caso é o qtde de checkbox checados). Como o i começa em zero foi encessario comparar com i+1
                            if ((i + 1).Equals(ultimo))
                            {
                                MessageBox.Show(mensagem, "Cadastro Atividade em Grupo do Funcionario", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }
                        }
                        else
                        {
                            MessageBox.Show(control.msg);
                        }
                    }
                    //varrer quais são os checkbox que estão marcados(paciente)
                    for (int i = 0; i < cbxPaciente.chkPacientes.CheckedItems.Count; i++)
                    {
                        String mensagem = control.CadastraAtividadeEmGrupoPaciente((cbxPaciente.SelectPosicaoPaciente(i).Id).ToString(), dataHoraSelecionada, txtTipoAtividade.Text, txtRelatorio.Text, Convert.ToInt32(txtID.Text), chkAtivo.Checked);
                        if (control.tem)
                        {
                            int ultimo = cbxPaciente.chkPacientes.CheckedItems.Count;
                            //so vai exibir a mensagem quando o registro for o ultimo (nesse caso é o qtde de checkbox checados). Como o i começa em zero foi encessario comparar com i+1
                            if ((i + 1).Equals(ultimo))
                            {
                                MessageBox.Show(mensagem, "Cadastro Atividade em Grupo do Paciente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                limpaTela();
                            }
                        }
                        else
                        {
                            MessageBox.Show(control.msg);
                        }

                    }

                }
                else
                {
                    MessageBox.Show("O Usuário não possui permissão para lançar retroativamente");
                }
                
            }

        }

        public void limpaTela()
        {

            String DiaAtual = DateTime.Now.ToString("dd-MM-yyyy");
            String DiaDatePicker = dtpData.Value.Date.ToString("dd-MM-yyyy");
            DateTime dataHoraSelecionada = (dtpData.Value.Date) + (DateTime.Now.TimeOfDay);
            DateTime dataHoraAgora = DateTime.Now;
            txtTipoAtividade.Clear();
            txtRelatorio.Clear();
            cbxFuncionario.desmcarTodosOsMarcados();
            cbxPaciente.desmcarTodosOsMarcados();
            chkAtivo.Checked= true;
            dtpData.Text = DateTime.Now.ToString("dd-MM-yyyy");
            dtpHora.Text = dataHoraSelecionada.ToString();
            txtID.Text = getMaiorID().ToString();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            limpaTela();
        }

        private void FmrCadastroAtividadeEmGrupo_Load(object sender, EventArgs e)
        {
            txtID.Text = getMaiorID().ToString();
        }
    }
}
