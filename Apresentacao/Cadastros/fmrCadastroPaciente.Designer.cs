﻿namespace SistemaShekinah.Apresentacao.Cadastros
{
    partial class fmrCadastroPaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label15;
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chkAtivo = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblCpf = new System.Windows.Forms.Label();
            this.txtCpf = new System.Windows.Forms.TextBox();
            this.txtTipoDoc = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpDataNasc = new System.Windows.Forms.DateTimePicker();
            this.txtCns = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.rdbMasculino = new System.Windows.Forms.RadioButton();
            this.rdbFeminino = new System.Windows.Forms.RadioButton();
            this.rdbOutros = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpAdmissao = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTel1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTel1 = new System.Windows.Forms.TextBox();
            this.txtTel2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNomeContato = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblContatoEmergencia = new System.Windows.Forms.Label();
            this.txtNumeroDeEmergencia = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtContatoDeEmergencia = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCep = new System.Windows.Forms.TextBox();
            this.lblLogradouro = new System.Windows.Forms.Label();
            this.txtLogradouro = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtComplemento = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.txtResponsavelEndereco = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label13 = new System.Windows.Forms.Label();
            this.txtObsEnderecoPaciente = new System.Windows.Forms.RichTextBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCepResidenciaAnterior = new System.Windows.Forms.TextBox();
            this.txtLogradouroResidenciaAnterior = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtNumeroResidenciaAnterior = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label19 = new System.Windows.Forms.Label();
            this.txtCompletoResidenciaAnterior = new System.Windows.Forms.TextBox();
            this.txtResponsavelResidenciaAnterior = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.txtObsResidenciaAnterior = new System.Windows.Forms.RichTextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.btnInativar = new System.Windows.Forms.Button();
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.txtNomeTipoIdentificacao = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtNomeRaca = new System.Windows.Forms.TextBox();
            this.txtNomeSexo = new System.Windows.Forms.TextBox();
            this.txtNomeEscolaridadade = new System.Windows.Forms.TextBox();
            this.txtNomeUFEnderecoAtual = new System.Windows.Forms.TextBox();
            this.txtNomeMunicipioEnderecoAtual = new System.Windows.Forms.TextBox();
            this.txtNomeUFEnderecoAnterior = new System.Windows.Forms.TextBox();
            this.txtNomeMunicipioEnderecoAnterior = new System.Windows.Forms.TextBox();
            this.cmbMunicipioAnterior = new SistemaShekinah.Apresentacao.User_control.UserControlTipoUF();
            this.cmbMunicipios = new SistemaShekinah.Apresentacao.User_control.UserControlTipoUF();
            this.cmbEscolaridade = new SistemaShekinah.Apresentacao.User_control.UserControlTipoEscolaridade();
            this.cmbRaca = new SistemaShekinah.Apresentacao.User_control.UserControlTipoRaca();
            this.cmbTipoRegistro = new SistemaShekinah.Apresentacao.User_control.UserControlTipodeIdentificacao();
            label15 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.SuspendLayout();
            // 
            // label15
            // 
            label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            label15.AutoSize = true;
            label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label15.Location = new System.Drawing.Point(3, 6);
            label15.Name = "label15";
            label15.Size = new System.Drawing.Size(118, 13);
            label15.TabIndex = 1;
            label15.Text = "Residencia Anterior";
            label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.488987F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 92.51102F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 751F));
            this.tableLayoutPanel1.Controls.Add(this.chkAtivo, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtNome, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1370, 29);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // chkAtivo
            // 
            this.chkAtivo.AutoSize = true;
            this.chkAtivo.Checked = true;
            this.chkAtivo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAtivo.Enabled = false;
            this.chkAtivo.Location = new System.Drawing.Point(621, 3);
            this.chkAtivo.Name = "chkAtivo";
            this.chkAtivo.Size = new System.Drawing.Size(56, 17);
            this.chkAtivo.TabIndex = 0;
            this.chkAtivo.Text = "Ativo?";
            this.chkAtivo.UseVisualStyleBackColor = true;
            this.chkAtivo.CheckedChanged += new System.EventHandler(this.chkAtivo_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(49, 3);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(328, 20);
            this.txtNome.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.42857F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 88.57143F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 336F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 663F));
            this.tableLayoutPanel2.Controls.Add(this.lblCpf, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtCpf, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtTipoDoc, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.cmbTipoRegistro, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 29);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1370, 35);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // lblCpf
            // 
            this.lblCpf.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblCpf.AutoSize = true;
            this.lblCpf.Location = new System.Drawing.Point(3, 11);
            this.lblCpf.Name = "lblCpf";
            this.lblCpf.Size = new System.Drawing.Size(36, 13);
            this.lblCpf.TabIndex = 0;
            this.lblCpf.Text = "C.P.F.";
            // 
            // txtCpf
            // 
            this.txtCpf.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtCpf.Location = new System.Drawing.Point(45, 7);
            this.txtCpf.MaxLength = 11;
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(221, 20);
            this.txtCpf.TabIndex = 1;
            this.txtCpf.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtCpf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCpf_KeyPress);
            // 
            // txtTipoDoc
            // 
            this.txtTipoDoc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtTipoDoc.Location = new System.Drawing.Point(709, 7);
            this.txtTipoDoc.Name = "txtTipoDoc";
            this.txtTipoDoc.Size = new System.Drawing.Size(189, 20);
            this.txtTipoDoc.TabIndex = 3;
            this.txtTipoDoc.TextChanged += new System.EventHandler(this.txtTipoDoc_TextChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.42857F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.57143F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 385F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 544F));
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.dtpDataNasc, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtCns, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.label2, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.cmbRaca, 2, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 64);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1370, 33);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data de Nascimento";
            // 
            // dtpDataNasc
            // 
            this.dtpDataNasc.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtpDataNasc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataNasc.Location = new System.Drawing.Point(107, 6);
            this.dtpDataNasc.Name = "dtpDataNasc";
            this.dtpDataNasc.Size = new System.Drawing.Size(164, 20);
            this.dtpDataNasc.TabIndex = 3;
            // 
            // txtCns
            // 
            this.txtCns.Location = new System.Drawing.Point(828, 3);
            this.txtCns.Name = "txtCns";
            this.txtCns.Size = new System.Drawing.Size(151, 20);
            this.txtCns.TabIndex = 1;
            this.txtCns.MouseHover += new System.EventHandler(this.textBox1_MouseHover);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(783, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "C.N.S.";
            this.label2.MouseHover += new System.EventHandler(this.label2_MouseHover);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 7;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.75F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 302F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 104F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 640F));
            this.tableLayoutPanel4.Controls.Add(this.cmbEscolaridade, 6, 0);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.rdbMasculino, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.rdbFeminino, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.rdbOutros, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.dtpAdmissao, 5, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 97);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1370, 33);
            this.tableLayoutPanel4.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Sexo";
            // 
            // rdbMasculino
            // 
            this.rdbMasculino.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.rdbMasculino.AutoSize = true;
            this.rdbMasculino.Checked = true;
            this.rdbMasculino.Location = new System.Drawing.Point(101, 8);
            this.rdbMasculino.Name = "rdbMasculino";
            this.rdbMasculino.Size = new System.Drawing.Size(73, 17);
            this.rdbMasculino.TabIndex = 1;
            this.rdbMasculino.TabStop = true;
            this.rdbMasculino.Text = "Masculino";
            this.rdbMasculino.UseVisualStyleBackColor = true;
            // 
            // rdbFeminino
            // 
            this.rdbFeminino.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.rdbFeminino.AutoSize = true;
            this.rdbFeminino.Location = new System.Drawing.Point(190, 8);
            this.rdbFeminino.Name = "rdbFeminino";
            this.rdbFeminino.Size = new System.Drawing.Size(67, 17);
            this.rdbFeminino.TabIndex = 2;
            this.rdbFeminino.Text = "Feminino";
            this.rdbFeminino.UseVisualStyleBackColor = true;
            // 
            // rdbOutros
            // 
            this.rdbOutros.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.rdbOutros.AutoSize = true;
            this.rdbOutros.Location = new System.Drawing.Point(268, 8);
            this.rdbOutros.Name = "rdbOutros";
            this.rdbOutros.Size = new System.Drawing.Size(56, 17);
            this.rdbOutros.TabIndex = 3;
            this.rdbOutros.Text = "Outros";
            this.rdbOutros.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(570, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Admissão";
            // 
            // dtpAdmissao
            // 
            this.dtpAdmissao.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dtpAdmissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAdmissao.Location = new System.Drawing.Point(629, 6);
            this.dtpAdmissao.Name = "dtpAdmissao";
            this.dtpAdmissao.Size = new System.Drawing.Size(86, 20);
            this.dtpAdmissao.TabIndex = 5;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 6;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.01183F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.98817F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 363F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 660F));
            this.tableLayoutPanel5.Controls.Add(this.lblTel1, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label7, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtTel1, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtTel2, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.label6, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtNomeContato, 5, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 130);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1370, 34);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // lblTel1
            // 
            this.lblTel1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTel1.AutoSize = true;
            this.lblTel1.Location = new System.Drawing.Point(3, 10);
            this.lblTel1.Name = "lblTel1";
            this.lblTel1.Size = new System.Drawing.Size(58, 13);
            this.lblTel1.TabIndex = 0;
            this.lblTel1.Text = "Telefone 1";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(180, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Telefone 2";
            // 
            // txtTel1
            // 
            this.txtTel1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtTel1.Location = new System.Drawing.Point(77, 7);
            this.txtTel1.MaxLength = 13;
            this.txtTel1.Name = "txtTel1";
            this.txtTel1.Size = new System.Drawing.Size(97, 20);
            this.txtTel1.TabIndex = 2;
            this.txtTel1.TextChanged += new System.EventHandler(this.txtTel1_TextChanged);
            this.txtTel1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTel1_KeyPress);
            // 
            // txtTel2
            // 
            this.txtTel2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtTel2.Location = new System.Drawing.Point(248, 7);
            this.txtTel2.MaxLength = 13;
            this.txtTel2.Name = "txtTel2";
            this.txtTel2.Size = new System.Drawing.Size(124, 20);
            this.txtTel2.TabIndex = 3;
            this.txtTel2.TextChanged += new System.EventHandler(this.txtTel2_TextChanged);
            this.txtTel2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTel2_KeyPress);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(611, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Nome do Contato";
            // 
            // txtNomeContato
            // 
            this.txtNomeContato.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtNomeContato.Location = new System.Drawing.Point(712, 7);
            this.txtNomeContato.Name = "txtNomeContato";
            this.txtNomeContato.Size = new System.Drawing.Size(294, 20);
            this.txtNomeContato.TabIndex = 5;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.31646F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.68355F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 591F));
            this.tableLayoutPanel6.Controls.Add(this.lblContatoEmergencia, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtNumeroDeEmergencia, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtContatoDeEmergencia, 3, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 164);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1370, 30);
            this.tableLayoutPanel6.TabIndex = 5;
            this.tableLayoutPanel6.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel6_Paint);
            // 
            // lblContatoEmergencia
            // 
            this.lblContatoEmergencia.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblContatoEmergencia.AutoSize = true;
            this.lblContatoEmergencia.Location = new System.Drawing.Point(3, 8);
            this.lblContatoEmergencia.Name = "lblContatoEmergencia";
            this.lblContatoEmergencia.Size = new System.Drawing.Size(148, 13);
            this.lblContatoEmergencia.TabIndex = 0;
            this.lblContatoEmergencia.Text = "Telefone Contato Emergência";
            // 
            // txtNumeroDeEmergencia
            // 
            this.txtNumeroDeEmergencia.Location = new System.Drawing.Point(157, 3);
            this.txtNumeroDeEmergencia.Name = "txtNumeroDeEmergencia";
            this.txtNumeroDeEmergencia.Size = new System.Drawing.Size(200, 20);
            this.txtNumeroDeEmergencia.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(612, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(162, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Nome do contato de emergencia";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtContatoDeEmergencia
            // 
            this.txtContatoDeEmergencia.Location = new System.Drawing.Point(781, 3);
            this.txtContatoDeEmergencia.Name = "txtContatoDeEmergencia";
            this.txtContatoDeEmergencia.Size = new System.Drawing.Size(294, 20);
            this.txtContatoDeEmergencia.TabIndex = 3;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1790F));
            this.tableLayoutPanel7.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 194);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1370, 26);
            this.tableLayoutPanel7.TabIndex = 6;
            this.tableLayoutPanel7.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel7_Paint);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(137, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Endereço do Paciente ";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCep
            // 
            this.txtCep.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtCep.Location = new System.Drawing.Point(508, 6);
            this.txtCep.MaxLength = 8;
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(94, 20);
            this.txtCep.TabIndex = 2;
            this.txtCep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // lblLogradouro
            // 
            this.lblLogradouro.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblLogradouro.AutoSize = true;
            this.lblLogradouro.Location = new System.Drawing.Point(3, 9);
            this.lblLogradouro.Name = "lblLogradouro";
            this.lblLogradouro.Size = new System.Drawing.Size(61, 13);
            this.lblLogradouro.TabIndex = 3;
            this.lblLogradouro.Text = "Logradouro";
            // 
            // txtLogradouro
            // 
            this.txtLogradouro.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtLogradouro.Location = new System.Drawing.Point(99, 6);
            this.txtLogradouro.Name = "txtLogradouro";
            this.txtLogradouro.Size = new System.Drawing.Size(330, 20);
            this.txtLogradouro.TabIndex = 4;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 8;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 362F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 664F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Controls.Add(this.label11, 5, 0);
            this.tableLayoutPanel8.Controls.Add(this.txtNumero, 6, 0);
            this.tableLayoutPanel8.Controls.Add(this.txtCep, 3, 0);
            this.tableLayoutPanel8.Controls.Add(this.label10, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.lblLogradouro, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.txtLogradouro, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 220);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1370, 32);
            this.tableLayoutPanel8.TabIndex = 7;
            this.tableLayoutPanel8.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel8_Paint);
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(633, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Número";
            // 
            // txtNumero
            // 
            this.txtNumero.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtNumero.Location = new System.Drawing.Point(689, 6);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(150, 20);
            this.txtNumero.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(465, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "C.E.P.";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtComplemento
            // 
            this.txtComplemento.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtComplemento.Location = new System.Drawing.Point(78, 5);
            this.txtComplemento.Name = "txtComplemento";
            this.txtComplemento.Size = new System.Drawing.Size(48, 20);
            this.txtComplemento.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 26);
            this.label12.TabIndex = 0;
            this.label12.Text = "Complemento";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 5;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.30539F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.69461F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 417F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 712F));
            this.tableLayoutPanel9.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtComplemento, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.label14, 3, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtResponsavelEndereco, 4, 0);
            this.tableLayoutPanel9.Controls.Add(this.cmbMunicipios, 2, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 252);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(1370, 31);
            this.tableLayoutPanel9.TabIndex = 8;
            this.tableLayoutPanel9.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel9_Paint);
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(579, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Responsavel";
            // 
            // txtResponsavelEndereco
            // 
            this.txtResponsavelEndereco.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtResponsavelEndereco.Location = new System.Drawing.Point(660, 5);
            this.txtResponsavelEndereco.Name = "txtResponsavelEndereco";
            this.txtResponsavelEndereco.Size = new System.Drawing.Size(222, 20);
            this.txtResponsavelEndereco.TabIndex = 4;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1274F));
            this.tableLayoutPanel10.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtObsEnderecoPaciente, 1, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 283);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(1370, 98);
            this.tableLayoutPanel10.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "OBS:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtObsEnderecoPaciente
            // 
            this.txtObsEnderecoPaciente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObsEnderecoPaciente.Location = new System.Drawing.Point(99, 3);
            this.txtObsEnderecoPaciente.Name = "txtObsEnderecoPaciente";
            this.txtObsEnderecoPaciente.Size = new System.Drawing.Size(1268, 92);
            this.txtObsEnderecoPaciente.TabIndex = 2;
            this.txtObsEnderecoPaciente.Text = "";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(label15, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 381);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1370, 26);
            this.tableLayoutPanel11.TabIndex = 10;
            this.tableLayoutPanel11.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel11_Paint);
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 6;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.58079F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.41921F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 728F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 256F));
            this.tableLayoutPanel12.Controls.Add(this.label16, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtCepResidenciaAnterior, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtLogradouroResidenciaAnterior, 3, 0);
            this.tableLayoutPanel12.Controls.Add(this.label17, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.label18, 4, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtNumeroResidenciaAnterior, 5, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(0, 407);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(1370, 29);
            this.tableLayoutPanel12.TabIndex = 11;
            this.tableLayoutPanel12.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel12_Paint);
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(37, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "C.E.P.";
            // 
            // txtCepResidenciaAnterior
            // 
            this.txtCepResidenciaAnterior.Location = new System.Drawing.Point(55, 3);
            this.txtCepResidenciaAnterior.MaxLength = 8;
            this.txtCepResidenciaAnterior.Name = "txtCepResidenciaAnterior";
            this.txtCepResidenciaAnterior.Size = new System.Drawing.Size(100, 20);
            this.txtCepResidenciaAnterior.TabIndex = 1;
            this.txtCepResidenciaAnterior.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCepDadosAnteriores_KeyPress);
            // 
            // txtLogradouroResidenciaAnterior
            // 
            this.txtLogradouroResidenciaAnterior.Location = new System.Drawing.Point(320, 3);
            this.txtLogradouroResidenciaAnterior.Name = "txtLogradouroResidenciaAnterior";
            this.txtLogradouroResidenciaAnterior.Size = new System.Drawing.Size(315, 20);
            this.txtLogradouroResidenciaAnterior.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(226, 8);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(61, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Logradouro";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(1048, 8);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "Número";
            // 
            // txtNumeroResidenciaAnterior
            // 
            this.txtNumeroResidenciaAnterior.Location = new System.Drawing.Point(1116, 3);
            this.txtNumeroResidenciaAnterior.Name = "txtNumeroResidenciaAnterior";
            this.txtNumeroResidenciaAnterior.Size = new System.Drawing.Size(100, 20);
            this.txtNumeroResidenciaAnterior.TabIndex = 5;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 5;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.55458F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.44542F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 404F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 79F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 664F));
            this.tableLayoutPanel13.Controls.Add(this.label19, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.txtCompletoResidenciaAnterior, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.txtResponsavelResidenciaAnterior, 4, 0);
            this.tableLayoutPanel13.Controls.Add(this.label20, 3, 0);
            this.tableLayoutPanel13.Controls.Add(this.cmbMunicipioAnterior, 2, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 436);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(1370, 31);
            this.tableLayoutPanel13.TabIndex = 12;
            this.tableLayoutPanel13.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel13_Paint);
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(3, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Complemento";
            // 
            // txtCompletoResidenciaAnterior
            // 
            this.txtCompletoResidenciaAnterior.Location = new System.Drawing.Point(86, 3);
            this.txtCompletoResidenciaAnterior.Name = "txtCompletoResidenciaAnterior";
            this.txtCompletoResidenciaAnterior.Size = new System.Drawing.Size(52, 20);
            this.txtCompletoResidenciaAnterior.TabIndex = 1;
            // 
            // txtResponsavelResidenciaAnterior
            // 
            this.txtResponsavelResidenciaAnterior.Location = new System.Drawing.Point(708, 3);
            this.txtResponsavelResidenciaAnterior.Name = "txtResponsavelResidenciaAnterior";
            this.txtResponsavelResidenciaAnterior.Size = new System.Drawing.Size(173, 20);
            this.txtResponsavelResidenciaAnterior.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(629, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Responsavel";
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 2;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.788321F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 93.21168F));
            this.tableLayoutPanel14.Controls.Add(this.txtObsResidenciaAnterior, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.label21, 0, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(0, 467);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(1370, 109);
            this.tableLayoutPanel14.TabIndex = 13;
            this.tableLayoutPanel14.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel14_Paint);
            // 
            // txtObsResidenciaAnterior
            // 
            this.txtObsResidenciaAnterior.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObsResidenciaAnterior.Location = new System.Drawing.Point(95, 3);
            this.txtObsResidenciaAnterior.Name = "txtObsResidenciaAnterior";
            this.txtObsResidenciaAnterior.Size = new System.Drawing.Size(1272, 103);
            this.txtObsResidenciaAnterior.TabIndex = 0;
            this.txtObsResidenciaAnterior.Text = "";
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 48);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(32, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "OBS:";
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 4;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.83616F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.16384F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 369F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 507F));
            this.tableLayoutPanel21.Controls.Add(this.btnInativar, 3, 0);
            this.tableLayoutPanel21.Controls.Add(this.btnAtualizar, 2, 0);
            this.tableLayoutPanel21.Controls.Add(this.btnCadastrar, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.btnLimpar, 1, 0);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(0, 576);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(1370, 38);
            this.tableLayoutPanel21.TabIndex = 15;
            // 
            // btnInativar
            // 
            this.btnInativar.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnInativar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInativar.Location = new System.Drawing.Point(865, 3);
            this.btnInativar.Name = "btnInativar";
            this.btnInativar.Size = new System.Drawing.Size(153, 30);
            this.btnInativar.TabIndex = 3;
            this.btnInativar.Text = "Inativar Paciente";
            this.btnInativar.UseVisualStyleBackColor = false;
            this.btnInativar.Visible = false;
            this.btnInativar.Click += new System.EventHandler(this.btnInativar_Click);
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnAtualizar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizar.Location = new System.Drawing.Point(496, 3);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(153, 30);
            this.btnAtualizar.TabIndex = 2;
            this.btnAtualizar.Text = "Atualizar";
            this.btnAtualizar.UseVisualStyleBackColor = false;
            this.btnAtualizar.Visible = false;
            this.btnAtualizar.Click += new System.EventHandler(this.btnAtualizar_Click);
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnCadastrar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar.Location = new System.Drawing.Point(3, 3);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(75, 30);
            this.btnCadastrar.TabIndex = 0;
            this.btnCadastrar.Text = "Cadastrar";
            this.btnCadastrar.UseVisualStyleBackColor = false;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnLimpar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.Location = new System.Drawing.Point(135, 3);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(153, 30);
            this.btnLimpar.TabIndex = 1;
            this.btnLimpar.Text = "Limpar todos os campos";
            this.btnLimpar.UseVisualStyleBackColor = false;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 9;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 213F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 222F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 138F));
            this.tableLayoutPanel15.Controls.Add(this.txtNomeTipoIdentificacao, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.txtID, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.txtNomeRaca, 2, 0);
            this.tableLayoutPanel15.Controls.Add(this.txtNomeSexo, 3, 0);
            this.tableLayoutPanel15.Controls.Add(this.txtNomeEscolaridadade, 4, 0);
            this.tableLayoutPanel15.Controls.Add(this.txtNomeUFEnderecoAtual, 5, 0);
            this.tableLayoutPanel15.Controls.Add(this.txtNomeMunicipioEnderecoAtual, 6, 0);
            this.tableLayoutPanel15.Controls.Add(this.txtNomeUFEnderecoAnterior, 7, 0);
            this.tableLayoutPanel15.Controls.Add(this.txtNomeMunicipioEnderecoAnterior, 8, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(0, 614);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(1370, 43);
            this.tableLayoutPanel15.TabIndex = 16;
            // 
            // txtNomeTipoIdentificacao
            // 
            this.txtNomeTipoIdentificacao.Location = new System.Drawing.Point(241, 3);
            this.txtNomeTipoIdentificacao.Name = "txtNomeTipoIdentificacao";
            this.txtNomeTipoIdentificacao.Size = new System.Drawing.Size(100, 20);
            this.txtNomeTipoIdentificacao.TabIndex = 17;
            this.txtNomeTipoIdentificacao.Visible = false;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(3, 3);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 20);
            this.txtID.TabIndex = 0;
            this.txtID.Visible = false;
            // 
            // txtNomeRaca
            // 
            this.txtNomeRaca.Location = new System.Drawing.Point(479, 3);
            this.txtNomeRaca.Name = "txtNomeRaca";
            this.txtNomeRaca.Size = new System.Drawing.Size(100, 20);
            this.txtNomeRaca.TabIndex = 18;
            this.txtNomeRaca.Visible = false;
            // 
            // txtNomeSexo
            // 
            this.txtNomeSexo.Location = new System.Drawing.Point(692, 3);
            this.txtNomeSexo.Name = "txtNomeSexo";
            this.txtNomeSexo.Size = new System.Drawing.Size(100, 20);
            this.txtNomeSexo.TabIndex = 19;
            this.txtNomeSexo.Visible = false;
            // 
            // txtNomeEscolaridadade
            // 
            this.txtNomeEscolaridadade.Location = new System.Drawing.Point(914, 3);
            this.txtNomeEscolaridadade.Name = "txtNomeEscolaridadade";
            this.txtNomeEscolaridadade.Size = new System.Drawing.Size(100, 20);
            this.txtNomeEscolaridadade.TabIndex = 20;
            this.txtNomeEscolaridadade.Visible = false;
            // 
            // txtNomeUFEnderecoAtual
            // 
            this.txtNomeUFEnderecoAtual.Location = new System.Drawing.Point(1042, 3);
            this.txtNomeUFEnderecoAtual.Name = "txtNomeUFEnderecoAtual";
            this.txtNomeUFEnderecoAtual.Size = new System.Drawing.Size(34, 20);
            this.txtNomeUFEnderecoAtual.TabIndex = 21;
            this.txtNomeUFEnderecoAtual.Visible = false;
            // 
            // txtNomeMunicipioEnderecoAtual
            // 
            this.txtNomeMunicipioEnderecoAtual.Location = new System.Drawing.Point(1082, 3);
            this.txtNomeMunicipioEnderecoAtual.Name = "txtNomeMunicipioEnderecoAtual";
            this.txtNomeMunicipioEnderecoAtual.Size = new System.Drawing.Size(47, 20);
            this.txtNomeMunicipioEnderecoAtual.TabIndex = 22;
            this.txtNomeMunicipioEnderecoAtual.Visible = false;
            // 
            // txtNomeUFEnderecoAnterior
            // 
            this.txtNomeUFEnderecoAnterior.Location = new System.Drawing.Point(1135, 3);
            this.txtNomeUFEnderecoAnterior.Name = "txtNomeUFEnderecoAnterior";
            this.txtNomeUFEnderecoAnterior.Size = new System.Drawing.Size(94, 20);
            this.txtNomeUFEnderecoAnterior.TabIndex = 23;
            this.txtNomeUFEnderecoAnterior.Visible = false;
            // 
            // txtNomeMunicipioEnderecoAnterior
            // 
            this.txtNomeMunicipioEnderecoAnterior.Location = new System.Drawing.Point(1235, 3);
            this.txtNomeMunicipioEnderecoAnterior.Name = "txtNomeMunicipioEnderecoAnterior";
            this.txtNomeMunicipioEnderecoAnterior.Size = new System.Drawing.Size(100, 20);
            this.txtNomeMunicipioEnderecoAnterior.TabIndex = 24;
            this.txtNomeMunicipioEnderecoAnterior.Visible = false;
            // 
            // cmbMunicipioAnterior
            // 
            this.cmbMunicipioAnterior.Location = new System.Drawing.Point(225, 3);
            this.cmbMunicipioAnterior.Name = "cmbMunicipioAnterior";
            this.cmbMunicipioAnterior.Size = new System.Drawing.Size(365, 22);
            this.cmbMunicipioAnterior.TabIndex = 4;
            // 
            // cmbMunicipios
            // 
            this.cmbMunicipios.Location = new System.Drawing.Point(162, 3);
            this.cmbMunicipios.Name = "cmbMunicipios";
            this.cmbMunicipios.Size = new System.Drawing.Size(363, 25);
            this.cmbMunicipios.TabIndex = 5;
            // 
            // cmbEscolaridade
            // 
            this.cmbEscolaridade.AutoScroll = true;
            this.cmbEscolaridade.Location = new System.Drawing.Point(733, 3);
            this.cmbEscolaridade.Name = "cmbEscolaridade";
            this.cmbEscolaridade.Size = new System.Drawing.Size(299, 26);
            this.cmbEscolaridade.TabIndex = 4;
            // 
            // cmbRaca
            // 
            this.cmbRaca.Location = new System.Drawing.Point(398, 3);
            this.cmbRaca.Name = "cmbRaca";
            this.cmbRaca.Size = new System.Drawing.Size(243, 27);
            this.cmbRaca.TabIndex = 4;
            // 
            // cmbTipoRegistro
            // 
            this.cmbTipoRegistro.Location = new System.Drawing.Point(373, 3);
            this.cmbTipoRegistro.Name = "cmbTipoRegistro";
            this.cmbTipoRegistro.Size = new System.Drawing.Size(330, 29);
            this.cmbTipoRegistro.TabIndex = 4;
            // 
            // fmrCadastroPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(1370, 669);
            this.Controls.Add(this.tableLayoutPanel15);
            this.Controls.Add(this.tableLayoutPanel21);
            this.Controls.Add(this.tableLayoutPanel14);
            this.Controls.Add(this.tableLayoutPanel13);
            this.Controls.Add(this.tableLayoutPanel12);
            this.Controls.Add(this.tableLayoutPanel11);
            this.Controls.Add(this.tableLayoutPanel10);
            this.Controls.Add(this.tableLayoutPanel9);
            this.Controls.Add(this.tableLayoutPanel8);
            this.Controls.Add(this.tableLayoutPanel7);
            this.Controls.Add(this.tableLayoutPanel6);
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.Name = "fmrCadastroPaciente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Paciente";
            this.Load += new System.EventHandler(this.fmrCadastroPaciente_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblCpf;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label lblTel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lblContatoEmergencia;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label lblLogradouro;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        public System.Windows.Forms.Button btnAtualizar;
        public System.Windows.Forms.Button btnInativar;
        public System.Windows.Forms.TextBox txtNome;
        public System.Windows.Forms.CheckBox chkAtivo;
        public System.Windows.Forms.TextBox txtCpf;
        public System.Windows.Forms.TextBox txtTipoDoc;
        public System.Windows.Forms.DateTimePicker dtpDataNasc;
        public User_control.UserControlTipoRaca cmbRaca;
        public System.Windows.Forms.TextBox txtCns;
        public System.Windows.Forms.RadioButton rdbMasculino;
        public System.Windows.Forms.RadioButton rdbFeminino;
        public System.Windows.Forms.RadioButton rdbOutros;
        public System.Windows.Forms.DateTimePicker dtpAdmissao;
        public User_control.UserControlTipoEscolaridade cmbEscolaridade;
        public System.Windows.Forms.TextBox txtTel1;
        public System.Windows.Forms.TextBox txtTel2;
        public System.Windows.Forms.TextBox txtNomeContato;
        public System.Windows.Forms.TextBox txtNumeroDeEmergencia;
        public System.Windows.Forms.TextBox txtContatoDeEmergencia;
        public System.Windows.Forms.TextBox txtLogradouro;
        public System.Windows.Forms.TextBox txtCep;
        public System.Windows.Forms.TextBox txtNumero;
        public System.Windows.Forms.TextBox txtComplemento;
        public User_control.UserControlTipoUF cmbMunicipios;
        public System.Windows.Forms.TextBox txtResponsavelEndereco;
        public System.Windows.Forms.RichTextBox txtObsEnderecoPaciente;
        public System.Windows.Forms.TextBox txtCepResidenciaAnterior;
        public System.Windows.Forms.TextBox txtLogradouroResidenciaAnterior;
        public System.Windows.Forms.TextBox txtNumeroResidenciaAnterior;
        public System.Windows.Forms.TextBox txtCompletoResidenciaAnterior;
        public User_control.UserControlTipoUF cmbMunicipioAnterior;
        public System.Windows.Forms.TextBox txtResponsavelResidenciaAnterior;
        public System.Windows.Forms.RichTextBox txtObsResidenciaAnterior;
        public System.Windows.Forms.Button btnCadastrar;
        public System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        public System.Windows.Forms.TextBox txtID;
        public System.Windows.Forms.TextBox txtNomeTipoIdentificacao;
        public System.Windows.Forms.TextBox txtNomeRaca;
        public System.Windows.Forms.TextBox txtNomeSexo;
        public System.Windows.Forms.TextBox txtNomeEscolaridadade;
        public System.Windows.Forms.TextBox txtNomeUFEnderecoAtual;
        public System.Windows.Forms.TextBox txtNomeMunicipioEnderecoAtual;
        public System.Windows.Forms.TextBox txtNomeUFEnderecoAnterior;
        public System.Windows.Forms.TextBox txtNomeMunicipioEnderecoAnterior;
        private User_control.UserControlTipodeIdentificacao cmbTipoRegistro;
    }
}