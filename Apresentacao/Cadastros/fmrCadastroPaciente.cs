﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrCadastroPaciente : Form
    {
        public fmrCadastroPaciente()
        {
            InitializeComponent();
        }


        Controle control = new Controle();


        private void chkAtivo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAtivo.Checked)
            {
            }
            else
            {
                MessageBox.Show("Atenção. Ao desmarcar essa opção o registro será cadastrado como inativo. Isso  significa que o registro não será listado como um paciente ativo. ");

            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (txtCpf.TextLength == 11)
            {
                long cpf = Convert.ToInt64(txtCpf.Text);
                string cpfFormatado = String.Format(@"{0:000\.000\.000\-00}", cpf);
                txtCpf.Text = cpfFormatado;
            }
            //se tiver menos  14 digitos significa q o usuario apagou algum 
            else if ((txtCpf.TextLength > 11)&& (txtCpf.TextLength < 14))
            {
                string newcpfFormat = txtCpf.Text;
                newcpfFormat = newcpfFormat.Replace(".","");
                newcpfFormat = newcpfFormat.Replace("-", "");
                txtCpf.Text = newcpfFormat;
            }
        }



        private void txtTipoDoc_TextChanged(object sender, EventArgs e)
        {

            
        }

        private void txtCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                MessageBox.Show("O campo aceita apenas números");
            }
        }

        private void textBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Informe o CNS ( Cadastro Nacional do S.U.S.)", txtCns);
        }

        private void label2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Informe o CNS ( Cadastro Nacional do S.U.S.)", txtCns);
        }

        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                MessageBox.Show("O campo aceita apenas números");
            }
        }

        private void tableLayoutPanel8_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel7_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel9_Paint(object sender, PaintEventArgs e)
        {

        }

        private void fmrCadastroPaciente_Load(object sender, EventArgs e)
        {

            if ((!string.IsNullOrWhiteSpace(txtNomeRaca.Text)))
            {

              
                cmbRaca.cmbRaca.SelectedIndex = cmbRaca.cmbRaca.FindStringExact(txtNomeRaca.Text);
            }
            if ((!string.IsNullOrWhiteSpace(txtNomeTipoIdentificacao.Text)))
            {
                cmbTipoRegistro.cmbTipoIdentificacao.SelectedIndex = cmbTipoRegistro.cmbTipoIdentificacao.FindStringExact(txtNomeTipoIdentificacao.Text);
            }
            if ((!string.IsNullOrWhiteSpace(txtNomeEscolaridadade.Text)))
            {
                cmbEscolaridade.cmbTipoEscolaridade.SelectedIndex = cmbEscolaridade.cmbTipoEscolaridade.FindStringExact(txtNomeEscolaridadade.Text);
            }
            if ((!string.IsNullOrWhiteSpace(txtNomeUFEnderecoAtual.Text)))
            {
                cmbMunicipios.cmbUF.SelectedIndex = cmbMunicipios.cmbUF.FindStringExact(txtNomeUFEnderecoAtual.Text);
            }
            if ((!string.IsNullOrWhiteSpace(txtNomeMunicipioEnderecoAtual.Text)))
            {
                cmbMunicipios.cmbMunicipio.SelectedIndex = cmbMunicipios.cmbMunicipio.FindStringExact(txtNomeMunicipioEnderecoAtual.Text);
            }

            if ((!string.IsNullOrWhiteSpace(txtNomeUFEnderecoAnterior.Text)))
            {
                cmbMunicipioAnterior.cmbUF.SelectedIndex = cmbMunicipioAnterior.cmbUF.FindStringExact(txtNomeUFEnderecoAnterior.Text);
            }
            if ((!string.IsNullOrWhiteSpace(txtNomeMunicipioEnderecoAnterior.Text)))
            {
                cmbMunicipioAnterior.cmbMunicipio.SelectedIndex = cmbMunicipioAnterior.cmbMunicipio.FindStringExact(txtNomeMunicipioEnderecoAnterior.Text);
            }

            if ((!string.IsNullOrWhiteSpace(txtNomeSexo.Text)))
            {
                switch (txtNomeSexo.Text)
                {
                    case "Masculino":
                        rdbMasculino.Checked = true;
                        break;
                    case "Feminino":
                        rdbFeminino.Checked = true;
                        break;
                    case "Outros":
                        rdbOutros.Checked = true;
                        break;
                }
                cmbMunicipioAnterior.cmbMunicipio.SelectedIndex = cmbMunicipioAnterior.cmbMunicipio.FindStringExact(txtNomeMunicipioEnderecoAnterior.Text);
            }
            if (!chkAtivo.Checked)
            {
                btnInativar.Text = "Reativar";
            }

        }

        private void tableLayoutPanel11_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel12_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtCepDadosAnteriores_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                MessageBox.Show("O campo aceita apenas números");
            }

        }

        private void tableLayoutPanel13_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel14_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                MessageBox.Show("O campo aceita apenas números");
            }
        }

        private void tableLayoutPanel18_Paint(object sender, PaintEventArgs e)
        {

        }

        public bool VerifcaAtivoInativo(bool ativoinativo)
        {

            if (ativoinativo == true)
            {
                return ativoinativo = true;
            }
            else
            {
                return ativoinativo = false;
            }

        }

        public void limpaCampos()
        {
            btnInativar.Visible = Enabled;
            btnAtualizar.Visible = Enabled;
            txtNome.Clear();
            txtCpf.Clear();
            chkAtivo.Checked = VerifcaAtivoInativo(true);
            // chkAtivo.Checked;
            txtTipoDoc.Clear();
            txtCns.Clear();
            txtTel1.Clear();
            txtTel2.Clear();
            txtNomeContato.Clear();
            txtContatoDeEmergencia.Clear();
            txtCep.Clear();
            txtContatoDeEmergencia.Clear();
            txtLogradouro.Clear();
            txtNumero.Clear();
            txtContatoDeEmergencia.Clear();
            txtComplemento.Clear();
            txtResponsavelEndereco.Clear();
            txtObsEnderecoPaciente.Clear();
            txtCepResidenciaAnterior.Clear();
            txtLogradouroResidenciaAnterior.Clear();
            txtNumeroResidenciaAnterior.Clear();
            txtCompletoResidenciaAnterior.Clear();
            txtResponsavelResidenciaAnterior.Clear();
            txtObsResidenciaAnterior.Clear();
            txtNumeroDeEmergencia.Clear();
            dtpAdmissao.Text = DateTime.Now.ToString("dd-MM-yyyy"); 
            dtpDataNasc.Text = DateTime.Now.ToString("dd-MM-yyyy");
            rdbMasculino.Checked = true;

        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            RadioButton radioBtn = tableLayoutPanel4.Controls.OfType<RadioButton>()
                                         .Where(x => x.Checked).FirstOrDefault();
        
            String nome = txtNome.Text;
            String cpf = txtCpf.Text;
            bool ativoinativo = VerifcaAtivoInativo(chkAtivo.Checked);
            String tipoIdentificacao = cmbTipoRegistro.SelectTipoIdentidade.nomeTipoIdentidade;
            String numIdentificacao = txtTipoDoc.Text;
            String dataNascimento = dtpDataNasc.Value.Date.ToString("dd-MM-yyyy");
            String raca = cmbRaca.SelectTipoRaca.nomeTipoRaca;
            String cns = txtCns.Text;
            String sexo = radioBtn.Text;
            String dataAdmissao = dtpAdmissao.Value.Date.ToString("dd-MM-yyyy");
            String escolaridade = cmbEscolaridade.SelectTipoEscolarodade.nomeTipoEscolaridade;
            String telefone1 = txtTel1.Text;
            String telefone2 = txtTel2.Text;
            String nomeContato = txtNomeContato.Text;
            String telefoneContatoEmergencia = txtContatoDeEmergencia.Text;
            String nomeContatoEmergencia = txtContatoDeEmergencia.Text;
            String cep = txtCep.Text;
            String logradouro = txtLogradouro.Text;
            String numLogradouro = txtNumero.Text;
            String complemento = txtComplemento.Text;
            String uf = cmbMunicipios.SelectMunicipios.siglaUF;
            String municipio = cmbMunicipios.SelectMunicipios.nomeMunicipio;
            String nomeResponsavelCep = txtResponsavelEndereco.Text;
            String obsEndereco = txtObsEnderecoPaciente.Text;
            String cepAnterior = txtCepResidenciaAnterior.Text;
            String logradouroAnterior = txtLogradouroResidenciaAnterior.Text;
            String numLogradouroAnterior = txtNumeroResidenciaAnterior.Text;
            String complementoAnterior = txtCompletoResidenciaAnterior.Text;
            String ufAnterior = cmbMunicipioAnterior.SelectMunicipios.siglaUF;
            String municipioAnterior = cmbMunicipioAnterior.SelectMunicipios.nomeMunicipio;
            String nomeResponsavelCepAnterior = txtResponsavelResidenciaAnterior.Text;
            String obsEnderecoAnterior = txtObsResidenciaAnterior.Text;
       




            String mensagem = control.cadastroPaciente(nome, cpf, ativoinativo, tipoIdentificacao, numIdentificacao,dataNascimento, raca, cns, sexo, dataAdmissao, 
                escolaridade, telefone1, telefone2, nomeContato, telefoneContatoEmergencia, nomeContatoEmergencia, cep, logradouro, numLogradouro, 
                complemento, uf, municipio, nomeResponsavelCep, obsEndereco, cepAnterior, logradouroAnterior, numLogradouroAnterior, complementoAnterior, 
                ufAnterior, municipioAnterior, nomeResponsavelCepAnterior, obsEnderecoAnterior);
                if (control.tem)
                {
                    MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpaCampos();
                }
                else
                {
                    MessageBox.Show(control.msg);
                }
            }

        string AplicarMascaraTelefone(string strNumero)
        {
            // por omissão tem 10 ou menos dígitos
            string strMascara = "{0:(00)0000-0000}";
            // converter o texto em número
            long lngNumero = Convert.ToInt64(strNumero);

            if (strNumero.Length == 11)
                strMascara = "{0:(00)00000-0000}";

            return string.Format(strMascara, lngNumero);
        }

        private void txtTel1_TextChanged(object sender, EventArgs e)
        {
            try
            {

                if (txtTel1.TextLength == 10)
                {
                    long tel = Convert.ToInt64(txtTel1.Text);
                    string telconvertido = String.Format(@"{0:(00)00000000}", tel);
                    txtTel1.Text = telconvertido;
                    txtTel1.SelectionStart = txtTel1.Text.Length;
                    txtTel1.ScrollToCaret();

                }
                else if (txtTel1.TextLength == 14)
                {
                    string apneasnumtel1 = txtTel1.Text;
                    apneasnumtel1 = apneasnumtel1.Replace("(", "");
                    apneasnumtel1 = apneasnumtel1.Replace(")", "");
                    apneasnumtel1 = apneasnumtel1.Replace("-", "");
                    txtTel1.Text = apneasnumtel1;

                    long tel = Convert.ToInt64(txtTel1.Text);
                    string telconvertido = String.Format(@"{0:(00)000000000}", tel);
                    txtTel1.Text = telconvertido;
                    txtTel1.SelectionStart = txtTel1.Text.Length;
                    txtTel1.ScrollToCaret();

                }
                else if((txtTel1.TextLength ==11))
                {
                    string apneasnumtel1 = txtTel1.Text;
                    apneasnumtel1 = apneasnumtel1.Replace("(", "");
                    apneasnumtel1 = apneasnumtel1.Replace(")", "");
                    apneasnumtel1 = apneasnumtel1.Replace("-", "");
                    txtTel1.Text = apneasnumtel1;
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show("Ops. Tivemos um problema"+ex);
            }
      

        }

        private void txtTel1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                MessageBox.Show("O campo aceita apenas números");
            }
        }

        private void txtTel2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Enter) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
                MessageBox.Show("O campo aceita apenas números");
            }
        }

        private void txtTel2_TextChanged(object sender, EventArgs e)
        {
            if (txtTel2.TextLength == 10)
            {
                long tel = Convert.ToInt64(txtTel2.Text);
                string telconvertido = String.Format(@"{0:(00)00000000}", tel);
                txtTel2.Text = telconvertido;
                txtTel2.SelectionStart = txtTel1.Text.Length;
                txtTel2.ScrollToCaret();

            }
            else if (txtTel2.TextLength == 14)
            {
                string apneasnumtel1 = txtTel1.Text;
                apneasnumtel1 = apneasnumtel1.Replace("(", "");
                apneasnumtel1 = apneasnumtel1.Replace(")", "");
                apneasnumtel1 = apneasnumtel1.Replace("-", "");
                txtTel2.Text = apneasnumtel1;

                long tel = Convert.ToInt64(txtTel2.Text);
                string telconvertido = String.Format(@"{0:(00)000000000}", tel);
                txtTel2.Text = telconvertido;
                txtTel2.SelectionStart = txtTel1.Text.Length;
                txtTel2.ScrollToCaret();

            }
            else if ((txtTel2.TextLength == 11))
            {
                string apneasnumtel1 = txtTel2.Text;
                apneasnumtel1 = apneasnumtel1.Replace("(", "");
                apneasnumtel1 = apneasnumtel1.Replace(")", "");
                apneasnumtel1 = apneasnumtel1.Replace("-", "");
                txtTel2.Text = apneasnumtel1;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void TabPage2_Click(object sender, EventArgs e)
        {

        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            var userInput = MessageBox.Show("Você tem certeza que deseja limpar todos os campos desse formulário?", "Decisão", MessageBoxButtons.YesNo);
            if (userInput == DialogResult.Yes)
            {
                limpaCampos();
            }
            else
            {
                MessageBox.Show("Nenhuma alteração foi realizada");
            }
        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtID.Text);
            RadioButton radioBtn = tableLayoutPanel4.Controls.OfType<RadioButton>()
                                                 .Where(x => x.Checked).FirstOrDefault();

            String nome = txtNome.Text;
            String cpf = txtCpf.Text;
            bool ativoinativo = VerifcaAtivoInativo(chkAtivo.Checked);
            String tipoIdentificacao = cmbTipoRegistro.SelectTipoIdentidade.nomeTipoIdentidade;
            String numIdentificacao = txtTipoDoc.Text;
            String dataNascimento = dtpDataNasc.Value.Date.ToString("dd-MM-yyyy");
            String raca = cmbRaca.SelectTipoRaca.nomeTipoRaca;
            String cns = txtCns.Text;
            String sexo = radioBtn.Text;
            String dataAdmissao = dtpAdmissao.Value.Date.ToString("dd-MM-yyyy");
            String escolaridade = cmbEscolaridade.SelectTipoEscolarodade.nomeTipoEscolaridade;
            String telefone1 = txtTel1.Text;
            String telefone2 = txtTel2.Text;
            String nomeContato = txtNomeContato.Text;
            String telefoneContatoEmergencia = txtNumeroDeEmergencia.Text;
            String nomeContatoEmergencia = txtContatoDeEmergencia.Text;
            String cep = txtCep.Text;
            String logradouro = txtLogradouro.Text;
            String numLogradouro = txtNumero.Text;
            String complemento = txtComplemento.Text;
            String uf = cmbMunicipios.SelectMunicipios.siglaUF;
            String municipio = cmbMunicipios.SelectMunicipios.nomeMunicipio;
            String nomeResponsavelCep = txtResponsavelEndereco.Text;
            String obsEndereco = txtObsEnderecoPaciente.Text;
            String cepAnterior = txtCepResidenciaAnterior.Text;
            String logradouroAnterior = txtLogradouroResidenciaAnterior.Text;
            String numLogradouroAnterior = txtNumeroResidenciaAnterior.Text;
            String complementoAnterior = txtCompletoResidenciaAnterior.Text;
            String ufAnterior = cmbMunicipioAnterior.SelectMunicipios.siglaUF;
            String municipioAnterior = cmbMunicipioAnterior.SelectMunicipios.nomeMunicipio;
            String nomeResponsavelCepAnterior = txtResponsavelResidenciaAnterior.Text;
            String obsEnderecoAnterior = txtObsResidenciaAnterior.Text;

            String mensagem = control.atualizaPaciente(id, nome, cpf, ativoinativo, tipoIdentificacao, numIdentificacao, dataNascimento, raca, cns, sexo, dataAdmissao,
                escolaridade, telefone1, telefone2, nomeContato, telefoneContatoEmergencia, nomeContatoEmergencia, cep, logradouro, numLogradouro,
                complemento, uf, municipio, nomeResponsavelCep, obsEndereco, cepAnterior, logradouroAnterior, numLogradouroAnterior, complementoAnterior,
                ufAnterior, municipioAnterior, nomeResponsavelCepAnterior, obsEnderecoAnterior);
            if (control.tem)
            {
                MessageBox.Show(mensagem, "Atualização", MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpaCampos();
                btnLimpar.Visible = true;
                btnCadastrar.Visible = true;
                btnAtualizar.Visible = false;
                btnInativar.Visible = false;
            }
            else
            {
                MessageBox.Show(control.msg);
            }
        }

        private void btnInativar_Click(object sender, EventArgs e)
        {
            this.Hide();
            fmrInativacaoPaciente inativapac = new fmrInativacaoPaciente();
            inativapac.txtNomePaciente.Text = txtNome.Text;
            inativapac.txtCodPaciente.Text = txtID.Text;
            inativapac.chkAtivo.Checked = chkAtivo.Checked;
            inativapac.Show();
            this.Close();
        }
    }
}
