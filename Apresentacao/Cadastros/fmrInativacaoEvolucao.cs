﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrInativacaoEvolucao : Form
    {
        public fmrInativacaoEvolucao()
        {
            InitializeComponent();
        }

        Controle control = new Controle();

        private void btnInativar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtIdRegistro.Text);

            DateTime dataHoraAgora = DateTime.Now;
            string msgInativativacaoFull = ("Motivo da Inativação: " + (txtMotivoInativacao.Text) + " Horário de Inativação : " + dataHoraAgora.ToString("dd-MM-yyyy h:mm:ss tt") + " Usuário que inativou: " + Usuario.NomeUsuario
                );

            String mensagem = control.inativaEvolucao(id, msgInativativacaoFull);

            if (control.tem)
            {
                MessageBox.Show(mensagem, "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Text = "Operação Executada com sucesso";
                this.Close();
            }
            else
            {
                MessageBox.Show(control.msg);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
