﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using SistemaShekinah.Modelo;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrCadastroFoto : Form
    {
        public fmrCadastroFoto()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private FilterInfoCollection dispositivo;
        private VideoCaptureDevice imagem;
        Controle control = new Controle();

        private void fmrCadastroFoto_Load(object sender, EventArgs e)
        {
            try
            {
                dispositivo = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                foreach (FilterInfo capturarDispositivo in dispositivo)
                {
                    cmbWebCam.Items.Add(capturarDispositivo.Name);
                }
                cmbWebCam.SelectedIndex = 0;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Nenhuma fonte de imagem foi identificada"+ex);
            }
        }

        private void btnIniciarCamera_Click(object sender, EventArgs e)
        {
            if (cmbWebCam.Items.Count == 0)
            {
                MessageBox.Show("Nenhuma fonte de imagem foi localizada. Tente reiniciar o computador, em muitos casos o problema é solucionado. Caso continue vendo essa mensagem , contate o suporte técnico ");
            }
            else
            {
                imagem = new VideoCaptureDevice(dispositivo[cmbWebCam.SelectedIndex].MonikerString);
                imagem.NewFrame += new NewFrameEventHandler(camera);
                imagem.Start();
                btnIniciarCamera.Enabled = false;
                btnCapturar.Enabled = true;
                btnEncerrarImagem.Enabled = true;
                
               
            }
      
        }
        void camera(object sender, NewFrameEventArgs eventargs)
        {
            Bitmap bitmap = (Bitmap)eventargs.Frame.Clone();
            pictureBox1.Image = bitmap;
        }

        private void btnCapturar_Click(object sender, EventArgs e)
        {

            if (imagem.IsRunning)
            {
                imagem.Stop();
                var userInput = MessageBox.Show("A imagem ficou boa?Note, ao clicar no botão SIM , essa imagem será salva no banco de dados. Clicando no botão NÃO ela será descartada e você poderá tirar outra foto", "Decisão", MessageBoxButtons.YesNo);
                if (userInput == DialogResult.No)
                {
                    imagem.Start();
                }
                else
                {
                    MemoryStream strem = new MemoryStream();
                    pictureBox1.Image.Save(strem, System.Drawing.Imaging.ImageFormat.Jpeg);
                    byte[] pic = strem.ToArray();

                    String mensagem = control.CadastraFotoPaciente(userControlPaciente1.SelectPaciente.Id,chkAtivo.Checked,txtDescricao.Text, pic);
                    if (control.tem)
                    {
                        MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        imagem.Start();
                    }
                    else
                    {
                        MessageBox.Show(control.msg);
                    }
                }
            }
        }

        private void exitcamera()
        {
            imagem.SignalToStop();
            // FinalVideo.WaitForStop();  << marking out that one solved it
            imagem.NewFrame -= new NewFrameEventHandler(camera); // as sugested
            imagem = null;
            pictureBox1.Image = null;
        }

        private void btnPausarImagem_Click(object sender, EventArgs e)
        {
           
            if (imagem.IsRunning)
            {
                exitcamera();
                btnEncerrarImagem.Enabled = false;
                btnIniciarCamera.Enabled = true;
                btnCapturar.Enabled = false;
            }
        }
    }
}
