﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrCadastroSaidaPaciente : Form
    {
        public fmrCadastroSaidaPaciente()
        {
            InitializeComponent();
        }

        private void richTextBox1_MouseMove(object sender, MouseEventArgs e)
        {
            toolTip1.Show("Digite nesse campo informações da saida do paciente( informe detalhadamente todas as características inerentes a sua saída) ",txtTextoSaida);

        }

        Controle control = new Controle();

        public bool VerifcaAtivoInativo(bool ativoinativo)
        {
            if (ativoinativo == true)
            {
                return ativoinativo = true;
            }
            else
            {
                return ativoinativo = false;
            }
        }

        public void LimpaCampos()
        {
            txtTextoSaida.Clear();
        }


        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            //String DiaAtual = DateTime.Now.ToString("dd-MM-yyyy");
            //String DiaDatePicker = dtpDia.Value.Date.ToString("dd-MM-yyyy");
            DateTime DataHoraSelecionada = (dtpDia.Value.Date) + (dtpHora.Value.TimeOfDay);

     
            //String DiaExpectativaRetorno = dtpDataExpectativaRetorno.Value.Date.ToString("dd-MM-yyyy");
            DateTime DataHoraExpectativaRetorno = (dtpDataExpectativaRetorno.Value.Date) + (dtpHoraExpectativaRetorno.Value.TimeOfDay);

      
                String mensagem = control.cadastroSaidaDePaciente(userControlPaciente1.SelectPaciente.Id, DataHoraSelecionada, txtTextoSaida.Text, VerifcaAtivoInativo(chkAtivo.Checked), DataHoraExpectativaRetorno);

                if (control.tem)
                {
                    MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LimpaCampos();
                }
                else
                {
                    MessageBox.Show(mensagem, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
 
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void dtpDia_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Selecione o Dia ", dtpDia);
        }

        private void dtpHora_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Selecione a Hora ", dtpHora);
        }

        private void userControlPaciente1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Selecione o paciente. OBS: caso não esteja aparecendo o paciente , pode ser que o mesmo esteja inativado ", userControlPaciente1);
        }

        private void fmrCadastroSaidaPaciente_Load(object sender, EventArgs e)
        {

            if ((!string.IsNullOrWhiteSpace(txtNomeDoPaciente.Text)) && (!string.IsNullOrWhiteSpace(txtNomeDoPaciente.Text)))
            {
                userControlPaciente1.cmbPaciente.SelectedIndex = userControlPaciente1.cmbPaciente.FindStringExact(txtNomeDoPaciente.Text);
            }
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnInativar_Click(object sender, EventArgs e)
        {
            this.Hide();
            fmrInativacaoEntradaSaida entradasaida = new fmrInativacaoEntradaSaida();
            entradasaida.txtId.Text = txtCodigo.Text;
            entradasaida.Show();
            this.Close();
        }
    }
}
