﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Cadastros
{
    public partial class fmrCadastroUsuario : Form
    {
        Controle control = new Controle();
        public fmrCadastroUsuario()
        {
            InitializeComponent();
        }

        public void limpaDados()
        {
            txtSenha.Clear();
            txtLogin.Clear();
            txtNome.Clear();
            txtCodUser.Clear();
            chkLanRetroativo.CheckState = CheckState.Unchecked;
            chkAtivo.Checked = true;
        }

        public int VerifcaAtivoInativo(bool ativoinativo)
        {
            int opcao;
           
            if (ativoinativo == true)
            {
                return opcao = 1;
            }
            else
            {
                return opcao = 0;
            }


        }


        private void btnLimpar_Click(object sender, EventArgs e)
        {
            limpaDados();
        }

        private void chkAtivo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAtivo.Checked)
            {
                MessageBox.Show("O usuário será cadastrado como um registro ativo");
            }
            else
            {
                MessageBox.Show("Esse usuário será cadastrado como um registro inativo");
            }
        }

        private void chkLanRetroativo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAtivo.Checked)
            {
                MessageBox.Show("Atenção. Habilitando essa opção o usuário poderá lançar dados retroativamente (ex: lançar evoluções com a data de ontem)");
            }
           
          
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            string login = txtLogin.Text;
            string senha = txtSenha.Text;
            string nomeUsuario = txtNome.Text;
            bool ativo = chkAtivo.Checked;
            bool permiteRetroativo = chkLanRetroativo.Checked;
            int idniveluser = Convert.ToInt32(cmbNivelUser.SelectNivelUsuario.Id.ToString());



            String mensagem = control.cadastroDeUsuarios (login, senha, idniveluser, permiteRetroativo, ativo, nomeUsuario);

            if (control.tem)
            {
                MessageBox.Show(mensagem, "Cadastro", MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpaDados();
            }
            else
            {
                MessageBox.Show(control.msg);
            }

        }

        private void btnAtualizar_Click(object sender, EventArgs e)
        {
            string login = txtLogin.Text;
            string senha = txtSenha.Text;
            string nomeUsuario = txtNome.Text;
            bool ativo = chkAtivo.Checked;
            bool permiteRetroativo = chkLanRetroativo.Checked;
            int id =Convert.ToInt32(txtCodUser.Text);
            int idniveluser = Convert.ToInt32(cmbNivelUser.SelectNivelUsuario.Id.ToString());
            String mensagem = control.atualizaUsuario(login, senha, idniveluser, permiteRetroativo, ativo, nomeUsuario, id);

            if (control.tem)
            {
                MessageBox.Show(mensagem, "Atualização", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                MessageBox.Show(control.msg);
            }
        }

        private void btnInativarRegistro_Click(object sender, EventArgs e)
        {
            int verifica = VerifcaAtivoInativo(chkAtivo.Checked);
            int id = Convert.ToInt32(txtCodUser.Text);
   
            String mensagem = control.inativaUsuario(verifica, id);

            if (control.tem)
            {
                MessageBox.Show(mensagem, "Inativação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpaDados();
                this.Close();
            }
            else
            {
                MessageBox.Show(control.msg);
                btnInativarRegistro.Enabled = false;
            }

        }

        private void FmrCadastroUsuario_Load(object sender, EventArgs e)
        {

            if ((!string.IsNullOrWhiteSpace(txtNomeNivel.Text)))
            {
                cmbNivelUser.cmbNivelUser.SelectedIndex = cmbNivelUser.cmbNivelUser.FindStringExact(txtNomeNivel.Text);
            }
        }
    }
}
