﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.DAL;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao
{
    public partial class fmrListagemEvolucoes : Form
    {
        public fmrListagemEvolucoes()
        {
            InitializeComponent();
        }

        private void fmrListagemEvolucoes_Load(object sender, EventArgs e)
        {
            string verificaAtivoOuInativo = tudoOuSoAtivo(VerifcaAtivoInativo(userControlPacienteListCheckbox2.chkAtivo.Checked));
            carregaEvolucoes();
        }

        private static void adjustListSize(ListBox lst)
        {
            int h = lst.ItemHeight * lst.Items.Count;
            lst.Height = h + lst.Height - lst.ClientSize.Height;
        }

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();

        public void carregaEvolucoes()
        {

            try
            {
                con.conectar();
                adpt = new SqlDataAdapter("select top 50 e.id, e.datahora,(select p.nome from paciente p where p.id=e.paciente) as paciente ,e.tipoevolucao,e.descricaoevolucao ,e.paciente as codpaciente from evolucoes e where e.ativo!=0 ", con.conectar());
               
                dt = new DataTable();

                adpt.Fill(dt);
      
                dt.Columns["datahora"].ColumnName = "Data Hora";
                dt.Columns["paciente"].ColumnName = "Paciente";
                dt.Columns["tipoevolucao"].ColumnName = "Tipo da Evolução";
                dt.Columns["descricaoevolucao"].ColumnName = "Descrição da Evolução";
                dt.Columns["codpaciente"].ColumnName = "Codigo de paciente";

                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("Listando apenas evoluções ativas");
                    dataListagemEvucoes.DataSource = dt;
                    dataListagemEvucoes.Columns["id"].Visible = false;

                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataListagemEvucoes.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }

        }

        public void carregaEvolucoes(String tipoEvolucao, String DataEvolucao, String idPacientes)
        {
   
            try
            {
                con.conectar();
                  adpt = new SqlDataAdapter("select top 50 e.id, e.datahora,(select p.nome from paciente p where p.id=e.paciente)as paciente ,e.tipoevolucao," +
                      "e.descricaoevolucao,e.paciente as codpaciente  from evolucoes e where e.ativo!=0 and e.paciente in(" + idPacientes + ") and  e.tipoevolucao='" + tipoEvolucao + "'  and ( e.datahora between '" + DataEvolucao + " 00:00:00.000' and '" + DataEvolucao + " 23:59:59.999') order by datahora desc", con.conectar());
      
                dt = new DataTable();
                adpt.Fill(dt);
          
                dt.Columns["datahora"].ColumnName = "Data Hora";
                dt.Columns["paciente"].ColumnName = "Paciente";
                dt.Columns["tipoevolucao"].ColumnName = "Tipo da Evolução";
                dt.Columns["descricaoevolucao"].ColumnName = "Descrição da Evolução";
                dt.Columns["codpaciente"].ColumnName = "Codigo de paciente";
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("Listando apenas evoluções ativas");
                    dataListagemEvucoes.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    this.dataListagemEvucoes.Columns["id"].Visible = false;
                    dataListagemEvucoes.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }

        public void carregaEvolucoes(String tipoEvolucao, String DataEvolucao)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter("select top 50 e.id, e.datahora,(select p.nome from paciente p where p.id=e.paciente)as paciente ,e.tipoevolucao," +
                    "e.descricaoevolucao,e.paciente as codpaciente  from evolucoes e where e.ativo!=0 and e.paciente in(select id from paciente where ativo=1) and  " +
                    "e.tipoevolucao='" + tipoEvolucao + "'  and ( e.datahora between '" + DataEvolucao + " 00:00:00.000' and '" + DataEvolucao + " 23:59:59.999') order by datahora desc", con.conectar());
                dt = new DataTable();
                adpt.Fill(dt);
             
                dt.Columns["datahora"].ColumnName = "Data Hora";
                dt.Columns["paciente"].ColumnName = "Paciente";
                dt.Columns["tipoevolucao"].ColumnName = "Tipo da Evolução";
                dt.Columns["descricaoevolucao"].ColumnName = "Descrição da Evolução";
                dt.Columns["codpaciente"].ColumnName = "Codigo de paciente";
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("Listando apenas evoluções ativas");
                    dataListagemEvucoes.DataSource = dt;
                    this.dataListagemEvucoes.Columns["id"].Visible = false;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataListagemEvucoes.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }

        public int VerifcaAtivoInativo(bool ativoinativo)
        {
            int opcao;
            ;
            if (ativoinativo == true)
            {
                return opcao = 1;
            }
            else
            {
                return opcao = 0;
            }


        }


        Paciente pc = new Paciente();

        public string UsuarioSelecionado()
        {
            int maiorIdDb = pc.getMaiorIdUser() + 1;
            string idPacientes = string.Empty;
            if ((userControlPacienteListCheckbox2.chkPacientes.CheckedItems. Count == 0))
            {

                for (int i = 0; i < maiorIdDb; i++)
                {

                    idPacientes += i + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;
                /*
      
                */
            }
            else
            {
                for (int i = 0; i < userControlPacienteListCheckbox2.chkPacientes.CheckedItems.Count; i++)
                {

                    idPacientes += userControlPacienteListCheckbox2.SelectPosicaoPaciente(i).Id + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;
            
            }

        }

        public string tudoOuSoAtivo(int opcao)
        {
            string inf;
            if (opcao.Equals(0))
            {
                return inf = "0,1";
            }
            else
            {
                return inf = "1";
            }
        }



        public string idPacientes = string.Empty;

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            PacienteDaoComandos pc = new PacienteDaoComandos();
            string verificaAtivoOuInativo = tudoOuSoAtivo(VerifcaAtivoInativo(userControlPacienteListCheckbox2.chkAtivo.Checked));
            String diaDatePickerSelecionado = dateTimePicker1.Value.Date.ToString("dd-MM-yyyy");
            if (userControlPacienteListCheckbox2.chkPacientes.CheckedItems.Count == 0)
            {
                carregaEvolucoes(userControlTipoAtendimento2.SelectTipoEvolucao.descricaoEvolucao, diaDatePickerSelecionado);
            }
            else
            {
                //se o usuário habilitar somente o checkbox de ativos
                carregaEvolucoes(userControlTipoAtendimento2.SelectTipoEvolucao.descricaoEvolucao, diaDatePickerSelecionado, UsuarioSelecionado());
            }



         
          
        }

        public Paciente SelectPaciente(int i)
        {
          
            {
                //return (Paciente)chkPessoas.SelectedItem;
                return (Paciente)userControlPacienteListCheckbox2.chkPacientes.CheckedItems[i];
            }
        }



        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnNovaEvolucao_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("atendimento", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroEvolucao pac = new fmrCadastroEvolucao();
                if (userControlPacienteListCheckbox2.chkPacientes.CheckedItems.Count == 1)
                {
                    for (int i = 0; i < userControlPacienteListCheckbox2.chkPacientes.CheckedIndices.Count; i++)
                    {
                        pac.txtNomePaciente.Text = userControlPacienteListCheckbox2.SelectPosicaoPaciente(i).Nome;
                        pac.userControlPaciente1.chkAtivo.Checked = userControlPacienteListCheckbox2.chkAtivo.Checked;
                        pac.txtNomeEvolucao.Text = userControlTipoAtendimento2.SelectTipoEvolucao.descricaoEvolucao;

                        pac.Show();
                    }
                }
                else
                {
                    MessageBox.Show("Atenção existe nenhum ou mais de 2 usuários selecionados. Por favor selecione apenas um paciente para o correto cadastro");
                    pac.txtNomeEvolucao.Text = userControlTipoAtendimento2.SelectTipoEvolucao.descricaoEvolucao;

                    pac.Show();
                }

            }
          
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void chkPessoas_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataListagemEvucoes_DoubleClick(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("atendimento", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroEvolucao cadevolucao = new fmrCadastroEvolucao();
                cadevolucao.txtCodigoRegistro.Text = dataListagemEvucoes.CurrentRow.Cells["id"].Value.ToString();
                cadevolucao.dtpData.Text = dataListagemEvucoes.CurrentRow.Cells["Data Hora"].Value.ToString();
                cadevolucao.dtpHora.Text = dataListagemEvucoes.CurrentRow.Cells["Data Hora"].Value.ToString();
                cadevolucao.txtNomePaciente.Text = dataListagemEvucoes.CurrentRow.Cells["Paciente"].Value.ToString();
                cadevolucao.txtEvolucao.Text = dataListagemEvucoes.CurrentRow.Cells["Descrição da Evolução"].Value.ToString();
                //cadevolucao.txtEvolucao.ScrollToCaret();
                cadevolucao.txtNomeEvolucao.Text = dataListagemEvucoes.CurrentRow.Cells["Tipo da Evolução"].Value.ToString();
                cadevolucao.btnCadastrar.Visible = false;
                cadevolucao.btnLimpar.Visible = false;
                cadevolucao.txtEvolucao.ReadOnly = true;
                cadevolucao.dtpData.Enabled = false;
                cadevolucao.dtpHora.Enabled = false;
                cadevolucao.btnInativar.Visible = true;
                cadevolucao.userControlPaciente1.Enabled = false;
                cadevolucao.userControlTipoAtendimento1.Enabled = false;

                cadevolucao.Show();
            }
          
        }

        private void dataListagemEvucoes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("usuarios return"+ UsuarioSelecionado());
        }

        private void UserControlPacienteListCheckbox2_Load(object sender, EventArgs e)
        {

        }

        private void dataListagemEvucoes_DockChanged(object sender, EventArgs e)
        {

        }
    }

}
