﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Listagem
{
    public partial class fmrListagemFoto : Form
    {
        public fmrListagemFoto()
        {
            InitializeComponent();
        }

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        NivelUsuario nivelUser = new NivelUsuario();
        SqlDataReader dr;


        private void btnCadatrar_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("foto_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroFoto foto = new fmrCadastroFoto();
                foto.Show();
            }
        
        }
        string sqlBaseEvento = "select  (select nome from paciente p where p.id=fp.id_paciente)as nomePaciente,fp.foto,fp.tipo_foto,fp.ativo from foto_paciente fp";
        public void listaFoto(bool ativo, int id)
        {
            try
            {

        
                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseEvento + " where fp.ativo='" + ativo + "' and fp.id_paciente  in(" + id + ") ", con.conectar());
                dt = new DataTable();
                adpt.Fill(dt);
                    
                
                dt.Columns["nomePaciente"].ColumnName = "Nome Do Paciente";
                dt.Columns["foto"].ColumnName = "Foto do Paciente";
                dt.Columns["tipo_foto"].ColumnName = "Tipo da Foto";
                dt.Columns["ativo"].ColumnName = "Foto Ativa?";

                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
          
                    /*
                    this.dataGridView1.Columns["Data e Hora Final"].Visible = false;
                    this.dataGridView1.Columns["Dia Todo?"].Visible = true;
                    this.dataGridView1.Columns["Tipo da Tarefa"].Visible = false;
                    this.dataGridView1.Columns["id"].Visible = false;
                    */


                }
                else
                {
                    MessageBox.Show("Não há fotos para esse paciente");
                    dataGridView1.DataSource = null;
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar as fotos. Contate o suporte" + ex);
            }
            finally
            {
                con.desconectar();

            }

        }

        private void frmListar_Click(object sender, EventArgs e)
        {

            //bool ApenasAtivo = (c.Checked);
            //DateTime datainicio = monthCalendar1.SelectionStart;
            //var datafinal = monthCalendar1.SelectionEnd.ToString("dd MMM yyyy");


            //     if (chkFiltrarPorCor.Checked)
            //    {
            listaFoto(chkAtivo.Checked, userControlPaciente1.SelectPaciente.Id);
            //  }
            //    else
            //  {
            //  //   carregaAgenda(ApenasAtivo, "Lembrete", UsuarioSelecionado(), datainicio, datafinal);
            //}
        }
    }
}
