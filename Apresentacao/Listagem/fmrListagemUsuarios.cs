﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Listagem
{
    public partial class fmrListagemUsuarios : Form
    {

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        NivelUsuario nivelUser = new NivelUsuario();



        string SQLbase = "select id,nomeUsuario,loginuser,psw,(select nu.nome_Nivel from nivel_usuario nu where nu.id=codNivelDeUsuario) as NivelUsuario,permite_lanc_retroativo,ativo from usuario";

        Usuario user = new Usuario();


        public fmrListagemUsuarios()
        {
            InitializeComponent();
            string verificaAtivoOuInativo = tudoOuSoAtivo(VerifcaAtivoInativo(cmbUsuarios.chkAtivo.Checked));
            carregaUsuarios(verificaAtivoOuInativo);
        }

        public string tudoOuSoAtivo(int opcao)
        {
            string inf;
            if (opcao.Equals(0))
            {
                return inf = "0,1";
            }
            else
            {
                return inf = "1";
            }
        }
        public void criaColunasTabela()
        {
            dt = new DataTable();
            adpt.Fill(dt);
            dt.Columns["id"].ColumnName = "ID";
            dt.Columns["nomeUsuario"].ColumnName = "Nome do usuario";
            dt.Columns["loginuser"].ColumnName = "Login do usuario";
            dt.Columns["psw"].ColumnName = "Senha";
            dt.Columns["NivelUsuario"].ColumnName = "Nivel de Usuario";
            dt.Columns["permite_lanc_retroativo"].ColumnName = "Permite lançamento retroativo?";
            dt.Columns["ativo"].ColumnName = "Ativo?";
        }
        public void inativaColunas(bool valor)
        {
            this.dataGridView1.Columns["ID"].Visible = valor;
            this.dataGridView1.Columns["Senha"].Visible = valor;
            this.dataGridView1.Columns["Ativo?"].Visible = valor;
 
        }

            public int VerifcaAtivoInativo(bool ativoinativo)
        {
            int opcao;
            ;
            if (ativoinativo == true)
            {
                return opcao = 1;
            }
            else
            {
                return opcao = 0;
            }


        }

    

        public string PacienteSelecionado()
        {
            int maiorIdDb = user.getMaiorIdUser() + 1;
            string idUsers = string.Empty;
            if (cmbUsuarios.chkUsuarios.CheckedItems.Count == 0)
            {

                for (int i = 0; i < maiorIdDb; i++)
                {

                    idUsers += i + ",";
                }
                //gambis para concaternar
                idUsers += maiorIdDb;
                return idUsers;
                /*
      
                */
            }
            else
            {
                for (int i = 0; i < cmbUsuarios.chkUsuarios.CheckedItems.Count; i++)
                {

                    idUsers += cmbUsuarios.SelectPosicaoUsuario(i).Id + ",";
                }
                //gambis para concaternar
                idUsers += maiorIdDb;
                return idUsers;

            }

        }

        public void carregaUsuarios(string ativo)
        {
            // string inf = tudoOuSoAtivo(registroativo);
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter("" + SQLbase + " where  ativo in(" + ativo + ")", con.conectar());
                criaColunasTabela();
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
                inativaColunas(false);
                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os usuarios registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }


        public void carregaUsuarios(string usuarios,string ativo)
        {
            // string inf = tudoOuSoAtivo(registroativo);
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter("" + SQLbase + " where  id in(" + usuarios + ") and ativo in("+ ativo + ")", con.conectar());
                criaColunasTabela();
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
                inativaColunas(false);
                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os usuarios registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }




        private void button1_Click(object sender, EventArgs e)
        {
            string verificaAtivoOuInativo = tudoOuSoAtivo(VerifcaAtivoInativo(cmbUsuarios.chkAtivo.Checked));
            carregaUsuarios(PacienteSelecionado(), verificaAtivoOuInativo);


        }

        private void btnNovoUsuario_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("cadastro_usuario", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroUsuario cadusuario = new fmrCadastroUsuario();
                cadusuario.ShowDialog();
            }
      
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("cadastro_usuario", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroUsuario cadUser = new fmrCadastroUsuario();
                cadUser.txtCodUser.Text = dataGridView1.CurrentRow.Cells["ID"].Value.ToString();
                cadUser.txtLogin.Text = dataGridView1.CurrentRow.Cells["Login do usuario"].Value.ToString();
                cadUser.txtNome.Text = dataGridView1.CurrentRow.Cells["Nome do usuario"].Value.ToString();
                cadUser.txtSenha.Text = dataGridView1.CurrentRow.Cells["Senha"].Value.ToString();
                cadUser.chkLanRetroativo.Checked = Convert.ToBoolean(dataGridView1.CurrentRow.Cells["Permite lançamento retroativo?"].Value.ToString());
                cadUser.chkAtivo.Checked = Convert.ToBoolean(dataGridView1.CurrentRow.Cells["Ativo?"].Value.ToString());
                cadUser.txtNomeNivel.Text = dataGridView1.CurrentRow.Cells["Nivel de Usuario"].Value.ToString();
                cadUser.btnCadastrar.Visible = false;
                cadUser.btnLimpar.Visible = false;
                cadUser.btnAtualizar.Visible = true;
                cadUser.btnInativarRegistro.Visible = true;
                cadUser.Show();
            }

           
            
        }

        private void fmrListagemUsuarios_Load(object sender, EventArgs e)
        {

        }

        private void CmbUsuarios_Load(object sender, EventArgs e)
        {

        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
