﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Listagem
{
    public partial class fmrListagemAvaliacao : Form
    {
        public fmrListagemAvaliacao()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.WindowState = FormWindowState.Maximized;
            this.FormBorderStyle = FormBorderStyle.None;
            carregaAvalicao();
        }


        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        Paciente pc = new Paciente();
        NivelUsuario nivelUser = new NivelUsuario();

        private void btnNova_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("avaliacao", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrAvaliacao avaliacao = new fmrAvaliacao();
                if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 1)
                {
                    for (int i = 0; i < userControlPacienteListCheckbox1.chkPacientes.CheckedIndices.Count; i++)
                    {
                        avaliacao.txtNomePaciente.Text = userControlPacienteListCheckbox1.SelectPosicaoPaciente(i).Nome;
                        avaliacao.userControlPaciente2.chkAtivo.Checked = userControlPacienteListCheckbox1.chkAtivo.Checked;

                        avaliacao.Show();
                    }

                }
                else
                {
                    MessageBox.Show("Atenção existe nenhum ou mais de 2 usuários selecionados. Por favor selecione apenas um paciente para o correto cadastro");
                    avaliacao.Show();
                }
            }
        

              
        }

        public string UsuarioSelecionado()
        {
            int maiorIdDb = pc.getMaiorIdUser() + 1;
            string idPacientes = string.Empty;
            if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 0)
            {

                for (int i = 0; i < maiorIdDb; i++)
                {

                    idPacientes += i + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;
                /*
      
                */
            }
            else
            {
                for (int i = 0; i < userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count; i++)
                {

                    idPacientes += userControlPacienteListCheckbox1.SelectPosicaoPaciente(i).Id + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;

            }

        }

        public string idPacientes = string.Empty;

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            String dia = dateTimePicker1.Value.Date.ToString("dd-MM-yyyy");

            if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 0)
            {
                carregaAvalicao(dia);
            }
            else
            {
              
                carregaAvalicao(dia, UsuarioSelecionado());
            }
        }

        public void CriaColunasDataGrid()
        {
            dt = new DataTable();
            adpt.Fill(dt);
            dt.Columns["id"].ColumnName = "ID";
            dt.Columns["paciente"].ColumnName = "Paciente";
            dt.Columns["datahora"].ColumnName = "Data hora";
            dt.Columns["peso"].ColumnName = "Peso";
            dt.Columns["altura"].ColumnName = "Altura";
            dt.Columns["sistolica"].ColumnName = "Sistólica";
            dt.Columns["diastolica"].ColumnName = "Diastólica";
            dt.Columns["bpm"].ColumnName = "Bpm";
            dt.Columns["rpm"].ColumnName = "Rpm";
            dt.Columns["sp02"].ColumnName = "Sp02";
            dt.Columns["grau"].ColumnName = "Grau";
            dt.Columns["obs"].ColumnName = "Obs";
            dt.Columns["dor"].ColumnName = "Dor";
            dt.Columns["hgt"].ColumnName = "Hgt";
            dataGridView1.DataSource = dt;

        }

        public void carregaAvalicao(string DataEvolucao,string Idpacientes)
        {
            try
            {
                con.conectar();
                MessageBox.Show("Listando apenas Avaliação 'ativa'");
                adpt = new SqlDataAdapter("select a.id,(select p.nome from paciente p where p.id=a.paciente)  as paciente,a.datahora,a.peso,a.altura,a.sistolica,a.diastolica,a.bpm,a.rpm,a.sp02,a.grau,a.obs,a.dor,a.hgt from avaliacao a where  a.ativo !=0  and (a.datahora between '" + DataEvolucao + " 00:00:00.0000000' and '" + DataEvolucao + " 23:59:59.0000000')and a.paciente in("+ Idpacientes + ") ", con.conectar());
                CriaColunasDataGrid();
                DattaGridApenasLeitura(true);
         
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
                this.dataGridView1.Columns["ID"].Visible = false;

                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }


        public void carregaAvalicao(string DataEvolucao)
        {
            try
            {
                con.conectar();
                MessageBox.Show("Listando apenas Avaliação 'ativa'");
                adpt = new SqlDataAdapter("select a.id,(select p.nome from paciente p where p.id=a.paciente)    as paciente,a.datahora,a.peso,a.altura,a.sistolica,a.diastolica,a.bpm,a.rpm,a.sp02,a.grau,a.obs,a.dor,a.hgt from avaliacao a where  a.ativo !=0 and a.datahora between '" + DataEvolucao + " 00:00:00.0000000' and '" + DataEvolucao + " 23:59:59.0000000'", con.conectar());

                CriaColunasDataGrid();
                DattaGridApenasLeitura(true);
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
                this.dataGridView1.Columns["ID"].Visible = false;

                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }

        public void carregaAvalicao()
        {
            try
            {
                con.conectar();
                MessageBox.Show("Listando apenas Avaliação 'ativa'");
                adpt = new SqlDataAdapter("select top 50 a.id,(select p.nome from paciente p where p.id=a.paciente)   as paciente,a.datahora,a.peso,a.altura,a.sistolica,a.diastolica,a.bpm,a.rpm,a.sp02,a.grau,a.obs,a.dor,a.hgt from avaliacao a , paciente  pa where a.paciente=pa.id and pa.ativo=1 and a.ativo !=0 order by datahora desc", con.conectar());

                CriaColunasDataGrid();
                DattaGridApenasLeitura(true);

                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
                this.dataGridView1.Columns["ID"].Visible = false;

                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }


        private void fmrListagemAvaliacao_Load(object sender, EventArgs e)
        {
            CriaColunasDataGrid();
        }

        public void DattaGridApenasLeitura(bool value)
        {
            this.dataGridView1.Columns["Peso"].ReadOnly = value;
            this.dataGridView1.Columns["Altura"].ReadOnly = value;
            this.dataGridView1.Columns["Dor"].ReadOnly = value;
            this.dataGridView1.Columns["Sistólica"].ReadOnly = value;
            this.dataGridView1.Columns["Diastólica"].ReadOnly = value;
            this.dataGridView1.Columns["Bpm"].ReadOnly = value;
            this.dataGridView1.Columns["Sp02"].ReadOnly = value;
            this.dataGridView1.Columns["Grau"].ReadOnly = value;
            this.dataGridView1.Columns["Obs"].ReadOnly = value;
            this.dataGridView1.Columns["Hgt"].ReadOnly = value;
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("avaliacao", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrAvaliacao cadAvalicao = new fmrAvaliacao();
                cadAvalicao.userControlPaciente2.chkAtivo.Checked = this.userControlPacienteListCheckbox1.chkAtivo.Checked;
                cadAvalicao.txtId.Text = dataGridView1.CurrentRow.Cells["ID"].Value.ToString();
                cadAvalicao.txtNomePaciente.Text = dataGridView1.CurrentRow.Cells["Paciente"].Value.ToString();
                cadAvalicao.txtPeso.Text = dataGridView1.CurrentRow.Cells["Peso"].Value.ToString();
                cadAvalicao.txtAltura.Text = dataGridView1.CurrentRow.Cells["Altura"].Value.ToString();
                cadAvalicao.txtDor.Text = dataGridView1.CurrentRow.Cells["Dor"].Value.ToString();


                
                cadAvalicao.txtSistolica.Text = dataGridView1.CurrentRow.Cells["Sistólica"].Value.ToString();
                cadAvalicao.txtDiastolica.Text = dataGridView1.CurrentRow.Cells["Diastólica"].Value.ToString();
                cadAvalicao.txtBpm.Text = dataGridView1.CurrentRow.Cells["Bpm"].Value.ToString();
                cadAvalicao.txtRpm.Text = dataGridView1.CurrentRow.Cells["Rpm"].Value.ToString();
                cadAvalicao.txtOximetria.Text = dataGridView1.CurrentRow.Cells["Sp02"].Value.ToString();
                cadAvalicao.txtGrau.Text = dataGridView1.CurrentRow.Cells["Grau"].Value.ToString();
                cadAvalicao.txtObs.Text = dataGridView1.CurrentRow.Cells["Obs"].Value.ToString();
                cadAvalicao.txtObs.ReadOnly = true;
                cadAvalicao.txtHgt.Text = dataGridView1.CurrentRow.Cells["Hgt"].Value.ToString();
                cadAvalicao.btnGravar.Visible = false;
                cadAvalicao.btnInativar.Visible = true;
                cadAvalicao.btnLimpar.Visible = false;
                cadAvalicao.userControlPaciente2.Enabled = false;
                cadAvalicao.dateTimePicker1.Enabled = false;
                cadAvalicao.txtPeso.Enabled = false;
                cadAvalicao.txtAltura.Enabled = false;
                cadAvalicao.txtDor.Enabled = false;
                cadAvalicao.txtSistolica.Enabled = false;
                cadAvalicao.txtDiastolica.Enabled = false;
                cadAvalicao.txtRpm.Enabled = false;
                cadAvalicao.txtBpm.Enabled = false;
                cadAvalicao.txtHgt.Enabled = false;
                cadAvalicao.txtOximetria.Enabled = false;
                cadAvalicao.txtGrau.Enabled = false;
                cadAvalicao.Show();
            }
              
        }

        private void userControlPacienteListCheckbox1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_DockChanged(object sender, EventArgs e)
        {

        }
    }
}
