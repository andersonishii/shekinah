﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao
{
    public partial class fmrAnexosPaciente : Form
    {



    

         public fmrAnexosPaciente()
        {
            InitializeComponent();
        }

        NivelUsuario nivelUser = new NivelUsuario();


        private void fmrAnexosPaciente_Load(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            table.Clear();
            table.Columns.Add("Paciente");
            table.Columns.Add("Tipo Anexo");
            table.Columns.Add("Data Hora");
            table.Columns.Add("Origem");
            table.Columns.Add("Destino");
            table.Columns.Add("Palavras Chave");
            table.Columns.Add("Obs");
            table.Columns.Add("Ativo?", typeof(bool));
            String datainicial = dateTimePicker1.Value.Date.ToString("dd-MM-yyyy");
            String datafinal = dateTimePicker2.Value.Date.ToString("dd-MM-yyyy");
           carregaAnexoPaciente(userControlPaciente1.SelectPaciente.Id, datainicial, datafinal, userControlTipoAnexo1.SelectTipoEvolucao.descricaoTipoAnexo);
            /*
                        String[] files = Directory.GetFiles(@"C:\Program Files\shekinah");
                        DataTable table = new DataTable();
                        table.Clear();
                        table.Columns.Add("Nome do Arquivo");
                        table.Columns.Add("teste2");

                        for (int i = 0; i < files.Length; i++)
                        {
                            FileInfo file = new FileInfo(files[i]);
                            DataRow _ravi = table.NewRow();
                            _ravi["Nome do Arquivo"] = file.Name;
                            _ravi["teste2"] = i;
                            table.Rows.Add(_ravi);
                           // table.Rows.Add(file.Name, typeof(String));
                           // table.Rows.Add(i);
                        }

                        dataHistAnexo.DataSource = table;
                        */

        }


    private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("clicoi aquiadmin3  admin");

            this.Hide();
            fmrCadastroAnexoPaciente pac = new fmrCadastroAnexoPaciente();
            pac.ShowDialog();




        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("anexo_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                // this.Hide();
                fmrCadastroAnexoPaciente pac = new fmrCadastroAnexoPaciente();
                pac.txtNomePaciente.Text = userControlPaciente1.SelectPaciente.Nome;
                pac.ShowDialog();
            }
           
        }
        public void LimpaCampos()
        {
            txtPalavraChave.Clear();
         
            
        }


        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimpaCampos();
        }
        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        public void carregaAnexoPaciente(int paciente, String DataInicial,String DataFinal, string TipoAnexo)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter("select  (select p.nome from paciente p where p.id = e.paciente) as 'Paciente',tipoanexo as 'Tipo Anexo',datahora as 'Data/Hora',origem 'Arquivo',destino as 'Destino',tag as 'Palavra chave',obs as 'Obs' ,ativo as 'Ativo?'  from anexo_paciente e where e.paciente=" + paciente + " and ( e.datahora between '" + DataInicial + " 00:00:00.000' and '" + DataFinal + " 23:59:59.999') and e.tipoanexo='" + TipoAnexo + "'", con.conectar());

                dt = new DataTable();
                adpt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    dataHistAnexo.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataHistAnexo.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Anexos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }

        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void BtnPesquisar_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            table.Clear();
            table.Columns.Add("Paciente");
            table.Columns.Add("Tipo Anexo");
            table.Columns.Add("Data Hora");
            table.Columns.Add("Origem");
            table.Columns.Add("Destino");
            table.Columns.Add("Tag");
            table.Columns.Add("Obs");
            table.Columns.Add("Ativo?", typeof(bool));
            String diaInicial = dateTimePicker1.Value.Date.ToString("dd-MM-yyyy");
            String diaFinal = dateTimePicker2.Value.Date.ToString("dd-MM-yyyy");
            carregaAnexoPaciente(userControlPaciente1.SelectPaciente.Id,  diaInicial , diaFinal, userControlTipoAnexo1.SelectTipoEvolucao.descricaoTipoAnexo);

        }

       



        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void DataHistAnexo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void UserControlTipoAnexo1_Load(object sender, EventArgs e)
        {

        }

        private void dataHistAnexo_DoubleClick(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("anexo_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                MessageBox.Show("Por favor aguarde a abertura do seu documento. Em alguns casos pode ser que ele demora a abrir");
                try
                {
                    // using (Process myProcess = new Process())
                    {
                        //   myProcess.StartInfo.UseShellExecute = false;
                        // You can start any process, HelloWorld is a do-nothing example.
                        //   myProcess.StartInfo.FileName = dataHistAnexo.CurrentRow.Cells["destino"].Value.ToString();
                        //   myProcess.StartInfo.CreateNoWindow = true;

                        System.Diagnostics.Process.Start(dataHistAnexo.CurrentRow.Cells["destino"].Value.ToString());
                        //   myProcess.Start();
                        // This code assumes the process you are starting will terminate itself. 
                        // Given that is is started without a window so you cannot terminate it 
                        // on the desktop, it must terminate itself or you can do it programmatically
                        // from this application using the Kill method.
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ops , infelizmente não foi possivel abrir o arquivo" + ex);
                }
            }
           
        }

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
