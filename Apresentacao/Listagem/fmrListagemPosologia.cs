﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Listagem
{
    public partial class fmrListagemPosologia : Form
    {
        public fmrListagemPosologia()
        {
            InitializeComponent();
            carregaEvolucoes("1", 3);
        }

        public fmrListagemPosologia(bool ativo)
        {
            InitializeComponent();
            carregaEvolucoes("1", 3);
        }
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();

        private void txtNovaPosologia_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("posologia", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroPosologia posologia = new fmrCadastroPosologia();

                if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 1)
                {
                    for (int i = 0; i < userControlPacienteListCheckbox1.chkPacientes.CheckedIndices.Count; i++)
                    {
                        posologia.txtNomePaciente.Text = userControlPacienteListCheckbox1.SelectPosicaoPaciente(i).Nome;
                        posologia.userControlPaciente1.chkAtivo.Checked = userControlPacienteListCheckbox1.chkAtivo.Checked;
                        posologia.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("Atenção existe nenhum ou mais de 2 usuários selecionados. Por favor selecione apenas um paciente para o correto cadastro");
                    posologia.ShowDialog();
                }
            }


        }


        public bool checkDataInicialMaiorQueFinal(DateTime data1, DateTime data2)
        {
            if (data1 == data2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private void btnFiltrar_Click(object sender, EventArgs e)
        {

            String diaInicial = dtpDataInicio.Value.Date.ToString("dd-MM-yyyy");
            String diaFinal = dtpDataFim.Value.Date.ToString("dd-MM-yyyy");
            string verificaCheckBoxAtivo = verificaInativo(chkAtivo);

            if (dtpDataFim.Value.Date< dtpDataInicio.Value.Date)
            {
                MessageBox.Show("A data final não pode ser menor que a data inicial");
            }
            //caso a data final seja maior que a data final
            else
            {
                if (chkMaisProxima.Checked)
                {
                    //3 é o parametro de 3 horas daqui para frente( considerando a  hora de agora)
                    carregaEvolucoes(verificaCheckBoxAtivo, 3);
                }
                else
                {
                    carregaEvolucoes(verificaCheckBoxAtivo,  diaInicial, diaFinal);
                }
            }

           

    







        }

        public string  verificaInativo(CheckBox checkboxativo)
        {
     
            if (checkboxativo.Checked)
            {
                return "1";
            }
            else
            {
                return "0,1";
            }
        
        }


        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;

        public void criaColunasTabela()
        {
            dt = new DataTable();
            adpt.Fill(dt);

            dt.Columns["modo_uso"].ColumnName = "Modo de Uso";
            dt.Columns["id"].ColumnName = "ID";
            dt.Columns["idpaciente"].ColumnName = "ID paciente";
            dt.Columns["paciente"].ColumnName = "Nome Paciente";
            dt.Columns["codigo_posologia"].ColumnName = "Codigo Posologia";
            dt.Columns["nome_medicacao"].ColumnName = "nome da Medicação";
            dt.Columns["data_pendencia"].ColumnName = "Data e Hora da Pendencia";
            dt.Columns["datahora_inicial"].ColumnName = "data e hora inicial";
            dt.Columns["data_final"].ColumnName = "Data final";
            dt.Columns["datahora_registromedicacao"].ColumnName = "Data e Hora Que a medicação foi dada";
            dt.Columns["num_periodicidade"].ColumnName = "Periodicidade";
            dt.Columns["nome_periodicidade"].ColumnName = "Nome da Periodicidade";
            dt.Columns["ativo"].ColumnName = "Pendencia ativa?";
            dt.Columns["uso_continuo"].ColumnName = "Uso Continio?";
            dt.Columns["posologia"].ColumnName = "Posologia";
            dt.Columns["observacao"].ColumnName = "Observação";
            dt.Columns["id_medicacao"].ColumnName = "id do medicamento";
            dt.Columns.Add("Registrar Horario da Medicação", typeof(bool));
        }

        public void inativaColunas (bool valor)
        {
            this.dataGridView1.Columns["ID"].Visible = valor;
            this.dataGridView1.Columns["ID paciente"].Visible = valor;
            this.dataGridView1.Columns["Codigo Posologia"].Visible = valor;
            this.dataGridView1.Columns["data e hora inicial"].Visible = valor;
            this.dataGridView1.Columns["Data final"].Visible = valor;
            this.dataGridView1.Columns["Periodicidade"].Visible = valor;
            this.dataGridView1.Columns["Nome da Periodicidade"].Visible = valor;
            this.dataGridView1.Columns["Pendencia ativa?"].Visible = valor;
            this.dataGridView1.Columns["Uso Continio?"].Visible = valor;
            this.dataGridView1.Columns["id do medicamento"].Visible = valor;
        }

       string baseSQl ="select (select m.modo_de_uso  from medicacoes m  where m.id=p.id_medicacao ) as modo_uso,p.id ,p.paciente as idpaciente, (select nome from paciente pc where pc.id=p.paciente) as paciente,p.codigo_posologia,(select m.nome_quimico from medicacoes m where m.id=p.nome_medicacao) as nome_medicacao,p.data_pendencia,p.datahora_inicial,p.data_final,p.datahora_registromedicacao,p.num_periodicidade,p.nome_periodicidade,p.ativo,p.uso_continuo,p.posologia,p.observacao,p.id_medicacao from posologia p";



        public void carregaEvolucoes(string checkboxAtivo,int horaAdicionar)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter(baseSQl+ "	where ((p.ativo in("+ checkboxAtivo + ")  and  (p.data_pendencia between getdate() and DATEADD(HOUR," + horaAdicionar + ",getdate())) ) or  (p.ativo=1 and  (p.data_pendencia <= getdate() ))) order by p.data_pendencia asc", con.conectar());
                criaColunasTabela();
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
                inativaColunas(false);

                if (dt.Rows.Count > 0 && checkboxAtivo.Equals("1"))
                {
                    this.dataGridView1.Columns["Data e Hora Que a medicação foi dada"].Visible = false;
                    dataGridView1.DataSource = dt;

                }
                else if(dt.Rows.Count > 0 && checkboxAtivo.Equals("0,1"))
                {
                    dataGridView1.Columns["Data e Hora Que a medicação foi dada"].Visible = true;
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Para as proximas 3 horas não há posologia registrada");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os medicamentos registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }

        public void carregaEvolucoes(string  checkboxAtivo, string datainicial, string datafinal)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter(baseSQl + "	where (p.ativo in(" + checkboxAtivo + ") and  (p.data_pendencia between '" + datainicial+ " 00:00:00.0000000' and '"+datafinal+ " 23:59:59.0000000') )  order by p.data_pendencia asc", con.conectar());
                criaColunasTabela();
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
                inativaColunas(false);

                if (dt.Rows.Count > 0 && checkboxAtivo.Equals("1"))
                { 
                    dataGridView1.Columns["Data e Hora Que a medicação foi dada"].Visible = false;
                    dataGridView1.DataSource = dt;
                
                }
                else if (dt.Rows.Count > 0 && checkboxAtivo.Equals("0,1"))
                {
                    dataGridView1.Columns["Data e Hora Que a medicação foi dada"].Visible = true;
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os medicamentos registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }







        private void chkMaisProxima_CheckedChanged(object sender, EventArgs e)
        {
            if(chkMaisProxima.Checked == true)
            {
                MessageBox.Show("Com essa opção ativa, o sistema exibirá nas próximas 3 horas de pendências  ");
                dtpDataInicio.Enabled = false;
                dtpDataFim.Enabled = false;
            }
            else
            {
                dtpDataInicio.Enabled = true;
                dtpDataFim.Enabled = true;
            }
        }

        private void chkAtivo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAtivo.Checked == true)
            {
                MessageBox.Show("Serão exibidos apenas as pendências que não estão registradas no sistema ");

            }
            else
            {
                MessageBox.Show("Serão exibidos todas as pendências e medicações , inclusive as que já foram registradas");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("posologia", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                DataGridViewRow linha = new DataGridViewRow();
                linha = dataGridView1.Rows[e.RowIndex];
                string idRegistro = linha.Cells["ID"].Value.ToString();
                string codposologia = linha.Cells["Codigo Posologia"].Value.ToString();
                string nomeMedicacao = linha.Cells["nome da Medicação"].Value.ToString();
                string dataPendencia = linha.Cells["Data e Hora da Pendencia"].Value.ToString();
                string idpaciente = linha.Cells["ID paciente"].Value.ToString();
                string iddamedicacao = linha.Cells["id do medicamento"].Value.ToString();

                //coluna 16 ==Registrar Horario da Medicação
                if (e.ColumnIndex == dataGridView1.Columns["Registrar Horario da Medicação"].Index)
                {
                    string teste = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    fmrRegistroHorarioMedicacao reg = new fmrRegistroHorarioMedicacao();
                    reg.txtIdRegistro.Text = idRegistro;
                    reg.txtHoraPendencia.Text = dataPendencia;
                    reg.txtCodPosologia.Text = codposologia;
                    reg.txtNomeMedicacao.Text = nomeMedicacao;
                    reg.txtIdPaciente.Text = idpaciente;
                    reg.txtIdmedicacao.Text = iddamedicacao;
                    reg.ShowDialog();
                }
            }
          
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void UserControlPacienteListCheckbox1_Load(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
