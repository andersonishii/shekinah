﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Listagem
{
    public partial class fmrListagemAtividadeEmGrupo : Form
    {
        public fmrListagemAtividadeEmGrupo()
        {
            InitializeComponent();
        }
        NivelUsuario nivelUser = new NivelUsuario();

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }



        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        Paciente pc = new Paciente();

        public void CriaColunasDataGrid()
        {
            dt = new DataTable();
            adpt.Fill(dt);
            dt.Columns["funcionariosEnvolvidos"].ColumnName = "Funcionarios Envolvidos";
            dt.Columns["pacienteEnvolvidos"].ColumnName = "Pacientes Envolvidos";
            dt.Columns["codigo_agrupador"].ColumnName = "Codigo Agrupador";
            dt.Columns["datahora"].ColumnName = "Data e Hora";
            dt.Columns["tipo_atividade"].ColumnName = "Tipo da Atividade";
            dt.Columns["relatorio"].ColumnName = "Relatorio";
            dt.Columns["ativo"].ColumnName = "Ativo?";
            dataGridView1.DataSource = dt;
        }


        string sqlBaseConsulta = "select STUFF((SELECT ',' + (select nomeUsuario from usuario u where u.id= idpaciente) FROM dbo.atividadegrupo_paciente  ap where ap.codigo_agrupador= co.codigo_agrupador FOR XML PATH('')), 1, 1, '')  as funcionariosEnvolvidos,STUFF((SELECT ',' + (select p.nome from paciente p where p.id= idfuncionario) FROM dbo.atividadegrupo_funcionario  ap where ap.codigo_agrupador= co.codigo_agrupador FOR XML PATH('')), 1, 1, '')  as pacienteEnvolvidos, codigo_agrupador,datahora,tipo_atividade,relatorio,ativo from  atividadegrupo_paciente co group by codigo_agrupador ,datahora,tipo_atividade,relatorio,ativo";


        public void carregaAtividadeEmGrupo()
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseConsulta, con.conectar());

                CriaColunasDataGrid();
               // DattaGridApenasLeitura(true);
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
               // this.dataGridView1.Columns["ID"].Visible = false;

                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("Lsitando todos as atividades em grupo, inclusive registros inativos");
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }

        public void carregaAtividadeEmGrupo(string dataInicial,string dataFinal,int ativo)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseConsulta+ "	having (datahora between '" + dataInicial + " 00:00:00.0000000' and '" + dataFinal + " 23:59:59.0000000') and ativo="+ativo+" 	order by datahora asc ", con.conectar());

                CriaColunasDataGrid();
                // DattaGridApenasLeitura(true);
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
                // this.dataGridView1.Columns["ID"].Visible = false;

                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }
        private void btnNovaAtividade_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("atividade_grupo", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroAtividadeEmGrupo cadSaidaPaciente = new fmrCadastroAtividadeEmGrupo();
                cadSaidaPaciente.ShowDialog();  
            }// MessageBox.Show("tela em validação. Caso aprovado , solicitar o desenvolvimento a operacionalidade da mesma. Por enquato ela não está cadastrando");
         
        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("atividade_grupo", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {

            }

        }

        public int VerifcaAtivoInativo(bool ativoinativo)
        {
            if (ativoinativo == true)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }


        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            String diaInicial = dtpDataInicio.Value.Date.ToString("dd-MM-yyyy");
            String diaFinal = dtpDataFim.Value.Date.ToString("dd-MM-yyyy");

            carregaAtividadeEmGrupo(diaInicial, diaFinal, VerifcaAtivoInativo(chkAtivo.Checked));
        }

        private void fmrListagemAtividadeEmGrupo_Load(object sender, EventArgs e)
        {

            carregaAtividadeEmGrupo();
        }
    }
}
