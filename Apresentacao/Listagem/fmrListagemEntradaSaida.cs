﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.DAL;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Listagem
{
    public partial class fmrListagemEntradaSaida : Form
    {
        public fmrListagemEntradaSaida()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            carregaEntradaESaida(UsuarioSelecionado(), dtpData.Value.Date.ToString("dd-MM-yyyy"));
        }

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        NivelUsuario nivelUser = new NivelUsuario();

        private void fmrListagemEntradaSaida_Load(object sender, EventArgs e)
        {

        }

        public int VerifcaAtivoInativo(bool ativoinativo)
        {
            int opcao;
;
            if (ativoinativo == true)
            {
                return opcao=1;
            }
            else
            {
                return opcao = 0;
            }
                  
          
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("entrada_saida_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroSaidaPaciente cadSaidaPaciente = new fmrCadastroSaidaPaciente();
                if (chkPessoas.chkPacientes.CheckedItems.Count == 1)
                {
                    for (int i = 0; i < chkPessoas.chkPacientes.CheckedIndices.Count; i++)
                    {
                        cadSaidaPaciente.userControlPaciente1.chkAtivo.Checked = chkPessoas.chkAtivo.Checked;
                        cadSaidaPaciente.txtNomeDoPaciente.Text = chkPessoas.SelectPosicaoPaciente(i).Nome;
                        cadSaidaPaciente.Show();
                    }
                }
                else
                {
                    MessageBox.Show("Atenção existe nenhum ou mais de 2 usuários selecionados. Por favor selecione apenas um paciente para o correto cadastro");
                    cadSaidaPaciente.Show();
                }
            }
        

        }

        string SQLbase = "select es.id, (select nome from paciente pc where pc.id=es.paciente) as paciente,es.datahora,es.descricao_saida,es.datahora_expectativa_retorno,es.ativo_inativo,retorno_concluido as Retornou from entrada_saida as es";
        public void criaColunasTabela()
        {
            dt = new DataTable();
            adpt.Fill(dt);
            dt.Columns["id"].ColumnName = "ID";
            dt.Columns["paciente"].ColumnName = "Paciente";
            dt.Columns["datahora"].ColumnName = "Data Hora";
            dt.Columns["descricao_saida"].ColumnName = "Descricao da Saida";
            dt.Columns["datahora_expectativa_retorno"].ColumnName = "Data e Hora da Expectativa de Retorno";
            dt.Columns["ativo_inativo"].ColumnName = "Ativo";
            dt.Columns["Retornou"].ColumnName = "Paciente Retornou";
            dt.Columns.Add("Registrar horário da retorno do idoso", typeof(bool));
        }

        public void inativaColunas(bool valor)
        {
            this.dataGridView1.Columns["ID"].Visible = valor;
            this.dataGridView1.Columns["Ativo"].Visible = valor;
            this.dataGridView1.Columns["Data Hora"].Visible = valor;
        }


        Paciente pc = new Paciente();

  

       

        public string tudoOuSoAtivo(int opcao)
        {
            string inf;
            if (opcao.Equals(1))
            {
                return inf = "0,1";
            }
            else
            {
                return inf = "1";
            }
        }

 

        public string UsuarioSelecionado()
        {
            int maiorIdDb = pc.getMaiorIdUser() + 1;
            string idPacientes = string.Empty;
            if (chkPessoas.chkPacientes.CheckedItems.Count == 0)
            {
                for (int i = 0; i < maiorIdDb; i++)
                {
                    idPacientes += i + ",";
                }
                idPacientes += maiorIdDb;
                return idPacientes;
            }
            else
            {
                for (int i = 0; i < chkPessoas.chkPacientes.CheckedItems.Count; i++)
                {
                    idPacientes += chkPessoas.SelectPosicaoPaciente(i).Id + ",";
                }
                idPacientes += maiorIdDb;
                return idPacientes;
            }

        }




        public void carregaEntradaESaida(string paciente, string data,int ativoouinativo)
        {
           // string inf = tudoOuSoAtivo(registroativo);
            try
            {
                con.conectar();
                //adpt = new SqlDataAdapter("" + SQLbase + " where  es.retorno_concluido in(" + inf + ") and (es.paciente in(" + paciente + ")) and (es.datahora between '"+ data + " 00:00:00.000000' and '" + data + " 23:59:59.000000' ) ", con.conectar());
                adpt = new SqlDataAdapter("select es.id, (select nome from paciente pc where pc.id=es.paciente) as paciente,es.datahora,es.descricao_saida,es.datahora_expectativa_retorno,es.ativo_inativo,retorno_concluido as Retornou from entrada_saida as es " +
                    "where   es.ativo_inativo!=0 and  (datahora between '" + data + " 00:00:00.7733333' and '"+data+" 23:59:59.7733333' ) and paciente in("+paciente+")", con.conectar());

                criaColunasTabela();
                dataGridView1.DataSource = dt;
                dataGridView1.Columns["Paciente Retornou"].Visible = true;
                //oculta as colunas que não são necessárias para o usuário8
                inativaColunas(false);
                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;

                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar as Pendencias  Registradas" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }

        public void carregaEntradaESaida(string paciente, string data)
        {
            // string inf = tudoOuSoAtivo(registroativo);
            try
            {
                con.conectar();
                //adpt = new SqlDataAdapter("" + SQLbase + " where  es.retorno_concluido in(" + inf + ") and (es.paciente in(" + paciente + ")) and (es.datahora between '"+ data + " 00:00:00.000000' and '" + data + " 23:59:59.000000' ) ", con.conectar());
                adpt = new SqlDataAdapter("select es.id, (select nome from paciente pc where pc.id=es.paciente) as paciente,es.datahora,es.descricao_saida,es.datahora_expectativa_retorno,es.ativo_inativo,retorno_concluido as Retornou from entrada_saida as es " +
                    "where  es.ativo_inativo!=0 and (datahora between '" + data + " 00:00:00.7733333' and '" + data + " 23:59:59.7733333' ) and paciente in(" + paciente + ") and retorno_concluido is null", con.conectar());

                criaColunasTabela();
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
                inativaColunas(false);
                dataGridView1.Columns["Paciente Retornou"].Visible = false;
                if (dt.Rows.Count > 0)
                {
                  
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar as Pendencias  Registradas" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }

        Usuario user = new Usuario();

        public string selecionaChecados()
        {
            string idPacientes="";
      
                for (int i = 0; i < chkPessoas.chkPacientes.CheckedIndices.Count; i++)
                {

                    idPacientes += chkPessoas.SelectPosicaoPaciente(i).Id + ",";
                    //             idPacientes += SelectPaciente(i).Id + Environment.NewLine;
                    //  chkPessoas.SetItemChecked(i,true);
                    //    MessageBox.Show("ID dele é " + SelectPaciente(i).Id);
                }
                //gambiarra para no caso do ultimo ID selecionado ele contaena com o 9999 senão ficaria ...9,) isso bugaria o SQL 
                idPacientes += user.getMaiorIdUser();
                return idPacientes;
        }
    
    

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            PacienteDaoComandos pc = new PacienteDaoComandos();
            int verifcica = VerifcaAtivoInativo(chkAtivo.Checked);
            int verifcica2 = VerifcaAtivoInativo(chkPessoas.chkAtivo.Checked);
            string todosOuSohInativo=pc.getPacienteAtivoOuInativo(verifcica2);
            String dia = dtpData.Value.Date.ToString("dd-MM-yyyy");
            //     carregaEvolucoes(verifcica, UsuarioSelecionado(), teste);
            if (chkAtivo.Checked)
            {
                carregaEntradaESaida(UsuarioSelecionado(), dia, verifcica);
            }
            else
            {
                carregaEntradaESaida(UsuarioSelecionado(), dia);
            
            }
      
        }

        public bool convertStringInBool(string valor)
        {
            bool retorno;
            if (valor.Equals("True"))
            {
                return retorno=true;
            }
            else
            {
                return retorno = false;
            }

           
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("entrada_saida_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroSaidaPaciente cadSaida = new fmrCadastroSaidaPaciente();
                cadSaida.txtCodigo.Text = dataGridView1.CurrentRow.Cells["ID"].Value.ToString();
                cadSaida.txtNomeDoPaciente.Text = dataGridView1.CurrentRow.Cells["Paciente"].Value.ToString();
                cadSaida.chkAtivo.Checked = convertStringInBool(dataGridView1.CurrentRow.Cells["Ativo"].Value.ToString());
                cadSaida.dtpDia.Text = dataGridView1.CurrentRow.Cells["Data Hora"].Value.ToString();
                cadSaida.dtpHora.Text = dataGridView1.CurrentRow.Cells["Data Hora"].Value.ToString();
                cadSaida.txtTextoSaida.Text = dataGridView1.CurrentRow.Cells["Descricao da Saida"].Value.ToString();
                cadSaida.dtpDataExpectativaRetorno.Text = dataGridView1.CurrentRow.Cells["Data e Hora da Expectativa de Retorno"].Value.ToString();
                cadSaida.dtpHoraExpectativaRetorno.Text = dataGridView1.CurrentRow.Cells["Data e Hora da Expectativa de Retorno"].Value.ToString();
                cadSaida.txtCodigo.Enabled = false;
                cadSaida.dtpDataExpectativaRetorno.Enabled = false;
                cadSaida.dtpHoraExpectativaRetorno.Enabled = false;
                cadSaida.chkAtivo.Enabled = false;
                cadSaida.userControlPaciente1.Enabled = false;
                cadSaida.dtpDia.Enabled = false;
                cadSaida.dtpHora.Enabled = false;
                cadSaida.btnCadastrar.Visible = false;
                cadSaida.btnSair.Visible = false;
                cadSaida.btnInativar.Visible = true;
                cadSaida.txtTextoSaida.ReadOnly = true;
                cadSaida.Show();
            }
         
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["Registrar horário da retorno do idoso"].Index)
            {
                fmrCadastroRetornoPaciente retornoPaciente = new fmrCadastroRetornoPaciente();
                retornoPaciente.txtNomePaciente.Text = dataGridView1.CurrentRow.Cells["Paciente"].Value.ToString();
                retornoPaciente.txtCodigoRegistro.Text = dataGridView1.CurrentRow.Cells["ID"].Value.ToString();
                retornoPaciente.dtpHoraSaida.Text = dataGridView1.CurrentRow.Cells["Data Hora"].Value.ToString();
                retornoPaciente.dtpSaidaIdoso.Text = dataGridView1.CurrentRow.Cells["Data Hora"].Value.ToString();
                retornoPaciente.chkAtivo.Checked = convertStringInBool(dataGridView1.CurrentRow.Cells["Ativo"].Value.ToString());
                retornoPaciente.Show();


                // MessageBox.Show("voce clicou nbo primeiro");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
