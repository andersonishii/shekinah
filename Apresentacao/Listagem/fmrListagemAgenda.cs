﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SistemaShekinah.Apresentacao.Listagem
{
    public partial class fmrListagemAgenda : Form
    {
        public fmrListagemAgenda()
        {
            InitializeComponent();
        }

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        Paciente pc = new Paciente();
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();

        private void btnLimpar_Click_1(object sender, EventArgs e)
        {
            btnCor.BackColor = SystemColors.Control;
            chkApenasAtivos.Checked = true;
            chkFiltrarPorCor.Checked = false;
        
            userControlPacienteListCheckbox1.chkAtivo.Checked = true;


        }

        private void btnCor_Click_1(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                btnCor.BackColor = colorDialog1.Color;
            }
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("agenda", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroAgenda agenda = new fmrCadastroAgenda();
                if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 1)
                {
                    for (int i = 0; i < userControlPacienteListCheckbox1.chkPacientes.CheckedIndices.Count; i++)
                    {
                        agenda.txtNomePaciente.Text = userControlPacienteListCheckbox1.SelectPosicaoPaciente(i).Nome;
                        agenda.cmbPaciente.chkAtivo.Checked = userControlPacienteListCheckbox1.chkAtivo.Checked;

                        agenda.Show();
                    }

                }
                else
                {
                    MessageBox.Show("Atenção existe nenhum ou mais de 2 usuários selecionados. Por favor selecione apenas um paciente para o correto cadastro");
                    agenda.cmbPaciente.chkAtivo.Checked = userControlPacienteListCheckbox1.chkAtivo.Checked;
                    agenda.Show();
                }

            }
         



   
        }

        private void fmrListagemAgenda_Load(object sender, EventArgs e)
        {
            /*
            btnCor.Enabled = false;
            DataTable table = new DataTable();
            table.Clear();
            
            table.Columns.Add("Paciente");
            table.Columns.Add("Data e Hora Inicial");
            table.Columns.Add("Data e Hora Final");
            table.Columns.Add("Titulo");
            table.Columns.Add("Informação");
            table.Columns.Add("Cor");
            table.Columns.Add("Tipo Tarefa");
            table.Columns.Add("Dia Todo?", typeof(bool));
            table.Columns.Add("Ativo?", typeof(bool));
            bool ApenasAtivo = VerifcaAtivoInativo(chkApenasAtivos.Checked);
            RadioButton radioBtn = this.tableLayoutPanel2.Controls.OfType<RadioButton>()
                                             .Where(x => x.Checked).FirstOrDefault();
            String removepalavradoRadioButton = (radioBtn.Text).Substring(6);
            carregaAgenda(ApenasAtivo, removepalavradoRadioButton.TrimStart().TrimEnd());
            */

        }

        string sqlBaseEvento = "select  id,(select p.nome from paciente p where p.id = ae.paciente) as 'Paciente',ae.datahora_inicial as 'Data e Hora Iniciais', ae.datahora_final as 'Data e Hora Final', ae.titulo, ae.informacao_compromisso_agenda, ae.cor as 'Cor', ae.tipo_tarefa as 'Tipo da Tarefa', ae.dia_todo as 'Dia Todo?', ae.ativo as 'Registro Ativo?'  from agenda_compromisso ae";

        public void carregaAgenda(bool ativo,String tipodoEvento)
        {
            try
            {
          
                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseEvento + " where ae.ativo='"+ ativo + "' and ae.tipo_tarefa='"+ tipodoEvento+"' ", con.conectar());
                dt = new DataTable();
                adpt.Fill(dt);
                dt.Columns["id"].ColumnName = "id";
                dt.Columns["paciente"].ColumnName = "Paciente";
                dt.Columns["Data e Hora Iniciais"].ColumnName = "Data e Hora Inicial";
                dt.Columns["Data e Hora Final"].ColumnName = "Data e Hora Final";
                dt.Columns["titulo"].ColumnName = "Titulo";
                dt.Columns["informacao_compromisso_agenda"].ColumnName = "Informação do Compromisso";
                dt.Columns["cor"].ColumnName = "Cor da Tag";
                dt.Columns["Tipo da Tarefa"].ColumnName = "Tipo da Tarefa";
                dt.Columns["Dia Todo?"].ColumnName = "Dia Todo?";
                dt.Columns["Registro Ativo?"].ColumnName = "Registro Ativo?";


                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                    if (tipodoEvento == "Evento")
                    {
                        this.dataGridView1.Columns["Dia Todo?"].Visible = false;
                        MessageBox.Show("Filtrando Apenas Evento");

                    }
                    else if (tipodoEvento == "Lembrete")
                    {

                        this.dataGridView1.Columns["Data e Hora Final"].Visible = false;
                        this.dataGridView1.Columns["Dia Todo?"].Visible = true;
                        MessageBox.Show("Filtrando Apenas Lembrete");

                    }
                    else
                    {
                        this.dataGridView1.Columns["Dia Todo?"].Visible = true;
                        this.dataGridView1.Columns["Data e Hora Final"].Visible = true;
                    }

                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
              
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar Agenda. Entrar em contato com o Sr Paulo!" + ex);
            }
            finally
            {
                con.desconectar();
             
            }

        }

        public void carregaAgenda(bool ativo, String tipodoEvento,string paciente, DateTime datainicial, String datafinal)
        {
            try
            {

                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseEvento + " where ae.ativo='" + ativo + "' and ae.tipo_tarefa='" + tipodoEvento + "' and ae.paciente in("+ paciente + ") and  (datahora_inicial>='"+ datainicial + "' and  ISNULL(datahora_final,datahora_inicial)<='"+ datafinal +" 23:59:59.9900000')", con.conectar());

                dt = new DataTable();
                adpt.Fill(dt);
                dt.Columns["id"].ColumnName = "id";
                dt.Columns["paciente"].ColumnName = "Paciente";
                dt.Columns["Data e Hora Iniciais"].ColumnName = "Data e Hora Inicial";
                dt.Columns["Data e Hora Final"].ColumnName = "Data e Hora Final";
                dt.Columns["titulo"].ColumnName = "Titulo";
                dt.Columns["informacao_compromisso_agenda"].ColumnName = "Informação do Compromisso";
                dt.Columns["cor"].ColumnName = "Cor da Tag";
                dt.Columns["Tipo da Tarefa"].ColumnName = "Tipo da Tarefa";
                dt.Columns["Dia Todo?"].ColumnName = "Dia Todo?";
                dt.Columns["Registro Ativo?"].ColumnName = "Registro Ativo?";


                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;


                    this.dataGridView1.Columns["Data e Hora Final"].Visible = false;
                    this.dataGridView1.Columns["Dia Todo?"].Visible = true;
                    this.dataGridView1.Columns["Tipo da Tarefa"].Visible = false;
                    this.dataGridView1.Columns["id"].Visible = false;

                    /*
                    }
                    else
                    {
                        this.dataGridView1.Columns["Dia Todo?"].Visible = true;
                        this.dataGridView1.Columns["Data e Hora Final"].Visible = true;
                    this.dataGridView1.Columns["Informação do Compromisso"].Visible = false;
               */
                }


                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Anexos Registrados" + ex);
            }
            finally
            {
                con.desconectar();

            }

        }

        public void carregaAgenda(bool ativo, String tipodoEvento, string paciente,string Cor,DateTime datainicial, String datafinal)
        {
            try
            {

                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseEvento + " where ae.ativo='" + ativo + "' and ae.tipo_tarefa='" + tipodoEvento + "' and ae.paciente in(" + paciente + ") and cor='" + Cor + "'  and  (datahora_inicial>='" + datainicial + "' and  ISNULL(datahora_final,datahora_inicial)<='" + datafinal + " 23:59:59.9900000')", con.conectar());
                dt = new DataTable();
                adpt.Fill(dt);
                dt.Columns["id"].ColumnName = "id";
                dt.Columns["paciente"].ColumnName = "Paciente";
                dt.Columns["Data e Hora Iniciais"].ColumnName = "Data e Hora Inicial";
                dt.Columns["Data e Hora Final"].ColumnName = "Data e Hora Final";
                dt.Columns["titulo"].ColumnName = "Titulo";
                dt.Columns["informacao_compromisso_agenda"].ColumnName = "Informação do Compromisso";
                dt.Columns["cor"].ColumnName = "Cor da Tag";
                dt.Columns["Tipo da Tarefa"].ColumnName = "Tipo da Tarefa";
                dt.Columns["Dia Todo?"].ColumnName = "Dia Todo?";
                dt.Columns["Registro Ativo?"].ColumnName = "Registro Ativo?";






                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
            

                    this.dataGridView1.Columns["Data e Hora Final"].Visible = false;
                    this.dataGridView1.Columns["Dia Todo?"].Visible = true;
                    this.dataGridView1.Columns["Tipo da Tarefa"].Visible = false;
                    this.dataGridView1.Columns["id"].Visible = false;

            
                 
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar Agenda" + ex);
            }
            finally
            {
                con.desconectar();

            }

        }


        public void coloreDataGrid()
        {
        
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
          
        }

        public bool VerifcaAtivoInativo(bool ativoinativo)
        {
            if (ativoinativo == true)
            {
                return ativoinativo = true;
            }
            else
            {
                return ativoinativo = false;
            }
        }

        public string SelecioneCor(string corCompleta,char inicioArgb)
        {
            string verificaCor;
            int inicioTermo = corCompleta.IndexOf(inicioArgb);
            int turbinado = inicioTermo + 4;
            try
            {
                 if (corCompleta.Substring(inicioTermo + 3, 1) == "]")
                {
                    return verificaCor = corCompleta.Substring(inicioTermo + 2, 1);
                    // return verificaCor = "0" + corCompleta.Substring(inicioTermo + 2, 2);
                    //  return verificaCor = String.Join("GOYUYSYS",corCompleta.Substring(inicioTermo + 2, 2));
                }
                else if (corCompleta.Substring(inicioTermo + 4, 1) == "]")
                {
                    return verificaCor = corCompleta.Substring(inicioTermo + 2, 2);
                    // return verificaCor = "0" + corCompleta.Substring(inicioTermo + 2, 2);
                    //  return verificaCor = String.Join("GOYUYSYS",corCompleta.Substring(inicioTermo + 2, 2));
                }
                else if (corCompleta.Substring(inicioTermo + 3, 1) == ",")
                {
                    return verificaCor = corCompleta.Substring(inicioTermo + 2, 1);
                    //return verificaCor = "00" + corCompleta.Substring(inicioTermo + 2, 1);
                }
                else if (corCompleta.Substring(inicioTermo + 4, 1) == ",")
                {
                    //  return verificaCor = "0" + corCompleta.Substring(inicioTermo + 2, 2);
                    return verificaCor = corCompleta.Substring(inicioTermo + 2, 2);
                }

                else
                {
                    return verificaCor = corCompleta.Substring(inicioTermo + 2, 3);
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show("O erro que deu FOI"+ex);
                return null;
            }
          




        }

        
        public void getColorDataGrid(int loopPosition)
        {
            string cordaCelula = Convert.ToString(dataGridView1.Rows[loopPosition].Cells[6].Value);
            string correctString = cordaCelula.Replace("G=0", "G=000");
            cordaCelula.Replace("R=0", "R=000");
            cordaCelula.Replace("B=0", "B=000");
            cordaCelula.Replace("R=0", "R=000");
            if (Convert.ToString(dataGridView1.Rows[loopPosition].Cells[5].Value).Contains("A="))
            {
                int A = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'A'));
                int R = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'R'));
                int G = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'G'));
                int B = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'B'));
                dataGridView1.Rows[loopPosition].Cells[5].Style.BackColor = Color.FromArgb(A, R, G, B);
            }
            else
            {
                int inicioTermo = cordaCelula.IndexOf("[");
                int turbinado = (cordaCelula.Length - 2) - inicioTermo;
                string apenasNomeCor = cordaCelula.Substring(inicioTermo + 1, turbinado);
                dataGridView1.Rows[loopPosition].Cells[5].Style.BackColor = Color.FromName(apenasNomeCor);
            }

          
        }





        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
      //      dataGridView1.Columns["Cor"].Visible = false;

            for (int i = 0; i < dataGridView1.RowCount-1; i++)
            {
              
                string cordaCelula =Convert.ToString( dataGridView1.Rows[i].Cells[6].Value);
                string correctString= cordaCelula.Replace("G=0", "G=000");
                cordaCelula.Replace("R=0", "R=000"); 
                cordaCelula.Replace("B=0", "B=000");
                cordaCelula.Replace("R=0", "R=000");


                //se a cor gravada com o nome
                  if (Convert.ToString(dataGridView1.Rows[i].Cells["Cor da Tag"].Value).Contains("A=")) {

                    int A = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'A'));
            int R = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'R'));
            int G = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'G'));
            int B = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'B'));
            dataGridView1.Rows[i].Cells["Cor da Tag"].Style.BackColor = Color.FromArgb(A, R, G, B);


        }
        else 
        {
                    int inicioTermo = cordaCelula.IndexOf("[");
                    int turbinado = (cordaCelula.Length-2) - inicioTermo ;
                    string apenasNomeCor = cordaCelula.Substring(inicioTermo+1, turbinado);
                        dataGridView1.Rows[i].Cells[6].Style.BackColor = Color.FromName(apenasNomeCor);
        }
 

            }
                
        }

       
        public string UsuarioSelecionado()
        {
            int maiorIdDb = pc.getMaiorIdUser() + 1;
            string idPacientes = string.Empty;
            if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 0)
            {

                for (int i = 0; i < maiorIdDb; i++)
                {

                    idPacientes += i + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;
                /*
      
                */
            }
            else
            {
                for (int i = 0; i < userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count; i++)
                {

                    idPacientes += userControlPacienteListCheckbox1.SelectPosicaoPaciente(i).Id + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;

            }

        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            bool ApenasAtivo = VerifcaAtivoInativo(chkApenasAtivos.Checked);
            DateTime datainicio = monthCalendar1.SelectionStart;
            var datafinal = monthCalendar1.SelectionEnd.ToString("dd MMM yyyy");
        
         
            if (chkFiltrarPorCor.Checked)
            {
                carregaAgenda(ApenasAtivo, "Lembrete", UsuarioSelecionado(), colorDialog1.Color.ToString(), datainicio, datafinal);
            }
            else
            {
                carregaAgenda(ApenasAtivo, "Lembrete", UsuarioSelecionado(), datainicio, datafinal);
            }
        
        }

        private void chkApenasAtivos_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkFiltrarPorCor_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFiltrarPorCor.Checked)
            {
                btnCor.Enabled = true;
                btnCor.Visible = true;
            }
            else
            {
                btnCor.Enabled = false;
                btnCor.Visible = false;
                btnCor.BackColor =Color.FromName( "DarkGray");
                colorDialog1.Color = Color.FromArgb(255, 128, 128, 64);
                  
            }
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("agenda", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Acesso negado.Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                try
                {
                    fmrCadastroAgenda cadagenda = new fmrCadastroAgenda();
                    cadagenda.btnGravar.Visible = false;
                    cadagenda.btnLimpar.Visible = false;
                    cadagenda.btnAtualizar.Visible = true;

                    cadagenda.tabEvento.SelectedIndex = 1;
                        cadagenda.txtIdRegistro.Text = dataGridView1.CurrentRow.Cells["id"].Value.ToString();
                        cadagenda.txtNomePaciente.Text = dataGridView1.CurrentRow.Cells["Paciente"].Value.ToString();
                        cadagenda.dtpDiaLembrete.Text = dataGridView1.CurrentRow.Cells["Data e Hora Inicial"].Value.ToString();
                        cadagenda.dtpHoraLembrete.Text = dataGridView1.CurrentRow.Cells["Data e Hora Inicial"].Value.ToString();
                        cadagenda.txtTituloLembrete.Text = dataGridView1.CurrentRow.Cells["Titulo"].Value.ToString();
                        cadagenda.txtDescricaoLembrete.Text = dataGridView1.CurrentRow.Cells["Informação do Compromisso"].Value.ToString();
                        cadagenda.chkAtivo.Checked = Convert.ToBoolean(dataGridView1.CurrentRow.Cells["Registro Ativo?"].Value.ToString());
                        cadagenda.chkDiaTodoLembrete.Checked = Convert.ToBoolean(dataGridView1.CurrentRow.Cells["Dia Todo?"].Value.ToString());
                        //verifica a cor da tag para conversao os mesmos(add no formato que o backcolor aceita
                        string cordaCelula = Convert.ToString(dataGridView1.CurrentRow.Cells["Cor da Tag"].Value);
                        string correctString = cordaCelula.Replace("G=0", "G=000");
                        cordaCelula.Replace("R=0", "R=000");
                        cordaCelula.Replace("B=0", "B=000");
                        cordaCelula.Replace("R=0", "R=000");

                        //se a cor gravada com o nome
                        if (Convert.ToString(dataGridView1.CurrentRow.Cells["Cor da Tag"].Value.ToString()).Contains("A="))
                        {

                            int A = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'A'));
                            int R = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'R'));
                            int G = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'G'));
                            int B = Convert.ToInt32(SelecioneCor(correctString.Substring(7, (correctString.Length - 7)), 'B'));
                            dataGridView1.CurrentRow.Cells["Cor da Tag"].Style.BackColor = Color.FromArgb(A, R, G, B);
                            cadagenda.btnCorLembrete.BackColor = Color.FromArgb(A, R, G, B);
                        }
                        else
                        {

                            int inicioTermo = cordaCelula.IndexOf("[");
                            int turbinado = (cordaCelula.Length - 2) - inicioTermo;
                            string apenasNomeCor = cordaCelula.Substring(inicioTermo + 1, turbinado);
                            dataGridView1.CurrentRow.Cells["Cor da Tag"].Style.BackColor = Color.FromName(apenasNomeCor);
                            cadagenda.btnCorLembrete.BackColor = Color.FromName(apenasNomeCor);

                        }
                        cadagenda.Show();

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ops , infelizmente não foi possivel abrir o arquivo" + ex);
                }
            }
        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}



