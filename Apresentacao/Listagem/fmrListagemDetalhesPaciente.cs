﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Listagem
{
    public partial class fmrListagemDetalhesPaciente : Form
    {
        Paciente pc = new Paciente();
        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        NivelUsuario nivelUser = new NivelUsuario();

        public fmrListagemDetalhesPaciente()
        {
            InitializeComponent();
        }

        private void btnNovoDetalhe_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("inf_comp_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadadastroPacienteInfComplementar cadPacienteComp = new fmrCadadastroPacienteInfComplementar();

                if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 1)
                {
                    for (int i = 0; i < userControlPacienteListCheckbox1.chkPacientes.CheckedIndices.Count; i++)
                    {
                        cadPacienteComp.txtNomePaciente.Text = userControlPacienteListCheckbox1.SelectPosicaoPaciente(i).Nome;
                        cadPacienteComp.cbxPaciente.chkAtivo.Checked = userControlPacienteListCheckbox1.chkAtivo.Checked;
                        cadPacienteComp.Show();
                    }
                }
                else
                {
                    MessageBox.Show("Atenção existe nenhum ou mais de 2 usuários selecionados. Por favor selecione apenas um paciente para o correto cadastro ");
                    cadPacienteComp.Show();
                }
            }
            

        }


        public void criaColunasTabela()
        {
            dt = new DataTable();
            adpt.Fill(dt);
            dt.Columns["id"].ColumnName = "ID";
            dt.Columns["Nome"].ColumnName = "Nome";
            dt.Columns["ativo"].ColumnName = "Ativo?";
            dt.Columns["composicao_familiar"].ColumnName = "Composição Familiar";
            dt.Columns["obs_composicao_familiar"].ColumnName = "Obs composição familiar";
            dt.Columns["nome_plano_saude"].ColumnName = "Nome plano de saude";
            dt.Columns["carencia_plano_cirurgia"].ColumnName = "Carencia plano cirurgia";
            dt.Columns["unidadetempo_carencia_plano_cirurgia"].ColumnName = "Unidade de Tempo da Carencia plano cirurgia";
            dt.Columns["carencia_plano_consulta"].ColumnName = "Carencia plano consulta";
            dt.Columns["unidadetempo_carencia_plano_consulta"].ColumnName = "Unidade de Tempo da Carencia plano consulta";
            dt.Columns["carencia_plano_outros"].ColumnName = "Carencia plano outros";
            dt.Columns["unidadetempo_carencia_plano_outros"].ColumnName = "Unidade de Tempo da Carencia plano outros";
            dt.Columns["obs_plano_saude"].ColumnName = "Obs plano de saude";
            dt.Columns["necessidades_especiais"].ColumnName = "Necessidades Especiais";
            dt.Columns["obs_necessidades_especiais"].ColumnName = "Obs Necessidades Especiais";
            dt.Columns["restricao_alimentares"].ColumnName = "Restrição Alimentares";
            dt.Columns["obs_restricao_alimentares"].ColumnName = "Obs Restrição Alimentares";
            dt.Columns["alergias"].ColumnName = "Alergias";
            dt.Columns["obs_alergias"].ColumnName = "Obs Alergias";
        }

        public void criaColunasInicias()
        {
            DataTable table = new DataTable();
            table.Clear();
            table.Columns.Add("ID");
            table.Columns.Add("Nome");
            table.Columns.Add("Ativo?", typeof(bool));
            table.Columns.Add("Composição Familiar");
            table.Columns.Add("Obs composição familiar");
            table.Columns.Add("Nome plano de saude");
            table.Columns.Add("Carencia plano cirurgia");
            table.Columns.Add("Unidade de Tempo da Carencia plano cirurgia");
            table.Columns.Add("Carencia plano consulta");
            table.Columns.Add("Unidade de Tempo da Carencia plano consulta");
            table.Columns.Add("Carencia plano outros");
            table.Columns.Add("Unidade de Tempo da Carencia plano outros");
            table.Columns.Add("Obs plano de saude");
            table.Columns.Add("Necessidades Especiais");
            table.Columns.Add("Obs Necessidades Especiais");
            table.Columns.Add("Restrição Alimentares");
            table.Columns.Add("Obs Restrição Alimentares");
            table.Columns.Add("Alergias");
            table.Columns.Add("Obs Alergias");
        }



        public void inativaColunas(bool valor)
        {
            this.dataGridView1.Columns["ID"].Visible = valor;
            /*
            this.dataGridView1.Columns["Ativo"].Visible = valor;
            this.dataGridView1.Columns["Composição Familiar"].Visible = valor;
            this.dataGridView1.Columns["Obs composição familiar"].Visible = valor;
            this.dataGridView1.Columns["Nome plano de saude"].Visible = valor;
   r;
            this.dataGridView1.Columns["Obs plano de saude"].Visible = valor;
            this.dataGridView1.Columns["Necessidades Especiais"].Visible = valor;
            this.dataGridView1.Columns["Restrição Alimentares"].Visible = valor;
            this.dataGridView1.Columns["Alergias"].Visible = valor;
                */
            this.dataGridView1.Columns["Carencia plano consulta"].Visible = valor;
            this.dataGridView1.Columns["Carencia plano cirurgia"].Visible = valor;
            this.dataGridView1.Columns["Carencia plano outros"].Visible = valor;
            this.dataGridView1.Columns["Unidade de Tempo da Carencia plano cirurgia"].Visible = valor;
            this.dataGridView1.Columns["Unidade de Tempo da Carencia plano consulta"].Visible = valor;
            this.dataGridView1.Columns["Unidade de Tempo da Carencia plano outros"].Visible = valor;
        
     
        }

        public void bloqueiaColunas(bool valor)
        {
            this.dataGridView1.Columns["ID"].ReadOnly = valor;
            this.dataGridView1.Columns["Ativo?"].ReadOnly = valor;
            this.dataGridView1.Columns["Composição Familiar"].ReadOnly = valor;
            this.dataGridView1.Columns["Obs composição familiar"].ReadOnly = valor;
            this.dataGridView1.Columns["Nome plano de saude"].ReadOnly = valor;
            this.dataGridView1.Columns["Carencia plano consulta"].ReadOnly = valor;
            this.dataGridView1.Columns["Unidade de Tempo da Carencia plano consulta"].ReadOnly = valor;
            this.dataGridView1.Columns["Carencia plano cirurgia"].ReadOnly = valor;
            this.dataGridView1.Columns["Unidade de Tempo da Carencia plano cirurgia"].ReadOnly = valor;
            this.dataGridView1.Columns["Carencia plano outros"].ReadOnly = valor;
            this.dataGridView1.Columns["Unidade de Tempo da Carencia plano outros"].ReadOnly = valor;
            this.dataGridView1.Columns["Obs plano de saude"].ReadOnly = valor;
            this.dataGridView1.Columns["Necessidades Especiais"].ReadOnly = valor;
            this.dataGridView1.Columns["Restrição Alimentares"].ReadOnly = valor;
            this.dataGridView1.Columns["Alergias"].ReadOnly = valor;
        }



        public string UsuarioSelecionado()
        {
            int maiorIdDb = pc.getMaiorIdUser() + 1;
            string idPacientes = string.Empty;
            if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 0)
            {

                for (int i = 0; i < maiorIdDb; i++)
                {

                    idPacientes += i + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;
                /*
      
                */
            }
            else
            {
                for (int i = 0; i < userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count; i++)
                {

                    idPacientes += userControlPacienteListCheckbox1.SelectPosicaoPaciente(i).Id + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;

            }

        }

        string SQLbase = "id,ativo,nome,composicao_familiar, obs_composicao_familiar, nome_plano_saude, carencia_plano_cirurgia,unidadetempo_carencia_plano_cirurgia, carencia_plano_consulta,unidadetempo_carencia_plano_consulta, carencia_plano_outros,unidadetempo_carencia_plano_outros, obs_plano_saude, necessidades_especiais, obs_necessidades_especiais, restricao_alimentares, obs_restricao_alimentares, alergias, obs_alergias   FROM dbo.paciente";

        public void carregaDetalhesPaciente()
        {
            // string inf = tudoOuSoAtivo(registroativo);
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter("SELECT top 10 "+ SQLbase +" where ativo="+VerifcaAtivoInativo(userControlPacienteListCheckbox1.chkAtivo.Checked)+"order by id desc", con.conectar());
                criaColunasTabela();
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias 
                inativaColunas(false);
                bloqueiaColunas(true);
                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }


        public void carregaDetalhesPaciente(String registroativo_ou_inativo, string paciente)
        {
            // string inf = tudoOuSoAtivo(registroativo);
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter("Select " + SQLbase + " where ativo in(" + registroativo_ou_inativo + ") and id in(" + paciente + ")  ", con.conectar());
                criaColunasTabela();
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias 
                inativaColunas(false);
                bloqueiaColunas(true);
                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }

        public string tudoOuSoAtivo(int opcao)
        {
            string inf;
            if (opcao.Equals(0))
            {
                return inf = "0,1";
            }
            else
            {
                return inf = "1";
            }
        }

        public int VerifcaAtivoInativo(bool ativoinativo)
        {
            int opcao;
            ;
            if (ativoinativo == true)
            {
                return opcao = 1;
            }
            else
            {
                return opcao = 0;
            }


        }


        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            string verificaAtivoOuInativo = tudoOuSoAtivo(VerifcaAtivoInativo(userControlPacienteListCheckbox1.chkAtivo.Checked));
            carregaDetalhesPaciente(verificaAtivoOuInativo, UsuarioSelecionado());

        }

        private void fmrListagemDetalhesPaciente_Load(object sender, EventArgs e)
        {
            criaColunasInicias();
            carregaDetalhesPaciente();
            inativaColunas(false);
            bloqueiaColunas(true);
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("inf_comp_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadadastroPacienteInfComplementar cadastroInfComplementarPac = new fmrCadadastroPacienteInfComplementar();
                cadastroInfComplementarPac.txtCodigoPaciente.Text = dataGridView1.CurrentRow.Cells["ID"].Value.ToString();
                cadastroInfComplementarPac.cbxPaciente.chkAtivo.Checked = Convert.ToBoolean(dataGridView1.CurrentRow.Cells["Ativo?"].Value.ToString());
                cadastroInfComplementarPac.chkAtivo.Checked = Convert.ToBoolean(dataGridView1.CurrentRow.Cells["Ativo?"].Value.ToString());
                cadastroInfComplementarPac.txtNomePaciente.Text = dataGridView1.CurrentRow.Cells["Nome"].Value.ToString();
                cadastroInfComplementarPac.txtComposiçãoFamiliar.Text = dataGridView1.CurrentRow.Cells["Composição Familiar"].Value.ToString();
                cadastroInfComplementarPac.txtObsComposicaoFamiliar.Text = dataGridView1.CurrentRow.Cells["Obs composição familiar"].Value.ToString();
                cadastroInfComplementarPac.txtNomePlano.Text = dataGridView1.CurrentRow.Cells["Nome plano de saude"].Value.ToString();
                cadastroInfComplementarPac.txtCarenciaCirurgia.Text = dataGridView1.CurrentRow.Cells["Carencia plano cirurgia"].Value.ToString();
                cadastroInfComplementarPac.txtCarenciaConsulta.Text = dataGridView1.CurrentRow.Cells["Carencia plano consulta"].Value.ToString();
                cadastroInfComplementarPac.txtCarenciaOutros.Text = dataGridView1.CurrentRow.Cells["Carencia plano outros"].Value.ToString();
                cadastroInfComplementarPac.txtObsPlanoSaude.Text = dataGridView1.CurrentRow.Cells["Obs plano de saude"].Value.ToString();
                cadastroInfComplementarPac.txtNecessidadeEspecial.Text = dataGridView1.CurrentRow.Cells["Necessidades Especiais"].Value.ToString();
                cadastroInfComplementarPac.txtObsNecessidadeEspecial.Text = dataGridView1.CurrentRow.Cells["Obs Necessidades Especiais"].Value.ToString();
                cadastroInfComplementarPac.txtRestricaoAlimentar.Text = dataGridView1.CurrentRow.Cells["Restrição Alimentares"].Value.ToString();
                cadastroInfComplementarPac.txtObsRestricaoAlimentar.Text = dataGridView1.CurrentRow.Cells["Obs Restrição Alimentares"].Value.ToString();
                cadastroInfComplementarPac.txtAlergias.Text = dataGridView1.CurrentRow.Cells["Alergias"].Value.ToString();
                cadastroInfComplementarPac.txtObsAlergias.Text = dataGridView1.CurrentRow.Cells["Obs Alergias"].Value.ToString();

                cadastroInfComplementarPac.txtUnidTempoCirurgia.Text = dataGridView1.CurrentRow.Cells["Unidade de Tempo da Carencia plano cirurgia"].Value.ToString();
                cadastroInfComplementarPac.txtUnidTempoConsulta.Text = dataGridView1.CurrentRow.Cells["Unidade de Tempo da Carencia plano consulta"].Value.ToString();
                cadastroInfComplementarPac.txtUnidTempoOutros.Text = dataGridView1.CurrentRow.Cells["Unidade de Tempo da Carencia plano outros"].Value.ToString();
                //identifica que veio de um dupplo clique
                cadastroInfComplementarPac.chkEhDuploClique.Checked = true;
                cadastroInfComplementarPac.cbxPaciente.Enabled = false;
                cadastroInfComplementarPac.btnCadastrar.Visible = false;
                cadastroInfComplementarPac.btnLimpar.Visible = false;
                cadastroInfComplementarPac.btnInativar.Visible = true;
                cadastroInfComplementarPac.btnAtualizarUsuario.Visible = true;


                cadastroInfComplementarPac.Show();
            }
       
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
