﻿namespace SistemaShekinah.Apresentacao
{
    partial class fmrListagemCadastroMedicacoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtNomeQuimico = new System.Windows.Forms.TextBox();
            this.lblNomeQuimico = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.chkAtivoInativo = new System.Windows.Forms.CheckBox();
            this.lblNomeComercial = new System.Windows.Forms.Label();
            this.txtNomeComercial = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.dataListagemMedicacoes = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.btnNovaMedicacao = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataListagemMedicacoes)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 664F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 473F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.txtNomeQuimico, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNomeQuimico, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1254, 37);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // txtNomeQuimico
            // 
            this.txtNomeQuimico.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtNomeQuimico.Location = new System.Drawing.Point(100, 8);
            this.txtNomeQuimico.Name = "txtNomeQuimico";
            this.txtNomeQuimico.Size = new System.Drawing.Size(274, 20);
            this.txtNomeQuimico.TabIndex = 1;
            // 
            // lblNomeQuimico
            // 
            this.lblNomeQuimico.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNomeQuimico.AutoSize = true;
            this.lblNomeQuimico.Location = new System.Drawing.Point(9, 12);
            this.lblNomeQuimico.Name = "lblNomeQuimico";
            this.lblNomeQuimico.Size = new System.Drawing.Size(78, 13);
            this.lblNomeQuimico.TabIndex = 0;
            this.lblNomeQuimico.Text = "Nome Químico";
            this.lblNomeQuimico.Click += new System.EventHandler(this.lblNomeQuimico_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.97297F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 87.02702F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 224F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 176F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.Controls.Add(this.chkAtivoInativo, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblNomeComercial, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtNomeComercial, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 37);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1254, 34);
            this.tableLayoutPanel2.TabIndex = 1;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.TableLayoutPanel2_Paint);
            // 
            // chkAtivoInativo
            // 
            this.chkAtivoInativo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.chkAtivoInativo.AutoSize = true;
            this.chkAtivoInativo.Checked = true;
            this.chkAtivoInativo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAtivoInativo.Location = new System.Drawing.Point(832, 8);
            this.chkAtivoInativo.Name = "chkAtivoInativo";
            this.chkAtivoInativo.Size = new System.Drawing.Size(147, 17);
            this.chkAtivoInativo.TabIndex = 2;
            this.chkAtivoInativo.Text = "Somente Registros Ativos";
            this.chkAtivoInativo.UseVisualStyleBackColor = true;
            // 
            // lblNomeComercial
            // 
            this.lblNomeComercial.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblNomeComercial.AutoSize = true;
            this.lblNomeComercial.Location = new System.Drawing.Point(3, 10);
            this.lblNomeComercial.Name = "lblNomeComercial";
            this.lblNomeComercial.Size = new System.Drawing.Size(84, 13);
            this.lblNomeComercial.TabIndex = 3;
            this.lblNomeComercial.Text = "Nome Comercial";
            // 
            // txtNomeComercial
            // 
            this.txtNomeComercial.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtNomeComercial.Location = new System.Drawing.Point(110, 7);
            this.txtNomeComercial.Name = "txtNomeComercial";
            this.txtNomeComercial.Size = new System.Drawing.Size(245, 20);
            this.txtNomeComercial.TabIndex = 4;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.375F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 89.625F));
            this.tableLayoutPanel3.Controls.Add(this.btnBuscar, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 71);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1254, 33);
            this.tableLayoutPanel3.TabIndex = 2;
            this.tableLayoutPanel3.Paint += new System.Windows.Forms.PaintEventHandler(this.TableLayoutPanel3_Paint);
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnBuscar.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Location = new System.Drawing.Point(3, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 27);
            this.btnBuscar.TabIndex = 0;
            this.btnBuscar.Text = "Filtrar";
            this.btnBuscar.UseVisualStyleBackColor = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.dataListagemMedicacoes, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 104);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1254, 276);
            this.tableLayoutPanel4.TabIndex = 3;
            // 
            // dataListagemMedicacoes
            // 
            this.dataListagemMedicacoes.AllowUserToOrderColumns = true;
            this.dataListagemMedicacoes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataListagemMedicacoes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataListagemMedicacoes.Location = new System.Drawing.Point(3, 3);
            this.dataListagemMedicacoes.Name = "dataListagemMedicacoes";
            this.dataListagemMedicacoes.Size = new System.Drawing.Size(1248, 270);
            this.dataListagemMedicacoes.TabIndex = 0;
            this.dataListagemMedicacoes.DoubleClick += new System.EventHandler(this.dataListagemMedicacoes_DoubleClick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.btnNovaMedicacao, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 380);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1254, 46);
            this.tableLayoutPanel5.TabIndex = 4;
            this.tableLayoutPanel5.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel5_Paint);
            // 
            // btnNovaMedicacao
            // 
            this.btnNovaMedicacao.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnNovaMedicacao.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovaMedicacao.Location = new System.Drawing.Point(3, 3);
            this.btnNovaMedicacao.Name = "btnNovaMedicacao";
            this.btnNovaMedicacao.Size = new System.Drawing.Size(181, 29);
            this.btnNovaMedicacao.TabIndex = 0;
            this.btnNovaMedicacao.Text = "Cadastrar Nova Medicação";
            this.btnNovaMedicacao.UseVisualStyleBackColor = false;
            this.btnNovaMedicacao.Click += new System.EventHandler(this.btnNovaMedicacao_Click);
            // 
            // fmrListagemCadastroMedicacoes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(1254, 450);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "fmrListagemCadastroMedicacoes";
            this.Load += new System.EventHandler(this.fmrListagemCadastroMedicacoes_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataListagemMedicacoes)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblNomeQuimico;
        private System.Windows.Forms.TextBox txtNomeQuimico;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button btnNovaMedicacao;
        private System.Windows.Forms.Label lblNomeComercial;
        private System.Windows.Forms.TextBox txtNomeComercial;
        public System.Windows.Forms.CheckBox chkAtivoInativo;
        public System.Windows.Forms.DataGridView dataListagemMedicacoes;
    }
}