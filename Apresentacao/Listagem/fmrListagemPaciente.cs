﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Listagem
{
    public partial class fmrListagemPaciente : Form
    {

        Paciente pc = new Paciente();

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        NivelUsuario nivelUser = new NivelUsuario();
        Usuario user = new Usuario();



        string SQLbase = "SELECT id,nome,cpf,ativo,tipo_identificacao,num_identificacao,data_nasc,raca,cns ,sexo,data_admissao,escolaridade,telefone1,telefone2,nome_contato,telefone_contato_emergencia,nome_contato_emergencia,cep,logradouro,numero_logradouro ,complemento ,uf,municipio,nome_responsavel_cep_paciente,obs_endereco_paciente,cep_residencia_anterior,logradouro_residencia_anterior,numero_logradouro_residencia_anterior,complemento_residencia_anterior,uf_residencia_anterior,municipio_residencia_anterior ,nome_responsavel_cep_paciente_residencia_anterior,obs_endereco_paciente_residencia_anterior ,composicao_familiar,obs_composicao_familiar,nome_plano_saude,carencia_plano_cirurgia,carencia_plano_consulta,carencia_plano_outros,obs_plano_saude ,necessidades_especiais,restricao_alimentares ,alergias FROM dbo.paciente";

        public fmrListagemPaciente()
        {
            InitializeComponent();
        }

        private void btnPaciente_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("cadastro_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroPaciente cadpaciente = new fmrCadastroPaciente();
                cadpaciente.btnInativar.Visible = false;
                cadpaciente.btnAtualizar.Visible = false;
                cadpaciente.ShowDialog();

            }
               
        }

        public int VerifcaAtivoInativo(bool ativoinativo)
        {
            int opcao;
            ;
            if (ativoinativo == true)
            {
                return opcao = 1;
            }
            else
            {
                return opcao = 0;
            }


        }

        public string UsuarioSelecionado()
        {
            int maiorIdDb = pc.getMaiorIdUser()+1;
            string idPacientes = string.Empty;
            if (cmbPaciente.chkPacientes.CheckedItems.Count==0)
            {

                for (int i = 0; i < maiorIdDb; i++)
                {

                    idPacientes += i + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;
                /*
      
                */
            }
            else
            {
                for (int i = 0; i < cmbPaciente.chkPacientes.CheckedItems.Count; i++)
                {

                    idPacientes += cmbPaciente.SelectPosicaoPaciente(i).Id + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;
               
            }

        }


      

        public void criaColunasTabela()
        {

                dt = new DataTable();
                adpt.Fill(dt);
                dt.Columns["id"].ColumnName = "ID";
                dt.Columns["nome"].ColumnName = "Nome do Paciente";
                dt.Columns["cpf"].ColumnName = "C.P.F.";
                dt.Columns["ativo"].ColumnName = "Ativo";
                dt.Columns["tipo_identificacao"].ColumnName = "Tipo de Identificação";
                dt.Columns["num_identificacao"].ColumnName = "Numero de Identificação";
                dt.Columns["data_nasc"].ColumnName = "Data de Nascimento";
                dt.Columns["raca"].ColumnName = "Raça";
                dt.Columns["cns"].ColumnName = "C.N.S.";
                dt.Columns["sexo"].ColumnName = "Sexo";
                dt.Columns["data_admissao"].ColumnName = "Data de Admissão";
                dt.Columns["escolaridade"].ColumnName = "Escolaridade";
                dt.Columns["telefone1"].ColumnName = "Telefone 1";
                dt.Columns["telefone2"].ColumnName = "Telefone 2";
                dt.Columns["nome_contato"].ColumnName = "Nome do Contato";
                dt.Columns["telefone_contato_emergencia"].ColumnName = "Numero de Contato de Emergencia";
                dt.Columns["nome_contato_emergencia"].ColumnName = "Nome do contato de emergencia";
                dt.Columns["cep"].ColumnName = "CEP";
                dt.Columns["logradouro"].ColumnName = "Logradouro";
                dt.Columns["numero_logradouro"].ColumnName = "Numero Logradouro";
                dt.Columns["complemento"].ColumnName = "Complemento";
                dt.Columns["uf"].ColumnName = "UF";
                dt.Columns["municipio"].ColumnName = "Municipio";
                dt.Columns["nome_responsavel_cep_paciente"].ColumnName = "Nome do Responsavel (endereço paciente)";
                dt.Columns["obs_endereco_paciente"].ColumnName = "Obs Endereço Paciente";
                dt.Columns["cep_residencia_anterior"].ColumnName = "Cep residencia anterior";
                dt.Columns["logradouro_residencia_anterior"].ColumnName = "Logradouro residencia anterior";
                dt.Columns["numero_logradouro_residencia_anterior"].ColumnName = "Numero residencia anterior";
                dt.Columns["complemento_residencia_anterior"].ColumnName = "Complemento residencia anterior";
                dt.Columns["uf_residencia_anterior"].ColumnName = "UF residencia anterior";
                dt.Columns["municipio_residencia_anterior"].ColumnName = "Municipio residencia anterior";
                dt.Columns["nome_responsavel_cep_paciente_residencia_anterior"].ColumnName = "Nome responsavel residencia anterior";
                dt.Columns["obs_endereco_paciente_residencia_anterior"].ColumnName = "Obs residencia anterior";
                dt.Columns["composicao_familiar"].ColumnName = "Composição Familiar";
                dt.Columns["obs_composicao_familiar"].ColumnName = "Obs composição familiar";
                dt.Columns["nome_plano_saude"].ColumnName = "Nome plano de saude";
                dt.Columns["carencia_plano_cirurgia"].ColumnName = "Carencia plano cirurgia";
                dt.Columns["carencia_plano_consulta"].ColumnName = "Carencia plano consulta";
                dt.Columns["carencia_plano_outros"].ColumnName = "Carencia plano outros";
                dt.Columns["obs_plano_saude"].ColumnName = "Obs plano de saude";
                dt.Columns["necessidades_especiais"].ColumnName = "Necessidades Especiais";
                dt.Columns["restricao_alimentares"].ColumnName = "Restrição Alimentares";
                dt.Columns["alergias"].ColumnName = "Alergias";

       


        }

        public void inativaColunas(bool valor)
        {
            this.dataGridView1.Columns["ID"].Visible = valor;
            this.dataGridView1.Columns["Ativo"].Visible = valor;
         //   this.dataGridView1.Columns["Tipo de Identificação"].Visible = valor;
         //   this.dataGridView1.Columns["Numero de Identificação"].Visible = valor;
            this.dataGridView1.Columns["Raça"].Visible = valor;
            this.dataGridView1.Columns["C.N.S."].Visible = valor;
            this.dataGridView1.Columns["Sexo"].Visible = valor;
            this.dataGridView1.Columns["Data de Admissão"].Visible = valor;
            this.dataGridView1.Columns["Escolaridade"].Visible = valor;
            this.dataGridView1.Columns["Telefone 1"].Visible = valor;
            this.dataGridView1.Columns["Telefone 2"].Visible = valor;
            this.dataGridView1.Columns["Nome do Contato"].Visible = valor;
            this.dataGridView1.Columns["Numero de Contato de Emergencia"].Visible = valor;
            this.dataGridView1.Columns["Nome do contato de emergencia"].Visible = valor;
            this.dataGridView1.Columns["CEP"].Visible = valor;
            this.dataGridView1.Columns["Logradouro"].Visible = valor;
            this.dataGridView1.Columns["Numero Logradouro"].Visible = valor;
            this.dataGridView1.Columns["Complemento"].Visible = valor;
            this.dataGridView1.Columns["UF"].Visible = valor;
            this.dataGridView1.Columns["Municipio"].Visible = valor;
            this.dataGridView1.Columns["Nome do Responsavel (endereço paciente)"].Visible = valor;
            this.dataGridView1.Columns["Obs Endereço Paciente"].Visible = valor;
            this.dataGridView1.Columns["Cep residencia anterior"].Visible = valor;
            this.dataGridView1.Columns["Logradouro residencia anterior"].Visible = valor;
            this.dataGridView1.Columns["Numero residencia anterior"].Visible = valor;
            this.dataGridView1.Columns["Complemento residencia anterior"].Visible = valor;
            this.dataGridView1.Columns["UF residencia anterior"].Visible = valor;
            this.dataGridView1.Columns["Municipio residencia anterior"].Visible = valor;
            this.dataGridView1.Columns["Nome responsavel residencia anterior"].Visible = valor;
            this.dataGridView1.Columns["Obs residencia anterior"].Visible = valor;
            this.dataGridView1.Columns["Composição Familiar"].Visible = valor;
            this.dataGridView1.Columns["Obs composição familiar"].Visible = valor;
            this.dataGridView1.Columns["Nome plano de saude"].Visible = valor;
            this.dataGridView1.Columns["Carencia plano consulta"].Visible = valor;
            this.dataGridView1.Columns["Carencia plano cirurgia"].Visible = valor;
            this.dataGridView1.Columns["Carencia plano outros"].Visible = valor;
            this.dataGridView1.Columns["Obs plano de saude"].Visible = valor;
            this.dataGridView1.Columns["Necessidades Especiais"].Visible = valor;
            this.dataGridView1.Columns["Restrição Alimentares"].Visible = valor;
            this.dataGridView1.Columns["Alergias"].Visible = valor;
        }

        public string tudoOuSoAtivo(int opcao)
        {
            string inf;
            if (opcao.Equals(0))
            {
                return inf = "0,1";
            }
            else
            {
                return inf = "1";
            }
        }



        public void carregaPaciente(String registroativo_ou_inativo, string paciente)
        {
           // string inf = tudoOuSoAtivo(registroativo);
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter("" + SQLbase + " where ativo in(" + registroativo_ou_inativo + ") and id in("+paciente+")  ", con.conectar());
                criaColunasTabela();
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
               inativaColunas(false);
                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }

        public void carregaPaciente(String registroativo_ou_inativo, string paciente,string tipodoc,string numdoc)
        {
            // string inf = tudoOuSoAtivo(registroativo);
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter("" + SQLbase + " where ativo in(" + registroativo_ou_inativo + ") and id in(" + paciente + ") and (num_identificacao=('"+numdoc+ "') and tipo_identificacao='"+ tipodoc + "')  ", con.conectar());
                criaColunasTabela();
                dataGridView1.DataSource = dt;
                //oculta as colunas que não são necessárias para o usuário8
                inativaColunas(false);
                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridView1.DataSource = null;

                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }





        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            string verificaAtivoOuInativo = tudoOuSoAtivo(VerifcaAtivoInativo(cmbPaciente.chkAtivo.Checked));
            if (txtTipoIdentificacao.Text.Equals(""))
            {
                carregaPaciente(verificaAtivoOuInativo, UsuarioSelecionado());
            }
            else
            {
                carregaPaciente(verificaAtivoOuInativo, UsuarioSelecionado(), cmbTipoDoc.SelectTipoIdentidade.nomeTipoIdentidade, txtTipoIdentificacao.Text);
            }
          
        }

        private void button1_Click(object sender, EventArgs e)
        {
   
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
           

        }

        private void CmbPaciente_Load(object sender, EventArgs e)
        {

        }

        public bool ativouOuInativo(string value)

        {
            if (value.Equals("True"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("cadastro_paciente", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroPaciente cadPaciente = new fmrCadastroPaciente();
                cadPaciente.txtID.Text = dataGridView1.CurrentRow.Cells["ID"].Value.ToString();
                cadPaciente.chkAtivo.Checked = ativouOuInativo(dataGridView1.CurrentRow.Cells["Ativo"].Value.ToString());
                cadPaciente.txtNome.Text = dataGridView1.CurrentRow.Cells["Nome do Paciente"].Value.ToString();
                cadPaciente.txtCpf.Text = dataGridView1.CurrentRow.Cells["C.P.F."].Value.ToString();
                cadPaciente.txtNomeTipoIdentificacao.Text = dataGridView1.CurrentRow.Cells["Tipo de Identificação"].Value.ToString();
                cadPaciente.txtTipoDoc.Text = dataGridView1.CurrentRow.Cells["Numero de Identificação"].Value.ToString();
                cadPaciente.dtpDataNasc.Text = dataGridView1.CurrentRow.Cells["Data de Nascimento"].Value.ToString();
                cadPaciente.txtNomeRaca.Text = dataGridView1.CurrentRow.Cells["Raça"].Value.ToString();
                cadPaciente.txtCns.Text = dataGridView1.CurrentRow.Cells["C.N.S."].Value.ToString();
                cadPaciente.txtNomeSexo.Text = dataGridView1.CurrentRow.Cells["Sexo"].Value.ToString();
                cadPaciente.dtpAdmissao.Text = dataGridView1.CurrentRow.Cells["Data de Admissão"].Value.ToString();
                cadPaciente.txtNomeEscolaridadade.Text = dataGridView1.CurrentRow.Cells["Escolaridade"].Value.ToString();
                cadPaciente.txtTel1.Text = dataGridView1.CurrentRow.Cells["Telefone 1"].Value.ToString();
                cadPaciente.txtTel2.Text = dataGridView1.CurrentRow.Cells["Telefone 2"].Value.ToString();
                cadPaciente.txtNomeContato.Text = dataGridView1.CurrentRow.Cells["Nome do Contato"].Value.ToString();
                cadPaciente.txtNumeroDeEmergencia.Text = dataGridView1.CurrentRow.Cells["Numero de Contato de Emergencia"].Value.ToString();
                cadPaciente.txtContatoDeEmergencia.Text = dataGridView1.CurrentRow.Cells["Nome do contato de emergencia"].Value.ToString();
                cadPaciente.txtCep.Text = dataGridView1.CurrentRow.Cells["CEP"].Value.ToString();
                cadPaciente.txtLogradouro.Text = dataGridView1.CurrentRow.Cells["Logradouro"].Value.ToString();
                cadPaciente.txtNumero.Text = dataGridView1.CurrentRow.Cells["Numero Logradouro"].Value.ToString();
                cadPaciente.txtComplemento.Text = dataGridView1.CurrentRow.Cells["Complemento"].Value.ToString();
                cadPaciente.txtNomeUFEnderecoAtual.Text = dataGridView1.CurrentRow.Cells["UF"].Value.ToString();
                cadPaciente.txtNomeMunicipioEnderecoAtual.Text = dataGridView1.CurrentRow.Cells["Municipio"].Value.ToString();
                cadPaciente.txtResponsavelEndereco.Text = dataGridView1.CurrentRow.Cells["Nome do Responsavel (endereço paciente)"].Value.ToString();
                cadPaciente.txtObsEnderecoPaciente.Text = dataGridView1.CurrentRow.Cells["Obs Endereço Paciente"].Value.ToString();
                cadPaciente.txtCepResidenciaAnterior.Text = dataGridView1.CurrentRow.Cells["Cep residencia anterior"].Value.ToString();
                cadPaciente.txtLogradouroResidenciaAnterior.Text = dataGridView1.CurrentRow.Cells["Logradouro residencia anterior"].Value.ToString();
                cadPaciente.txtNumeroResidenciaAnterior.Text = dataGridView1.CurrentRow.Cells["Numero residencia anterior"].Value.ToString();
                cadPaciente.txtCompletoResidenciaAnterior.Text = dataGridView1.CurrentRow.Cells["Complemento residencia anterior"].Value.ToString();
                cadPaciente.txtNomeUFEnderecoAnterior.Text = dataGridView1.CurrentRow.Cells["UF residencia anterior"].Value.ToString();
                cadPaciente.txtNomeMunicipioEnderecoAnterior.Text = dataGridView1.CurrentRow.Cells["Municipio residencia anterior"].Value.ToString();
                cadPaciente.txtResponsavelResidenciaAnterior.Text = dataGridView1.CurrentRow.Cells["Nome responsavel residencia anterior"].Value.ToString();
                cadPaciente.txtObsResidenciaAnterior.Text = dataGridView1.CurrentRow.Cells["Obs residencia anterior"].Value.ToString();
                cadPaciente.btnLimpar.Visible = false;
                cadPaciente.btnCadastrar.Visible = false;
                cadPaciente.btnAtualizar.Visible = true;
                cadPaciente.btnInativar.Visible = true;
                cadPaciente.Show();
            }
        }
    }
}
