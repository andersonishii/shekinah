﻿using SistemaShekinah.Apresentacao.Cadastros;
using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Listagem
{
    public partial class fmrListagemDeNivelDeUsuarios : Form
    {
        public fmrListagemDeNivelDeUsuarios()
        {
            InitializeComponent();
        }

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;

        NivelUsuario nivelUser = new NivelUsuario();

        string sqlBaseConsulta = "SELECT id,ativo,nome_Nivel,cadastro_paciente,anexo_paciente,inf_comp_paciente,atividade_grupo,foto_paciente,agenda,nivel_usuario,entrada_saida_paciente,atendimento,avaliacao,medicacao,posologia,cadastro_usuario FROM dbo.nivel_usuario";


        private void btnNovo_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("nivel_usuario", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCadastroNivelDeUsuarioPermissoes pac = new fmrCadastroNivelDeUsuarioPermissoes();
                pac.Show();
            }
         
        }

        public void carregaNivelDeUsuario(Boolean ativo)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseConsulta + "  where  ativo='" + ativo + "'", con.conectar());

                dt = new DataTable();
                adpt.Fill(dt);
                dt.Columns["id"].ColumnName = "id";
                dt.Columns["ativo"].ColumnName = "Ativo?";
                dt.Columns["nome_Nivel"].ColumnName = "Nome do Nivel";
                dt.Columns["cadastro_paciente"].ColumnName = "Permissão para Func. de  Paciente";
                dt.Columns["anexo_paciente"].ColumnName = "Permissão para Func. de Anexo do Paciente";
                dt.Columns["inf_comp_paciente"].ColumnName = "Permissão para Func. de Inf Complementar do Paciente";
                dt.Columns["atividade_grupo"].ColumnName = "Permissão para Func. de Atividade em Grupo";
                dt.Columns["foto_paciente"].ColumnName = "Permissão para Func.de Foto do Paciente";
                dt.Columns["agenda"].ColumnName = "Permissão para Func. de Agenda";
                dt.Columns["entrada_saida_paciente"].ColumnName = "Permissão para Func. de Entrada e Saida de Paciente";
                dt.Columns["atendimento"].ColumnName = "Permissão para Func. de Atendimento";
                dt.Columns["avaliacao"].ColumnName = "Permissão para Func. de avaliação";
                dt.Columns["medicacao"].ColumnName = "Permissão para Func. de Medicação";
                dt.Columns["posologia"].ColumnName = "Permissão para Func. de Posologia";
                dt.Columns["cadastro_usuario"].ColumnName = "Permissão para Func. de Cadastro de Usuário";
                dt.Columns["nivel_usuario"].ColumnName = "Permissão para Func. de  Nivel de Usuario";


                if (dt.Rows.Count > 0)
                {
                    dataGridNivelUsuer.DataSource = dt;
                    this.dataGridNivelUsuer.Columns["Ativo?"].Visible = false;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridNivelUsuer.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os niveis de Usuários Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }

        }

        public void carregaNivelDeUsuario(int idregistro)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseConsulta + "  where  id='" + idregistro + "'", con.conectar());

                dt = new DataTable();
                adpt.Fill(dt);
                dt.Columns["id"].ColumnName = "id";
                dt.Columns["ativo"].ColumnName = "Ativo?";
                dt.Columns["nome_Nivel"].ColumnName = "Nome do Nivel";
                dt.Columns["cadastro_paciente"].ColumnName = "Permissão para Func. de  Paciente";
                dt.Columns["anexo_paciente"].ColumnName = "Permissão para Func. de Anexo do Paciente";
                dt.Columns["inf_comp_paciente"].ColumnName = "Permissão para Func. de Inf Complementar do Paciente";
                dt.Columns["atividade_grupo"].ColumnName = "Permissão para Func. de Atividade em Grupo";
                dt.Columns["foto_paciente"].ColumnName = "Permissão para Func.de Foto do Paciente";
                dt.Columns["agenda"].ColumnName = "Permissão para Func. de Agenda";
                dt.Columns["entrada_saida_paciente"].ColumnName = "Permissão para Func. de Entrada e Saida de Paciente";
                dt.Columns["atendimento"].ColumnName = "Permissão para Func. de Atendimento";
                dt.Columns["avaliacao"].ColumnName = "Permissão para Func. de avaliação";
                dt.Columns["medicacao"].ColumnName = "Permissão para Func. de Medicação";
                dt.Columns["posologia"].ColumnName = "Permissão para Func. de Posologia";
                dt.Columns["cadastro_usuario"].ColumnName = "Permissão para Func. de Cadastro de Usuário";
                dt.Columns["nivel_usuario"].ColumnName = "Permissão para Func. de  Nivel de Usuario";


                if (dt.Rows.Count > 0)
                {
                    dataGridNivelUsuer.DataSource = dt;
                    this.dataGridNivelUsuer.Columns["ativo?"].Visible = false;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataGridNivelUsuer.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os niveis de Usuários Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }

        }

        private void fmrListagemDeNivelDeUsuarios_Load(object sender, EventArgs e)
        {
            carregaNivelDeUsuario(cbxNivelUser.chkAtivo.Checked);
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            int idusuario=Convert.ToInt32(cbxNivelUser.SelectNivelUsuario.Id.ToString());
            carregaNivelDeUsuario(idusuario);
        }

        private void dataGridNivelUsuer_DoubleClick(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("nivel_usuario", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                
                fmrCadastroNivelDeUsuarioPermissoes cadnivel = new fmrCadastroNivelDeUsuarioPermissoes();


  
                cadnivel.txtIdNivelUser.Text = dataGridNivelUsuer.CurrentRow.Cells["id"].Value.ToString();
                cadnivel.txtNomeNivel.Text = dataGridNivelUsuer.CurrentRow.Cells["Nome do Nivel"].Value.ToString();
                cadnivel.txtCadPaciente.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de  Paciente"].Value.ToString();
                cadnivel.txtAnexoPaciente.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de Anexo do Paciente"].Value.ToString();
                cadnivel.txtInfCompPac.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de Inf Complementar do Paciente"].Value.ToString();
                cadnivel.txtAtividadeEmgrupo.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de Atividade em Grupo"].Value.ToString();
                cadnivel.txtFotoPaciente.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func.de Foto do Paciente"].Value.ToString();
                cadnivel.txtAgenda.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de Agenda"].Value.ToString();
                cadnivel.txtEntSaida.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de Entrada e Saida de Paciente"].Value.ToString();
                cadnivel.txtEvolucao.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de Atendimento"].Value.ToString();
                cadnivel.txtAvaliacao.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de avaliação"].Value.ToString();
                cadnivel.txtMedicacao.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de Medicação"].Value.ToString();
                cadnivel.txttPosologia.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de Posologia"].Value.ToString();
                cadnivel.txtCadUser.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de Cadastro de Usuário"].Value.ToString();
                cadnivel.txtNivelUser.Text = dataGridNivelUsuer.CurrentRow.Cells["Permissão para Func. de  Nivel de Usuario"].Value.ToString();
                cadnivel.chkAtivo.Checked = Convert.ToBoolean(dataGridNivelUsuer.CurrentRow.Cells["Ativo?"].Value.ToString());
                cadnivel.btnGravar.Visible = false;
                cadnivel.btnAtualizar.Visible = true;
                cadnivel.Show();

                
            }

        }
    }
}
