﻿using SistemaShekinah.Modelo;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao
{
    public partial class fmrListagemCadastroMedicacoes : Form
    {
        public fmrListagemCadastroMedicacoes()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
        }

        NivelUsuario nivelUser = new NivelUsuario();

        private void btnNovaMedicacao_Click(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("medicacao", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCaCadastroPosologia med = new fmrCaCadastroPosologia();
                med.ShowDialog();
            }
             
        }

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;

        string sqlBaseConsulta = "SELECT  e.id,e.nome_quimico as 'Nome Químico',e.nome_comercial as 'Nome Comercial',e.forma_farmaceutica as 'Forma Farmacêutica',e.modo_de_uso as 'Modo de Uso',e.ativo as 'Ativo',nome_laboratorio as 'Nome do Laboratorio',reacoes_adversas as 'Reações Adversas'";
        public void carregaMedicacao(String nomeQuimico, Boolean ativo)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseConsulta+" FROM medicacoes e  where  e.nome_quimico='" + nomeQuimico + "' and  e.ativo='" + ativo + "'", con.conectar());

                dt = new DataTable();
                adpt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    dataListagemMedicacoes.DataSource = dt;
                    this.dataListagemMedicacoes.Columns["id"].Visible = false;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }

        }

        public void carregaMedicacao(Boolean ativo, String nomeComercial)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseConsulta+" FROM medicacoes e  where  e.nome_comercial='" + nomeComercial + "' and  e.ativo='" + ativo + "'", con.conectar());

                dt = new DataTable();
                adpt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    dataListagemMedicacoes.DataSource = dt;
                    this.dataListagemMedicacoes.Columns["id"].Visible = false;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }

        }


        public void carregaMedicacao(String nomeQuimico, String nomeComercial, Boolean ativo)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseConsulta+"FROM medicacoes e  where  e.nome_quimico='" + nomeQuimico + "' and  e.ativo='" + ativo + "' and e.nome_comercial='" + nomeComercial + "'", con.conectar());

                dt = new DataTable();
                adpt.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    dataListagemMedicacoes.DataSource = dt;
                    this.dataListagemMedicacoes.Columns["id"].Visible = false;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }

        }

        public void carregaMedicacao(Boolean ativo)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter(sqlBaseConsulta+" FROM medicacoes e  where  e.ativo='" + ativo + "'", con.conectar());

                dt = new DataTable();
                adpt.Fill(dt);
                dt.Columns["id"].ColumnName = "ID";
                dt.Columns["Nome Químico"].ColumnName = "Nome Quimico";
                dt.Columns["Nome Comercial"].ColumnName = "Nome Comercial";
                dt.Columns["Forma Farmacêutica"].ColumnName = "Forma Farmaceutica";
                dt.Columns["Ativo"].ColumnName = "Ativo";
                dt.Columns["Nome do Laboratorio"].ColumnName = "Nome do laboratorio";
                dt.Columns["Reações Adversas"].ColumnName = "Reações Adversas";


                if (dt.Rows.Count > 0)
                {
                    dataListagemMedicacoes.DataSource = dt;
                    this.dataListagemMedicacoes.Columns["ID"].Visible = false;
                }
                else
                {
                    MessageBox.Show("Não há resultados para a busca");
                    dataListagemMedicacoes.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar os Medicamentos Registrados" + ex);
            }
            finally
            {
                con.desconectar();
            }

        }

        public void decideRelatorio(String txtNomeComercial, String txtNomeQuimico, bool ativoinativo)
        {
            if (string.IsNullOrWhiteSpace(txtNomeComercial) & (!string.IsNullOrWhiteSpace(txtNomeQuimico)))
            {
                carregaMedicacao(txtNomeQuimico, ativoinativo);
            }
            else
            if (string.IsNullOrWhiteSpace(txtNomeComercial) & (string.IsNullOrWhiteSpace(txtNomeQuimico)))
            {
                carregaMedicacao(ativoinativo);
            }
            else
            if (!string.IsNullOrWhiteSpace(txtNomeComercial) & (string.IsNullOrWhiteSpace(txtNomeQuimico)))
            {
                //MessageBox.Show("Apenas o nome quimico é vazio");
                carregaMedicacao(ativoinativo, txtNomeComercial);
            }
            else
            {
                //MessageBox.Show("ambos estao preenchidos");
                carregaMedicacao(txtNomeQuimico, txtNomeComercial, ativoinativo);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            decideRelatorio(txtNomeComercial.Text, txtNomeQuimico.Text, chkAtivoInativo.Checked);
        }

        private void fmrListagemCadastroMedicacoes_Load(object sender, EventArgs e)
        {
            carregaMedicacao(chkAtivoInativo.Checked);
        }

        private void lblNomeQuimico_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataListagemMedicacoes_DoubleClick(object sender, EventArgs e)
        {
            string nomePermissao = nivelUser.retornaNivelPermissao("medicacao", Usuario.NomeUsuario);
            if (nomePermissao == "Apenas Consulta")
            {
                MessageBox.Show("Usuário  possui apenas permissão para Listagem do  Módulo");
            }
            else
            {
                fmrCaCadastroPosologia cadmed = new fmrCaCadastroPosologia();
                cadmed.txtCodigo.Text = dataListagemMedicacoes.CurrentRow.Cells[0].Value.ToString();
                cadmed.txtNomeQuimico.Text = dataListagemMedicacoes.CurrentRow.Cells[1].Value.ToString();
                cadmed.txtNomeComercial.Text = dataListagemMedicacoes.CurrentRow.Cells[2].Value.ToString();
                cadmed.txtFormaFarmaceutica_Gambis.Text = dataListagemMedicacoes.CurrentRow.Cells[3].Value.ToString();
                cadmed.txtModoDeUso.Text = dataListagemMedicacoes.CurrentRow.Cells[4].Value.ToString();
                cadmed.chkAtivo.Checked = Convert.ToBoolean(dataListagemMedicacoes.CurrentRow.Cells[5].Value.ToString());
                cadmed.txtNomeLaboratorio.Text = dataListagemMedicacoes.CurrentRow.Cells[6].Value.ToString();
                cadmed.txtreacoesAdversas.Text = dataListagemMedicacoes.CurrentRow.Cells[7].Value.ToString();
                cadmed.Text = "Atualização de Cadastro - Medicação";
                cadmed.btnLimpar.Visible = false;
                cadmed.btnInativar.Visible = true;
                cadmed.btnCadastrar.Text = "Atualizar";
                cadmed.Show();
            }

                
        }

        private void TableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
