﻿using SistemaShekinah.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao.Listagem
{
    public partial class fmrListagemPendencias : Form
    {
        public fmrListagemPendencias()
        {
            InitializeComponent();
            carregaPendencias(dtpInicial.Value.Date.ToString("dd-MM-yyyy"), dtpFinal.Value.Date.ToString("dd-MM-yyyy"),true);


        }

        Conexao con = new Conexao();
        SqlCommand cmd;
        SqlDataAdapter adpt;
        DataTable dt;
        Paciente pc = new Paciente();


        public string UsuarioSelecionado()
        {
            int maiorIdDb = pc.getMaiorIdUser() + 1;
            string idPacientes = string.Empty;
            if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 0)
            {

                for (int i = 0; i < maiorIdDb; i++)
                {

                    idPacientes += i + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;
                /*
      
                */
            }
            else
            {
                for (int i = 0; i < userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count; i++)
                {

                    idPacientes += userControlPacienteListCheckbox1.SelectPosicaoPaciente(i).Id + ",";
                }
                //gambis para concaternar
                idPacientes += maiorIdDb;
                return idPacientes;

            }

        }


        public void CriaColunasDataGrid()
        {
            dt = new DataTable();
            adpt.Fill(dt);
            dt.Columns["DataCompromisso"].ColumnName = "Data do Compromisso";
            dt.Columns["Compromisso"].ColumnName = "Compromisso";
            dt.Columns["Paciente"].ColumnName = "Paciente";
            dt.Columns["Tipo"].ColumnName = "Tipo Pendencia";
            dataGridView1.DataSource = dt;

        }

        public void DattaGridApenasLeitura(bool value)
        {
            this.dataGridView1.Columns["Data do Compromisso"].ReadOnly = value;
            this.dataGridView1.Columns["Compromisso"].ReadOnly = value;
            this.dataGridView1.Columns["Paciente"].ReadOnly = value;
            this.dataGridView1.Columns["Tipo Pendencia"].ReadOnly = value;
 
        }

        public void carregaPendencias(String datainicial, String datefinal, bool ehInicial)
        {
            try
            {
                con.conectar();
                
                adpt = new SqlDataAdapter("select   ac.datahora_inicial as DataCompromisso,'Titulo='+titulo+' | Compromisso=' +informacao_compromisso_agenda as Compromisso,  (select nome from paciente p where p.id=ac.paciente) as Paciente,'AGENDA'  as Tipo from agenda_compromisso ac where ac.ativo=1  and  ( ac.datahora_inicial  between '" + datainicial + " 00:00:00.0000000' and '" + datefinal + " 23:59:59.0000000')" +
                    " union all select po.data_pendencia as DataCompromisso, 'posologia=' + posologia + ' | medicação=' + (select m.nome_quimico from medicacoes m where m.id = po.id_medicacao ) + ' | obs = ' + observacao as Compromisso, (select nome from paciente p where p.id = po.paciente) Paciente,'POSOLOGIA' as Tipo  from posologia po where po.ativo = 1   and(po.data_pendencia  between '" + datainicial+ " 00:00:00.0000000' and '"+datefinal+ " 23:59:59.0000000') " +
                    " union all  select datahora_expectativa_retorno,CONCAT ('Descrição do registro de saide do idoso : ',(case when   descricao_saida is null then '*** MSG AUTOMATICA DESCRIÇÃO>>> NÃO INFORMADA NO REGISTRO<<<*** ' else descricao_saida  end),' Previsão do retorno as : ',datahora_expectativa_retorno), (select nome from paciente p where p.id=ea.paciente) as Paciente,'PENDENCIA DE SAIDA DO IDOSO'  from entrada_saida ea where  ea.datahora_expectativa_retorno is not null  and( ea.datahora_expectativa_retorno  between '" + datainicial + " 00:00:00.0000000' and '" + datefinal + " 23:59:59.0000000')  order by 1 asc", con.conectar());
            

                CriaColunasDataGrid();
                DattaGridApenasLeitura(true);
                dataGridView1.DataSource = dt;
                // this.dataGridView1.Columns["ID"].Visible = false;

                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else if (!ehInicial)
                {
                    MessageBox.Show("Para o periodo selecionado , não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar as pendencias" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }

        public void carregaPendencias(String datainicial, String datefinal, string Idpacientes,bool ehInicial)
        {
            try
            {
                con.conectar();
                adpt = new SqlDataAdapter("select   ac.datahora_inicial  as DataCompromisso,'Titulo='+titulo+' | Compromisso=' +informacao_compromisso_agenda as Compromisso,  (select nome from paciente p where p.id=ac.paciente) as Paciente,'AGENDA'  as Tipo from agenda_compromisso ac where ac.ativo=1  and ( ac.datahora_inicial  between '" + datainicial + " 00:00:00.0000000' and '" + datefinal + " 23:59:59.0000000') and ac.paciente in(" + Idpacientes + ") " +
                    " union all select po.data_pendencia as DataCompromisso, 'posologia=' + posologia + ' | medicação=' + (select m.nome_quimico from medicacoes m where m.id = po.id_medicacao ) + ' | obs = ' + observacao as Compromisso, (select nome from paciente p where p.id = po.paciente) Paciente,'POSOLOGIA' as Tipo  from posologia po where po.ativo = 1   and(po.data_pendencia  between '" + datainicial + " 00:00:00.0000000' and '" + datefinal + " 23:59:59.0000000')  and po.paciente in(" + Idpacientes + ")  " +
                    " union all select datahora_expectativa_retorno,CONCAT ('Descrição do registro de saide do idoso : ',(case when   descricao_saida is null then '*** MSG AUTOMATICA DESCRIÇÃO>>> NÃO INFORMADA NO REGISTRO<<<*** ' else descricao_saida  end),' Previsão do retorno as : ',datahora_expectativa_retorno), (select nome from paciente p where p.id=ea.paciente) as Paciente,'PENDENCIA DE SAIDA DO IDOSO'  from entrada_saida ea where  ea.datahora_expectativa_retorno is not null  and( ea.datahora_expectativa_retorno  between '" + datainicial + " 00:00:00.0000000' and '" + datefinal + " 23:59:59.0000000') and   ea.paciente in(" + Idpacientes + ")  order by 1 asc", con.conectar());




                CriaColunasDataGrid();
                DattaGridApenasLeitura(true);
                dataGridView1.DataSource = dt;
                // this.dataGridView1.Columns["ID"].Visible = false;

                if (dt.Rows.Count > 0)
                {
                    dataGridView1.DataSource = dt;
                }
                else if (!ehInicial)
                {
                    MessageBox.Show("Para o periodo selecionado , não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }/*
                else
                {
                    MessageBox.Show("Para o periodo selecionado , não há resultados para a busca");
                    dataGridView1.DataSource = null;
                }*/
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ops. Tivemos um problema ao consultar as pendencias" + ex);
            }
            finally
            {
                con.desconectar();
            }
        }



        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            String datainicial = dtpInicial.Value.Date.ToString("dd-MM-yyyy");
            String datafinal = dtpFinal.Value.Date.ToString("dd-MM-yyyy");
           if(dtpFinal.Value< dtpInicial.Value.Date )
            {
                MessageBox.Show("A data final deve ser maior ou igual a data inicial");
            }
            else
            {
                if (userControlPacienteListCheckbox1.chkPacientes.CheckedItems.Count == 0)
                {
                    carregaPendencias(datainicial, datafinal, false);
                }
                else
                {

                    carregaPendencias(datainicial, datafinal, UsuarioSelecionado(), false);
                }
            }


        
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e) 
        {
            MessageBox.Show("Apenas Visualização. Para obter mais detalhes, entre na tela correspondente");
        }

        private void TableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
