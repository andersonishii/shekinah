﻿using SistemaShekinah.Apresentacao.Listagem;
using SistemaShekinah.Apresentacao.Menus;
using SistemaShekinah.Modelo;
using System;
using System.Windows.Forms;

namespace SistemaShekinah.Apresentacao
{
    public partial class frmPrincipal : Form
    {
        Usuario userlogado = new Usuario();
        public frmPrincipal()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            AbrirFormFilho(new fmrListagemPendencias());

        }


        private void pnlExibeContexto_Paint(object sender, PaintEventArgs e)
        {

        }
     
        private void button2_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrAtendimento());
            lblUserCoenctado.Text = "Usuário: " + Usuario.NomeUsuario;

        }
        private void AbrirFormFilho(object formFilho)
        {
            if (this.pnlFilho.Controls.Count > 0)
                this.pnlFilho.Controls.RemoveAt(0);
            Form fh = formFilho as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pnlFilho.Controls.Add(fh);
            this.pnlFilho.Tag = fh;
            fh.Show();
   
        }

        private void pnlFilho_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {
      
        }

        private void lblUserCoenctado_Click(object sender, EventArgs e)
        {
        
        }



        private void btnPaciente_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrPaciente());
        }

        private void btnMedicacao_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrMedicacao());
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnAgendaAnotacao_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrPaciente());
            lblUserCoenctado.Text = "Usuário: " + Usuario.NomeUsuario;
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrAdminConfig());
            lblUserCoenctado.Text = "Usuário: " + Usuario.NomeUsuario;
        }

        private void PictureBox1_Click_1(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrAgendaAnotacoes());
            lblUserCoenctado.Text = "Usuário: " + Usuario.NomeUsuario;
        }

        private void PictureBox3_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrMedicacao());
            lblUserCoenctado.Text = "Usuário: " + Usuario.NomeUsuario;
        }

        private void PictureBox4_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrListagemPendencias());
            lblUserCoenctado.Text = "Usuário: " + Usuario.NomeUsuario;
        }

        private void PictureBox5_Click(object sender, EventArgs e)
        {
            AbrirFormFilho(new fmrAtendimento());
            lblUserCoenctado.Text = "Usuário: " + Usuario.NomeUsuario;
        }

        private void pictureBox5_MouseMove(object sender, MouseEventArgs e)
        {
            //toolTip1.Show("Atendimento ", picAtendimento);
        }

        private void picPaciente_MouseMove(object sender, MouseEventArgs e)
        {
 
        }

        private void picMedicacao_MouseMove(object sender, MouseEventArgs e)
        {
      
        }

        private void picAgendaAnotacoes_MouseMove(object sender, MouseEventArgs e)
        {
      
        }

        private void picPendencias_MouseMove(object sender, MouseEventArgs e)
        {
  
        }

        private void picAdministracao_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void picAtendimento_MouseClick(object sender, MouseEventArgs e)
        {
            AbrirFormFilho(new fmrAtendimento());
        }

        private void picAdministracao_MouseClick(object sender, MouseEventArgs e)
        {
            AbrirFormFilho(new fmrAdminConfig());
        }

        private void picAgendaAnotacoes_MouseClick(object sender, MouseEventArgs e)
        {
            AbrirFormFilho(new fmrAgendaAnotacoes());
        }

        private void picMedicacao_MouseClick(object sender, MouseEventArgs e)
        {
            AbrirFormFilho(new fmrMedicacao());
        }

        private void picPaciente_MouseClick(object sender, MouseEventArgs e)
        {
            AbrirFormFilho(new fmrPaciente());
        }

        private void picAtendimento_MouseEnter(object sender, EventArgs e)
        {
            toolTip1.Active = false;
            toolTip1.Active = true;
        }

        private void picPaciente_MouseEnter(object sender, EventArgs e)
        {
            toolTip2.Active = false;
            toolTip2.Active = true;
        }

        private void picMedicacao_MouseEnter(object sender, EventArgs e)
        {
            toolTip3.Active = false;
            toolTip3.Active = true;
        }

        private void picAgendaAnotacoes_MouseEnter(object sender, EventArgs e)
        {
            toolTip4.Active = false;
            toolTip4.Active = true;
        }

        private void picPendencias_MouseEnter(object sender, EventArgs e)
        {
            toolTip5.Active = false;
            toolTip5.Active = true;
        }

        private void picAdministracao_MouseEnter(object sender, EventArgs e)
        {
            toolTip6.Active = false;
            toolTip6.Active = true;
        }

        private void picAtendimento_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("Atendimento ", picAtendimento);
        }

        private void picPaciente_MouseHover(object sender, EventArgs e)
        {
            toolTip2.Show("Paciente ", picPaciente);
        }

        private void picMedicacao_MouseHover(object sender, EventArgs e)
        {
            toolTip3.Show("Medicação ", picMedicacao);
        }

        private void picAgendaAnotacoes_MouseHover(object sender, EventArgs e)
        {
            toolTip4.Show("Agenda / Anotações ", picAgendaAnotacoes);
        }

        private void picPendencias_MouseHover(object sender, EventArgs e)
        {
            toolTip5.Show("Pendencias (posologia e agenda) ", picPendencias);
        }

        private void picAdministracao_MouseHover(object sender, EventArgs e)
        {
            toolTip6.Show("Administração ", picAdministracao);
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            lblUserCoenctado.Text = "Usuário: " + Usuario.NomeUsuario;
        }

        private void picRelatorio_MouseClick(object sender, MouseEventArgs e)
        {
            AbrirFormFilho(new fmrRelatorios());
        }

        private void picRelatorio_MouseEnter(object sender, EventArgs e)
        {
            toolTip7.Active = false;
            toolTip7.Active = true;
        }

        private void picRelatorio_MouseHover(object sender, EventArgs e)
        {
            toolTip7.Show("Relatorio ", picRelatorio);
        }
    }
}
   